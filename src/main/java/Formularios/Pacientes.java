package Formularios;

import Clases.ConexionMySQLLocal;
import Clases.Escape;
import static Clases.HiloInicio.contadorobrasocial;
import static Clases.HiloInicio.idobrasocial;
import static Clases.HiloInicio.obrasocial;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableRowSorter;

public class Pacientes extends javax.swing.JDialog {

    DefaultTableModel model;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    public int id[] = new int[100];
    public int bmodificar = 0, id_paciente;
    TextAutoCompleter textAutoAcompleter2;

    public Pacientes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Pacientes");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());        
        cbosexo.addItem("Masculino");
        cbosexo.addItem("Femenino");
        textAutoAcompleter2 = new TextAutoCompleter(txtobrasocialpaciente);
        cargartabla("");
        eventotabla();
        cargarcombo();
        dobleclick();
        Escape.funcionescape(this);
        this.setLocationRelativeTo(null);
        txtafiliado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toLowerCase(c));
                }
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtobrasocialpaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
    }

    void cargarcombo() {

        int i = 0;
        // Recorro y cargo las obras sociales
        while (i < contadorobrasocial) {
            textAutoAcompleter2.addItem(obrasocial[i]);
            i++;
        }
        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        textAutoAcompleter2.setMode(0);
        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter2.setCaseSensitive(false);
    }

    void dobleclick() {
        tablapaciente.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    id_paciente = Integer.valueOf(tablapaciente.getValueAt(tablapaciente.getSelectedRow(), 0).toString());
                    txtapellido.setText(tablapaciente.getValueAt(tablapaciente.getSelectedRow(), 1).toString());
                    txtnombre.setText(tablapaciente.getValueAt(tablapaciente.getSelectedRow(), 2).toString());
                    txtfechanac.setText(tablapaciente.getValueAt(tablapaciente.getSelectedRow(), 3).toString());
                    txtdni.setText(tablapaciente.getValueAt(tablapaciente.getSelectedRow(), 4).toString());
                    cbosexo.setSelectedItem(tablapaciente.getValueAt(tablapaciente.getSelectedRow(), 5).toString());
                    txttelefono.setText(tablapaciente.getValueAt(tablapaciente.getSelectedRow(), 6).toString());
                    txtcelular.setText(tablapaciente.getValueAt(tablapaciente.getSelectedRow(), 7).toString());
                    txtmail.setText(tablapaciente.getValueAt(tablapaciente.getSelectedRow(), 8).toString());
                    bmodificar = 1;
                }
            }
        });
    }

    void cargartabla(String valor) {
        String[] Titulo = {"id_paciente", "Apellido", "Nombre", "Fecha Nac.", "Dni", "sexo", "Telefono", "Celular", "Mail","Estado"};
        String[] Registros = new String[10];
        String sql = "select id_pacientes, apellido, nombre, fecha_nacimiento, dni, sexo, telefono, celular, mail,estado from pacientes inner join personas on personas.dni = pacientes.personas_dni WHERE concat(apellido,'', nombre,'',dni) LIKE '%" + valor + "%'";

        int i = 0;
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                if (rs.getString(6).equals("F")) {
                    Registros[5] = "Femenino";
                } else {
                    Registros[5] = "Masculino";
                }
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                Registros[8] = rs.getString(9);                
                if (rs.getString(10).equals("1")) {
                    Registros[9] = "OK";
                } else {
                    Registros[9] = "BAJA";
                }
                
                model.addRow(Registros);
            }
            tablapaciente.setModel(model);
            tablapaciente.setAutoCreateRowSorter(true);
            /////////////////////////////////////////////////////////////
            tablapaciente.getColumnModel().getColumn(0).setMaxWidth(0);
            tablapaciente.getColumnModel().getColumn(0).setMinWidth(0);
            tablapaciente.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablapaciente.getColumnModel().getColumn(3).setMaxWidth(0);
            tablapaciente.getColumnModel().getColumn(3).setMinWidth(0);
            tablapaciente.getColumnModel().getColumn(3).setPreferredWidth(0);
            tablapaciente.getColumnModel().getColumn(5).setMaxWidth(0);
            tablapaciente.getColumnModel().getColumn(5).setMinWidth(0);
            tablapaciente.getColumnModel().getColumn(5).setPreferredWidth(0);
            tablapaciente.getColumnModel().getColumn(6).setMaxWidth(0);
            tablapaciente.getColumnModel().getColumn(6).setMinWidth(0);
            tablapaciente.getColumnModel().getColumn(6).setPreferredWidth(0);
            tablapaciente.getColumnModel().getColumn(7).setMaxWidth(0);
            tablapaciente.getColumnModel().getColumn(7).setMinWidth(0);
            tablapaciente.getColumnModel().getColumn(7).setPreferredWidth(0);
            tablapaciente.getColumnModel().getColumn(8).setMaxWidth(0);
            tablapaciente.getColumnModel().getColumn(8).setMinWidth(0);
            tablapaciente.getColumnModel().getColumn(8).setPreferredWidth(0);

/////////////////////////////////////////////////////////////
            alinear();
            tablapaciente.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
            tablapaciente.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
            tablapaciente.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
            //////////////////////////
            ///cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        }
    }

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    void eventotabla() {
        tablapaciente.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        ListSelectionModel rowSM = tablapaciente.getSelectionModel();

        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                //Ignore extra messages.
                if (e.getValueIsAdjusting()) {
                    return;
                }

                ListSelectionModel lsm = (ListSelectionModel) e.getSource();

                if (lsm.isSelectionEmpty()) {
                } else {

                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtapellido = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cbosexo = new javax.swing.JComboBox();
        txtdni = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtmail = new javax.swing.JTextField();
        txtafiliado = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txttelefono = new javax.swing.JTextField();
        txtfechanac = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtcelular = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtobrasocialpaciente = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablapaciente = new javax.swing.JTable();
        txtbuscar = new javax.swing.JTextField();
        btnsalir = new javax.swing.JButton();
        btnborrar = new javax.swing.JButton();
        btnborrar1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Pacientes"));

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Nombre:");

        txtnombre.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.setPreferredSize(new java.awt.Dimension(200, 21));
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Apellido:");

        txtapellido.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtapellido.setForeground(new java.awt.Color(0, 102, 204));
        txtapellido.setMinimumSize(new java.awt.Dimension(200, 21));
        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtapellidoKeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Sexo:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Dni:");

        cbosexo.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbosexo.setForeground(new java.awt.Color(0, 102, 204));
        cbosexo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbosexoKeyPressed(evt);
            }
        });

        txtdni.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtdni.setForeground(new java.awt.Color(0, 102, 204));
        txtdni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdniKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Mail:");

        txtmail.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtmail.setForeground(new java.awt.Color(0, 102, 204));
        txtmail.setMinimumSize(new java.awt.Dimension(200, 21));
        txtmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmailKeyPressed(evt);
            }
        });

        txtafiliado.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtafiliado.setForeground(new java.awt.Color(0, 102, 204));
        txtafiliado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtafiliadoKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Telefeno:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(51, 51, 51));
        jLabel7.setText("Fecha Nac:");

        txttelefono.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txttelefono.setForeground(new java.awt.Color(0, 102, 204));
        txttelefono.setPreferredSize(new java.awt.Dimension(200, 21));
        txttelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttelefonoKeyPressed(evt);
            }
        });

        txtfechanac.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtfechanac.setForeground(new java.awt.Color(0, 102, 204));
        txtfechanac.setMinimumSize(new java.awt.Dimension(200, 21));
        txtfechanac.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtfechanacKeyPressed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setText("Celular:");

        txtcelular.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtcelular.setForeground(new java.awt.Color(0, 102, 204));
        txtcelular.setMinimumSize(new java.awt.Dimension(200, 21));
        txtcelular.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcelularKeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 51, 51));
        jLabel9.setText("Num. Afiliado:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setText("Obra Social:");

        txtobrasocialpaciente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtobrasocialpaciente.setForeground(new java.awt.Color(0, 102, 204));
        txtobrasocialpaciente.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtobrasocialpacienteFocusGained(evt);
            }
        });
        txtobrasocialpaciente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtobrasocialpacienteMouseClicked(evt);
            }
        });
        txtobrasocialpaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtobrasocialpacienteActionPerformed(evt);
            }
        });
        txtobrasocialpaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtobrasocialpacienteKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtobrasocialpacienteKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(4, 4, 4)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtapellido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtnombre, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(8, 8, 8)
                        .addComponent(txtdni, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE))
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtcelular, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtobrasocialpaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtafiliado)
                            .addComponent(txtmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cbosexo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txttelefono, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(txtfechanac, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtapellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtfechanac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtdni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cbosexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txttelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtcelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtobrasocialpaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtafiliado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tablapaciente.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablapaciente.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablapaciente);

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtbuscar.setPreferredSize(new java.awt.Dimension(200, 21));
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 332, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728980 - home.png"))); // NOI18N
        btnsalir.setText("Volver");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        btnborrar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnborrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        btnborrar.setMnemonic('b');
        btnborrar.setText("Modiifcar");
        btnborrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnborrarActionPerformed(evt);
            }
        });

        btnborrar1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnborrar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        btnborrar1.setMnemonic('b');
        btnborrar1.setText("Borrar");
        btnborrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnborrar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnborrar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnborrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnborrar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnborrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnborrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnborrarActionPerformed

        if (!txtdni.getText().equals("")) {
            ConexionMySQLLocal mysql = new ConexionMySQLLocal();
            Connection cn = mysql.Conectar();
            String sSQL7;
            sSQL7 = "UPDATE personas SET nombre=?, apellido=?, dni=?, sexo=?  WHERE dni='" + txtdni.getText() + "'";
            try {
                PreparedStatement pst2 = cn.prepareStatement(sSQL7);
                pst2.setString(1, txtnombre.getText());
                pst2.setString(2, txtapellido.getText());
                pst2.setString(3, txtdni.getText());
                if (cbosexo.getSelectedItem().toString().equals("Masculino")) {
                    pst2.setString(4, "M");
                } else {
                    pst2.setString(4, "F");
                }
                pst2.executeUpdate();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
            ////////////////////////////////////////////////////////////////
            if (!txtdni.getText().equals("")) {
                String sSQL6;
                sSQL6 = "UPDATE pacientes SET telefono=?, celular=?,mail=?, fecha_nacimiento=? WHERE personas_dni='" + txtdni.getText() + "'";
                try {
                    PreparedStatement pst = cn.prepareStatement(sSQL6);
                    pst.setString(1, txttelefono.getText());
                    pst.setString(2, txtcelular.getText());
                    pst.setString(3, txtmail.getText());
                    pst.setString(4, txtfechanac.getText());
                    int n5 = pst.executeUpdate();
                    if (n5 > 0) {
                        String sSQL5;
                        sSQL5 = "UPDATE pacientes_tienen_obrasociales SET numero_afiliado=? WHERE id_Pacientes='" + id_paciente + "'";
                        try {
                            PreparedStatement pst2 = cn.prepareStatement(sSQL5);
                            pst2.setString(1, txtafiliado.getText());
                            int n67 = pst2.executeUpdate();
                            if (n67 > 0) {
                                JOptionPane.showMessageDialog(null, "Se modificó con exito...");
                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                    }
                    //////
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
            }
        }
    }//GEN-LAST:event_btnborrarActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        TableRowSorter sorter = new TableRowSorter(model);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar.getText() + ".*"));
        tablapaciente.setRowSorter(sorter);
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarKeyPressed

    private void txtobrasocialpacienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobrasocialpacienteKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtobrasocialpacienteKeyReleased

    private void txtobrasocialpacienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobrasocialpacienteKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            int band = 0;
            String obra = txtobrasocialpaciente.getText();
            if (!obra.equals("")) {

                int i = 0, banderapaciente = 0;
                while (i < contadorobrasocial) {
                    if (obra.equals(obrasocial[i])) {
                        int id_obra_social = idobrasocial[i];
                        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                        Connection cn = mysql.Conectar();
                        try {
                            String sSQL = "SELECT  numero_afiliado FROM pacientes_tienen_obrasociales WHERE id_Pacientes=" + id_paciente + " AND id_obrasocial=" + id_obra_social;
                            Statement st = cn.createStatement();
                            ResultSet rs = st.executeQuery(sSQL);
                            while (rs.next()) {
                                txtafiliado.setText(rs.getString("numero_afiliado"));
                                banderapaciente = 1;
                            }
                            ////
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                        break;
                    }
                    i++;
                }
                if (banderapaciente == 0) {
                    txtafiliado.setText("");
                }
            }
        }
    }//GEN-LAST:event_txtobrasocialpacienteKeyPressed

    private void txtobrasocialpacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtobrasocialpacienteActionPerformed

    }//GEN-LAST:event_txtobrasocialpacienteActionPerformed

    private void txtobrasocialpacienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtobrasocialpacienteMouseClicked
        txtobrasocialpaciente.selectAll();
    }//GEN-LAST:event_txtobrasocialpacienteMouseClicked

    private void txtobrasocialpacienteFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtobrasocialpacienteFocusGained

    }//GEN-LAST:event_txtobrasocialpacienteFocusGained

    private void txtcelularKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcelularKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcelularKeyPressed

    private void txtfechanacKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfechanacKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfechanacKeyPressed

    private void txttelefonoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelefonoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttelefonoKeyPressed

    private void txtafiliadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtafiliadoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtafiliadoKeyPressed

    private void txtmailKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmailKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtmail.transferFocus();
        }
    }//GEN-LAST:event_txtmailKeyPressed

    private void txtdniKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdniKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtdni.transferFocus();
        }
    }//GEN-LAST:event_txtdniKeyPressed

    private void cbosexoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbosexoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cbosexo.transferFocus();
        }
    }//GEN-LAST:event_cbosexoKeyPressed

    private void txtapellidoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtapellidoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtapellido.transferFocus();
        }
    }//GEN-LAST:event_txtapellidoKeyPressed

    private void txtnombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtnombre.transferFocus();
        }
    }//GEN-LAST:event_txtnombreKeyPressed

    private void btnborrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnborrar1ActionPerformed
        if (tablapaciente.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila...");
        } else {
            if (!tablapaciente.getValueAt(tablapaciente.getSelectedRow(), 9).equals("OK")) {
                JOptionPane.showMessageDialog(null, "El Paciente ya se encuentra dado de Baja");
            } else {

                int opt = JOptionPane.showConfirmDialog(this, "Desea borrar al Paciente?", "Confirmación", JOptionPane.YES_NO_OPTION);
                if (opt == 0) {
                    try {
                        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                        Connection cn = mysql.Conectar();
                        int id_Pacientes = Integer.valueOf(tablapaciente.getValueAt(tablapaciente.getSelectedRow(), 0).toString());
                        String actualizar = "UPDATE pacientes SET estado=? WHERE id_Pacientes= " + id_Pacientes;
                        PreparedStatement pst = cn.prepareStatement(actualizar);
                        pst.setInt(1, 0);
                        pst.executeUpdate();
                        cn.close();
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Error en la base de datos");
                        JOptionPane.showMessageDialog(null, ex);
                    }
                    cargartabla("");
                    txtbuscar.setText("");
                }
            }
        }
    }//GEN-LAST:event_btnborrar1ActionPerformed

    public String revertir(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }
        String salida = "";
        int i = 0;
        /////Año/////
        for (i = 6; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ///Mes///
        for (i = 3; i <= 4; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ////Dia////
        for (i = 0; i <= 1; i++) {
            salida = salida + entrada.charAt(i);
        }

        return salida;

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnborrar;
    private javax.swing.JButton btnborrar1;
    private javax.swing.JButton btnsalir;
    private javax.swing.JComboBox cbosexo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablapaciente;
    private javax.swing.JTextField txtafiliado;
    private javax.swing.JTextField txtapellido;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txtcelular;
    private javax.swing.JTextField txtdni;
    private javax.swing.JTextField txtfechanac;
    private javax.swing.JTextField txtmail;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextField txtobrasocialpaciente;
    private javax.swing.JTextField txttelefono;
    // End of variables declaration//GEN-END:variables
}
