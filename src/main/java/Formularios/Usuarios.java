package Formularios;

import Clases.ConexionMySQLLocal;
import Clases.Escape;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;


public class Usuarios extends javax.swing.JDialog {

    DefaultTableModel model;
    String msj;
    int mod = 0, idusuarios;
    int elim = 0;
    public static int ban = 0;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    
    public Usuarios(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        dobleclick();
        this.setLocationRelativeTo(null);
        setTitle("Usuarios");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        cargartabla("");
        Escape.funcionescape(this);
        this.setResizable(false);
        this.setResizable(false);
        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
    }

     void dobleclick() {

        tablaempleados.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int filasel;
                    String msj2;
                    filasel = tablaempleados.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        mod = 1;
                        cargardatos(tablaempleados.getValueAt(tablaempleados.getSelectedRow(), 0).toString());
                        btncancelar.setText("Cancelar");
                    }
                }
            }
        });

    }

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);

        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);

        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    void cargartabla(String valor) {
        String[] Titulo = {"N°", "Apellido", "Nombre", "Usuario"};
        String[] Registros = new String[4];

        String sql = "SELECT id_usuarios, apellido, nombre, usuario FROM usuarios WHERE apellido LIKE '%" + valor + "%'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString("id_usuarios");
                Registros[1] = rs.getString("apellido");
                Registros[2] = rs.getString("nombre");
                Registros[3] = rs.getString("usuario");

                model.addRow(Registros);
            }
            tablaempleados.setModel(model);
            tablaempleados.setAutoCreateRowSorter(true);
            /////////////////////////////////////////////////////////////
            tablaempleados.getColumnModel().getColumn(0).setMaxWidth(0);
            tablaempleados.getColumnModel().getColumn(0).setMinWidth(0);
            tablaempleados.getColumnModel().getColumn(0).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            alinear();
            tablaempleados.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
            tablaempleados.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
            tablaempleados.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
            tablaempleados.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
            //////////////////////////

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        }

    }

    void cargardatos(String id) {
        String sSQL = "";
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        sSQL = "SELECT * FROM usuarios WHERE id_usuarios="+id;
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                if (id.equals(rs.getString("id_usuarios"))) {
                    idusuarios = rs.getInt("id_usuarios");
                    txtapellido.setText(rs.getString("apellido"));
                    txtnombre.setText(rs.getString("nombre"));
                    txtusuario.setText(rs.getString("usuario"));
                    txtcontraseña.setText(rs.getString("contraseña"));
                    jCheckBoxdatos.setSelected(rs.getBoolean("datos"));
                    jCheckBoxinformes.setSelected(rs.getBoolean("informes"));                    
                    jCheckBoxcbt.setSelected(rs.getBoolean("cbt"));
                    jCheckBoxfacturacion.setSelected(rs.getBoolean("facturacion"));
                   

                }
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        }
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnaplicar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtapellido = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        txtusuario = new javax.swing.JTextField();
        txtcontraseña = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaempleados = new javax.swing.JTable();
        txtbuscar = new javax.swing.JTextField();
        btncancelar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jCheckBoxdatos = new javax.swing.JCheckBox();
        jCheckBoxinformes = new javax.swing.JCheckBox();
        jCheckBoxcbt = new javax.swing.JCheckBox();
        jCheckBoxfacturacion = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnaplicar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaplicar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728934 - enter login right.png"))); // NOI18N
        btnaplicar.setMnemonic('p');
        btnaplicar.setText("Aplicar");
        btnaplicar.setMaximumSize(new java.awt.Dimension(75, 23));
        btnaplicar.setMinimumSize(new java.awt.Dimension(75, 23));
        btnaplicar.setNextFocusableComponent(btncancelar);
        btnaplicar.setPreferredSize(new java.awt.Dimension(75, 23));
        btnaplicar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaplicarActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agregar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Apellidos:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Nombres:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Nombre de Usuario:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Contraseña:");

        txtapellido.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtapellido.setForeground(new java.awt.Color(0, 102, 204));
        txtapellido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtapellidoActionPerformed(evt);
            }
        });
        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtapellidoKeyReleased(evt);
            }
        });

        txtnombre.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });

        txtusuario.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtusuario.setForeground(new java.awt.Color(0, 102, 204));
        txtusuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtusuarioActionPerformed(evt);
            }
        });

        txtcontraseña.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtcontraseña.setForeground(new java.awt.Color(0, 102, 204));
        txtcontraseña.setNextFocusableComponent(jCheckBoxdatos);
        txtcontraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcontraseñaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtusuario)
                    .addComponent(txtcontraseña, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtapellido, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtnombre, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtapellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtcontraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 16, Short.MAX_VALUE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablaempleados.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaempleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {}
            },
            new String [] {

            }
        ));
        tablaempleados.setToolTipText("Hacer doble clic para modificar un usuario");
        tablaempleados.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tablaempleados.setNextFocusableComponent(txtapellido);
        tablaempleados.setOpaque(false);
        tablaempleados.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaempleadosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaempleados);

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtbuscar.setNextFocusableComponent(tablaempleados);
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btncancelar.setMnemonic('v');
        btncancelar.setText("Volver");
        btncancelar.setNextFocusableComponent(txtapellido);
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Seleccionar niveles de acceso", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jCheckBoxdatos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jCheckBoxdatos.setText("Datos");
        jCheckBoxdatos.setNextFocusableComponent(jCheckBoxcbt);
        jCheckBoxdatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxdatosActionPerformed(evt);
            }
        });

        jCheckBoxinformes.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jCheckBoxinformes.setText("Informes");
        jCheckBoxinformes.setNextFocusableComponent(jCheckBoxfacturacion);

        jCheckBoxcbt.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jCheckBoxcbt.setText("CBT");
        jCheckBoxcbt.setNextFocusableComponent(jCheckBoxinformes);

        jCheckBoxfacturacion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jCheckBoxfacturacion.setText("Facturacion");
        jCheckBoxfacturacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxfacturacionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jCheckBoxdatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jCheckBoxinformes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jCheckBoxfacturacion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jCheckBoxcbt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxdatos)
                    .addComponent(jCheckBoxcbt))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxinformes)
                    .addComponent(jCheckBoxfacturacion))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnaplicar, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(65, 65, 65)
                        .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnaplicar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnaplicarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaplicarActionPerformed
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        String apellido, nombre, usuario, contraseña;
        boolean datos, cbt, informes, facturacion;
        String sSQL = "";
        apellido = txtapellido.getText();
        nombre = txtnombre.getText();
        usuario = txtusuario.getText();
        contraseña = txtcontraseña.getText();
        datos = jCheckBoxdatos.isSelected();
        informes = jCheckBoxinformes.isSelected();
        cbt = jCheckBoxcbt.isSelected();
        facturacion = jCheckBoxfacturacion.isSelected();
        if (!"".equals(apellido)&&!"".equals(nombre)&&!"".equals(usuario)&&!"".equals(contraseña)) {
            if (mod == 0) {
                ///////////////////agregar datos//////////////////////////////
                try {

                    String sql = "SELECT apellido, usuario FROM usuarios";
                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sql);
                    int contador = 0; //inicio

                    while (rs.next()) {

                        contador++;
                    }

                    rs.beforeFirst();// fin
                    if (contador != 0) {
                        while (rs.next()) {

                            if (apellido.equals(rs.getString("apellido"))) {
                                if (usuario.equals(rs.getString("usuario"))) {
                                    JOptionPane.showMessageDialog(null, "El usuario ya esta en la base de datos");
                                    break;
                                }

                            }

                        }
                    }
                    rs.beforeFirst();

                    sSQL = "INSERT INTO usuarios(nombre, apellido, usuario, "
                    + "contraseña, datos, cbt, informes,"
                    + "facturacion) VALUES(?,?,?,?,?,?,?,?)";

                    PreparedStatement pst = cn.prepareStatement(sSQL);
                    pst.setString(1, nombre);
                    pst.setString(2, apellido);
                    pst.setString(3, usuario);
                    pst.setString(4, contraseña);
                    pst.setBoolean(5, datos);
                    pst.setBoolean(6, cbt);
                    pst.setBoolean(7, informes);
                    pst.setBoolean(8, facturacion);
                    int n = pst.executeUpdate();

                    if (n > 0) {
                        JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                    }
                    txtapellido.setText("");
                    txtnombre.setText("");
                    txtusuario.setText("");
                    txtcontraseña.setText("");
                    jCheckBoxdatos.setSelected(false);
                    jCheckBoxinformes.setSelected(false);
                    jCheckBoxcbt.setSelected(false);
                    jCheckBoxfacturacion.setSelected(false);
                    cargartabla("");

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                }
            } ////////////////////////modifica datos/////////////////////////////////
            else {
                try {

                    String sql = "SELECT apellido, usuario FROM usuarios";
                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sql);

                    sSQL = "UPDATE usuarios SET apellido=?,"
                    + " nombre=?, usuario=?, "
                    + "contraseña=?, datos=?, "
                    + " cbt=?, informes=?, "
                    + "facturacion=?"
                    + " WHERE id_usuarios=" + idusuarios;
                    PreparedStatement pst = cn.prepareStatement(sSQL);

                    pst.setString(1, apellido);
                    pst.setString(2, nombre);
                    pst.setString(3, usuario);
                    pst.setString(4, contraseña);
                    pst.setBoolean(5, datos);
                    pst.setBoolean(6, cbt);
                    pst.setBoolean(7, informes);
                    pst.setBoolean(8, facturacion);
                    int n = pst.executeUpdate();

                    if (n > 0) {
                        JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                    }
                    txtapellido.setText("");
                    txtnombre.setText("");
                    txtusuario.setText("");
                    txtcontraseña.setText("");
                    jCheckBoxdatos.setSelected(false);
                    jCheckBoxinformes.setSelected(false);
                    jCheckBoxcbt.setSelected(false);
                    jCheckBoxfacturacion.setSelected(false);
                    mod = 0;
                    cargartabla("");
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                }
            }
        } else
        JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios");
    }//GEN-LAST:event_btnaplicarActionPerformed

    private void txtapellidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtapellidoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtapellidoActionPerformed

    private void txtapellidoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtapellidoKeyReleased
        cargartabla(txtapellido.getText());
    }//GEN-LAST:event_txtapellidoKeyReleased

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void txtusuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtusuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtusuarioActionPerformed

    private void txtcontraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcontraseñaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcontraseñaActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        cargartabla(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        if (mod == 1) {
            txtapellido.setText("");
            txtnombre.setText("");
            txtusuario.setText("");
            txtcontraseña.setText("");
            jCheckBoxdatos.setSelected(false);
            jCheckBoxinformes.setSelected(false);
            jCheckBoxcbt.setSelected(false);
            jCheckBoxfacturacion.setSelected(false);
            mod = 0;
            btncancelar.setText("Salir");
        } else {
            this.dispose();
        }
    }//GEN-LAST:event_btncancelarActionPerformed

    private void jCheckBoxdatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxdatosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxdatosActionPerformed

    private void jCheckBoxfacturacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxfacturacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxfacturacionActionPerformed

    private void tablaempleadosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaempleadosKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {

            evt.consume();
            tablaempleados.transferFocus();

        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            
                    int filasel;
                    String msj2;
                    filasel = tablaempleados.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        mod = 1;
                        cargardatos(tablaempleados.getValueAt(tablaempleados.getSelectedRow(), 0).toString());
                        btncancelar.setText("Cancelar");
                    }

        }
        
    }//GEN-LAST:event_tablaempleadosKeyPressed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaplicar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JCheckBox jCheckBoxcbt;
    private javax.swing.JCheckBox jCheckBoxdatos;
    private javax.swing.JCheckBox jCheckBoxfacturacion;
    private javax.swing.JCheckBox jCheckBoxinformes;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaempleados;
    private javax.swing.JTextField txtapellido;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txtcontraseña;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextField txtusuario;
    // End of variables declaration//GEN-END:variables
}
