package Formularios;

import Clases.ConexionMySQLLocal;
import Clases.Escape;
import Clases.TreeState;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

public class Practicas extends javax.swing.JDialog {

    DefaultMutableTreeNode labPrioridad = new DefaultMutableTreeNode("Secciones");
    DefaultMutableTreeNode practicasPrioridad;
    DefaultMutableTreeNode analisisPrioridad;
    DefaultMutableTreeNode seccionPrioridad;
    DefaultTableModel model, modelPrioridad;
    String nombre_analisis, padre, izq;
    String nombre_analisisPrioridad;
    int nivel;

    public Practicas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Prácticas");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        
        crearArbolesPrioridad();
        eventojtreePrioridad();
        setResizable(false);
        setLocationRelativeTo(null);
        Escape.funcionescape(this);
    }

    void crearArbolesPrioridad() {

        //////////////////////////////////////////////////////////////
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        String sSQLsecc = "SELECT nombre, prioridad, id_secciones FROM secciones where estado!=0 order by secciones.prioridad";

        try {
            Statement st2 = cn.createStatement();
            ResultSet rs2 = st2.executeQuery(sSQLsecc);
            labPrioridad.removeAllChildren();
            while (rs2.next()) {
                seccionPrioridad = new DefaultMutableTreeNode(String.format("%03d", rs2.getInt("prioridad")) + "-" + rs2.getString(1));
                labPrioridad.add(seccionPrioridad);
                String sSQL = "SELECT id_practicas,determinacion_practica FROM practicas INNER JOIN secciones ON practicas.id_seccion=secciones.id_secciones  where id_secciones=" + rs2.getString(3) + " order by practicas.prioridad";
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sSQL);
                rs.beforeFirst();
                while (rs.next()) {
                    practicasPrioridad = new DefaultMutableTreeNode(rs.getString(2));
                    seccionPrioridad.add(practicasPrioridad);
                    String sSQL3 = "SELECT id_analisis, nombre FROM analisis where id_practicas=" + rs.getString(1) + " AND estado = 1 order by analisis.prioridad";
                    Statement st3 = cn.createStatement();
                    ResultSet rs3 = st3.executeQuery(sSQL3);
                    rs3.beforeFirst();
                    while (rs3.next()) {
                        analisisPrioridad = new DefaultMutableTreeNode(rs3.getString(2));
                        practicasPrioridad.add(analisisPrioridad);
                    }
                }

                ///////////////////////////////////////////////////////////////
            }
            DefaultTreeModel modelo = new DefaultTreeModel(labPrioridad);
            this.jTree4.setModel(modelo);
            TreeState.setTreeState(jTree4, false);
            //
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarnivel(int valor) {
        if (valor == 0) {
            System.out.println("valor0 "+valor);
            try {
                ConexionMySQLLocal cc = new ConexionMySQLLocal();
                Connection cn = cc.Conectar();
                String[] Titulo = {"Nombre", "Prioridad", "id"};
                String[] Registros = new String[3];
                modelPrioridad = new DefaultTableModel(null, Titulo) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return true;
                    }
                };

                String sql = "SELECT nombre, prioridad, id_secciones FROM secciones where estado!=0 order by secciones.prioridad";
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    Registros[0] = rs.getString(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    modelPrioridad.addRow(Registros);
                }
                tablaprioridad.setModel(modelPrioridad);
                ////////////////////////////////////////////////////////////////////
                tablaprioridad.getColumnModel().getColumn(2).setMaxWidth(0);
                tablaprioridad.getColumnModel().getColumn(2).setMinWidth(0);
                tablaprioridad.getColumnModel().getColumn(2).setPreferredWidth(0);
                ////////////////////////////////////////////////////////////////////
                // 
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, ex);
            }
        }
        if (valor == 1) {
            System.out.println("valor1 "+valor);
            try {
                ConexionMySQLLocal cc = new ConexionMySQLLocal();
                Connection cn = cc.Conectar();
                String[] Titulo = {"Nombre", "Prioridad", "id"};
                String[] Registros = new String[3];
                model = new DefaultTableModel(null, Titulo) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return true;
                    }
                };
                int fin;
                int inicio=nombre_analisisPrioridad.indexOf("-") +1;
                fin = nombre_analisisPrioridad.length();
                System.out.println("nombre_analisisPrioridad "+nombre_analisisPrioridad);
                System.out.println("inicio "+inicio);
                System.out.println("fin "+fin);
                String sSQL = "SELECT determinacion_practica, practicas.prioridad, id_practicas FROM practicas INNER JOIN secciones ON practicas.id_seccion=secciones.id_secciones  where secciones.nombre='" + nombre_analisisPrioridad.substring(inicio, fin) + "' order by practicas.prioridad";
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sSQL);
                while (rs.next()) {
                    Registros[0] = rs.getString(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    model.addRow(Registros);
                }

                tablaprioridad.setModel(model);
                ////////////////////////////////////////////////////////////////////
                tablaprioridad.getColumnModel().getColumn(2).setMaxWidth(0);
                tablaprioridad.getColumnModel().getColumn(2).setMinWidth(0);
                tablaprioridad.getColumnModel().getColumn(2).setPreferredWidth(0);
                ////////////////////////////////////////////////////////////////////ç
                //
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, ex);
            }
        }
        if (valor == 2) {
            try {
                ConexionMySQLLocal cc = new ConexionMySQLLocal();
                Connection cn = cc.Conectar();
                String[] Titulo = {"Nombre", "Prioridad", "id"};
                String[] Registros = new String[3];
                model = new DefaultTableModel(null, Titulo) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return true;
                    }
                };

                String sql = "SELECT nombre, analisis.prioridad,id_analisis,codigo_interno FROM analisis INNER JOIN practicas ON practicas.id_practicas=analisis.id_practicas where practicas.determinacion_practica='" + nombre_analisisPrioridad + "' AND analisis.estado = 1 order by analisis.prioridad";
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    //Registros[0] = rs.getString(1);
                    Registros[0] = rs.getString(4) + "-" + rs.getString(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    model.addRow(Registros);
                }
                tablaprioridad.setModel(model);
                ////////////////////////////////////////////////////////////////////
                tablaprioridad.getColumnModel().getColumn(2).setMaxWidth(0);
                tablaprioridad.getColumnModel().getColumn(2).setMinWidth(0);
                tablaprioridad.getColumnModel().getColumn(2).setPreferredWidth(0);
                ////////////////////////////////////////////////////////////////////
                //
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, ex);
            }
        }
    }

    void eventojtreePrioridad() {

        jTree4.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {

            public void valueChanged(TreeSelectionEvent e) {
                // Se obtiene el Path seleccionado
                TreePath path = e.getPath();
                Object[] nodos = path.getPath();
                for (Object nodo : nodos) {
                    nodo.toString();
                }
                // Se obtiene el Nodo seleccionado
                DefaultMutableTreeNode NodoSeleccionado = (DefaultMutableTreeNode) nodos[nodos.length - 1];
                nombre_analisisPrioridad = NodoSeleccionado.getUserObject().toString();
                if (NodoSeleccionado.getPreviousNode() != null) {
                    nivel = NodoSeleccionado.getLevel();
                    if (!nombre_analisisPrioridad.equals("Secciones")) {
                        System.out.println("nombre_analisisPrioridad");
                        cargarnivel(nivel);
                    }
                } else {
                    cargarnivel(0);
                }
            }
        });

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        Baja = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree4 = new javax.swing.JTree();
        btnsalir = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaprioridad = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();

        Baja.setText("Baja");
        Baja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BajaActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Baja);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTree4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jTree4.setComponentPopupMenu(jPopupMenu1);
        jTree4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTree4MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTree4);

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728980 - home.png"))); // NOI18N
        btnsalir.setMnemonic('p');
        btnsalir.setText("Principal");
        btnsalir.setMaximumSize(new java.awt.Dimension(110, 41));
        btnsalir.setMinimumSize(new java.awt.Dimension(110, 41));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        tablaprioridad.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaprioridad.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre", "Prioridad"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tablaprioridad);

        jButton3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        jButton3.setText("Modificar prioridad");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton3))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 317, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void jTree4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTree4MouseClicked

    }//GEN-LAST:event_jTree4MouseClicked

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        if (tablaprioridad.getSelectedRow() >= 0) {
            int niveles = 0;
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTree4.getSelectionPath().getLastPathComponent();
            niveles = node.getLevel();

            //JOptionPane.showMessageDialog(null, "Nivel: " +  node.toString());
            System.out.println(node.getLevel());
            //JOptionPane.showMessageDialog(null, "Nivel: " + node.getLevel());

            if (niveles == 0) {
                try {
                    // JOptionPane.showMessageDialog(null, "Entra nivel 0");
                    int n2 = 0, j = 0;
                    ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                    Connection cn = mysql.Conectar();
                    n2 = tablaprioridad.getRowCount();
                    //JOptionPane.showMessageDialog(null, "Entra nivel: " + n2);
                    while (n2 > j) {
                        String prioridad = tablaprioridad.getValueAt(j, 1).toString().replace(" ", "");
                        int id_secciones = Integer.valueOf(tablaprioridad.getValueAt(j, 2).toString());
                        String actualizar = "UPDATE secciones SET prioridad=? WHERE id_secciones= " + id_secciones;
                        PreparedStatement pst = cn.prepareStatement(actualizar);
                        pst.setString(1, prioridad);
                        int p = pst.executeUpdate();
                        j++;
                    }

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos");
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
            if (niveles == 1) {
                try {
                    //  JOptionPane.showMessageDialog(null, "Entra nivel 1");
                    int n2 = 0, j = 0;
                    ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                    Connection cn = mysql.Conectar();
                    n2 = tablaprioridad.getRowCount();
                    //  JOptionPane.showMessageDialog(null, "cantidad: " + n2);
                    while (n2 > j) {
                        String prioridad = tablaprioridad.getValueAt(j, 1).toString().replace(" ", "");
                        int id_practica = Integer.valueOf(tablaprioridad.getValueAt(j, 2).toString());
                        //JOptionPane.showMessageDialog(null, "cantidad: " + id_practica);
                        String actualizar = "UPDATE practicas SET prioridad=? WHERE id_practicas= " + id_practica;
                        PreparedStatement pst = cn.prepareStatement(actualizar);
                        pst.setString(1, prioridad);
                        int p = pst.executeUpdate();
                        j++;
                    }

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos");
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
            if (niveles == 2) {
                try {
                    //JOptionPane.showMessageDialog(null, "Entra nivel 2");
                    int n2 = 0, j = 0;
                    ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                    Connection cn = mysql.Conectar();
                    n2 = tablaprioridad.getRowCount();

                    while (n2 > j) {
                        String prioridad = tablaprioridad.getValueAt(j, 1).toString().replace(" ", "");
                        int id_analisis = Integer.valueOf(tablaprioridad.getValueAt(j, 2).toString());
                        String actualizar = "UPDATE analisis SET prioridad=? WHERE id_analisis= " + id_analisis + " AND estado = 1";
                        PreparedStatement pst = cn.prepareStatement(actualizar);
                        pst.setString(1, prioridad);
                        int p = pst.executeUpdate();
                        j++;
                    }

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos");
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
            cargarnivel(niveles);
        } else {
            JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila");
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void BajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BajaActionPerformed
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTree4.getSelectionPath().getLastPathComponent();

        String nombre_nodo = "";
        nombre_nodo = node.toString();
        int p;
        p = nombre_nodo.length();
        nombre_nodo = nombre_nodo.substring(4, p);
        try {
            JOptionPane.showMessageDialog(null, "Selecciono : " + nombre_nodo);
            String sql = "Select  secciones.*, practicas.codigo_practica\n"
            + "from secciones inner JOIN practicas on secciones.id_secciones= practicas.id_seccion\n"
            + "where nombre='" + nombre_nodo + "'";

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                JOptionPane.showMessageDialog(null, "La seccion possee Practicas, por ello no puede ser dada de baja");
            } else {
                String actualizar = "UPDATE secciones SET estado=? WHERE nombre='" + nombre_nodo + "' ";
                PreparedStatement pst = cn.prepareStatement(actualizar);
                pst.setInt(1, 0);
                int s = pst.executeUpdate();
                if (s > 0) {
                    JOptionPane.showMessageDialog(null, "La seccion fue dada de baja");

                    crearArbolesPrioridad();
                }
            }
        } catch (SQLException ex) {
        }

    }//GEN-LAST:event_BajaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Baja;
    private javax.swing.JButton btnsalir;
    private javax.swing.JButton jButton3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTree jTree4;
    private javax.swing.JTable tablaprioridad;
    // End of variables declaration//GEN-END:variables
}
