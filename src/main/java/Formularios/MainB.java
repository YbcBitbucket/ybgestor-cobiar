package Formularios;

import Clases.Anula_Online;
import Clases.ConexionMySQLLocal;
import Clases.Contraseña_Boreal;
import Clases.NumberToLetterConverter;
import static Clases.HiloInicio.arancel;
import static Clases.HiloInicio.añonbu;
import static Clases.HiloInicio.contadorlocalidad;
import static Clases.HiloInicio.idlocalidad;
import static Clases.HiloInicio.nombrelocalidad;
import static Clases.HiloInicio.nombreobrasocial;
import Clases.camposordenes;
import Clases.informe_mesada;
import Clases.Cargar_orden;
import Clases.ConexionMySQL;
import Clases.Exportar;
import static Clases.HiloInicio.contadormedico;
import static Clases.HiloInicio.contadorobrasocial;
import static Clases.HiloInicio.idmedico;
import static Clases.HiloInicio.idobrasocial;
import static Clases.HiloInicio.matriculamedico;
import static Clases.HiloInicio.nombremedico;
import static Clases.HiloInicio.obrasocial;
import Clases.Valida_Online;
import Clases.informe_mesada_2_analisis;
import Clases.valida_orden_colegio;
import static Formularios.ElegirBioquimicos.id_colegiado;
import static Formularios.FechaInforme.fechai;
import static Formularios.FechaInforme.fechao;
import static Formularios.Login.id_usuario;
import static Formularios.Login.matricula_colegiado;
import static Formularios.Login.nombre_colegiado;
import static Formularios.Proxima_Orden.numorden;
import static Formularios.Resultados.fechaPaciente;
import static Formularios.Resultados.mail;
import static Formularios.Resultados.nombre_paciente;
import static Formularios.Resultados.observacion;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableRowSorter;
import javax.xml.datatype.DatatypeConfigurationException;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import net.sf.jasperreports.engine.JasperPrintManager;

public class MainB extends javax.swing.JFrame {

    public static int[] idprovincia = new int[25];
    public static String[] nombreprovincia = new String[25];
    public String[] descripcion = new String[100000];
    public static String[][] matloc = new String[2387][3];///2382
    public static int contadorprovincia, facturacion = 0, plan = 0;
    public static int contadorlocalidad2, id_especialidad = 1, id_medico = 1;
    public static int banderacarga;
    public static String url, sexo, direccion, lugar, documento_afiliado, apellido_afiliado, nombre_afiliado, numero_afiliado, fecha, fecha2, id_obra2 = "", localidad_lab, domicilio_lab, total_pesos_letras, total_centavos_letras, periodo, periododjj;
    int id_obra_social, nbu, id_historia_clinica = 0, banderamodifica = 0, id_paciente, mod = 0, id_localidad = 2358, contadorj = 0, contadorj2 = 0, id_orden, pacientes = 0, practicas = 0, total = 0, contadorobra = 0;
    public static int idPaciente;
    DefaultTableModel model, model1, model2, model3, modelPaciente, modelPracticas;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    HiloOrdenes hilo;
    HiloBusquedaPacientes hiloPacientes;
    HiloBusquedaPacientesPorFecha hiloPacientesPorFecha;
    String hora = "", hora2 = "", fechaonline = "", Codigo_afiliado = "", pasaporte = "", mensaje = "";
    public static String cuil = "", observacionesOrdenes = "";
    static Image imagen;
    Vector columna = new Vector();
    Vector filas = new Vector();
    public static DefaultTableModel myModel;
    public static String ObraSocial, CodObra, mes = "", año = "", medico = "";
    public static double seña_publica = 0.0;
    public static int idobraimprime = 0, totalRow = 0, logincolegio, banderapaciente = 0, bandera_online = 0, tipo_orden = 0;
    String[] practica = new String[10000];
    String[] practica2 = new String[10000];
    String[] practica_fac;
    String[] preciopractica = new String[10000];
    String[] preciopractica2 = new String[10000];
    int[] tiempo = new int[10000];
    int[] tiempo2 = new int[10000];
    String[] preciopracticaparticular1 = new String[10000];
    String[] preciopracticaparticular2 = new String[10000];
    String[] preciopracticaparticular3 = new String[10000];
    String[] preciopracticaparticular4 = new String[10000];
    String[] preciopracticaparticular5 = new String[10000];
    String[] idpractica = new String[10000];
    String[] idpractica2 = new String[10000];
    String[] instruciones = new String[10000];
    String[] instruciones2 = new String[20000];
    String[][] tablaimportar;
    int[] informe_mesada_false = new int[60];
    int tiempo_procesamiento = 0, cantidad_practica_false = 0, indice_if_false = 0, bandera_paciente = 0;
    Object prueba[] = new Object[10];
    TextAutoCompleter textAutoAcompleter;
    TextAutoCompleter textAutoAcompletermatricula2;
    TextAutoCompleter textAutoAcompleter2;
    TextAutoCompleter textAutoAcompletertipo;
    public static String ruta = "C:\\Descargas-CBT\\";

    public static double totalParticular;

    String filamodifica, nom_medico, matricula_medico, obra = "", ip = "", mensajepractica = "", mensajeanulacion = "", respuestapractica = "", respuestaafiliado = "", respuestaanulacion = "";
    public static int contadorPracticas = 0;
    String edad = "0";

    public MainB() {
        initComponents();
        limpiarDatosPaciente();
        setTitle("COBIAR");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        textAutoAcompleter = new TextAutoCompleter(txtpractica2);
        textAutoAcompletermatricula2 = new TextAutoCompleter(txtmedico);
        textAutoAcompleter2 = new TextAutoCompleter(txtobrasocialpaciente);
        textAutoAcompletertipo = new TextAutoCompleter(txttipo);
        textAutoAcompletertipo.addItem("Clínico.");
        textAutoAcompletertipo.addItem("Embarazada.");
        textAutoAcompletertipo.addItem("Prelaborales.");
        textAutoAcompletertipo.addItem("Recíen Nacidos.");
        textAutoAcompletertipo.addItem("Transplantes.");
        textAutoAcompletertipo.addItem("Otros.");
        textAutoAcompletertipo.addItem("Indistinto.");
        textAutoAcompletertipo.addItem("VIH.");
        textAutoAcompletertipo.addItem("S/D.");
        textAutoAcompletertipo.setMode(0);
        textAutoAcompletertipo.setCaseSensitive(false);
        btnPatologias.setEnabled(false);
        this.setLocationRelativeTo(null);
        alinear();
        empleados();
        cargarperiodo();
        cargarobrasocial();
        cargarorden();
        cargartablapacientes();
        unclick();
        dobleclickordenes();
        cargarlocalidad();
        dobleclick();
        btncargarorden.setEnabled(false);
        cargarmatricula();
        cbosexo.setSelectedIndex(0);
        unclickhistoriaclinica();
        cargarhistoriaclinica();

        txtapellidopaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtdescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtdireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtinstrucciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtmedico.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toLowerCase(c));
                }
            }
        });
        txtnombreafiliado1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtnombrepaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtobrasocialpaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtordenes1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtpacientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtpractica2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtprotocolo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtreciennacido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txttipo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        ////////////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(6).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(6).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(6).setPreferredWidth(0);
        ////////////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(7).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(7).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(7).setPreferredWidth(0);
        ////////////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(10).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(10).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(10).setPreferredWidth(0);
        ////////////////////////////////////////////////////////////////////      
        tablapracticas.getColumnModel().getColumn(11).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(11).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(11).setPreferredWidth(0);
        ////////////////////////////////////////////////////////////////////      
        tablapracticas.getColumnModel().getColumn(12).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(12).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(12).setPreferredWidth(0);
        txtdnipaciente.requestFocus();
        ////////////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(0).setPreferredWidth(30);
        tablapracticas.getColumnModel().getColumn(1).setPreferredWidth(30);
        tablapracticas.getColumnModel().getColumn(2).setPreferredWidth(20);
        tablapracticas.getColumnModel().getColumn(3).setPreferredWidth(270);
        tablapracticas.getColumnModel().getColumn(4).setPreferredWidth(20);
        tablapracticas.getColumnModel().getColumn(5).setPreferredWidth(40);
        tablapracticas.getColumnModel().getColumn(8).setPreferredWidth(10);
        tablapracticas.getColumnModel().getColumn(9).setPreferredWidth(15);
        cargarfechafacturacion();
        cargarhora();
        cargarip();
        jTabbedPane3.setEnabledAt(2, false);
        fechainicial.setLocation(0, 0);
        txtlogo.setVisible(false);
    }

    void cargarip() {
        ////////////////////////////////////////////////////////////////////////
        try {
            String thisIp = InetAddress.getLocalHost().getHostAddress();
            String thisname = InetAddress.getLocalHost().getHostName();
            ip = thisIp + "-" + thisname;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void cargarfechafacturacion() {

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
        fechainicial.setText(fecha);
        fechafinal.setText(fecha);
        txtfechaPaciente.setText(fecha);
        txtfechaPaciente2.setText(fecha);
    }

    private Image ConvertirImagen(byte[] bytes) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Iterator readers = ImageIO.getImageReadersByFormatName("jpeg");
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis; // File or InputStream
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
        return reader.read(0, param);
    }

    void empleados() {
        // Eligue q pueden ver los empleados
        if (Login.datos == false) {
            jdatos.setVisible(false);
        }

        if (Login.cbt == false) {
            jcbt.setVisible(false);
        }

        if (Login.informes == false) {
            jinformes.setVisible(false);
        }

        if (Login.facturacion == false) {
            ///  JOptionPane.showConfirmDialog(null, "si");
            //jTabbedPane3.setSelectedIndex(2)
            txtmedico.requestFocus();
        }

    }

    public void llama_excel() {
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + ruta + "Errores Transferencia" + ".xls");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    void clic_practicatodas() {
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        double total = 0.0;
        ////////////////////////////////////////////////////////////
        String[] titulos2 = {"N° Orden", "cod_practica", "1", "2", "3", "4"};//estos seran los titulos de la tabla.            
        String[] datos2 = new String[6];
        model = new DefaultTableModel(null, titulos2) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        try {
            int i = 0;
            while (i < tablaordenes.getRowCount()) {
                Statement St = cn.createStatement();
                ResultSet Rs = St.executeQuery("SELECT ordenes.id_ordenes, practicas.codigo_practica, practicas.determinacion_practica,precio_practica,cod_practica_fac FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas WHERE ordenes.estado_orden=0 and ordenes_tienen_practicas.factura!=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' AND id_obrasocial=" + tablaordenes.getValueAt(i, 10).toString());
                while (Rs.next()) {
                    if (tablaordenes.getValueAt(i, 7).equals(true) && Rs.getString(1).equals(tablaordenes.getValueAt(i, 0).toString())) {
                        datos2[0] = Rs.getString(1);///id orden
                        datos2[1] = Rs.getString(2);////cod practica    
                        datos2[2] = Rs.getString(3);////determinacion
                        datos2[3] = Rs.getString(4);////precio practica
                        total = total + Rs.getDouble(4);
                        datos2[4] = Rs.getString(5);////cod fac prac
                        datos2[5] = "idorden" + Rs.getString(1);
                        model.addRow(datos2);
                    }
                }
                i++;
            }
            txttotal1.setText((String.valueOf(Redondear(total))));
            tabladetalle.setModel(model);
            /////////////////////////////////////////////////////////////
            tabladetalle.getColumnModel().getColumn(2).setMaxWidth(0);
            tabladetalle.getColumnModel().getColumn(2).setMinWidth(0);
            tabladetalle.getColumnModel().getColumn(2).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            tabladetalle.getColumnModel().getColumn(3).setMaxWidth(0);
            tabladetalle.getColumnModel().getColumn(3).setMinWidth(0);
            tabladetalle.getColumnModel().getColumn(3).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////////
            tabladetalle.getColumnModel().getColumn(4).setMaxWidth(0);
            tabladetalle.getColumnModel().getColumn(4).setMinWidth(0);
            tabladetalle.getColumnModel().getColumn(4).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////////
            tabladetalle.getColumnModel().getColumn(5).setMaxWidth(0);
            tabladetalle.getColumnModel().getColumn(5).setMinWidth(0);
            tabladetalle.getColumnModel().getColumn(5).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////////
            ///
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        ////////////////////////////////////////////////////////////
        ///cargatotales();
        cargatotalesordenesfacturacion();
    }

    void clic_practica() {

        ////  if (!cboestado.getSelectedItem().toString().equals("Filtrar por Estado")) {
        /* } else {
        
         }*/
    }

    /*void cargatotales() {
        double total = 0.00, sumatoria = 0.0;

        DecimalFormat df = new DecimalFormat("0.00");
        //AQUI SE SUMAN LOS VALORES DE CADA FILA PARA COLOCARLO EN EL CAMPO DE TOTAL
        int totalRow = tablaordenes.getRowCount();
        totalRow -= 1;
        for (int i = 0; i <= (totalRow); i++) {
            if (tablaordenes.getValueAt(i, 7).toString().equals("true")) {
                String x = tablaordenes.getValueAt(i, 6).toString();
                sumatoria = Double.valueOf(x);
                total = total + sumatoria;
            }
        }
        //txttotalordenes.setText((String.valueOf(totalRow + 1)));

        txttotal1.setText((String.valueOf(Redondear(total))));

    }
     */
    ///////////////////////////////////////HILO Ordenes///////////////////////////////////////////////
    public class HiloOrdenes extends Thread {

        JProgressBar progreso;

        public HiloOrdenes(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            model3 = (DefaultTableModel) tablaordenes.getModel();
            ConexionMySQLLocal cc = new ConexionMySQLLocal();
            Connection cn = cc.Conectar();
            double total = 0.0;
            try {
                Statement St1 = cn.createStatement();///
                ResultSet Rs1;
                if (jCheckBox1.isSelected()) {
                    System.out.println("Es seleccionado");
                    Rs1 = St1.executeQuery("SELECT * FROM vista_ordenes WHERE Date(fecha) BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' AND id_obrasocial=" + idobraimprime);

                } else {
                    Rs1 = St1.executeQuery("SELECT * FROM vista_ordenes WHERE Date(fecha) BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "'");
                }
                String estado = "";
                int fila = 0;
                while (Rs1.next()) {
                    if (Rs1.getInt(13) == 0) {
                        estado = "OK";
                    } else {
                        estado = "Anulada";
                    }
                    Object nuevo[] = {
                        Rs1.getString(1),
                        Rs1.getString(2),
                        Rs1.getString(3) + " " + Rs1.getString(12),
                        Rs1.getString(4) + " " + Rs1.getString(11),
                        Rs1.getString(5),
                        Rs1.getString(6),
                        Rs1.getString(7),
                        true,
                        Rs1.getString(8),
                        Rs1.getString(9),
                        Rs1.getString(10),
                        estado,
                        Rs1.getInt(14)};
                    model3.addRow(nuevo);
                    tablaordenes.setValueAt(true, fila, 7);
                    fila++;
                }
                tablaordenes.setModel(model3);
                /////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(8).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(8).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(8).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(9).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(9).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(9).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(10).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(10).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(10).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                // alinear();
                tablaordenes.getColumnModel().getColumn(0).setPreferredWidth(30);
                //tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(20);
                tablaordenes.getColumnModel().getColumn(2).setPreferredWidth(200);
                tablaordenes.getColumnModel().getColumn(3).setPreferredWidth(200);
                tablaordenes.getColumnModel().getColumn(4).setPreferredWidth(50);
                tablaordenes.getColumnModel().getColumn(5).setPreferredWidth(60);
                tablaordenes.getColumnModel().getColumn(6).setPreferredWidth(40);
                tablaordenes.getColumnModel().getColumn(7).setPreferredWidth(40);
                tablaordenes.getColumnModel().getColumn(11).setPreferredWidth(60);
                progreso.setValue(100);
                // cargatotales();
                txtordenes1.requestFocus();
                //   btnbuscar1.setEnabled(true);
                txtordenes1.setEnabled(true);
                cargatotalesordenesfacturacion();
                ///

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);

            }
            ////////////////////////////////////////////////////////////
            String[] titulos2 = {"N° Orden", "cod_practica", "1", "2", "3", "4"};//estos seran los titulos de la tabla.            
            String[] datos2 = new String[6];
            model = new DefaultTableModel(null, titulos2) {
                ////Celdas no editables////////
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            try {
                int i = 0;
                while (i < tablaordenes.getRowCount()) {

                    Statement St = cn.createStatement();
                    //Rs = St.executeQuery("SELECT ordenes.id_ordenes, practicas.codigo_practica, practicas.determinacion_practica,precio_practica,cod_practica_fac FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas  WHERE ordenes.estado_orden=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' AND ordenes.id_obrasocial=" + idobraimprime);
                    ResultSet Rs = St.executeQuery("SELECT ordenes.id_ordenes, practicas.codigo_practica, practicas.determinacion_practica,precio_practica,cod_practica_fac FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas WHERE ordenes.estado_orden=0 and ordenes_tienen_practicas.factura!=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' AND id_obrasocial=" + tablaordenes.getValueAt(i, 10).toString());
                    //Rs.first();
                    while (Rs.next()) {
//                    System.out.println(tablaordenes.getValueAt(i, 7));
                        if (tablaordenes.getValueAt(i, 7).equals(true) && Rs.getString(1).equals(tablaordenes.getValueAt(i, 0).toString())) {
                            datos2[0] = Rs.getString(1);///id orden
                            datos2[1] = Rs.getString(2);////cod practica    
                            datos2[2] = Rs.getString(3);////determinacion
                            datos2[3] = Rs.getString(4);////precio practica
                            total = total + Rs.getDouble(4);
                            datos2[4] = Rs.getString(5);////cod fac prac
                            datos2[5] = "idorden" + Rs.getString(1);
                            model.addRow(datos2);
                        }
                    }
                    i++;
                }
                txttotal1.setText((String.valueOf(Redondear(total))));
                tabladetalle.setModel(model);
                /////////////////////////////////////////////////////////////
                tabladetalle.getColumnModel().getColumn(2).setMaxWidth(0);
                tabladetalle.getColumnModel().getColumn(2).setMinWidth(0);
                tabladetalle.getColumnModel().getColumn(2).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////
                tabladetalle.getColumnModel().getColumn(3).setMaxWidth(0);
                tabladetalle.getColumnModel().getColumn(3).setMinWidth(0);
                tabladetalle.getColumnModel().getColumn(3).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tabladetalle.getColumnModel().getColumn(4).setMaxWidth(0);
                tabladetalle.getColumnModel().getColumn(4).setMinWidth(0);
                tabladetalle.getColumnModel().getColumn(4).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tabladetalle.getColumnModel().getColumn(5).setMaxWidth(0);
                tabladetalle.getColumnModel().getColumn(5).setMinWidth(0);
                tabladetalle.getColumnModel().getColumn(5).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                ///

                cn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
            ////////////////////////////////////////////////////////////
            ///cargatotales();
            cargatotalesordenesfacturacion();
            ////////////////////////////////////////////////////////////
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }
        }

    }

    public class HiloBusquedaPacientesPorFecha extends Thread {

        JProgressBar progreso;

        public HiloBusquedaPacientesPorFecha(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            String[] Titulo = {"N°", "Apellido", "Nombre", "DNI", "Edad", "Fecha Orden", "Seña", "Diag", "Tipo", "Servicio", "id_OS", "hora", "id", "Mail", "Celular"};
            Object[] Registros = new Object[15];
            double total = 0;
            int i = 0;

            String sql = "SELECT\n"
                    + "  `historia_clinica`.`idhistoria_clinica`,\n"
                    + "  `personas`.`apellido`,\n"
                    + "  `personas`.`nombre`,\n"
                    + "    `pacientes`.`personas_dni`,\n"
                    + "  DATE_FORMAT(`pacientes`.`fecha_nacimiento`, '%d-%m-%Y') AS 'fecha_nacimiento',\n"
                    + "    ((YEAR(CURDATE()) - YEAR(`pacientes`.`fecha_nacimiento`)) + IF((DATE_FORMAT(CURDATE(), '%m-%d') > DATE_FORMAT(`pacientes`.`fecha_nacimiento`, '%m-%d')), 0, -(1))) AS 'edad',\n"
                    + "  DATE_FORMAT(`ordenes`.`fecha`, '%d-%m-%Y') AS 'fecha',\n"
                    + "    `seña`.`seña`,\n"
                    + "  `ordenes`.`observaciones`,\n"
                    + "  `ordenes`.`tipo_orden`,\n"
                    + "  `ordenes`.`servicio`,\n"
                    + "  `historia_clinica`.`descripcion`,\n"
                    + "  `ordenes`.`id_obrasocial`,\n"
                    + "  CONCAT(SUBSTR(`ordenes`.`hora`,1, 2), ':', SUBSTR(`ordenes`.`hora`,3, 2), ':', SUBSTR(`ordenes`.`hora`,4, 2)) AS 'hora',\n"
                    + "  `ordenes`.`id_ordenes`,\n"
                    + "    `pacientes`.`mail`  ,\n"
                    + "        `pacientes`.`celular`\n"
                    + "FROM\n"
                    + "  ((((`ordenes`\n"
                    + "    JOIN `pacientes` ON ((`pacientes`.`id_Pacientes` = `ordenes`.`id_Pacientes`)))\n"
                    + "    JOIN `personas` ON ((`personas`.`dni` = `pacientes`.`personas_dni`)))\n"
                    + "    JOIN `historia_clinica` ON ((`historia_clinica`.`id_ordenes` = `ordenes`.`id_ordenes`)))\n"
                    + "    LEFT JOIN `seña` ON ((`historia_clinica`.`idhistoria_clinica` = `seña`.`idhistoria_clinica`)))\n"
                    + "WHERE\n"
                    + "  (`ordenes`.`estado_orden` = 0) and Date(fecha) BETWEEN '" + invertir(txtfechaPaciente.getText()) + "'  AND '" + invertir(txtfechaPaciente2.getText()) + "'\n"
                    + "GROUP BY\n"
                    + "  `historia_clinica`.`idhistoria_clinica`\n"
                    + "ORDER BY `ordenes`.`id_ordenes`";

            modelPaciente = new DefaultTableModel(null, Titulo) {//"+txtfechaPaciente.getText()+"  AND "+txtfechaPaciente2.getText()+"\n"
                ////Celdas no editables////////
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            ConexionMySQLLocal cc = new ConexionMySQLLocal();
            Connection cn = cc.Conectar();
            try {
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    total = 0;
                    Registros[0] = rs.getString("idhistoria_clinica");
                    Registros[1] = rs.getString("apellido");
                    Registros[2] = rs.getString("nombre");
                    Registros[3] = rs.getString("personas_dni");
                    Registros[4] = rs.getString("edad");
                    Registros[5] = rs.getString("fecha");
                    Registros[6] = "$  " + rs.getDouble("seña");
                    Registros[7] = rs.getString("observaciones");
                    Registros[8] = rs.getString("tipo_orden");
                    Registros[9] = rs.getString("servicio");
                    descripcion[i] = rs.getString("descripcion");
                    Registros[10] = rs.getString("id_obrasocial");
                    Registros[11] = rs.getString("hora");
                    Registros[12] = rs.getString("id_ordenes");
                    Registros[13] = rs.getString("mail");
                    Registros[14] = rs.getString("celular");

                    modelPaciente.addRow(Registros);
                    i++;
                }
                tablapacientes.setModel(modelPaciente);
                tablapacientes.setAutoCreateRowSorter(true);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(0).setMaxWidth(90);
                tablapacientes.getColumnModel().getColumn(0).setMinWidth(90);
                tablapacientes.getColumnModel().getColumn(0).setPreferredWidth(90);
                /////////////////////////////////////////////////////////////
                //tablapacientes.getColumnModel().getColumn(1).setMaxWidth(150);
                tablapacientes.getColumnModel().getColumn(1).setMinWidth(150);
                tablapacientes.getColumnModel().getColumn(1).setPreferredWidth(150);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(2).setMaxWidth(150);
                tablapacientes.getColumnModel().getColumn(2).setMinWidth(150);
                tablapacientes.getColumnModel().getColumn(2).setPreferredWidth(150);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(3).setMaxWidth(95);
                tablapacientes.getColumnModel().getColumn(3).setMinWidth(95);
                tablapacientes.getColumnModel().getColumn(3).setPreferredWidth(95);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(4).setMaxWidth(60);
                tablapacientes.getColumnModel().getColumn(4).setMinWidth(60);
                tablapacientes.getColumnModel().getColumn(4).setPreferredWidth(60);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(5).setMaxWidth(95);
                tablapacientes.getColumnModel().getColumn(5).setMinWidth(95);
                tablapacientes.getColumnModel().getColumn(5).setPreferredWidth(95);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(6).setMaxWidth(60);
                tablapacientes.getColumnModel().getColumn(6).setMinWidth(60);
                tablapacientes.getColumnModel().getColumn(6).setPreferredWidth(60);
                //////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(7).setMaxWidth(0);
                tablapacientes.getColumnModel().getColumn(7).setMinWidth(0);
                tablapacientes.getColumnModel().getColumn(7).setPreferredWidth(85);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(8).setMaxWidth(0);
                tablapacientes.getColumnModel().getColumn(8).setMinWidth(0);
                tablapacientes.getColumnModel().getColumn(8).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(9).setMaxWidth(0);
                tablapacientes.getColumnModel().getColumn(9).setMinWidth(0);
                tablapacientes.getColumnModel().getColumn(9).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(10).setMaxWidth(0);
                tablapacientes.getColumnModel().getColumn(10).setMinWidth(0);
                tablapacientes.getColumnModel().getColumn(10).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(11).setMaxWidth(0);
                tablapacientes.getColumnModel().getColumn(11).setMinWidth(0);
                tablapacientes.getColumnModel().getColumn(11).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(12).setMaxWidth(0);
                tablapacientes.getColumnModel().getColumn(12).setMinWidth(0);
                tablapacientes.getColumnModel().getColumn(12).setPreferredWidth(0);
                ////////////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(13).setMaxWidth(85);
                tablapacientes.getColumnModel().getColumn(13).setMinWidth(85);
                tablapacientes.getColumnModel().getColumn(13).setPreferredWidth(85);
                ////////////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(14).setMaxWidth(85);
                tablapacientes.getColumnModel().getColumn(14).setMinWidth(85);
                tablapacientes.getColumnModel().getColumn(14).setPreferredWidth(85);
                ///////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                tablapacientes.getColumnModel().getColumn(1).setCellRenderer(alinearIzquierda);
                tablapacientes.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
                tablapacientes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                tablapacientes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                tablapacientes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                tablapacientes.getColumnModel().getColumn(6).setCellRenderer(alinearDerecha);

                Rectangle r = tablapacientes.getCellRect(tablapacientes.getRowCount() - 1, 0, true);
                tablapacientes.scrollRectToVisible(r);
                tablapacientes.getSelectionModel().setSelectionInterval(tablapacientes.getRowCount() - 1, tablapacientes.getRowCount() - 1);

                cn.close();
            } catch (SQLException ex) {
                //ex
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "Error 846 en la base de datos...");
            }
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }
        }

    }

    public class HiloBusquedaPacientes extends Thread {

        JProgressBar progreso;

        public HiloBusquedaPacientes(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            String[] Titulo = {"N°", "Apellido", "Nombre", "DNI", "Edad", "Fecha Orden", "Seña", "Diag", "Tipo", "Servicio", "id_OS", "hora", "id", "Mail", "Celular"};
            Object[] Registros = new Object[15];
            double total = 0;
            int i = 0;

            String sql = "SELECT\n"
                    + "  `historia_clinica`.`idhistoria_clinica`,\n"
                    + "  `personas`.`apellido`,\n"
                    + "  `personas`.`nombre`,\n"
                    + "    `pacientes`.`personas_dni`,\n"
                    + "  DATE_FORMAT(`pacientes`.`fecha_nacimiento`, '%d-%m-%Y') AS 'fecha_nacimiento',\n"
                    + "    ((YEAR(CURDATE()) - YEAR(`pacientes`.`fecha_nacimiento`)) + IF((DATE_FORMAT(CURDATE(), '%m-%d') > DATE_FORMAT(`pacientes`.`fecha_nacimiento`, '%m-%d')), 0, -(1))) AS 'edad',\n"
                    + "  DATE_FORMAT(`ordenes`.`fecha`, '%d-%m-%Y') AS 'fecha',\n"
                    + "    `seña`.`seña`,\n"
                    + "  `ordenes`.`observaciones`,\n"
                    + "  `ordenes`.`tipo_orden`,\n"
                    + "  `ordenes`.`servicio`,\n"
                    + "  `historia_clinica`.`descripcion`,\n"
                    + "  `ordenes`.`id_obrasocial`,\n"
                    + "  CONCAT(SUBSTR(`ordenes`.`hora`,1, 2), ':', SUBSTR(`ordenes`.`hora`,3, 2), ':', SUBSTR(`ordenes`.`hora`,4, 2)) AS 'hora',\n"
                    + "  `ordenes`.`id_ordenes`,\n"
                    + "    `pacientes`.`mail`  as mail,\n"
                    + "        `pacientes`.`celular` as celular\n"
                    + "FROM\n"
                    + "  ((((`ordenes`\n"
                    + "    JOIN `pacientes` ON ((`pacientes`.`id_Pacientes` = `ordenes`.`id_Pacientes`)))\n"
                    + "    JOIN `personas` ON ((`personas`.`dni` = `pacientes`.`personas_dni`)))\n"
                    + "    JOIN `historia_clinica` ON ((`historia_clinica`.`id_ordenes` = `ordenes`.`id_ordenes`)))\n"
                    + "    LEFT JOIN `seña` ON ((`historia_clinica`.`idhistoria_clinica` = `seña`.`idhistoria_clinica`)))\n"
                    + "WHERE\n"
                    + "  (`ordenes`.`estado_orden` = 0) and (pacientes.estado=1) and Date(fecha) BETWEEN DATE_SUB(NOW(),INTERVAL 60 DAY)  AND curdate()\n"
                    + "GROUP BY\n"
                    + "  `historia_clinica`.`idhistoria_clinica`\n"
                    + "ORDER BY `ordenes`.`id_ordenes`";

            modelPaciente = new DefaultTableModel(null, Titulo) {
                ////Celdas no editables////////
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            ConexionMySQLLocal cc = new ConexionMySQLLocal();
            Connection cn = cc.Conectar();
            try {
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    total = 0;
                    Registros[0] = rs.getString("idhistoria_clinica");
                    Registros[1] = rs.getString("apellido");
                    Registros[2] = rs.getString("nombre");
                    Registros[3] = rs.getString("personas_dni");
                    Registros[4] = rs.getString("edad");
                    Registros[5] = rs.getString("fecha");
                    Registros[6] = "$  " + rs.getDouble("seña");
                    Registros[7] = rs.getString("observaciones");
                    Registros[8] = rs.getString("tipo_orden");
                    Registros[9] = rs.getString("servicio");
                    descripcion[i] = rs.getString("descripcion");
                    Registros[10] = rs.getString("id_obrasocial");
                    Registros[11] = rs.getString("hora");
                    Registros[12] = rs.getString("id_ordenes");
                    Registros[13] = rs.getString("mail");
                    Registros[14] = rs.getString("celular");

                    modelPaciente.addRow(Registros);
                    i++;
                }
                tablapacientes.setModel(modelPaciente);
                tablapacientes.setAutoCreateRowSorter(true);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(0).setMaxWidth(90);
                tablapacientes.getColumnModel().getColumn(0).setMinWidth(90);
                tablapacientes.getColumnModel().getColumn(0).setPreferredWidth(90);
                /////////////////////////////////////////////////////////////
                //tablapacientes.getColumnModel().getColumn(1).setMaxWidth(150);
                tablapacientes.getColumnModel().getColumn(1).setMinWidth(150);
                tablapacientes.getColumnModel().getColumn(1).setPreferredWidth(150);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(2).setMaxWidth(150);
                tablapacientes.getColumnModel().getColumn(2).setMinWidth(150);
                tablapacientes.getColumnModel().getColumn(2).setPreferredWidth(150);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(3).setMaxWidth(95);
                tablapacientes.getColumnModel().getColumn(3).setMinWidth(95);
                tablapacientes.getColumnModel().getColumn(3).setPreferredWidth(95);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(4).setMaxWidth(60);
                tablapacientes.getColumnModel().getColumn(4).setMinWidth(60);
                tablapacientes.getColumnModel().getColumn(4).setPreferredWidth(60);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(5).setMaxWidth(95);
                tablapacientes.getColumnModel().getColumn(5).setMinWidth(95);
                tablapacientes.getColumnModel().getColumn(5).setPreferredWidth(95);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(6).setMaxWidth(60);
                tablapacientes.getColumnModel().getColumn(6).setMinWidth(60);
                tablapacientes.getColumnModel().getColumn(6).setPreferredWidth(60);
                //////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(7).setMaxWidth(0);
                tablapacientes.getColumnModel().getColumn(7).setMinWidth(0);
                tablapacientes.getColumnModel().getColumn(7).setPreferredWidth(85);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(8).setMaxWidth(0);
                tablapacientes.getColumnModel().getColumn(8).setMinWidth(0);
                tablapacientes.getColumnModel().getColumn(8).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(9).setMaxWidth(0);
                tablapacientes.getColumnModel().getColumn(9).setMinWidth(0);
                tablapacientes.getColumnModel().getColumn(9).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(10).setMaxWidth(0);
                tablapacientes.getColumnModel().getColumn(10).setMinWidth(0);
                tablapacientes.getColumnModel().getColumn(10).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(11).setMaxWidth(0);
                tablapacientes.getColumnModel().getColumn(11).setMinWidth(0);
                tablapacientes.getColumnModel().getColumn(11).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(12).setMaxWidth(0);
                tablapacientes.getColumnModel().getColumn(12).setMinWidth(0);
                tablapacientes.getColumnModel().getColumn(12).setPreferredWidth(0);
                ////////////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(13).setMaxWidth(85);
                tablapacientes.getColumnModel().getColumn(13).setMinWidth(85);
                tablapacientes.getColumnModel().getColumn(13).setPreferredWidth(85);
                ////////////////////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(14).setMaxWidth(85);
                tablapacientes.getColumnModel().getColumn(14).setMinWidth(85);
                tablapacientes.getColumnModel().getColumn(14).setPreferredWidth(85);
                ///////////////////////////////////////////////////////
                tablapacientes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                tablapacientes.getColumnModel().getColumn(1).setCellRenderer(alinearIzquierda);
                tablapacientes.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
                tablapacientes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                tablapacientes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                tablapacientes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                tablapacientes.getColumnModel().getColumn(6).setCellRenderer(alinearDerecha);

                Rectangle r = tablapacientes.getCellRect(tablapacientes.getRowCount() - 1, 0, true);
                tablapacientes.scrollRectToVisible(r);
                tablapacientes.getSelectionModel().setSelectionInterval(tablapacientes.getRowCount() - 1, tablapacientes.getRowCount() - 1);

                cn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "Error 1016 en la base de datos...");
            }
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }
        }

    }

    public void cargatotalesordenesfacturacion() {
        int totalRow = tablaordenes.getRowCount(), contador = 0;
        totalRow -= 1;
        for (int i = 0; i <= (totalRow); i++) {
            if (tablaordenes.getValueAt(i, 7).toString().equals("true")) {
                contador++;
            }
        }
        txttotalordenes.setText((String.valueOf(contador)));
    }

    String invertir(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }
        String salida = "";
        int i = 0;
        /////Año/////
        for (i = 6; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ///Mes///
        for (i = 3; i <= 4; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ////Dia////
        for (i = 0; i <= 1; i++) {
            salida = salida + entrada.charAt(i);
        }

        return salida;

    }

    /////////////////////////////////////////Leo archivo Text///////////////////////////////////////////////    
    void leer_archivo() {

        myModel = new DefaultTableModel() {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return true;
            }
        };
        myModel.addColumn("Matricula");
        myModel.addColumn("Fecha Orden");
        myModel.addColumn("Cod. OS");
        myModel.addColumn("Nombre Paciente");
        myModel.addColumn("Numero de Afiliado");
        myModel.addColumn("Numero de Orden");
        myModel.addColumn("Fecha Prescripsión");
        myModel.addColumn("U. G.");
        myModel.addColumn("DNI");
        myModel.addColumn("P1");
        myModel.addColumn("P2");
        myModel.addColumn("P3");
        myModel.addColumn("P4");
        myModel.addColumn("P5");
        myModel.addColumn("P6");
        myModel.addColumn("P7");
        myModel.addColumn("P8");
        myModel.addColumn("P9");
        myModel.addColumn("P10");
        myModel.addColumn("P11");
        myModel.addColumn("P12");
        myModel.addColumn("P13");
        myModel.addColumn("P14");
        myModel.addColumn("P15");
        myModel.addColumn("P16");
        myModel.addColumn("P17");
        myModel.addColumn("P18");
        myModel.addColumn("P19");
        myModel.addColumn("P20");
    }

    void cargaprovincia() {

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT * FROM provincia ";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las provincias
            while (rs.next()) {
                idprovincia[contadorprovincia] = rs.getInt("id_provincia");
                nombreprovincia[contadorprovincia] = (rs.getString("nombre_provincia"));
                contadorprovincia++;
            }
            cn.close();
            ////
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargalocalidad() {

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT * FROM localidad ";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las localidades

            while (rs.next()) {

                matloc[contadorlocalidad2][0] = rs.getString("id_localidad");
                matloc[contadorlocalidad2][1] = rs.getString("id_provincia");
                matloc[contadorlocalidad2][2] = rs.getString("nombre_localidad");
                contadorlocalidad2++;
            }
            ////
            cn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    void cargartablapacientes() {

        borrartablapacientes();
        iniciarSplash();//
        hiloPacientes = new HiloBusquedaPacientes(progreso);
        hiloPacientes.start();
        hiloPacientes = null;

        /* String[] Titulo = {"N°", "Apellido", "Nombre", "DNI", "Edad", "Fecha Orden", "Seña", "Diag", "Tipo", "Servicio", "id_OS", "hora", "id", "Mail", "Celular"};
        Object[] Registros = new Object[15];
        double total = 0;
        int i = 0;

//        String sql = "SELECT * from vista_ordenes_seña";
        String sql = "SELECT\n"
                + "  `historia_clinica`.`idhistoria_clinica`,\n"
                + "  `personas`.`apellido`,\n"
                + "  `personas`.`nombre`,\n"
                + "  DATE_FORMAT(`pacientes`.`fecha_nacimiento`, '%d-%m-%Y') AS 'fecha_nacimiento',\n"
                + "  DATE_FORMAT(`ordenes`.`fecha`, '%d-%m-%Y') AS 'fecha',\n"
                + "  `ordenes`.`observaciones`,\n"
                + "  `ordenes`.`tipo_orden`,\n"
                + "  `ordenes`.`servicio`,\n"
                + "  `seña`.`seña`,\n"
                + "  `historia_clinica`.`descripcion`,\n"
                + "  `pacientes`.`personas_dni`,\n"
                + "  `ordenes`.`id_obrasocial`,\n"
                + "  CONCAT(SUBSTR(`ordenes`.`hora`,1, 2), ':', SUBSTR(`ordenes`.`hora`,3, 2), ':', SUBSTR(`ordenes`.`hora`,4, 2)) AS 'hora',\n"
                + "  `ordenes`.`id_ordenes`,\n"
                + "  ((YEAR(CURDATE()) - YEAR(`pacientes`.`fecha_nacimiento`)) + IF((DATE_FORMAT(CURDATE(), '%m-%d') > DATE_FORMAT(`pacientes`.`fecha_nacimiento`, '%m-%d')), 0, -(1))) AS 'edad'\n"
                + "FROM\n"
                + "  ((((`ordenes`\n"
                + "    JOIN `pacientes` ON ((`pacientes`.`id_Pacientes` = `ordenes`.`id_Pacientes`)))\n"
                + "    JOIN `personas` ON ((`personas`.`dni` = `pacientes`.`personas_dni`)))\n"
                + "    JOIN `historia_clinica` ON ((`historia_clinica`.`id_ordenes` = `ordenes`.`id_ordenes`)))\n"
                + "    LEFT JOIN `seña` ON ((`historia_clinica`.`idhistoria_clinica` = `seña`.`idhistoria_clinica`)))\n"
                + "WHERE\n"
                + "  (`ordenes`.`estado_orden` = 0) and Date(fecha) BETWEEN DATE_SUB(NOW(),INTERVAL 10 DAY)  AND curdate()\n"
                + "GROUP BY\n"
                + "  `historia_clinica`.`idhistoria_clinica`\n"
                + "ORDER BY `ordenes`.`id_ordenes`";

        modelPaciente = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                total = 0;
                Registros[0] = rs.getString("idhistoria_clinica");
                Registros[1] = rs.getString("apellido");
                Registros[2] = rs.getString("nombre");
                Registros[3] = rs.getString("personas_dni");
//                Registros[4] = rs.getString("fecha_nacimiento");
                Registros[4] = rs.getString("edad");
                Registros[5] = rs.getString("fecha");
                Registros[6] = "$  " + rs.getDouble("seña");
                Registros[7] = rs.getString("observaciones");
                Registros[8] = rs.getString("tipo_orden");
                Registros[9] = rs.getString("servicio");
                descripcion[i] = rs.getString("descripcion");
                Registros[10] = rs.getString("id_obrasocial");
                Registros[11] = rs.getString("hora");
                Registros[12] = rs.getString("id_ordenes");
                Registros[13] = rs.getString("mail");
                Registros[14] = rs.getString("celular");

                modelPaciente.addRow(Registros);
                i++;
            }
            tablapacientes.setModel(modelPaciente);
            tablapacientes.setAutoCreateRowSorter(true);
            
            /////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(0).setMaxWidth(90);
            tablapacientes.getColumnModel().getColumn(0).setMinWidth(90);
            tablapacientes.getColumnModel().getColumn(0).setPreferredWidth(90);
            /////////////////////////////////////////////////////////////
            //tablapacientes.getColumnModel().getColumn(1).setMaxWidth(150);
            tablapacientes.getColumnModel().getColumn(1).setMinWidth(150);
            tablapacientes.getColumnModel().getColumn(1).setPreferredWidth(150);
            /////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(2).setMaxWidth(150);
            tablapacientes.getColumnModel().getColumn(2).setMinWidth(150);
            tablapacientes.getColumnModel().getColumn(2).setPreferredWidth(150);
            /////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(3).setMaxWidth(95);
            tablapacientes.getColumnModel().getColumn(3).setMinWidth(95);
            tablapacientes.getColumnModel().getColumn(3).setPreferredWidth(95);
            /////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(4).setMaxWidth(60);
            tablapacientes.getColumnModel().getColumn(4).setMinWidth(60);
            tablapacientes.getColumnModel().getColumn(4).setPreferredWidth(60);
            /////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(5).setMaxWidth(95);
            tablapacientes.getColumnModel().getColumn(5).setMinWidth(95);
            tablapacientes.getColumnModel().getColumn(5).setPreferredWidth(95);
            /////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(6).setMaxWidth(60);
            tablapacientes.getColumnModel().getColumn(6).setMinWidth(60);
            tablapacientes.getColumnModel().getColumn(6).setPreferredWidth(60);
            //////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(7).setMaxWidth(0);
            tablapacientes.getColumnModel().getColumn(7).setMinWidth(0);
            tablapacientes.getColumnModel().getColumn(7).setPreferredWidth(85);
            /////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(8).setMaxWidth(0);
            tablapacientes.getColumnModel().getColumn(8).setMinWidth(0);
            tablapacientes.getColumnModel().getColumn(8).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(9).setMaxWidth(0);
            tablapacientes.getColumnModel().getColumn(9).setMinWidth(0);
            tablapacientes.getColumnModel().getColumn(9).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(10).setMaxWidth(0);
            tablapacientes.getColumnModel().getColumn(10).setMinWidth(0);
            tablapacientes.getColumnModel().getColumn(10).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(11).setMaxWidth(0);
            tablapacientes.getColumnModel().getColumn(11).setMinWidth(0);
            tablapacientes.getColumnModel().getColumn(11).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(12).setMaxWidth(0);
            tablapacientes.getColumnModel().getColumn(12).setMinWidth(0);
            tablapacientes.getColumnModel().getColumn(12).setPreferredWidth(0);
            ////////////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(13).setMaxWidth(85);
            tablapacientes.getColumnModel().getColumn(13).setMinWidth(85);
            tablapacientes.getColumnModel().getColumn(13).setPreferredWidth(85);
            ////////////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(14).setMaxWidth(85);
            tablapacientes.getColumnModel().getColumn(14).setMinWidth(85);
            tablapacientes.getColumnModel().getColumn(14).setPreferredWidth(85);
            ///////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
            tablapacientes.getColumnModel().getColumn(1).setCellRenderer(alinearIzquierda);
            tablapacientes.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
            tablapacientes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
            tablapacientes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
            tablapacientes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
            tablapacientes.getColumnModel().getColumn(6).setCellRenderer(alinearDerecha);

            Rectangle r = tablapacientes.getCellRect(tablapacientes.getRowCount() - 1, 0, true);
            tablapacientes.scrollRectToVisible(r);
            tablapacientes.getSelectionModel().setSelectionInterval(tablapacientes.getRowCount() - 1, tablapacientes.getRowCount() - 1);

            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error 919 en la base de datos...");
        }
         */
    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    /* public javax.swing.JProgressBar getjProgressBar2() {
     return progreso3;
     }*/
    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ////para alinear columnas////
    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    void cargarorden() {
        String sSQL = "";
        String numero = "";
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();

        sSQL = "SELECT MAX(id_ordenes) AS id_ordenes FROM ordenes";

        //“SELECT MAX(id) AS id FROM tabla”
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.last();
            if (rs.getInt("id_ordenes") != 0) {
                id_orden = rs.getInt("id_ordenes") + 1;
            } else {
                id_orden = 1;
            }
            cn.close();
            ////
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    void cargarhistoriaclinica() {
        String sSQL = "";
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();

        sSQL = "SELECT MAX(idhistoria_clinica) AS idhistoria_clinica FROM historia_clinica";

        //“SELECT MAX(id) AS id FROM tabla”
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.last();
            if (rs.getInt("idhistoria_clinica") != 0) {
                id_historia_clinica = rs.getInt("idhistoria_clinica") + 1;
            } else {
                id_historia_clinica = 1;
            }
            cn.close();
            ////
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        txtprotocolo.setText(completarceros(String.valueOf(id_historia_clinica), 8));
    }

    void cargarobrasocial() {

        int i = 0;
        textAutoAcompleter2.removeAllItems();
        // Recorro y cargo las obras sociales
        while (i < contadorobrasocial) {
            textAutoAcompleter2.addItem(obrasocial[i]);
            i++;
        }
        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        textAutoAcompleter2.setMode(0);
        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter2.setCaseSensitive(false);
    }

    void recargarobrasosial() {

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT id_obrasocial,razonsocial_obrasocial,importeunidaddearancel_obrasocial,codigo_obrasocial, añonbu FROM obrasocial where estado_obrasocial=1";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                idobrasocial[contadorobrasocial] = rs.getInt("id_obrasocial");
                nombreobrasocial[contadorobrasocial] = (rs.getString("razonsocial_obrasocial"));
                arancel[contadorobrasocial] = (rs.getDouble("importeunidaddearancel_obrasocial"));
                obrasocial[contadorobrasocial] = (rs.getString("codigo_obrasocial") + " - " + rs.getString("razonsocial_obrasocial"));
                añonbu[contadorobrasocial] = (rs.getInt("añonbu"));
                contadorobrasocial++;
            }
            cn.close();
            ////
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarlocalidad() {

        TextAutoCompleter textAutoAcompleter3 = new TextAutoCompleter(txtlocalidad);
        int i = 0;
        // Recorro y cargo las obras sociales
        while (i < contadorlocalidad) {
            textAutoAcompleter3.addItem(nombrelocalidad[i]);
            i++;
        }
        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        textAutoAcompleter3.setMode(0);
        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter3.setCaseSensitive(false);
    }

    void cargarmatricula() {
        textAutoAcompletermatricula2.removeAllItems();
        int i = 0;
        // Recorro y cargo las obras sociales
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        String sSQL = "SELECT medicos.id_medicos,medicos.nombre, medicos.apellido, medicos_tienen_especialidades.matricula FROM medicos INNER JOIN medicos_tienen_especialidades ON medicos_tienen_especialidades.id_medicos = medicos.id_medicos where estado=1";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                idmedico[contadormedico] = rs.getInt(1);
                matriculamedico[contadormedico] = (rs.getInt(4));
                nombremedico[contadormedico] = (rs.getString(3) + " " + rs.getString(2));
                textAutoAcompletermatricula2.addItem(matriculamedico[i] + "-" + nombremedico[i]);
                contadormedico++;
                i++;
            }
            cn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        /*while (i < contadormedico) {
            //textAutoAcompletermatricula.addItem(matriculamedico[i]);
            textAutoAcompletermatricula2.addItem(matriculamedico[i] + "-" + nombremedico[i]);
            i++;
        }*/
        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        // textAutoAcompletermatricula.setMode(0); // infijo     
        textAutoAcompletermatricula2.setMode(0);
        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        ////  textAutoAcompletermatricula.setCaseSensitive(false); //No sensible a mayúsculas        
        textAutoAcompletermatricula2.setCaseSensitive(false);

    }

    void borrarpractica() {
        textAutoAcompleter.removeAllItems();
    }

    void borrarobrasocial() {
        textAutoAcompleter2.removeAllItems();
    }

    void borrarmedicos() {
        textAutoAcompletermatricula2.removeAllItems();
    }

    void cargarpracticaconobra() {
        practica2 = new String[500000];
        practica_fac = new String[500000];
        preciopractica2 = new String[500000];
        tiempo2 = new int[500000];
        idpractica2 = new String[500000];
        contadorj2 = 0;
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        //JOptionPane.showMessageDialog(null,"carga id en cargapraticasobra"+ id_obra_social);
        try {
            Statement St = cn.createStatement();
            ResultSet Rs = St.executeQuery("SELECT practicas.codigo_practica, practicas.determinacion_practica, practicas_nbu.id_practicas, practicas.instrucciones, practicas.precio1, practicas.precio2, practicas.precio3, practicas.precio4, practicas.tiempo_procesamiento, obrasocial_tiene_practicas_nbu.preciototal, codigo_fac_practicas_obrasocial FROM practicas_nbu INNER JOIN practicas ON practicas.id_practicas = practicas_nbu.id_practicas INNER JOIN obrasocial_tiene_practicas_nbu ON obrasocial_tiene_practicas_nbu.id_practicasnbu = practicas_nbu.id_practicasnbu WHERE obrasocial_tiene_practicas_nbu.id_obrasocial=" + id_obra_social + " and obrasocial_tiene_practicas_nbu.estado=1");
            while (Rs.next()) {
                practica2[contadorj2] = (Rs.getString(1) + " - " + Rs.getString(2));
                preciopractica2[contadorj2] = Rs.getString(10);
                preciopracticaparticular5[contadorj2] = Rs.getString(8);
                idpractica2[contadorj2] = Rs.getString(3);
                preciopracticaparticular1[contadorj2] = Rs.getString(5);
                preciopracticaparticular2[contadorj2] = Rs.getString(6);
                preciopracticaparticular3[contadorj2] = Rs.getString(7);
                instruciones2[contadorj2] = Rs.getString(4);
                tiempo2[contadorj2] = Rs.getInt(9);
                practica_fac[contadorj2] = Rs.getString("codigo_fac_practicas_obrasocial");
                contadorj2++;
            }
            ////
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }

    }

    void cargarpracticasparticular() {
        practica = new String[500000];
        preciopractica = new String[500000];
        tiempo = new int[500000];
        idpractica = new String[500000];
        contadorj = 0;
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement St = cn.createStatement();
            ResultSet Rs = St.executeQuery("SELECT practicas.codigo_practica, practicas.determinacion_practica, practicas_nbu.id_practicas, practicas.instrucciones, practicas.precio1, practicas.precio2, practicas.precio3, practicas.precio4, practicas.tiempo_procesamiento, obrasocial_tiene_practicas_nbu.preciototal FROM practicas_nbu INNER JOIN practicas ON practicas.id_practicas = practicas_nbu.id_practicas INNER JOIN obrasocial_tiene_practicas_nbu ON obrasocial_tiene_practicas_nbu.id_practicasnbu = practicas_nbu.id_practicasnbu WHERE obrasocial_tiene_practicas_nbu.id_obrasocial=" + 96);
            while (Rs.next()) {
                practica[contadorj] = (Rs.getString(1) + " - " + Rs.getString(2));
                textAutoAcompleter.addItem(practica[contadorj]);
                preciopractica[contadorj] = Rs.getString(10);
                preciopracticaparticular4[contadorj] = Rs.getString(8);
                preciopracticaparticular1[contadorj2] = Rs.getString(5);
                preciopracticaparticular2[contadorj2] = Rs.getString(6);
                preciopracticaparticular3[contadorj2] = Rs.getString(7);
                idpractica[contadorj] = Rs.getString(3);
                instruciones[contadorj] = Rs.getString(4);
                tiempo[contadorj] = Rs.getInt(9);
                contadorj++;
            }
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        textAutoAcompleter.setMode(0); // infijo
        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter.setCaseSensitive(false);
    }

    void cargarperiodo() {
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formato2 = new SimpleDateFormat("d' de 'MMMM' de 'yyyy");
        SimpleDateFormat formato3 = new SimpleDateFormat("yyyyMMdd");

        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
        fecha2 = formato2.format(currentDate);
        fechaonline = formato3.format(currentDate);

        txtfecha1.setText(fecha);
        txtfecha2.setText(fecha);
        txtfecha3.setText(hora);
        /////////////////////////////////////////////////////////
        String año2 = "", mes2 = "", dia2 = "", salida = "";

        calendar.setTime(currentDate);

        int i = 0;
        ///Mes/// 21/06/1985
        for (i = 3; i <= 4; i++) {
            mes2 = mes2 + fecha.charAt(i);
        }

        /////Año/////
        for (i = 6; i <= 9; i++) {
            año2 = año2 + fecha.charAt(i);
        }

        ////Dia////
        for (i = 8; i <= 9; i++) {
            dia2 = dia2 + fecha.charAt(i);
        }
        periodo = año2 + mes2;
        /// txtmes.setText(mes2);
        //txtmes1.setText(mes2);
        //txtaño.setText(año2);
        //txtaño1.setText(año2);

    }

    String agregartiempo(String fecha, int valor) {
        System.out.println("fecha " + fecha);
        /////////////////////////////////////////////////////////

        String salida = "", m = "", d = "";
        int i = 0, mes = 0, año = 0, dia = 0;
        ///Mes/// 21/06/1985

        mes = Integer.valueOf(fecha.substring(3, 5));
        /////Año/////
        año = Integer.valueOf(fecha.substring(6, 10));

        ////Dia////
        dia = Integer.valueOf(fecha.substring(0, 2)) + valor;
        if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
            if (dia > 31) {
                dia = dia - 31;
                mes++;
                if (mes > 12) {
                    mes = mes - 12;
                    año++;
                }
            }
        }
        if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
            if (dia > 30) {
                dia = dia - 30;
                mes++;
                if (mes > 12) {
                    mes = mes - 12;
                    año++;
                }
            }
        }
        if (mes == 2) {
            if (dia > 28) {
                dia = dia - 28;
                mes++;
                if (mes > 12) {
                    mes = mes - 12;
                    año++;
                }
            }
        }

        System.out.println("dia2 " + dia);
        System.out.println("mes " + mes);
        System.out.println("año " + año);
        System.out.println("valor " + valor);
        if (dia < 10) {
            d = "0";
        }
        if (mes < 10) {
            m = "0";
        }
        salida = d + String.valueOf(dia) + "-" + m + String.valueOf(mes) + "-" + String.valueOf(año);
        System.out.println("agregartiempo " + salida);
        return salida;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        Practicas = new javax.swing.JMenuItem();
        jPopupMenu2 = new javax.swing.JPopupMenu();
        Modificar = new javax.swing.JMenuItem();
        Anular = new javax.swing.JMenuItem();
        jPopupMenu3 = new javax.swing.JPopupMenu();
        Resultados = new javax.swing.JMenuItem();
        modifica = new javax.swing.JMenuItem();
        cargar = new javax.swing.JMenuItem();
        jPopupMenu4 = new javax.swing.JPopupMenu();
        borrar = new javax.swing.JMenuItem();
        Bioquimicos = new javax.swing.JScrollPane();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        Principal = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnobrasociales6 = new javax.swing.JButton();
        btnobrasociales7 = new javax.swing.JButton();
        btnobrasociales8 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnobrasociales9 = new javax.swing.JButton();
        Pacientes = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tablapacientes = new javax.swing.JTable();
        txtpacientes = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtfechaPaciente = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        txtfechaPaciente2 = new javax.swing.JFormattedTextField();
        btnsalir7 = new javax.swing.JButton();
        btncargarorden = new javax.swing.JButton();
        btnborrarpaciente = new javax.swing.JButton();
        jPanel16 = new javax.swing.JPanel();
        jLabel50 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        txtapellidopaciente = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        txtnombrepaciente = new javax.swing.JTextField();
        txtdnipaciente = new javax.swing.JTextField();
        jLabel52 = new javax.swing.JLabel();
        cbosexo = new javax.swing.JComboBox();
        jLabel43 = new javax.swing.JLabel();
        txtlocalidad = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        txttelfijo = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        txttelcelular = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        txtmail = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        txtfechanac = new javax.swing.JFormattedTextField();
        jLabel48 = new javax.swing.JLabel();
        txtedadpaciente = new javax.swing.JTextField();
        jLabel53 = new javax.swing.JLabel();
        txtobrasocialpaciente = new javax.swing.JTextField();
        jLabel54 = new javax.swing.JLabel();
        txtnumafiliadopaciente = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtdescripcion = new javax.swing.JTextArea();
        btnmodificarpaciente = new javax.swing.JButton();
        btnborrarpaciente2 = new javax.swing.JButton();
        btnPatologias = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        Orden = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        txtnombreafiliado1 = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        txtcama = new javax.swing.JFormattedTextField();
        txtfecha1 = new javax.swing.JFormattedTextField();
        txtmedico = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        cboservicio = new javax.swing.JComboBox();
        jLabel37 = new javax.swing.JLabel();
        txtnumorden2 = new javax.swing.JTextField();
        btnaceptar3 = new javax.swing.JButton();
        chkcoseguro = new javax.swing.JCheckBox();
        jLabel57 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtinstrucciones = new javax.swing.JTextArea();
        jLabel56 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        txtreciennacido = new javax.swing.JTextField();
        txtprecio = new javax.swing.JFormattedTextField();
        jLabel30 = new javax.swing.JLabel();
        txtprotocolo = new javax.swing.JTextField();
        txtfecha2 = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        txttipo = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel61 = new javax.swing.JLabel();
        txttotal3 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txttotal2 = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel39 = new javax.swing.JLabel();
        txttotal5 = new javax.swing.JTextField();
        jLabel55 = new javax.swing.JLabel();
        txttotal4 = new javax.swing.JTextField();
        txtpractica2 = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        tablapracticas = new javax.swing.JTable();
        jLabel40 = new javax.swing.JLabel();
        txtfecha3 = new javax.swing.JTextField();
        btngrabarimprimir = new javax.swing.JButton();
        btngrabar = new javax.swing.JButton();
        btnborrar2 = new javax.swing.JButton();
        btnobservaciones = new javax.swing.JButton();
        btnborrar4 = new javax.swing.JButton();
        btnborrar5 = new javax.swing.JButton();
        btnborrar6 = new javax.swing.JButton();
        btnborrar7 = new javax.swing.JButton();
        btnsalir11 = new javax.swing.JButton();
        btnborrar8 = new javax.swing.JButton();
        Factura = new javax.swing.JPanel();
        btncancelar3 = new javax.swing.JButton();
        btnsalir9 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        fechainicial = new javax.swing.JFormattedTextField();
        fechafinal = new javax.swing.JFormattedTextField();
        jCheckBox1 = new javax.swing.JCheckBox();
        btnFiltrar = new javax.swing.JButton();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaordenes = new javax.swing.JTable();
        jLabel25 = new javax.swing.JLabel();
        txttotal1 = new javax.swing.JTextField();
        txtordenes1 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txttotalordenes = new javax.swing.JTextField();
        progreso = new javax.swing.JProgressBar();
        jScrollPane5 = new javax.swing.JScrollPane();
        tabladetalle = new javax.swing.JTable();
        btnimprimirdjj1 = new javax.swing.JButton();
        btnimprimirdjj2 = new javax.swing.JButton();
        txtlogo = new javax.swing.JLabel();
        Utilitario = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        btnobrasociales1 = new javax.swing.JButton();
        btnsalir8 = new javax.swing.JButton();
        btnnomenclador3 = new javax.swing.JButton();
        btnobrasociales2 = new javax.swing.JButton();
        btnnomenclador4 = new javax.swing.JButton();
        btnnomenclador6 = new javax.swing.JButton();
        btnsalir14 = new javax.swing.JButton();
        btnobrasociales3 = new javax.swing.JButton();
        btnnomenclador7 = new javax.swing.JButton();
        btnobrasociales4 = new javax.swing.JButton();
        btnnomenclador8 = new javax.swing.JButton();
        btnsalir16 = new javax.swing.JButton();
        btnnomenclador2 = new javax.swing.JButton();
        btnsalir12 = new javax.swing.JButton();
        btnsalir13 = new javax.swing.JButton();
        btnnomenclador5 = new javax.swing.JButton();
        btnnomenclador1 = new javax.swing.JButton();
        btnsalir15 = new javax.swing.JButton();
        btnnomenclador9 = new javax.swing.JButton();
        btnnomenclador10 = new javax.swing.JButton();
        btnobrasociales5 = new javax.swing.JButton();
        btnobrasociales10 = new javax.swing.JButton();
        btnsalir17 = new javax.swing.JButton();
        btnsalir18 = new javax.swing.JButton();
        jMenuBar2 = new javax.swing.JMenuBar();
        jdatos = new javax.swing.JMenu();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jinformes = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jcbt = new javax.swing.JMenu();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();

        Practicas.setText("Ver Detalle");
        Practicas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PracticasActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Practicas);

        Modificar.setText("Modificar Orden");
        Modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModificarActionPerformed(evt);
            }
        });
        jPopupMenu2.add(Modificar);

        Anular.setText("Anular Orden");
        Anular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnularActionPerformed(evt);
            }
        });
        jPopupMenu2.add(Anular);

        jPopupMenu3.setComponentPopupMenu(jPopupMenu3);

        Resultados.setText("Cargar Resultados");
        Resultados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ResultadosActionPerformed(evt);
            }
        });
        jPopupMenu3.add(Resultados);

        modifica.setText("Modificar Orden");
        modifica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificaActionPerformed(evt);
            }
        });
        jPopupMenu3.add(modifica);

        cargar.setText("Cargar Paciente");
        cargar.setToolTipText("");
        cargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cargarActionPerformed(evt);
            }
        });
        jPopupMenu3.add(cargar);

        borrar.setText("Borrar Practica");
        borrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                borrarActionPerformed(evt);
            }
        });
        jPopupMenu4.add(borrar);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(925, 700));
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        Bioquimicos.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        Bioquimicos.setMinimumSize(new java.awt.Dimension(925, 700));
        Bioquimicos.setPreferredSize(new java.awt.Dimension(950, 700));

        jTabbedPane3.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jTabbedPane3.setMinimumSize(new java.awt.Dimension(920, 700));
        jTabbedPane3.setPreferredSize(new java.awt.Dimension(950, 700));
        jTabbedPane3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTabbedPane3KeyPressed(evt);
            }
        });

        Principal.setPreferredSize(new java.awt.Dimension(950, 668));
        Principal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PrincipalKeyPressed(evt);
            }
        });

        btnobrasociales6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnobrasociales6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728900 - application browser.png"))); // NOI18N
        btnobrasociales6.setMnemonic('p');
        btnobrasociales6.setText("Cargar Paciente");
        btnobrasociales6.setToolTipText("[Alt + p]");
        btnobrasociales6.setPreferredSize(new java.awt.Dimension(80, 23));
        btnobrasociales6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnobrasociales6ActionPerformed(evt);
            }
        });

        btnobrasociales7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnobrasociales7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728912 - book reading.png"))); // NOI18N
        btnobrasociales7.setMnemonic('f');
        btnobrasociales7.setText("Facturarción");
        btnobrasociales7.setToolTipText("[Alt + f]");
        btnobrasociales7.setPreferredSize(new java.awt.Dimension(80, 23));
        btnobrasociales7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnobrasociales7ActionPerformed(evt);
            }
        });

        btnobrasociales8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnobrasociales8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnobrasociales8.setMnemonic('s');
        btnobrasociales8.setText("Salir");
        btnobrasociales8.setToolTipText("[Alt + s]");
        btnobrasociales8.setPreferredSize(new java.awt.Dimension(80, 23));
        btnobrasociales8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnobrasociales8ActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/1451190150_Experiment.png"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 890, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnobrasociales9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnobrasociales9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728925 - computer monitor screen.png"))); // NOI18N
        btnobrasociales9.setMnemonic('r');
        btnobrasociales9.setText("Cargar Resultados");
        btnobrasociales9.setToolTipText("[Alt + r]");
        btnobrasociales9.setPreferredSize(new java.awt.Dimension(80, 23));
        btnobrasociales9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnobrasociales9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnobrasociales6, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(60, 60, 60)
                        .addComponent(btnobrasociales9, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(66, 66, 66)
                        .addComponent(btnobrasociales7, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 86, Short.MAX_VALUE)
                        .addComponent(btnobrasociales8, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnobrasociales6, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnobrasociales7, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnobrasociales8, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnobrasociales9, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(74, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout PrincipalLayout = new javax.swing.GroupLayout(Principal);
        Principal.setLayout(PrincipalLayout);
        PrincipalLayout.setHorizontalGroup(
            PrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        PrincipalLayout.setVerticalGroup(
            PrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane3.addTab("Principal", Principal);

        Pacientes.setMinimumSize(new java.awt.Dimension(950, 700));
        Pacientes.setPreferredSize(new java.awt.Dimension(950, 700));
        Pacientes.setRequestFocusEnabled(false);
        Pacientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PacientesKeyPressed(evt);
            }
        });

        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Modificar o Cargar resultdos de una consulta", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablapacientes.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablapacientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablapacientes.setComponentPopupMenu(jPopupMenu3);
        tablapacientes.setOpaque(false);
        tablapacientes.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tablapacientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablapacientesKeyPressed(evt);
            }
        });
        jScrollPane8.setViewportView(tablapacientes);

        txtpacientes.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        txtpacientes.setForeground(new java.awt.Color(0, 102, 204));
        txtpacientes.setToolTipText("Buscar por dni paciente o num paciente o nom paciente o num orden");
        txtpacientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpacientesActionPerformed(evt);
            }
        });
        txtpacientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtpacientesKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpacientesKeyReleased(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Desde:");

        txtfechaPaciente.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtfechaPaciente.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfechaPaciente.setToolTipText("");
        txtfechaPaciente.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtfechaPaciente.setDropMode(javax.swing.DropMode.INSERT);
        txtfechaPaciente.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        txtfechaPaciente.setNextFocusableComponent(txtfechaPaciente2);
        txtfechaPaciente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtfechaPacienteMouseClicked(evt);
            }
        });
        txtfechaPaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtfechaPacienteKeyPressed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Hasta:");

        txtfechaPaciente2.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtfechaPaciente2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfechaPaciente2.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtfechaPaciente2.setDropMode(javax.swing.DropMode.INSERT);
        txtfechaPaciente2.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        txtfechaPaciente2.setNextFocusableComponent(btnFiltrar);
        txtfechaPaciente2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtfechaPaciente2MouseClicked(evt);
            }
        });
        txtfechaPaciente2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfechaPaciente2ActionPerformed(evt);
            }
        });
        txtfechaPaciente2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtfechaPaciente2KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(txtpacientes, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtfechaPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtfechaPaciente2)
                        .addGap(221, 221, 221))
                    .addComponent(jScrollPane8))
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtfechaPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtfechaPaciente2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtpacientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnsalir7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728980 - home.png"))); // NOI18N
        btnsalir7.setMnemonic('p');
        btnsalir7.setText("Principal");
        btnsalir7.setToolTipText("[Alt + p]");
        btnsalir7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir7ActionPerformed(evt);
            }
        });

        btncargarorden.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncargarorden.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728913 - book bookmark reading.png"))); // NOI18N
        btncargarorden.setMnemonic('c');
        btncargarorden.setText("Cargar Consulta");
        btncargarorden.setToolTipText("[Alt + s]");
        btncargarorden.setNextFocusableComponent(txtmedico);
        btncargarorden.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncargarordenActionPerformed(evt);
            }
        });

        btnborrarpaciente.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnborrarpaciente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        btnborrarpaciente.setMnemonic('b');
        btnborrarpaciente.setText("Borrar");
        btnborrarpaciente.setToolTipText("Borrar Datos");
        btnborrarpaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnborrarpacienteActionPerformed(evt);
            }
        });

        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ingrese Paciente", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel50.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel50.setText("Apellido:");

        jLabel41.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel41.setText("DNI:");

        txtapellidopaciente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtapellidopaciente.setForeground(new java.awt.Color(0, 102, 204));
        txtapellidopaciente.setNextFocusableComponent(txtnombrepaciente);
        txtapellidopaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtapellidopacienteKeyPressed(evt);
            }
        });

        jLabel51.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel51.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel51.setText("Nombre:");

        txtnombrepaciente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtnombrepaciente.setForeground(new java.awt.Color(0, 102, 204));
        txtnombrepaciente.setNextFocusableComponent(cbosexo);
        txtnombrepaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombrepacienteKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnombrepacienteKeyReleased(evt);
            }
        });

        txtdnipaciente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtdnipaciente.setForeground(new java.awt.Color(0, 102, 204));
        txtdnipaciente.setNextFocusableComponent(txtapellidopaciente);
        txtdnipaciente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtdnipacienteMouseClicked(evt);
            }
        });
        txtdnipaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdnipacienteActionPerformed(evt);
            }
        });
        txtdnipaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdnipacienteKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdnipacienteKeyReleased(evt);
            }
        });

        jLabel52.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel52.setText("Sexo:");

        cbosexo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cbosexo.setForeground(new java.awt.Color(0, 102, 204));
        cbosexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Femenino", "Masculino" }));
        cbosexo.setNextFocusableComponent(txtlocalidad);
        cbosexo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbosexoKeyPressed(evt);
            }
        });

        jLabel43.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel43.setText("Localidad:");

        txtlocalidad.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtlocalidad.setForeground(new java.awt.Color(0, 102, 204));
        txtlocalidad.setNextFocusableComponent(txtdireccion);
        txtlocalidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtlocalidadActionPerformed(evt);
            }
        });
        txtlocalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtlocalidadKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlocalidadKeyReleased(evt);
            }
        });

        jLabel49.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel49.setText("Dirección:");

        txtdireccion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtdireccion.setForeground(new java.awt.Color(0, 102, 204));
        txtdireccion.setNextFocusableComponent(txttelfijo);
        txtdireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdireccionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdireccionKeyReleased(evt);
            }
        });

        jLabel46.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel46.setText("Tel Fijo:");

        txttelfijo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txttelfijo.setForeground(new java.awt.Color(0, 102, 204));
        txttelfijo.setNextFocusableComponent(txttelcelular);
        txttelfijo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttelfijoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttelfijoKeyReleased(evt);
            }
        });

        jLabel47.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel47.setText("Celular:");

        txttelcelular.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txttelcelular.setForeground(new java.awt.Color(0, 102, 204));
        txttelcelular.setNextFocusableComponent(txtmail);
        txttelcelular.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttelcelularKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttelcelularKeyReleased(evt);
            }
        });

        jLabel45.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel45.setText("Mail:");

        txtmail.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtmail.setForeground(new java.awt.Color(0, 102, 204));
        txtmail.setNextFocusableComponent(txtfechanac);
        txtmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmailKeyPressed(evt);
            }
        });

        jLabel38.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel38.setText("Fec. Nac.:");

        txtfechanac.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtfechanac.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##-##-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfechanac.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        txtfechanac.setNextFocusableComponent(txtobrasocialpaciente);
        txtfechanac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfechanacActionPerformed(evt);
            }
        });
        txtfechanac.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtfechanacKeyPressed(evt);
            }
        });

        jLabel48.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel48.setText("Edad:");

        txtedadpaciente.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        txtedadpaciente.setForeground(new java.awt.Color(0, 102, 204));
        txtedadpaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtedadpacienteKeyReleased(evt);
            }
        });

        jLabel53.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel53.setText("Obra Social:");

        txtobrasocialpaciente.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        txtobrasocialpaciente.setForeground(new java.awt.Color(0, 102, 204));
        txtobrasocialpaciente.setNextFocusableComponent(txtnumafiliadopaciente);
        txtobrasocialpaciente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtobrasocialpacienteMouseClicked(evt);
            }
        });
        txtobrasocialpaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtobrasocialpacienteActionPerformed(evt);
            }
        });
        txtobrasocialpaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtobrasocialpacienteKeyPressed(evt);
            }
        });

        jLabel54.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel54.setText("N° Afiliado:");

        txtnumafiliadopaciente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtnumafiliadopaciente.setForeground(new java.awt.Color(0, 102, 204));
        txtnumafiliadopaciente.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtnumafiliadopacienteFocusGained(evt);
            }
        });
        txtnumafiliadopaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnumafiliadopacienteActionPerformed(evt);
            }
        });
        txtnumafiliadopaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnumafiliadopacienteKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel38)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtfechanac, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(754, 754, 754))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel16Layout.createSequentialGroup()
                                .addComponent(jLabel46)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttelfijo, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttelcelular, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel45)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtmail))
                            .addGroup(jPanel16Layout.createSequentialGroup()
                                .addGap(165, 165, 165)
                                .addComponent(jLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtedadpaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel53)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtobrasocialpaciente)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel54, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtnumafiliadopaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel16Layout.createSequentialGroup()
                                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel16Layout.createSequentialGroup()
                                        .addComponent(jLabel41)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtdnipaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(20, 20, 20)
                                        .addComponent(jLabel50, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtapellidopaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel51, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel16Layout.createSequentialGroup()
                                        .addComponent(jLabel52)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cbosexo, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(20, 20, 20)
                                        .addComponent(jLabel43)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtlocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel49, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtnombrepaciente)
                                    .addGroup(jPanel16Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addContainerGap())))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel41)
                    .addComponent(txtdnipaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtapellidopaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel50)
                    .addComponent(txtnombrepaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel51))
                .addGap(18, 18, 18)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel52, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbosexo)
                    .addComponent(txtlocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel43)
                    .addComponent(jLabel49)
                    .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txttelfijo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel46)
                    .addComponent(txttelcelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel47)
                    .addComponent(jLabel45)
                    .addComponent(txtmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtfechanac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtedadpaciente)
                    .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtobrasocialpaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel54)
                        .addComponent(txtnumafiliadopaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel53, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15))
        );

        txtdescripcion.setEditable(false);
        txtdescripcion.setColumns(20);
        txtdescripcion.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        txtdescripcion.setForeground(new java.awt.Color(51, 51, 51));
        txtdescripcion.setRows(2);
        txtdescripcion.setBorder(null);
        txtdescripcion.setOpaque(false);
        jScrollPane1.setViewportView(txtdescripcion);

        btnmodificarpaciente.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodificarpaciente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        btnmodificarpaciente.setMnemonic('m');
        btnmodificarpaciente.setText("Modificar");
        btnmodificarpaciente.setToolTipText("Modifcar Paciente");
        btnmodificarpaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarpacienteActionPerformed(evt);
            }
        });

        btnborrarpaciente2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnborrarpaciente2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728952 - magnifier zoom.png"))); // NOI18N
        btnborrarpaciente2.setMnemonic('b');
        btnborrarpaciente2.setText("Buscar");
        btnborrarpaciente2.setToolTipText("Buscar Personas");
        btnborrarpaciente2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnborrarpaciente2ActionPerformed(evt);
            }
        });

        btnPatologias.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnPatologias.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728987 - experiment.png"))); // NOI18N
        btnPatologias.setText("Patologias");
        btnPatologias.setToolTipText("Patologias Preexistentes");
        btnPatologias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPatologiasActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728966 - edit pen pencil writing.png"))); // NOI18N
        jButton1.setText("Sin DNI");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PacientesLayout = new javax.swing.GroupLayout(Pacientes);
        Pacientes.setLayout(PacientesLayout);
        PacientesLayout.setHorizontalGroup(
            PacientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PacientesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PacientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(PacientesLayout.createSequentialGroup()
                        .addComponent(btncargarorden, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnborrarpaciente2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPatologias, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnmodificarpaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnborrarpaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(PacientesLayout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addGap(44, 44, 44)
                        .addComponent(btnsalir7)))
                .addGap(23, 23, 23))
        );
        PacientesLayout.setVerticalGroup(
            PacientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PacientesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PacientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(PacientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btncargarorden)
                        .addComponent(btnborrarpaciente2))
                    .addGroup(PacientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnborrarpaciente)
                        .addComponent(btnmodificarpaciente)
                        .addComponent(btnPatologias))
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(PacientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(PacientesLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(btnsalir7)))
                .addContainerGap(110, Short.MAX_VALUE))
        );

        jTabbedPane3.addTab("Pacientes", Pacientes);

        Orden.setMinimumSize(new java.awt.Dimension(920, 700));
        Orden.setNextFocusableComponent(txtmedico);
        Orden.setPreferredSize(new java.awt.Dimension(950, 700));
        Orden.setRequestFocusEnabled(false);
        Orden.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                OrdenKeyPressed(evt);
            }
        });

        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ingrese Consulta", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        jPanel13.setEnabled(false);
        jPanel13.setNextFocusableComponent(txtmedico);

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("Motivo:");

        jLabel28.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 51, 51));
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel28.setText("Paciente:");

        txtnombreafiliado1.setEditable(false);
        txtnombreafiliado1.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        txtnombreafiliado1.setForeground(new java.awt.Color(0, 102, 204));
        txtnombreafiliado1.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtnombreafiliado1.setBorder(null);
        txtnombreafiliado1.setOpaque(false);
        txtnombreafiliado1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreafiliado1KeyPressed(evt);
            }
        });

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel31.setText("Cama:");
        jLabel31.setEnabled(false);

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel32.setText("Fecha de Orden:");

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(51, 51, 51));
        jLabel33.setText("Cargar Practicas:");

        jLabel34.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel34.setText("Servicio:");

        txtcama.setForeground(new java.awt.Color(0, 102, 204));
        txtcama.setEnabled(false);
        txtcama.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtcama.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcamaKeyPressed(evt);
            }
        });

        txtfecha1.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtfecha1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##-##-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtfecha1.setToolTipText("");
        txtfecha1.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtfecha1.setDropMode(javax.swing.DropMode.INSERT);
        txtfecha1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtfecha1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtfecha1MouseClicked(evt);
            }
        });
        txtfecha1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfecha1ActionPerformed(evt);
            }
        });
        txtfecha1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtfecha1KeyPressed(evt);
            }
        });

        txtmedico.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtmedico.setForeground(new java.awt.Color(0, 102, 204));
        txtmedico.setToolTipText("");
        txtmedico.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtmedico.setDropMode(javax.swing.DropMode.INSERT);
        txtmedico.setNextFocusableComponent(cboservicio);
        txtmedico.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtmedicoFocusLost(evt);
            }
        });
        txtmedico.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtmedicoMouseClicked(evt);
            }
        });
        txtmedico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmedicoActionPerformed(evt);
            }
        });
        txtmedico.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmedicoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmedicoKeyReleased(evt);
            }
        });

        jLabel35.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel35.setText("Medico:");

        cboservicio.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cboservicio.setForeground(new java.awt.Color(0, 102, 204));
        cboservicio.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ambulatorio", "Domicilio", "Internación" }));
        cboservicio.setNextFocusableComponent(txtcama);
        cboservicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboservicioActionPerformed(evt);
            }
        });
        cboservicio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cboservicioKeyPressed(evt);
            }
        });

        jLabel37.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel37.setText("Numero de Orden:");

        txtnumorden2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtnumorden2.setForeground(new java.awt.Color(0, 102, 204));
        txtnumorden2.setToolTipText("Buscar por dni paciente o num paciente o nom paciente o num orden");
        txtnumorden2.setDropMode(javax.swing.DropMode.INSERT);
        txtnumorden2.setNextFocusableComponent(txtfecha1);
        txtnumorden2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtnumorden2MouseClicked(evt);
            }
        });
        txtnumorden2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnumorden2ActionPerformed(evt);
            }
        });
        txtnumorden2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnumorden2KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnumorden2KeyReleased(evt);
            }
        });

        btnaceptar3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaceptar3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728928 - document paper.png"))); // NOI18N
        btnaceptar3.setMnemonic('b');
        btnaceptar3.setText("Siguiente Bono");
        btnaceptar3.setToolTipText("[Alt + b o [/]");
        btnaceptar3.setNextFocusableComponent(btngrabar);
        btnaceptar3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptar3ActionPerformed(evt);
            }
        });

        chkcoseguro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chkcoseguro.setText("Coseguro");
        chkcoseguro.setNextFocusableComponent(txtprecio);
        chkcoseguro.setOpaque(false);
        chkcoseguro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkcoseguroActionPerformed(evt);
            }
        });
        chkcoseguro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                chkcoseguroKeyPressed(evt);
            }
        });

        jLabel57.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel57.setText("Instrucciones:");

        txtinstrucciones.setEditable(false);
        txtinstrucciones.setColumns(20);
        txtinstrucciones.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txtinstrucciones.setForeground(new java.awt.Color(204, 0, 0));
        txtinstrucciones.setRows(1);
        txtinstrucciones.setOpaque(false);
        jScrollPane2.setViewportView(txtinstrucciones);

        jLabel56.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel56.setText("Precio Cos.:");
        jLabel56.setEnabled(false);

        jLabel58.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel58.setEnabled(false);

        txtreciennacido.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtreciennacido.setForeground(new java.awt.Color(0, 102, 204));
        txtreciennacido.setToolTipText("");
        txtreciennacido.setDropMode(javax.swing.DropMode.INSERT);
        txtreciennacido.setEnabled(false);
        txtreciennacido.setNextFocusableComponent(txtpractica2);
        txtreciennacido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtreciennacidoActionPerformed(evt);
            }
        });
        txtreciennacido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtreciennacidoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtreciennacidoKeyReleased(evt);
            }
        });

        txtprecio.setForeground(new java.awt.Color(0, 102, 204));
        txtprecio.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        txtprecio.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtprecio.setDropMode(javax.swing.DropMode.INSERT);
        txtprecio.setEnabled(false);
        txtprecio.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtprecio.setNextFocusableComponent(txtreciennacido);
        txtprecio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtprecioKeyPressed(evt);
            }
        });

        jLabel30.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(255, 51, 51));
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel30.setText("N° Protocolo:");

        txtprotocolo.setEditable(false);
        txtprotocolo.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        txtprotocolo.setForeground(new java.awt.Color(0, 102, 204));
        txtprotocolo.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtprotocolo.setToolTipText("");
        txtprotocolo.setBorder(null);
        txtprotocolo.setDropMode(javax.swing.DropMode.INSERT);
        txtprotocolo.setOpaque(false);
        txtprotocolo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtprotocoloActionPerformed(evt);
            }
        });
        txtprotocolo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtprotocoloKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtprotocoloKeyReleased(evt);
            }
        });

        txtfecha2.setEditable(false);
        txtfecha2.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        txtfecha2.setForeground(new java.awt.Color(0, 102, 204));
        txtfecha2.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtfecha2.setToolTipText("");
        txtfecha2.setBorder(null);
        txtfecha2.setDropMode(javax.swing.DropMode.INSERT);
        txtfecha2.setOpaque(false);
        txtfecha2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfecha2ActionPerformed(evt);
            }
        });
        txtfecha2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtfecha2KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfecha2KeyReleased(evt);
            }
        });

        jLabel36.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(255, 51, 51));
        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel36.setText("Hora:");

        txttipo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txttipo.setForeground(new java.awt.Color(0, 102, 204));
        txttipo.setNextFocusableComponent(chkcoseguro);
        txttipo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttipoKeyPressed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Totales a cobrar por:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel61.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel61.setText("Obra S. :   $");

        txttotal3.setEditable(false);
        txttotal3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txttotal3.setForeground(new java.awt.Color(0, 102, 204));
        txttotal3.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txttotal3.setBorder(null);
        txttotal3.setOpaque(false);
        txttotal3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotal3ActionPerformed(evt);
            }
        });

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel29.setText("Particular: $");

        txttotal2.setEditable(false);
        txttotal2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txttotal2.setForeground(new java.awt.Color(0, 102, 204));
        txttotal2.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txttotal2.setBorder(null);
        txttotal2.setOpaque(false);
        txttotal2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotal2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel61, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txttotal2, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttotal3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel61)
                    .addComponent(txttotal3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(txttotal2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, ".", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel39.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(255, 51, 51));
        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel39.setText("Seña: $");

        txttotal5.setEditable(false);
        txttotal5.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txttotal5.setForeground(new java.awt.Color(255, 51, 51));
        txttotal5.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txttotal5.setText("0");
        txttotal5.setBorder(null);
        txttotal5.setOpaque(false);
        txttotal5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotal5ActionPerformed(evt);
            }
        });

        jLabel55.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel55.setForeground(new java.awt.Color(0, 102, 204));
        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel55.setText("Total: $");

        txttotal4.setEditable(false);
        txttotal4.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txttotal4.setForeground(new java.awt.Color(0, 102, 204));
        txttotal4.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txttotal4.setBorder(null);
        txttotal4.setOpaque(false);
        txttotal4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotal4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel39, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel55, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txttotal4, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                    .addComponent(txttotal5))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(txttotal5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel55)
                    .addComponent(txttotal4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtpractica2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtpractica2.setForeground(new java.awt.Color(0, 102, 204));
        txtpractica2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpractica2ActionPerformed(evt);
            }
        });
        txtpractica2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtpractica2KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpractica2KeyReleased(evt);
            }
        });

        tablapracticas.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablapracticas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nº Practicas", "Nº Orden", "Código", "Descripción", "Precio O.S.", "Precio Particular", "id_practicas", "Precio Costo", "Factura", "F. Trabajo", "Tiempo", "Cod_Fac", "Id_orden"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, false, false, false, true, false, false, true, true, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablapracticas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablapracticasKeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(tablapracticas);

        jLabel40.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(255, 51, 51));
        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel40.setText("Fecha:");

        txtfecha3.setEditable(false);
        txtfecha3.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        txtfecha3.setForeground(new java.awt.Color(0, 102, 204));
        txtfecha3.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtfecha3.setToolTipText("");
        txtfecha3.setBorder(null);
        txtfecha3.setDropMode(javax.swing.DropMode.INSERT);
        txtfecha3.setOpaque(false);
        txtfecha3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfecha3ActionPerformed(evt);
            }
        });
        txtfecha3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtfecha3KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfecha3KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator5)
                            .addComponent(jSeparator6)
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addComponent(jLabel57)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addGap(33, 33, 33)
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(jPanel13Layout.createSequentialGroup()
                                        .addComponent(chkcoseguro)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel56)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtprecio, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(33, 33, 33)
                                        .addComponent(jLabel58, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtreciennacido, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel13Layout.createSequentialGroup()
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel13Layout.createSequentialGroup()
                                                .addComponent(jLabel35)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(txtmedico, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel13Layout.createSequentialGroup()
                                                .addComponent(jLabel37)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(txtnumorden2, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(22, 22, 22)
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel13Layout.createSequentialGroup()
                                                .addComponent(jLabel32)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtfecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel13Layout.createSequentialGroup()
                                                .addComponent(jLabel34)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(cboservicio, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txtcama)
                                            .addComponent(txttipo, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addComponent(jLabel30)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtprotocolo, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel40)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtfecha2, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel36)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtfecha3, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtnombreafiliado1)))
                        .addContainerGap())
                    .addComponent(jScrollPane4)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel33)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtpractica2, javax.swing.GroupLayout.PREFERRED_SIZE, 539, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnaceptar3)
                        .addGap(44, 44, 44))))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(txtprotocolo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36)
                    .addComponent(txtfecha2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40)
                    .addComponent(txtfecha3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtnombreafiliado1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtmedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34)
                    .addComponent(jLabel31)
                    .addComponent(txtcama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboservicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel32)
                        .addComponent(txtfecha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txttipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel27))
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel37)
                        .addComponent(txtnumorden2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtreciennacido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(chkcoseguro)
                        .addComponent(jLabel56)
                        .addComponent(txtprecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel58, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnaceptar3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33)
                    .addComponent(txtpractica2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel57)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btngrabarimprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btngrabarimprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728959 - announcement flyer news newspaper .png"))); // NOI18N
        btngrabarimprimir.setMnemonic('i');
        btngrabarimprimir.setText("Grabar e Imprimir");
        btngrabarimprimir.setToolTipText("[Alt + i] o [-]");
        btngrabarimprimir.setPreferredSize(new java.awt.Dimension(80, 23));
        btngrabarimprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btngrabarimprimirActionPerformed(evt);
            }
        });

        btngrabar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btngrabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728991 - diskette save.png"))); // NOI18N
        btngrabar.setMnemonic('g');
        btngrabar.setText("Grabar");
        btngrabar.setToolTipText("[Alt + g] o [+]");
        btngrabar.setPreferredSize(new java.awt.Dimension(80, 23));
        btngrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btngrabarActionPerformed(evt);
            }
        });

        btnborrar2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnborrar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        btnborrar2.setMnemonic('b');
        btnborrar2.setText("Cancelar");
        btnborrar2.setToolTipText("[Alt + p] o [.]");
        btnborrar2.setPreferredSize(new java.awt.Dimension(80, 23));
        btnborrar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnborrar2ActionPerformed(evt);
            }
        });

        btnobservaciones.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnobservaciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728928 - document paper.png"))); // NOI18N
        btnobservaciones.setMnemonic('i');
        btnobservaciones.setText("Observaciones");
        btnobservaciones.setToolTipText("[Alt + r] o [*]");
        btnobservaciones.setPreferredSize(new java.awt.Dimension(80, 23));
        btnobservaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnobservacionesActionPerformed(evt);
            }
        });

        btnborrar4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnborrar4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728927 - copy document.png"))); // NOI18N
        btnborrar4.setMnemonic('i');
        btnborrar4.setText("Imprimir Listado");
        btnborrar4.setToolTipText("[Alt + l] o [/]");
        btnborrar4.setEnabled(false);
        btnborrar4.setPreferredSize(new java.awt.Dimension(80, 23));
        btnborrar4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnborrar4ActionPerformed(evt);
            }
        });

        btnborrar5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnborrar5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728958 - economy finance money shopping.png"))); // NOI18N
        btnborrar5.setMnemonic('i');
        btnborrar5.setText("Señas");
        btnborrar5.setToolTipText("[Alt + r] o [*]");
        btnborrar5.setPreferredSize(new java.awt.Dimension(80, 23));
        btnborrar5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnborrar5ActionPerformed(evt);
            }
        });

        btnborrar6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnborrar6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728912 - book reading.png"))); // NOI18N
        btnborrar6.setMnemonic('i');
        btnborrar6.setText("Comprob. de Asist.");
        btnborrar6.setToolTipText("[Alt + r] o [*]");
        btnborrar6.setEnabled(false);
        btnborrar6.setPreferredSize(new java.awt.Dimension(80, 23));
        btnborrar6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnborrar6ActionPerformed(evt);
            }
        });

        btnborrar7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnborrar7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728995 - clipboard.png"))); // NOI18N
        btnborrar7.setMnemonic('i');
        btnborrar7.setText("Presupuesto");
        btnborrar7.setToolTipText("[Alt + r] o [*]");
        btnborrar7.setEnabled(false);
        btnborrar7.setPreferredSize(new java.awt.Dimension(80, 23));
        btnborrar7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnborrar7ActionPerformed(evt);
            }
        });

        btnsalir11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728980 - home.png"))); // NOI18N
        btnsalir11.setMnemonic('p');
        btnsalir11.setText("Principal");
        btnsalir11.setToolTipText("[Alt + p]");
        btnsalir11.setPreferredSize(new java.awt.Dimension(80, 23));
        btnsalir11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir11ActionPerformed(evt);
            }
        });

        btnborrar8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnborrar8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728916 - calendar date.png"))); // NOI18N
        btnborrar8.setMnemonic('i');
        btnborrar8.setText("Resultados");
        btnborrar8.setToolTipText("[Alt + r] o [*]");
        btnborrar8.setEnabled(false);
        btnborrar8.setPreferredSize(new java.awt.Dimension(80, 23));
        btnborrar8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnborrar8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrdenLayout = new javax.swing.GroupLayout(Orden);
        Orden.setLayout(OrdenLayout);
        OrdenLayout.setHorizontalGroup(
            OrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrdenLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(OrdenLayout.createSequentialGroup()
                        .addComponent(btngrabarimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btngrabar, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnobservaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnborrar7, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnborrar8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(OrdenLayout.createSequentialGroup()
                        .addComponent(btnborrar4, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnborrar6, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnborrar5, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnborrar2, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnsalir11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(20, 20, 20))
        );
        OrdenLayout.setVerticalGroup(
            OrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrdenLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(OrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btnobservaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnborrar7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnborrar8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(btngrabar, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btngrabarimprimir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnborrar5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnborrar6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnborrar4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnborrar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnsalir11, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(101, Short.MAX_VALUE))
        );

        jTabbedPane3.addTab("Consulta", Orden);

        Factura.setMinimumSize(new java.awt.Dimension(920, 700));
        Factura.setPreferredSize(new java.awt.Dimension(950, 700));
        Factura.setRequestFocusEnabled(false);
        Factura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                FacturaKeyPressed(evt);
            }
        });

        btncancelar3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        btncancelar3.setMnemonic('c');
        btncancelar3.setText("Cancelar");
        btncancelar3.setToolTipText("[Alt + c]");
        btncancelar3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelar3ActionPerformed(evt);
            }
        });

        btnsalir9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728980 - home.png"))); // NOI18N
        btnsalir9.setMnemonic('p');
        btnsalir9.setText("Principal");
        btnsalir9.setToolTipText("[Alt + P]");
        btnsalir9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir9ActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtrar por Fecha", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 102))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Desde:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Hasta:");

        fechainicial.setForeground(new java.awt.Color(0, 102, 204));
        try {
            fechainicial.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        fechainicial.setToolTipText("");
        fechainicial.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        fechainicial.setDropMode(javax.swing.DropMode.INSERT);
        fechainicial.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        fechainicial.setNextFocusableComponent(fechafinal);
        fechainicial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fechainicialMouseClicked(evt);
            }
        });
        fechainicial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechainicialKeyPressed(evt);
            }
        });

        fechafinal.setForeground(new java.awt.Color(0, 102, 204));
        try {
            fechafinal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        fechafinal.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        fechafinal.setDropMode(javax.swing.DropMode.INSERT);
        fechafinal.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        fechafinal.setNextFocusableComponent(btnFiltrar);
        fechafinal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fechafinalMouseClicked(evt);
            }
        });
        fechafinal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fechafinalActionPerformed(evt);
            }
        });
        fechafinal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechafinalKeyPressed(evt);
            }
        });

        jCheckBox1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jCheckBox1.setText("Por Obra social");
        jCheckBox1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jCheckBox1.setNextFocusableComponent(fechainicial);
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        btnFiltrar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnFiltrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728930 - down download.png"))); // NOI18N
        btnFiltrar.setText("Filtrar");
        btnFiltrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFiltrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jCheckBox1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(fechainicial, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(fechafinal, javax.swing.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addComponent(btnFiltrar, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jCheckBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(fechainicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(fechafinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnFiltrar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Seleccionar Ordenes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablaordenes.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaordenes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Orden", "Periodo", "Obra Social", "Afiliado", "N° Orden", "Fecha Orden ", "Total", "Selección", "num afiliado", "Dni Afiliado", "id_obrasocial", "Estado", "Historia Clinica"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true, false, false, true, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaordenes.setComponentPopupMenu(jPopupMenu2);
        tablaordenes.setOpaque(false);
        tablaordenes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaordenesMouseClicked(evt);
            }
        });
        tablaordenes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaordenesKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(tablaordenes);

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel25.setText("Total: $");

        txttotal1.setEditable(false);
        txttotal1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txttotal1.setForeground(new java.awt.Color(0, 102, 204));
        txttotal1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txttotal1.setBorder(null);
        txttotal1.setOpaque(false);
        txttotal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotal1ActionPerformed(evt);
            }
        });

        txtordenes1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtordenes1.setForeground(new java.awt.Color(0, 102, 102));
        txtordenes1.setToolTipText("Buscar por dni paciente o num paciente o nom paciente o num orden");
        txtordenes1.setEnabled(false);
        txtordenes1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtordenes1ActionPerformed(evt);
            }
        });
        txtordenes1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtordenes1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtordenes1KeyReleased(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel26.setText("Total Ordenes a Enviar:");

        txttotalordenes.setEditable(false);
        txttotalordenes.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txttotalordenes.setForeground(new java.awt.Color(0, 102, 204));
        txttotalordenes.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txttotalordenes.setBorder(null);
        txttotalordenes.setOpaque(false);
        txttotalordenes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotalordenesActionPerformed(evt);
            }
        });

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setString("");

        tabladetalle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tabladetalle.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabladetalle.setOpaque(false);
        jScrollPane5.setViewportView(tabladetalle);

        btnimprimirdjj1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimirdjj1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728959 - announcement flyer news newspaper .png"))); // NOI18N
        btnimprimirdjj1.setMnemonic('i');
        btnimprimirdjj1.setText("Imprimir Resumen");
        btnimprimirdjj1.setToolTipText("[Alt + i]");
        btnimprimirdjj1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimirdjj1ActionPerformed(evt);
            }
        });

        btnimprimirdjj2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimirdjj2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728902 - attach office.png"))); // NOI18N
        btnimprimirdjj2.setMnemonic('i');
        btnimprimirdjj2.setText("Exportar a Excel");
        btnimprimirdjj2.setToolTipText("[Alt + i]");
        btnimprimirdjj2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimirdjj2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(txtordenes1)
                        .addGap(470, 470, 470))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel12Layout.createSequentialGroup()
                                .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel26)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttotalordenes, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 758, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel12Layout.createSequentialGroup()
                                .addComponent(jLabel25)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(btnimprimirdjj1)
                        .addGap(18, 18, 18)
                        .addComponent(btnimprimirdjj2)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtordenes1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txttotal1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel25))
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel26)
                            .addComponent(txttotalordenes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(progreso, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnimprimirdjj1)
                    .addComponent(btnimprimirdjj2))
                .addContainerGap())
        );

        javax.swing.GroupLayout FacturaLayout = new javax.swing.GroupLayout(Factura);
        Factura.setLayout(FacturaLayout);
        FacturaLayout.setHorizontalGroup(
            FacturaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FacturaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FacturaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FacturaLayout.createSequentialGroup()
                        .addComponent(txtlogo, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(109, 109, 109)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 237, Short.MAX_VALUE))
                    .addGroup(FacturaLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btncancelar3)
                        .addGap(18, 18, 18)
                        .addComponent(btnsalir9))
                    .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        FacturaLayout.setVerticalGroup(
            FacturaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FacturaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FacturaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FacturaLayout.createSequentialGroup()
                        .addComponent(txtlogo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FacturaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnsalir9, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncancelar3))
                .addGap(108, 108, 108))
        );

        jTabbedPane3.addTab("Facturación", Factura);

        Utilitario.setMinimumSize(new java.awt.Dimension(920, 700));
        Utilitario.setPreferredSize(new java.awt.Dimension(950, 700));
        Utilitario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                UtilitarioKeyPressed(evt);
            }
        });

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Novedades", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        btnobrasociales1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnobrasociales1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728922 - apartment building city.png"))); // NOI18N
        btnobrasociales1.setMnemonic('o');
        btnobrasociales1.setText("Obras Sociales");
        btnobrasociales1.setToolTipText("[Alt + o]");
        btnobrasociales1.setPreferredSize(new java.awt.Dimension(80, 23));
        btnobrasociales1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnobrasociales1ActionPerformed(evt);
            }
        });

        btnsalir8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728997 - announcement.png"))); // NOI18N
        btnsalir8.setMnemonic('p');
        btnsalir8.setText("Secciones");
        btnsalir8.setToolTipText("[Alt + s]");
        btnsalir8.setPreferredSize(new java.awt.Dimension(80, 23));
        btnsalir8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir8ActionPerformed(evt);
            }
        });

        btnnomenclador3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728987 - experiment.png"))); // NOI18N
        btnnomenclador3.setMnemonic('n');
        btnnomenclador3.setText("Bioquimicos");
        btnnomenclador3.setToolTipText("[Alt + n]");
        btnnomenclador3.setPreferredSize(new java.awt.Dimension(80, 23));
        btnnomenclador3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador3ActionPerformed(evt);
            }
        });
        btnnomenclador3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador3KeyPressed(evt);
            }
        });

        btnobrasociales2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnobrasociales2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728940 - daisy flower.png"))); // NOI18N
        btnobrasociales2.setMnemonic('o');
        btnobrasociales2.setText("Pacientes");
        btnobrasociales2.setToolTipText("[Alt + o]");
        btnobrasociales2.setPreferredSize(new java.awt.Dimension(80, 23));
        btnobrasociales2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnobrasociales2ActionPerformed(evt);
            }
        });

        btnnomenclador4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728929 - alcohol drink glass wine.png"))); // NOI18N
        btnnomenclador4.setMnemonic('n');
        btnnomenclador4.setText("Analisis");
        btnnomenclador4.setToolTipText("[Alt + n]");
        btnnomenclador4.setPreferredSize(new java.awt.Dimension(80, 23));
        btnnomenclador4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador4ActionPerformed(evt);
            }
        });
        btnnomenclador4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador4KeyPressed(evt);
            }
        });

        btnnomenclador6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728986 - fire.png"))); // NOI18N
        btnnomenclador6.setMnemonic('n');
        btnnomenclador6.setText("Ayuda");
        btnnomenclador6.setToolTipText("[Alt + n]");
        btnnomenclador6.setPreferredSize(new java.awt.Dimension(80, 23));
        btnnomenclador6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador6ActionPerformed(evt);
            }
        });
        btnnomenclador6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador6KeyPressed(evt);
            }
        });

        btnsalir14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728983 - folder.png"))); // NOI18N
        btnsalir14.setMnemonic('s');
        btnsalir14.setText("Ir a Informes");
        btnsalir14.setToolTipText("[Alt + s]");
        btnsalir14.setPreferredSize(new java.awt.Dimension(80, 23));
        btnsalir14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir14ActionPerformed(evt);
            }
        });

        btnobrasociales3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnobrasociales3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728943 - glasses reading.png"))); // NOI18N
        btnobrasociales3.setMnemonic('o');
        btnobrasociales3.setText("Medicos");
        btnobrasociales3.setToolTipText("[Alt + o]");
        btnobrasociales3.setPreferredSize(new java.awt.Dimension(80, 23));
        btnobrasociales3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnobrasociales3ActionPerformed(evt);
            }
        });

        btnnomenclador7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728912 - book reading.png"))); // NOI18N
        btnnomenclador7.setMnemonic('n');
        btnnomenclador7.setText("Historias Clinicas");
        btnnomenclador7.setToolTipText("[Alt + n]");
        btnnomenclador7.setPreferredSize(new java.awt.Dimension(80, 23));
        btnnomenclador7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador7ActionPerformed(evt);
            }
        });
        btnnomenclador7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador7KeyPressed(evt);
            }
        });

        btnobrasociales4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnobrasociales4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728960 - instrument music piano.png"))); // NOI18N
        btnobrasociales4.setText("Ir a Descargas");
        btnobrasociales4.setToolTipText("[Alt + i]");
        btnobrasociales4.setPreferredSize(new java.awt.Dimension(80, 23));
        btnobrasociales4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnobrasociales4ActionPerformed(evt);
            }
        });

        btnnomenclador8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728930 - down download.png"))); // NOI18N
        btnnomenclador8.setMnemonic('n');
        btnnomenclador8.setText("Descargar DDJJ");
        btnnomenclador8.setToolTipText("[Alt + n]");
        btnnomenclador8.setPreferredSize(new java.awt.Dimension(80, 23));
        btnnomenclador8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador8ActionPerformed(evt);
            }
        });
        btnnomenclador8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador8KeyPressed(evt);
            }
        });

        btnsalir16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728924 - clock time.png"))); // NOI18N
        btnsalir16.setMnemonic('p');
        btnsalir16.setText("Actualizar BD");
        btnsalir16.setToolTipText("[Alt + s]");
        btnsalir16.setPreferredSize(new java.awt.Dimension(80, 23));
        btnsalir16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir16ActionPerformed(evt);
            }
        });

        btnnomenclador2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728963 - flying holiday plane tourism trave.png"))); // NOI18N
        btnnomenclador2.setMnemonic('n');
        btnnomenclador2.setText("Exportar Datos");
        btnnomenclador2.setToolTipText("[Alt + n]");
        btnnomenclador2.setPreferredSize(new java.awt.Dimension(80, 23));
        btnnomenclador2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador2ActionPerformed(evt);
            }
        });
        btnnomenclador2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador2KeyPressed(evt);
            }
        });

        btnsalir12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728905 - bar barchart chart graph stats.png"))); // NOI18N
        btnsalir12.setMnemonic('s');
        btnsalir12.setText("Historico");
        btnsalir12.setToolTipText("[Alt + s]");
        btnsalir12.setPreferredSize(new java.awt.Dimension(80, 23));
        btnsalir12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir12ActionPerformed(evt);
            }
        });

        btnsalir13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728900 - application browser.png"))); // NOI18N
        btnsalir13.setMnemonic('p');
        btnsalir13.setText("Cargar Resultados");
        btnsalir13.setToolTipText("[Alt + s]");
        btnsalir13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir13ActionPerformed(evt);
            }
        });

        btnnomenclador5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728996 - bin.png"))); // NOI18N
        btnnomenclador5.setMnemonic('n');
        btnnomenclador5.setText("Materiales");
        btnnomenclador5.setToolTipText("[Alt + n]");
        btnnomenclador5.setPreferredSize(new java.awt.Dimension(80, 23));
        btnnomenclador5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador5ActionPerformed(evt);
            }
        });
        btnnomenclador5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador5KeyPressed(evt);
            }
        });

        btnnomenclador1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728913 - book bookmark reading.png"))); // NOI18N
        btnnomenclador1.setMnemonic('n');
        btnnomenclador1.setText("Nomenclador");
        btnnomenclador1.setToolTipText("[Alt + n]");
        btnnomenclador1.setPreferredSize(new java.awt.Dimension(80, 23));
        btnnomenclador1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador1ActionPerformed(evt);
            }
        });
        btnnomenclador1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador1KeyPressed(evt);
            }
        });

        btnsalir15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728901 - archive office.png"))); // NOI18N
        btnsalir15.setMnemonic('p');
        btnsalir15.setText("Practicas");
        btnsalir15.setToolTipText("[Alt + s]");
        btnsalir15.setPreferredSize(new java.awt.Dimension(80, 23));
        btnsalir15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir15ActionPerformed(evt);
            }
        });

        btnnomenclador9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728977 - key.png"))); // NOI18N
        btnnomenclador9.setMnemonic('n');
        btnnomenclador9.setText("Usuarios");
        btnnomenclador9.setToolTipText("[Alt + n]");
        btnnomenclador9.setPreferredSize(new java.awt.Dimension(80, 23));
        btnnomenclador9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador9ActionPerformed(evt);
            }
        });
        btnnomenclador9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador9KeyPressed(evt);
            }
        });

        btnnomenclador10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        btnnomenclador10.setMnemonic('n');
        btnnomenclador10.setText("Actualizar Precio");
        btnnomenclador10.setToolTipText("[Alt + n]");
        btnnomenclador10.setPreferredSize(new java.awt.Dimension(80, 23));
        btnnomenclador10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador10ActionPerformed(evt);
            }
        });
        btnnomenclador10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador10KeyPressed(evt);
            }
        });

        btnobrasociales5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnobrasociales5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728995 - clipboard.png"))); // NOI18N
        btnobrasociales5.setMnemonic('i');
        btnobrasociales5.setText("Informe Diario");
        btnobrasociales5.setToolTipText("[Alt + o]");
        btnobrasociales5.setPreferredSize(new java.awt.Dimension(80, 23));
        btnobrasociales5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnobrasociales5ActionPerformed(evt);
            }
        });

        btnobrasociales10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnobrasociales10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728995 - clipboard.png"))); // NOI18N
        btnobrasociales10.setMnemonic('i');
        btnobrasociales10.setText("Cierre de Caja");
        btnobrasociales10.setToolTipText("[Alt + o]");
        btnobrasociales10.setPreferredSize(new java.awt.Dimension(80, 23));

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnobrasociales5, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                    .addComponent(btnnomenclador4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                    .addComponent(btnobrasociales3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                    .addComponent(btnnomenclador8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                    .addComponent(btnsalir15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                    .addComponent(btnnomenclador7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(89, 89, 89)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnnomenclador6, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                    .addComponent(btnnomenclador3, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                    .addComponent(btnnomenclador2, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                    .addComponent(btnnomenclador1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnsalir8, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                    .addComponent(btnobrasociales10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(76, 76, 76)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnobrasociales1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnsalir13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnsalir12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnsalir16, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnnomenclador9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(63, 63, 63)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnnomenclador10, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
                    .addComponent(btnobrasociales2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnnomenclador5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnobrasociales4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnsalir14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnnomenclador4, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnnomenclador6, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir16, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir14, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnobrasociales4, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir13, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnnomenclador3, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnnomenclador7, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnnomenclador5, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir12, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnnomenclador2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnnomenclador8, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(48, 48, 48)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnobrasociales2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnobrasociales1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnobrasociales3, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnnomenclador1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnnomenclador10, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnnomenclador9, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir15, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir8, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnobrasociales5, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                    .addComponent(btnobrasociales10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(19, 19, 19))
        );

        btnsalir17.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir17.setMnemonic('s');
        btnsalir17.setText("Salir");
        btnsalir17.setToolTipText("[Alt + s]");
        btnsalir17.setPreferredSize(new java.awt.Dimension(80, 23));
        btnsalir17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir17ActionPerformed(evt);
            }
        });

        btnsalir18.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728980 - home.png"))); // NOI18N
        btnsalir18.setMnemonic('p');
        btnsalir18.setText("Principal");
        btnsalir18.setToolTipText("[Alt + s]");
        btnsalir18.setPreferredSize(new java.awt.Dimension(80, 23));
        btnsalir18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir18ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout UtilitarioLayout = new javax.swing.GroupLayout(Utilitario);
        Utilitario.setLayout(UtilitarioLayout);
        UtilitarioLayout.setHorizontalGroup(
            UtilitarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, UtilitarioLayout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addGroup(UtilitarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(UtilitarioLayout.createSequentialGroup()
                        .addComponent(btnsalir18, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsalir17, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        UtilitarioLayout.setVerticalGroup(
            UtilitarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(UtilitarioLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(UtilitarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnsalir17, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir18, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(114, Short.MAX_VALUE))
        );

        jTabbedPane3.addTab("Utilitarios", Utilitario);

        Bioquimicos.setViewportView(jTabbedPane3);

        jMenuBar2.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jMenuBar2.setPreferredSize(new java.awt.Dimension(258, 30));

        jdatos.setMnemonic('d');
        jdatos.setText("Datos");
        jdatos.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728995 - clipboard.png"))); // NOI18N
        jMenu1.setText("Datos Fijos");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jMenuItem16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728929 - alcohol drink glass wine.png"))); // NOI18N
        jMenuItem16.setText("Analisis");
        jMenuItem16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem16ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem16);

        jMenuItem7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728919 - car taxi transfer transport travel.png"))); // NOI18N
        jMenuItem7.setText("Derivaciones");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem7);

        jMenuItem11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728996 - bin.png"))); // NOI18N
        jMenuItem11.setText("Materiales");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem11);

        jMenuItem9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728943 - glasses reading.png"))); // NOI18N
        jMenuItem9.setText("Médicos");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem9);

        jMenuItem10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728997 - announcement.png"))); // NOI18N
        jMenuItem10.setText("Secciones");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem10);

        jMenuItem12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728987 - experiment.png"))); // NOI18N
        jMenuItem12.setText("Bioquimicos");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem12);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728922 - apartment building city.png"))); // NOI18N
        jMenu2.setText("Obras Sociales");
        jMenu2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jMenuItem6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        jMenuItem6.setMnemonic('a');
        jMenuItem6.setText("Agregar");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem6);

        jMenuItem14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        jMenuItem14.setText("Modificar");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem14);

        jMenuItem8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728900 - application browser.png"))); // NOI18N
        jMenuItem8.setMnemonic('l');
        jMenuItem8.setText("Listar");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem8);

        jMenu1.add(jMenu2);

        jdatos.add(jMenu1);

        jMenuItem1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728977 - key.png"))); // NOI18N
        jMenuItem1.setText("Usuarios");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jdatos.add(jMenuItem1);

        jMenuBar2.add(jdatos);

        jinformes.setMnemonic('i');
        jinformes.setText("Informes");
        jinformes.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N

        jMenuItem3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728949 - control device game joystick playi.png"))); // NOI18N
        jMenuItem3.setText("Configurar Informes");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jinformes.add(jMenuItem3);

        jMenuItem5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728916 - calendar date.png"))); // NOI18N
        jMenuItem5.setText("Configurar Plantillas");
        jMenuItem5.setEnabled(false);
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jinformes.add(jMenuItem5);

        jMenuBar2.add(jinformes);

        jcbt.setMnemonic('c');
        jcbt.setText("CBT");
        jcbt.setToolTipText("");
        jcbt.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N

        jMenuItem13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728924 - clock time.png"))); // NOI18N
        jMenuItem13.setText("Actualizar Bases de Datos");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jcbt.add(jMenuItem13);

        jMenuItem15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728963 - flying holiday plane tourism trave.png"))); // NOI18N
        jMenuItem15.setText("Exportar Facturación a CBT");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        jcbt.add(jMenuItem15);

        jMenuItem4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728992 - control dashboard speed.png"))); // NOI18N
        jMenuItem4.setText("Cerrar Periodo y Descargar DDJJ ");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jcbt.add(jMenuItem4);

        jMenuBar2.add(jcbt);

        setJMenuBar(jMenuBar2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Bioquimicos, javax.swing.GroupLayout.DEFAULT_SIZE, 961, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Bioquimicos, javax.swing.GroupLayout.PREFERRED_SIZE, 635, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    void borrartabla() {
        DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
        int a = temp.getRowCount() - 1;  //Índices van de 0 a n-1
        for (int i = a; i >= 0; i--) {
            temp.removeRow(i);
        }
    }

    void borrartablapacientes() {
        DefaultTableModel temp = (DefaultTableModel) tablapacientes.getModel();
        int a = temp.getRowCount() - 1;  //Índices van de 0 a n-1
        for (int i = a; i >= 0; i--) {
            temp.removeRow(i);
        }
    }

    void cargarhora() {
        SimpleDateFormat formatoTiempo2 = new SimpleDateFormat("HHmmss");
        SimpleDateFormat formatoTiempo = new SimpleDateFormat("HH:mm:ss");
        java.util.Date currentDate1 = new java.util.Date();
        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(currentDate1);
        hora = formatoTiempo.format(currentDate1);
        hora2 = formatoTiempo2.format(currentDate1);
        /////////////////////////////////////
    }

    public class Httpswmpractica {

        private final String USER_AGENT = "Mozilla/5.0";

        // HTTP GET request
        public void sendGet() throws Exception {
            ///http://ws.itcsoluciones.com:38080/jSitelServlet/Do?////////////////////////////////////////////bda221f8-a7e3-11e4-b085-000c29a675b5
            String urlString = "http://ws.itcsoluciones.com:48080/jSitelServlet/Do?pas=" + URLEncoder.encode("bda221f8-a7e3-11e4-b085-000c29a675b5", "UTF-8") + "&msj=" + URLEncoder.encode(mensajepractica, "UTF-8");

            URL obj = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + urlString);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());
            respuestapractica = response.toString();

        }
    }

    public class HttpOsdePractica {

        private final String USER_AGENT = "Mozilla/5.0";

        // HTTP GET request
        public void sendGet() throws Exception {

            String urlString = "http://ws.itcsoluciones.com:48080/jSitelServlet/Do?" + "pas=" + URLEncoder.encode("bda221f8-a7e3-11e4-b085-000c29a675b5", "UTF-8") + "&msj=" + URLEncoder.encode(mensajepractica, "UTF-8");

            URL obj = new URL(urlString);////////////////////////////////////////////////////////////////////////bda221f8-a7e3-11e4-b085-000c29a675b5
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + urlString);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());
            respuestapractica = response.toString();

        }
    }

    public class HttpBorealPractica {

        private final String USER_AGENT = "Mozilla/5.0";

        // HTTP GET request
        public void sendGet() throws Exception {

            String urlString = "http://sistemasboreal.com.ar:5480/WsBoreal/servlet/awsboreal?wsdl" + URLEncoder.encode(mensajepractica, "UTF-8");

            URL obj = new URL(urlString);////////////////////////////////////////////////////////////////////////bda221f8-a7e3-11e4-b085-000c29a675b5
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + urlString);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());
            respuestapractica = response.toString();

        }
    }

    public class HttpOsdeAnulacion {

        private final String USER_AGENT = "Mozilla/5.0";

        // HTTP GET request
        public void sendGet() throws Exception {

            String urlString = "http://ws.itcsoluciones.com:48080/jSitelServlet/Do?" + "pas=" + URLEncoder.encode("bda221f8-a7e3-11e4-b085-000c29a675b5", "UTF-8") + "&msj=" + URLEncoder.encode(mensajeanulacion, "UTF-8");

            URL obj = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + urlString);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            System.out.println(response.toString());
            respuestaanulacion = response.toString();

        }
    }

    public class HttpSwAnulacion {

        private final String USER_AGENT = "Mozilla/5.0";

        // HTTP GET request
        public void sendGet() throws Exception {

            String urlString = "http://ws.itcsoluciones.com:48080/jSitelServlet/Do?" + "pas=" + URLEncoder.encode("bda221f8-a7e3-11e4-b085-000c29a675b5", "UTF-8") + "&msj=" + URLEncoder.encode(mensajeanulacion, "UTF-8");

            URL obj = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + urlString);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            System.out.println(response.toString());
            respuestaanulacion = response.toString();

        }
    }

    void cargatotalespracticas() {
        double total2 = 0.00, total3 = 0.0, sumatoria2 = 0.0, total = 0.00, sumatoria = 0.0, sumatoria3 = 0.0;

        DecimalFormat df = new DecimalFormat("0.00");
        //txttotal5.setText("0");
        //AQUI SE SUMAN LOS VALORES DE CADA FILA PARA COLOCARLO EN EL CAMPO DE TOTAL
        int totalRow = tablapracticas.getRowCount(), aux = 0;
        totalRow -= 1;
        for (int i = 0; i <= (totalRow); i++) {
            String x1 = "0", x2 = "0", x3 = "0";
            if (tablapracticas.getValueAt(i, 8).toString().equals("true")) {
                x1 = tablapracticas.getValueAt(i, 4).toString();///suma por Obra Social                        
            }
            if (tablapracticas.getValueAt(i, 8).toString().equals("false")) {
                x2 = tablapracticas.getValueAt(i, 5).toString();/// suma por particular                
            }
            if (aux < Integer.valueOf(tablapracticas.getValueAt(i, 10).toString())) {
                aux = Integer.valueOf(tablapracticas.getValueAt(i, 10).toString());
            }
            x3 = tablapracticas.getValueAt(i, 7).toString();///suma por costo

            sumatoria = Double.valueOf(x1);
            sumatoria2 = Double.valueOf(x2);
            sumatoria3 = Double.valueOf(x3);

            total = total + sumatoria;
            ///JOptionPane.showMessageDialog(null, total);
            total2 = total2 + sumatoria2;
            total3 = total3 + sumatoria3;
            /* if(tablapracticas.gets){
            }*/
        }
        //// tiempo_procesamiento = aux;
        txttotal2.setText((String.valueOf(Redondear(total2))));
        txttotal3.setText((String.valueOf(Redondear(total))));
        txttotal4.setText((String.valueOf(Redondear(total))));

    }

    void CargaTotalesPrecios() {
        double totalOS = 0.00, totalSeña = 0.0, sumatoria = 0.0;
        totalParticular = 0.0;
        DecimalFormat df = new DecimalFormat("0.00");
        if (txttotal3.getText().equals("")) {
            totalOS = 0;
        } else {
            totalOS = Double.valueOf(txttotal3.getText());
        }

        if (txttotal2.getText().equals("")) {
            totalParticular = 0;
        } else {
            totalParticular = Double.valueOf(txttotal2.getText());
        }

        if (txttotal5.getText().equals("")) {
            totalSeña = 0;
        } else {
            totalSeña = Double.valueOf(txttotal5.getText());
        }
        sumatoria = totalOS + totalParticular - totalSeña;
        System.out.println("sumatoria = totalOS + totalParticular - totalSeña;" + sumatoria + " " + totalOS + " " + totalParticular + " " + totalSeña);
        txttotal4.setText((String.valueOf(Redondear(sumatoria))));
    }

    public double Redondearentero(double numero) {
        return Math.round(numero);
    }

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    public double Redondearcentavos(double numero) {
        return Math.rint(numero * 100) % 100;
    }

    public double Redondearpesos(double numero) {
        return Math.rint(numero);
    }

    void dobleclickordenes() {
        tablaordenes.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    System.out.println(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 12).toString());
                    Detalle_Practicas_ordenes.id_orden = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0).toString();
                    Detalle_Practicas_ordenes.historia_clinica = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 12).toString();
                    Detalle_Practicas_ordenes.afiliado = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 3).toString();
                    Detalle_Practicas_ordenes.servicio = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 2).toString();
                    Detalle_Practicas_ordenes.fecha = revertirfecha_1(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 5).toString());
                    Detalle_Practicas_ordenes.total = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 6).toString();
                    new Detalle_Practicas_ordenes(null, true).setVisible(true);
                }
            }
        });
    }

    void dobleclick() {
        tablapacientes.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    Detalle_Practicas.id_ordenes = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 12).toString();
                    Detalle_Practicas.historia_clinica = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString();
                    Detalle_Practicas.afiliado = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 1).toString() + " " + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 2).toString();
                    Detalle_Practicas.servicio = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 9).toString();
                    Detalle_Practicas.fecha = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 5).toString();
                    Detalle_Practicas.num_orden = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString();
                    Detalle_Practicas.hora = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 11).toString();
                    new Detalle_Practicas(null, true).setVisible(true);
                }
            }
        });
    }

    void unclick() {
        tablapracticas.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    cargatotalespracticas();
                    CargaTotalesPrecios();
                }
            }
        });
    }

    void unclickhistoriaclinica() {
        tablapacientes.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    int i = tablapacientes.getSelectedRow();
                    txtdescripcion.setText(" Observaciones: " + descripcion[i]);
                }
            }
        });
    }

    void descargarpdf() {

        // String url = "C:\\Users\\Lucas\\Documents\\NetBeansProjects\\AppTray\\src\\PDF DECLARACIONES\\1220141.pdf";
        String url = "http://181.15.126.2/djj/" + periododjj + "-" + matricula_colegiado + ".pdf";
        //dirección url del recurso a descargar
        String name = "ddjj" + periododjj + "-" + matricula_colegiado + ".pdf";
        //Directorio destino para las descargas
        String folder = "C:\\Descargas-CBT\\";
        //////////gordo puto
        //Crea el directorio de destino en caso de que no exista
        try {
            File dir = new File(folder);

            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    return; // no se pudo crear la carpeta de destino
                }
            }
            File file = new File(folder + name);
            URLConnection conn = new URL(url).openConnection();
            conn.connect();
            System.out.println("\nempezando descarga: \n");
            System.out.println(">> URL: " + url);
            System.out.println(">> Nombre: " + name);
            System.out.println(">> tamaño: " + conn.getContentLength() + " bytes");
            InputStream in = conn.getInputStream();
            OutputStream out = new FileOutputStream(file);
            int b = 0;
            while (b != -1) {
                b = in.read();
                if (b != -1) {
                    out.write(b);
                }
            }
            out.close();
            in.close();
            JOptionPane.showMessageDialog(null, "Su declaración Jurada se descargó exitosamente...");
        } catch (MalformedURLException e) {
            System.out.println("la url: " + url + " no es valida!");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    void descargarpdfvalidacion() {

        // String url = "C:\\Users\\Lucas\\Documents\\NetBeansProjects\\AppTray\\src\\PDF DECLARACIONES\\1220141.pdf";
        String url = "http://181.15.126.2/validacion/" + periododjj + "-" + matricula_colegiado + ".pdf";
        //dirección url del recurso a descargar+ periodo + "-" + matricula + "-" + cod_obra +
        String name = periododjj + "-" + matricula_colegiado + ".pdf";
        //Directorio destino para las descargas
        String folder = "C:\\Descargas-CBT\\";
        //////////gordo puto
        //Crea el directorio de destino en caso de que no exista
        try {
            File dir = new File(folder);

            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    return; // no se pudo crear la carpeta de destino
                }
            }
            File file = new File(folder + name);
            URLConnection conn = new URL(url).openConnection();
            conn.connect();
            System.out.println("\nempezando descarga: \n");
            System.out.println(">> URL: " + url);
            System.out.println(">> Nombre: " + name);
            System.out.println(">> tamaño: " + conn.getContentLength() + " bytes");
            InputStream in = conn.getInputStream();
            OutputStream out = new FileOutputStream(file);
            int b = 0;
            while (b != -1) {
                b = in.read();
                if (b != -1) {
                    out.write(b);
                }
            }
            out.close();
            in.close();
            JOptionPane.showMessageDialog(null, "Su comprobantes se descargaron exitosamente...");
        } catch (MalformedURLException e) {
            System.out.println("la url: " + url + " no es valida!");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void CrearTabla(File file) throws IOException {
        Workbook workbook = null;
        try {
            workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);
            columna.clear();
            for (int i = 0; i < sheet.getColumns(); i++) {
                Cell cell1 = sheet.getCell(i, 0);
                columna.add(cell1.getContents());
            }
            filas.clear();
            for (int j = 1; j < sheet.getRows(); j++) {
                Vector d = new Vector();
                for (int i = 0; i < sheet.getColumns(); i++) {
                    Cell cell = sheet.getCell(i, j);
                    d.add(cell.getContents());
                }
                d.add("\n");
                filas.add(d);
            }
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

    /*   public void cargatotalesordenes() {
     int totalRow = tablaordenes1.getRowCount(), contador = 0;
     totalRow -= 1;
     for (int i = 0; i <= (totalRow); i++) {
     contador++;
     }
     txttotalordenes1.setText((String.valueOf(contador)));
     }

     public void cargatotalesordenesfacturacion() {
     int totalRow = tablaordenes.getRowCount(), contador = 0;
     totalRow -= 1;
     for (int i = 0; i <= (totalRow); i++) {
     if (tablaordenes.getValueAt(i, 10).toString().equals("OK")) {
     contador++;
     }
     }
     txttotalordenes.setText((String.valueOf(contador)));
     }

     void deshabilitar() {
     jTabbedPane2.enable(false);
     }

     void habilitar() {
     jTabbedPane2.enable(true);
     }*/
 /*  public class HiloOrdenesImportar extends Thread {

     JProgressBar progreso3;

     public HiloOrdenesImportar(JProgressBar progreso3) {
     super();
     this.progreso3 = progreso3;

     }

     public void run() {
     ConexionMySQL mysql = new ConexionMySQL();
     Connection cn = mysql.Conectar();
     int cantidad;
     String errores = "";
     ////////////////////////////////////////////
     int indice = 1; //indice contara los numeros primos que vamos encontrando    
     DefaultTableModel temp = (DefaultTableModel) MainB.myModel;
     int i2 = 0, nbu;

     int p = 0, i4 = 0, bandera_orden;
     int n4 = temp.getRowCount();
     progreso3.setMaximum(n4);

     progreso3.setValue(1);
     //cantidad = 1000 / n4;
     if (n4 != 0) {
     btnaceptar2.setEnabled(false);
     btncancelar2.setEnabled(false);
     btnsalir3.setEnabled(false);
     btnimportar.setEnabled(false);
     txtaño3.setEnabled(false);
     txtmes3.setEnabled(false);
     cargarorden();
     bandera_orden = 0;
     if (Redondearentero(n4 * 5 / 136) != 0) {
     JOptionPane.showMessageDialog(null, "El proceso durará aproximadamente " + Redondearentero(n4 * 5 / 136) + " minutos...");
     } else {
     JOptionPane.showMessageDialog(null, "El proceso durará unos segundos...");
     }
     while (i4 < n4) {

     if (periodo_colegiado <= Integer.valueOf(periodo)) {
     try {
     String sSQL4 = "SELECT estado FROM periodos ";
     Statement st4 = cn.createStatement();
     ResultSet rs4 = st4.executeQuery(sSQL4);
     rs4.next();
     if (rs4.getBoolean("estado") == true) {
     if (temp.getRowCount() != 0) {

     double total = 0.0, totalordenes = 0.0;
     String num_afiliado, precio, nom_afiliado, dni_afiliado, fecha_orden, matricula_presc, mes, año, num_orden, cod_practca;
     String sSQL = "", ssQL = "", nombre_practca;
     int n, n2, n3, i, j, bandera = 0, obra_social;
     num_afiliado = temp.getValueAt(i4, 4).toString();
     nom_afiliado = temp.getValueAt(i4, 3).toString();
     dni_afiliado = temp.getValueAt(i4, 8).toString();
     fecha_orden = temp.getValueAt(i4, 6).toString();
     matricula_presc = "0";
     obra_social = Integer.valueOf(temp.getValueAt(i4, 2).toString());
     num_orden = temp.getValueAt(i4, 5).toString();
     /////////////////////////////////////
     i = 0;
     while (i < num_afiliado.length()) {
     if (String.valueOf(num_afiliado.charAt(i)).equals("-") && String.valueOf(num_afiliado.charAt(i + 1)).equals("-") || String.valueOf(num_afiliado.charAt(i)).equals("/")) {
     bandera = 1;
     i = num_afiliado.length();
     }
     i++;
     }
     if (fecha_orden.length() < 10) {
     bandera = 2;
     }
     if (dni_afiliado.length() > 8) {
     bandera = 3;
     }
     if ((num_orden.length() > 10 && obra_social == 1800) || (num_orden.length() > 10 && obra_social == 1801) || (num_orden.length() > 10 && obra_social == 1803) || (num_orden.length() > 10 && obra_social == 1804)) {
     bandera = 4;
     }
     if (bandera == 0) {

     if (obra_social != 13800 && obra_social != 13700) {
     ////////////////////////ordenes/////////////////////////
     sSQL = "INSERT INTO ordenes (periodo,id_obrasocial,nombre_afiliado,dni_afiliado,numero_afiliado,"
     + "matricula_prescripcion,numero_orden,"
     + "fecha_orden, id_colegiados,total)"
     + "VALUES(?,?,?,?,?,?,?,?,?,?)";
     try {
     /////////////////////////////////////////////////////////////////
     String sSQL3 = "SELECT id_obrasocial,añonbu FROM obrasocial WHERE codigo_obrasocial=" + obra_social;
     Statement st3 = cn.createStatement();
     ResultSet rs3 = st3.executeQuery(sSQL3);
     try {
     rs3.next();
     id_obra_social = rs3.getInt("id_obrasocial");
     ////////////////////////////////////////////////////////////
     PreparedStatement pst = cn.prepareStatement(sSQL);
     pst.setString(1, MainB.año + MainB.mes);
     pst.setInt(2, id_obra_social);
     pst.setString(3, nom_afiliado);
     pst.setString(4, dni_afiliado);
     pst.setString(5, num_afiliado);
     pst.setString(6, matricula_presc);
     pst.setString(7, num_orden);
     pst.setString(8, fecha_orden);
     pst.setInt(9, id_usuario);
     pst.setDouble(10, Redondear(totalordenes));
     n = pst.executeUpdate();
     if (n > 0) {
     //////////////////////////////////////////////////detalle de orden////////////////////
     try {
     cargarorden();
     i = 1;

     nbu = rs3.getInt("añonbu");
     j = 9;
     while (j <= 28) {

     if (temp.getValueAt(i4, j) != null) {
     int cod = Integer.valueOf(temp.getValueAt(i4, j).toString()) + 660000;

     String sSQL5 = "SELECT codigo_practica,round(preciototal,2),añonbu,determinacion FROM obrasocial_tiene_practicasnbu WHERE (id_obrasocial=" + id_obra_social + "  AND añonbu=" + nbu + "  AND codigo_practica=" + cod + " )";
     Statement st5 = cn.createStatement();                                                               ///(colegiados.id_colegiados=" + id_usuario + "  AND ordenes.periodo=" + periodo2 + ")");
     ResultSet rs5 = st5.executeQuery(sSQL5);
     try {
     rs5.next();

     ///////////////////////////////inserta en la tabla detalles////////////////////////////////////////////////////////////
     String SQL = "INSERT INTO detalle_ordenes (id_orden, cod_practica,nombre_practica,precio_practica)"
     + "VALUES(?,?,?,?)";

     PreparedStatement st = cn.prepareStatement(SQL);
     cod_practca = rs5.getString("codigo_practica");
     nombre_practca = rs5.getString("determinacion");
     total = Double.valueOf(rs5.getString("round(preciototal,2)"));
     totalordenes = totalordenes + total;
     st.setInt(1, id_orden - 1);
     st.setString(2, cod_practca);
     st.setString(3, nombre_practca);
     st.setDouble(4, Redondear(total));
     n3 = st.executeUpdate();
     if (n3 > 0) {
     }
     } catch (Exception e) {
     errores = errores + "Orden: " + (id_orden - 1) + " Practica Inexistente:" + temp.getValueAt(i4, j).toString() + "\r\n";
     }
     String sSQL7;
     sSQL7 = "UPDATE ordenes SET total=?  WHERE id_orden=" + (id_orden - 1);
     PreparedStatement pst2 = cn.prepareStatement(sSQL7);
     pst2.setDouble(1, Redondear(totalordenes));
     int n5 = pst2.executeUpdate();
     if (n5 > 0) {
     // JOptionPane.showMessageDialog(null, totalordenes);
     }
     ////////////////////////////////////////////////////////
     }
     j++;
     }
     } catch (Exception e) {

     }
     ///////////////////////////////////////////////////////////////////////
     temp.removeRow(i4);
     i4--;
     n4--;
     cargatotalesordenes();
     }
     } catch (Exception e) {
     errores = errores + "Fila: " + (i4 + 1) + " Obra Social Inexistente:" + temp.getValueAt(i4, 2).toString() + "\r\n";
     }
     } catch (SQLException ex) {
     //  JOptionPane.showMessageDialog(null, ex);
     }
     }
     }
     if (bandera == 1) {
     errores = errores + "Fila: " + (i4 + 1) + " Numero de afiliado erroneo:" + num_afiliado + "\r\n";
     }
     if (bandera == 2) {
     errores = errores + "Fila: " + (i4 + 1) + " Fecha de orden erroneo:" + fecha_orden + "\r\n";
     }
     if (bandera == 3) {
     errores = errores + "Fila: " + (i4 + 1) + " numero de documento erroneo:" + dni_afiliado + "\r\n";
     }
     if (bandera == 4) {
     errores = errores + "Fila: " + (i4 + 1) + " Orden de SUBSIDIO erronea:" + num_orden + "\r\n";
     }
     } else {
     JOptionPane.showMessageDialog(null, "No hay practicas en la tabla...");
     i4 = n4;
     }
     }
     } catch (Exception e) {
     JOptionPane.showMessageDialog(null, e);
     JOptionPane.showMessageDialog(null, "Error en la base de datos");
     }
     bandera_orden = 1;
     }

     progreso3.setValue(indice); //aumentamos la barra 
     //porcentaje.setText(((indice * 100) / cantidad) + "%");
     indice++;
     i4++;
     }
     if (bandera_orden == 1) {
     JOptionPane.showMessageDialog(null, "Proceso realizado con exito...");
     cargatotalesordenes();
     ///////////////////////////////////////////////////
     try {

     StringBuffer numeros = null;
     for (int i = 0; i < temp.getRowCount(); i++) //recorro las filas
     {
     numeros = new StringBuffer(errores);
     EscribeTxt("Transferencia.txt", numeros);

     }
     Desktop.getDesktop().open(new File(ruta + "Transferencia.txt"));
     } catch (Exception e) {
     JOptionPane.showMessageDialog(null, e);
     }
     ///////////////////////////////////////////////////                    
     try {
     List<JTable> tb = new ArrayList<JTable>();
     tb.add(tablaordenes1);
     export_excel excelExporter = new export_excel(tb, new File(ruta + "Errores Transferencia" + ".xls"));
     if (excelExporter.export()) {
     // JOptionPane.showMessageDialog(null, "TABLAS EXPORTADOS CON EXITOS!");
     }
     } catch (Exception ex) {
     ex.printStackTrace();
     }
     llama_excel();
     ///////////////////////////////////////////////////
     } else {
     JOptionPane.showMessageDialog(null, "Período finalizado. Debe verificar que Período desea cargar");
     }
     }

     // progreso3.setValue(100);
     btnaceptar2.setEnabled(
     true);
     btncancelar2.setEnabled(
     true);
     btnsalir3.setEnabled(
     true);
     btnimportar.setEnabled(
     true);
     txtaño3.setEnabled(
     true);
     txtmes3.setEnabled(
     true);
     }

     public void pausa(int mlSeg) {
     try {
     // pausa para el splash
     Thread.sleep(mlSeg);
     } catch (Exception e) {
     }

     }

     }*/
    public static void EscribeTxt(String ruta_archivo, StringBuffer numeros) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter(ruta + ruta_archivo);
            pw = new PrintWriter(fichero);
            pw.println(numeros + "\n");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }


    private void PracticasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PracticasActionPerformed
        /*     Detalle_txt.afiliado = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 3).toString();
         Detalle_txt.obrasocial = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 2).toString();
         Detalle_txt.fecha = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 1).toString();
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 9) != null) {
         Detalle_txt.p1 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 9).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 10) != null) {
         Detalle_txt.p2 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 10).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 11) != null) {
         Detalle_txt.p3 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 11).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 12) != null) {
         Detalle_txt.p4 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 12).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 13) != null) {
         Detalle_txt.p5 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 13).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 14) != null) {
         Detalle_txt.p6 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 14).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 15) != null) {
         Detalle_txt.p7 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 15).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 16) != null) {
         Detalle_txt.p8 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 16).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 17) != null) {
         Detalle_txt.p9 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 17).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 18) != null) {
         Detalle_txt.p10 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 18).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 19) != null) {
         Detalle_txt.p11 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 19).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 20) != null) {
         Detalle_txt.p12 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 20).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 21) != null) {
         Detalle_txt.p13 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 21).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 22) != null) {
         Detalle_txt.p14 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 22).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 23) != null) {
         Detalle_txt.p15 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 23).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 24) != null) {
         Detalle_txt.p16 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 24).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 25) != null) {
         Detalle_txt.p17 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 25).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 26) != null) {
         Detalle_txt.p18 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 26).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 27) != null) {
         Detalle_txt.p19 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 27).toString();
         }
         if (tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 28) != null) {
         Detalle_txt.p20 = tablaordenes1.getValueAt(tablaordenes1.getSelectedRow(), 28).toString();
         }
         new Detalle_txt(null, true).setVisible(true);*/
    }//GEN-LAST:event_PracticasActionPerformed

    boolean isNumeric(String cadena) {
        try {
            Long.parseLong(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    /* void filtrarordenes(String valor) {
     //estos seran los titulos de la tabla.
     String[] Titulo = {"Orden", "Periodo", "Obra Social", "N° Afiliado", "Nombre Afiliado", "Matricula Prescripcion", "N° Orden Prescripcion", "Fecha Orden", "Colegiado", "Total", "Estado"};
     String[] Registros = new String[11];
     String sql = "SELECT id_orden,periodo,id_obrasocial,numero_afiliado,nombre_afiliado,matricula_prescripcion,numero_orden,fecha_orden,id_colegiados,total,estado_orden FROM ordenes WHERE CONCAT(id_orden, ' ',numero_orden, ' ',numero_afiliado ,' ',dni_afiliado,' ',nombre_afiliado)"
     + "LIKE '%" + valor + "%'";
     model = new DefaultTableModel(null, Titulo) {
     ////Celdas no editables////////
     public boolean isCellEditable(int row, int column) {
     return false;
     }
     };
     ConexionMySQL cc = new ConexionMySQL();
     Connection cn = cc.Conectar();

     try {
     /////////////////////////////////////////////////////
     Statement st = cn.createStatement();
     ResultSet rs = st.executeQuery(sql);
     while (rs.next()) {
     if (id_usuario == rs.getInt("id_colegiados") && rs.getString("periodo").equals(txtaño1.getText() + txtmes1.getText())) {
     Registros[0] = rs.getString("id_orden");
     Registros[1] = rs.getString("periodo");
     /////////////////////////////////////////////////////
     Statement st2 = cn.createStatement();
     String sql2 = "SELECT id_obrasocial,razonsocial_obrasocial FROM obrasocial WHERE id_obrasocial=" + rs.getString("id_obrasocial");
     ResultSet rs2 = st2.executeQuery(sql2);
     while (rs2.next()) {
     if (rs.getString("id_obrasocial").equals(rs2.getString("id_obrasocial")) && rs.getString("periodo").equals(txtaño1.getText() + txtmes1.getText())) {
     Registros[2] = rs2.getString("razonsocial_obrasocial");
     }
     }
     /////////////////////////////////////////////////////
     Registros[3] = rs.getString("numero_afiliado");
     Registros[4] = rs.getString("nombre_afiliado");
     Registros[5] = rs.getString("matricula_prescripcion");
     Registros[6] = rs.getString("numero_orden");
     Registros[7] = rs.getString("fecha_orden");
     Registros[8] = rs.getString("id_colegiados");
     Registros[9] = rs.getString("total");
     String estado = "";
     if (rs.getInt("estado_orden") == 1) {
     estado = "OK";
     } else {
     estado = "ANULADA";
     }
     Registros[10] = estado;
     model.addRow(Registros);

     tablaordenes.setModel(model);
     /////////////////////////////////////////////////////////////
     //    tablaordenes.getColumnModel().getColumn(0).setMaxWidth(0);
     //  tablaordenes.getColumnModel().getColumn(0).setMinWidth(0);
     //tablaordenes.getColumnModel().getColumn(0).setPreferredWidth(0);
     /////////////////////////////////////////////////////////////////
     tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
     tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
     tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);
     /////////////////////////////////////////////////////////////////
     tablaordenes.getColumnModel().getColumn(8).setMaxWidth(0);
     tablaordenes.getColumnModel().getColumn(8).setMinWidth(0);
     tablaordenes.getColumnModel().getColumn(8).setPreferredWidth(0);
     /////////////////////////////////////////////////////////////
     alinear();
     tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
     tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
     tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
     tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
     tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
     tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
     tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
     tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
     tablaordenes.getColumnModel().getColumn(8).setCellRenderer(alinearCentro);
     tablaordenes.getColumnModel().getColumn(9).setCellRenderer(alinearCentro);
     tablaordenes.getColumnModel().getColumn(10).setCellRenderer(alinearCentro);
     }
     }

     } catch (SQLException ex) {
     JOptionPane.showMessageDialog(null, ex);
     }
     }*/

    private void ModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificarActionPerformed
        if (banderamodifica == 1) {
            btnborrar2.doClick();
        }
        DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
        int fila = 0, bandera_limpiar = 0;
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        try {
            String sSQL = "SELECT personas.apellido,personas.nombre, personas.domicilio, personas.sexo, localidad.nombre_localidad FROM personas INNER JOIN localidad ON localidad.id_localidad = personas.id_localidad WHERE dni=" + tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 9).toString();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                txtdnipaciente.setText(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 9).toString());
                txtapellidopaciente.setText(rs.getString(1));
                txtnombrepaciente.setText(rs.getString(2));
                txtlocalidad.setText(rs.getString(5));
                txtdireccion.setText(rs.getString(3));
                if (rs.getString(4).equals("M")) {
                    cbosexo.setSelectedIndex(0);
                } else {
                    cbosexo.setSelectedIndex(1);
                }
                txtnombreafiliado1.setText(rs.getString("personas.apellido") + " " + rs.getString("personas.nombre"));
                txttelfijo.setText("");
                txttelcelular.setText("");
                txtmail.setText("");
                banderapaciente = 1;
            }

        } catch (Exception e) {
            bandera_limpiar = 1;
            JOptionPane.showMessageDialog(null, e);
        }
        try {
            String sSQL = "SELECT id_Pacientes, telefono, celular, mail, fecha_nacimiento FROM pacientes WHERE personas_dni=" + tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 9).toString();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                txttelfijo.setText(rs.getString("telefono"));
                txttelcelular.setText(rs.getString("celular"));
                txtmail.setText(rs.getString("mail"));
                id_paciente = rs.getInt("id_Pacientes");
                idPaciente = id_paciente;
                txtfechanac.setText(revertirfecha_1(rs.getString("fecha_nacimiento")));
                String fechanac = txtfechanac.getText();
                txtedadpaciente.setText(edad(revertir(fechanac)));
                txtfechanac.transferFocus();
                id_obra_social = Integer.valueOf(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 10).toString());
                banderapaciente = 2;

            }
            ////
        } catch (Exception e) {
            bandera_limpiar = 1;
            JOptionPane.showMessageDialog(null, e);
        }
        try {
            String sSQL = "SELECT  numero_afiliado , obrasocial.Int_codigo_obrasocial, obrasocial.razonsocial_obrasocial FROM pacientes_tienen_obrasociales inner join obrasocial on obrasocial.id_obrasocial=pacientes_tienen_obrasociales.id_obrasocial WHERE pacientes_tienen_obrasociales.id_Pacientes=" + id_paciente + " AND pacientes_tienen_obrasociales.id_obrasocial=" + tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 10).toString();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                txtnumafiliadopaciente.setText(rs.getString(1));
                txtobrasocialpaciente.setText(rs.getString(2) + " - " + rs.getString(3));
                banderapaciente = 3;
                borrarpractica();
                ///cargarpracticaconobra();
                ///borrartabla();
                cargarpracticaconobra();
                //cargarpracticas();
                cargarpracticasparticular();
            }
            /////
        } catch (Exception e) {
            bandera_limpiar = 1;
            JOptionPane.showMessageDialog(null, e);
        }
        try {
            System.out.println("entra");
            String sSQL = "SELECT ordenes.*, medicos.apellido, medicos.nombre, medicos.matricula FROM ordenes INNER JOIN medicos ON medicos.id_medicos = ordenes.id_medicos INNER JOIN medicos_tienen_especialidades ON medicos_tienen_especialidades.id_medicos = ordenes.id_medicos INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes WHERE ordenes.estado_orden=0 and historia_clinica.idhistoria_clinica =" + tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 12).toString();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            if (rs.next()) {
                if (!rs.getString(3).equals("?")) {
                    id_medico = rs.getInt(8);
                    id_paciente = rs.getInt(10);
                    txtmedico.setText(rs.getString(27) + "-" + rs.getString(25) + " " + rs.getString(26));
                    cboservicio.setSelectedItem(rs.getString(11));
                    txtcama.setText(rs.getString(12));
                    txtnumorden2.setText(rs.getString(3));
                    txtfecha1.setText(revertirfecha_1(rs.getString(6)));
                    txtfecha2.setText(revertirfecha_1(rs.getString(6)));
                    txttipo.setText(rs.getString(13));
                    txtprecio.setText(rs.getString(19));
                    txtreciennacido.setText(rs.getString(20));
                    id_especialidad = rs.getInt(9);
                    observacionesOrdenes = rs.getString(17);
                    System.out.println("Formularios.MainB.ModificarActionPerformed():" + observacionesOrdenes);
                } else {
                    bandera_limpiar = 1;
                }
            }
            /////
        } catch (Exception e) {
            bandera_limpiar = 1;
            JOptionPane.showMessageDialog(null, e);
        }
        try {
            String sSQL3 = "select * from vista_ordenes_tiene_practicas WHERE estado_orden=0 and idhistoria_clinica =" + tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 12).toString();
            //String sSQL3 = "SELECT ordenes.numero_orden, practicas.codigo_practica ,practicas.determinacion_practica , ordenes_tienen_practicas.*,practicas.precio1,practicas.precio2,practicas.precio3,practicas.precio4, practicas.tipo_informe FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes = ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas = ordenes_tienen_practicas.id_practicas INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes WHERE ordenes.estado_orden=0 and historia_clinica.idhistoria_clinica =" + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString();
            Statement st3 = cn.createStatement();
            ResultSet rs3 = st3.executeQuery(sSQL3);
            while (rs3.next()) {
                Object nuevo[] = {
                    fila + 1, "", ""};
                temp.addRow(nuevo);
                tablapracticas.setValueAt(rs3.getString(1), fila, 1);
                tablapracticas.setValueAt(rs3.getString(2), fila, 2);
                tablapracticas.setValueAt(rs3.getString(3), fila, 3);
                tablapracticas.setValueAt(rs3.getString(6), fila, 4);
                tablapracticas.setValueAt(0, fila, 5);
                tablapracticas.setValueAt(rs3.getString(4), fila, 6);
                tablapracticas.setValueAt(rs3.getString(11), fila, 7);
                if (rs3.getInt(8) == 1) {
                    tablapracticas.setValueAt(true, fila, 8);
                } else {
                    tablapracticas.setValueAt(false, fila, 8);
                }
                if (rs3.getInt(9) == 1) {
                    tablapracticas.setValueAt(true, fila, 9);
                } else {
                    tablapracticas.setValueAt(false, fila, 9);
                }
                tablapracticas.setValueAt(rs3.getString(13), fila, 10);
                tablapracticas.setValueAt(rs3.getString(7), fila, 11);
                tablapracticas.setValueAt(rs3.getString(5), fila, 12);
                fila++;
                filamodifica = rs3.getString(5);
                ////////////////////////////////////////////////////////////////////                    
                tablapracticas.getColumnModel().getColumn(11).setMaxWidth(0);
                tablapracticas.getColumnModel().getColumn(11).setMinWidth(0);
                tablapracticas.getColumnModel().getColumn(11).setPreferredWidth(0);
                ////////////////////////////////////////////////////////////////////
            }
            cn.close();
            /////
        } catch (Exception e) {
            bandera_limpiar = 1;
            JOptionPane.showMessageDialog(null, e);
        }
        if (bandera_limpiar == 1) {
            JOptionPane.showMessageDialog(null, "No se pudo acceder para hacer la modificación");
            btnborrar2.doClick();
        } else {
            jTabbedPane3.setEnabledAt(2, true);
            jTabbedPane3.setEnabledAt(1, false);
            jTabbedPane3.setSelectedIndex(2);
            txtmedico.requestFocus();
            banderamodifica = 1;
            // System.out.println(tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString());
            id_historia_clinica = Integer.valueOf(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 12).toString());
            txtprotocolo.setText(completarceros(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 12).toString(), 10));
            cargatotalespracticas();
            txtpractica2.setText("");
        }


    }//GEN-LAST:event_ModificarActionPerformed

    private void AnularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnularActionPerformed
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        String num_orden = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 4).toString();
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        int i = 0;
        try {

            /*String sSQL = "SELECT estado,periodo FROM periodos ";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.next();
            int periodo_anula = Integer.valueOf(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 1).toString());
            if (rs.getBoolean("estado") == true && rs.getInt("periodo") <= Integer.valueOf(periodo_anula)) {*/
            i = 0;
            //////////////////////////////////////////////////////////////
            if (tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 11).toString().equals("OK")) {

                id_orden = Integer.valueOf(tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0).toString());
                String cod_afiliado = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 8).toString();
                String fecha = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 5).toString();
                periodo = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 1).toString();
                try {
                    String sSQL2 = "UPDATE ordenes SET estado_orden=? WHERE id_ordenes=" + id_orden;
                    PreparedStatement pst = cn.prepareStatement(sSQL2);
                    pst.setInt(1, 1);
                    cursor();
                    int n = pst.executeUpdate();
                    System.out.println("1");

                    if (n > 0) {
                        cursor2();
                        JOptionPane.showMessageDialog(null, "La orden fué anulada localmente...");
                        cursor();
                        /////////OSDE////////////////////////////////////////////////////
                        if (tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 2).toString().equals("3100 OSDE")) {
                            String sql = "SELECT nombre, apellido, cuit, matricula, usuario, contraseña FROM laboratorios";
                            try {
                                Statement st = cn.createStatement();
                                ResultSet rs = st.executeQuery(sql);
                                rs.last();
                                if (rs.getRow() == 1) {
                                    cuil = rs.getString(3);
                                    Enviar_Facturacion.matricula = rs.getString(4);
                                    Enviar_Facturacion.usuario = rs.getString(5);
                                    Enviar_Facturacion.contraseña = rs.getString(6);
                                    /////////////////////////////////////////////////////////////////////
                                    System.out.println(Enviar_Facturacion.matricula);
                                    System.out.println(Enviar_Facturacion.usuario);
                                    System.out.println(Enviar_Facturacion.contraseña);
                                    System.out.println(cuil);
                                } else {
                                    facturacion = 3;
                                    new ElegirBioquimicos(null, true).setVisible(true);
                                    System.out.println(Enviar_Facturacion.matricula);
                                    System.out.println(Enviar_Facturacion.usuario);
                                    System.out.println(Enviar_Facturacion.contraseña);
                                    System.out.println(cuil);
                                }
                            } catch (SQLException ex) {
                                JOptionPane.showMessageDialog(null, ex);
                                JOptionPane.showMessageDialog(null, "Error en la base de datos...");

                            }
                            //////////////////////////////////////////////////////////////                            
                            System.out.println("2: " + hora + " fecha " + fechaonline + "  cuil " + cuil + " num_orden " + num_orden + " numero_afiliado " + cod_afiliado);
                            mensajeanulacion = "<Mensaje><EncabezadoMensaje><VersionMsj>1.0</VersionMsj><NroReferenciaCancel>" + num_orden + "</NroReferenciaCancel><TipoTransaccion>04A</TipoTransaccion><IdMsj>" + hora + "</IdMsj><InicioTrx><FechaTrx>" + fechaonline + "</FechaTrx><HoraTrx>" + hora + "</HoraTrx></InicioTrx><Financiador><CodigoFinanciador>11</CodigoFinanciador><CuitFinanciador>30546741253</CuitFinanciador></Financiador><Prestador><CuitPrestador>" + cuil + "</CuitPrestador></Prestador></EncabezadoMensaje><EncabezadoAtencion><Efector/><Prescriptor/><Credencial><NumeroCredencial>" + cod_afiliado + "</NumeroCredencial></Credencial><Atencion><FechaAtencion>20150902</FechaAtencion></Atencion></EncabezadoAtencion></Mensaje>";
                            HttpOsdeAnulacion http = new HttpOsdeAnulacion();
                            System.out.println("Testing 3 - Send Http GET request");
                            try {
                                http.sendGet();
                            } catch (Exception ex) {
                                cursor2();
                                JOptionPane.showMessageDialog(null, ex);

                            }
                            int pos = respuestaanulacion.indexOf("MensajeDisplay");
                            int pos2 = respuestaanulacion.indexOf("</MensajeDisplay");
                            cursor2();
                            JOptionPane.showMessageDialog(null, "Numero de Anulación:" + respuestaanulacion.substring(pos + 15, pos2));
                            System.out.println("2--");
                        }
                        /////SWISS MEDICAL GROUP S.A.
                        if (tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 2).toString().equals("10070 SWISS MEDICAL GROUP S.A. - ONLINE")) {
                            String sql = "SELECT nombre, apellido, cuit, matricula, usuario, contraseña FROM laboratorios";
                            try {
                                Statement st = cn.createStatement();
                                ResultSet rs = st.executeQuery(sql);
                                rs.last();
                                if (rs.getRow() == 1) {
                                    cuil = rs.getString(3);
                                    Enviar_Facturacion.matricula = rs.getString(4);
                                    Enviar_Facturacion.usuario = rs.getString(5);
                                    Enviar_Facturacion.contraseña = rs.getString(6);
                                    System.out.println(Enviar_Facturacion.matricula);
                                    System.out.println(Enviar_Facturacion.usuario);
                                    System.out.println(Enviar_Facturacion.contraseña);
                                    System.out.println(cuil);
                                } else {
                                    facturacion = 3;
                                    new ElegirBioquimicos(null, true).setVisible(true);
                                    System.out.println(Enviar_Facturacion.matricula);
                                    System.out.println(Enviar_Facturacion.usuario);
                                    System.out.println(Enviar_Facturacion.contraseña);
                                    System.out.println(cuil);
                                }
                            } catch (SQLException ex) {
                                JOptionPane.showMessageDialog(null, ex);
                                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                            }
                            //////////////////////////////////////////////////////////////
                            System.out.println("3 orden " + num_orden + " fecha " + fechaonline + " hora " + hora + " Cuil " + cuil + " ca " + cod_afiliado);
                            mensajeanulacion = "<Mensaje><EncabezadoMensaje><VersionMsj>1.0</VersionMsj><NroReferenciaCancel>" + num_orden + "</NroReferenciaCancel><TipoTransaccion>04A</TipoTransaccion><IdMsj>" + hora + "</IdMsj><InicioTrx><FechaTrx>" + fechaonline + "</FechaTrx><HoraTrx>" + hora + "</HoraTrx></InicioTrx><Financiador><CodigoFinanciador>26</CodigoFinanciador><CuitFinanciador>30654855168</CuitFinanciador></Financiador><Prestador><CuitPrestador>" + cuil + "</CuitPrestador></Prestador></EncabezadoMensaje><EncabezadoAtencion><Efector/><Prescriptor/><Credencial><NumeroCredencial>" + cod_afiliado + "</NumeroCredencial></Credencial><Atencion><FechaAtencion>20150902</FechaAtencion></Atencion></EncabezadoAtencion></Mensaje>";
                            HttpOsdeAnulacion http = new HttpOsdeAnulacion();
                            System.out.println("Testing 3 - Send Http GET request");
                            try {
                                http.sendGet();
                            } catch (Exception ex) {
                                cursor2();
                                JOptionPane.showMessageDialog(null, ex);
                            }
                            int pos = respuestaanulacion.indexOf("CodAutorizacion");
                            int pos2 = respuestaanulacion.indexOf("</CodAutorizacion");
                            cursor2();
                            JOptionPane.showMessageDialog(null, "Numero de Anulación:" + respuestaanulacion.substring(pos + 16, pos2));
                            System.out.println("3---");
                        }
                        ////////////////////////BOREAL//////////////
                        if (tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 2).toString().equals("50015 BOREAL")) {
                            String sql = "SELECT nombre, apellido, cuit, matricula, usuario, contraseña, id_colegiado FROM laboratorios";
                            try {
                                Statement st = cn.createStatement();
                                ResultSet rs = st.executeQuery(sql);
                                rs.last();
                                if (rs.getRow() == 1) {
                                    cuil = rs.getString(3);
                                    Enviar_Facturacion.matricula = rs.getString(4);
                                    Enviar_Facturacion.usuario = rs.getString(5);
                                    Enviar_Facturacion.contraseña = rs.getString(6);
                                    ElegirBioquimicos.id_colegiado = rs.getInt(7);
                                    System.out.println(Enviar_Facturacion.matricula);
                                    System.out.println(Enviar_Facturacion.usuario);
                                    System.out.println(Enviar_Facturacion.contraseña);
                                    System.out.println(cuil);

                                } else {
                                    facturacion = 3;
                                    new ElegirBioquimicos(null, true).setVisible(true);
                                    System.out.println(Enviar_Facturacion.matricula);
                                    System.out.println(Enviar_Facturacion.usuario);
                                    System.out.println(Enviar_Facturacion.contraseña);
                                    System.out.println(cuil);
                                }
                                String emisor = "CBT" + completarceros(Enviar_Facturacion.matricula, 9);
                                Contraseña_Boreal contraseña = new Contraseña_Boreal();
                                String clave = contraseña.Boreal_Contraseña();
                                //////////////////////////////////////////////////////////////////
                                System.out.println("valida boreal");
                                Anula_Online orden = new Anula_Online();
                                int bandera_boreal = orden.Boreal_online(emisor, clave, cuil, num_orden);
                                System.out.println("boreal 4");
                                /////////////////////////////////////////////////////////////////
                                if (bandera_boreal == 1) {
                                    valida_orden_colegio orden_anula = new valida_orden_colegio();
                                    bandera_boreal = orden_anula.anula(num_orden, periodo, ElegirBioquimicos.id_colegiado);
                                    if (bandera_boreal == 1) {
                                        JOptionPane.showMessageDialog(null, "La orden fué anulada del servidor del CBT");
                                    } else {
                                        JOptionPane.showMessageDialog(null, "La orden no pudo ser anulada");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "La orden no pudo ser anulada");
                                }
                                cursor2();
                            } catch (SQLException ex) {
                                cursor2();
                                JOptionPane.showMessageDialog(null, ex);
                                JOptionPane.showMessageDialog(null, "Error en la base de datos...");

                            }

                        }
                        /* //////////////////////SANCOR/////////////////////////////////////////////
                            if (tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 2).toString().equals("ASOCIACION MUTUAL SANCOR")) {
                                System.out.println("5");
                                //////////////////////////////////////////////////////////
                                ClienteSancor.PAWESSAV2ANULACION servicio = new ClienteSancor.PAWESSAV2ANULACION();
                                servicio.setModo("P");
                                servicio.setEntidad(8999);
                                servicio.setNroautorizacion(Integer.valueOf(num_orden));
                                servicio.setUsuario("WSRVSSA");
                                servicio.setClave("15WSSA08");
                                PAWESSAV2ANULACIONResponse anulacion_sancor = anulacion(servicio);
                                //System.out.println(anulacion_sancor);
                                if (anulacion_sancor.getCodigorespuesta() == 35) {
                                    habilitado = "AUTORIZADO";
                                    num_orden = String.valueOf(anulacion_sancor.getNroordenrta());
                                    txtnumorden.setText(num_orden);
                                    cursor2();
                                    JOptionPane.showMessageDialog(null, "Numero de Anulación:" + num_orden);
                                    ///////////////////////    tablaordenes.setValueAt("ANULADA", tablaordenes.getSelectedRow(), 10);
                                    ///  cargatotales();
                                    /// cargatotalesordenesfacturacion();
                                } else {
                                    habilitado = "ERROR";
                                    cursor2();
                                    JOptionPane.showMessageDialog(null, anulacion(servicio).getDescripcionrespuesta());
                                }
                                System.out.println("5-----");
                                //hilo91.stop();
                            }
                         */
                        cursor();
                        ///cargatotales();
                        tablaordenes.setValueAt("ANULADA", tablaordenes.getSelectedRow(), 10);
                        cargatotalesordenesfacturacion();
                    }
                    System.out.println("1-");
                    cursor2();
                    cn.close();
                } catch (Exception e) {
                    cursor2();
                    JOptionPane.showMessageDialog(null, e);
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////
            } else {
                cursor2();
                JOptionPane.showMessageDialog(null, "La orden ya está anulada...");
                banderamodifica = 0;
            }
            ///////////////////////////////////////////////////////////////

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "Error en la base de datos");
        }
    }//GEN-LAST:event_AnularActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            btnborrarpaciente2.doClick();
        }

        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            //  jTabbedPane2.setSelectedIndex(1);
        }

        if (evt.getKeyCode() == KeyEvent.VK_F3) {

            //  jTabbedPane2.setSelectedIndex(2);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F4) {

            //   jTabbedPane2.setSelectedIndex(3);
        }
    }//GEN-LAST:event_formKeyPressed

    private void txtnombreafiliado1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreafiliado1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtnombreafiliado1.transferFocus();
        }
    }//GEN-LAST:event_txtnombreafiliado1KeyPressed

    private void txtcamaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcamaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtcama.transferFocus();
        }
    }//GEN-LAST:event_txtcamaKeyPressed

    private void txtfecha1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfecha1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtfecha1.equals("  -  -    ")) {
                if (Integer.valueOf(txtfecha1.getText().substring(0, 2)) <= 31 && Integer.valueOf(txtfecha1.getText().substring(0, 2)) >= 1) {
                    if (Integer.valueOf(txtfecha1.getText().substring(3, 5)) <= 12 && Integer.valueOf(txtfecha1.getText().substring(3, 5)) >= 1) {
                        if (Integer.valueOf(txtfecha1.getText().substring(3, 5)) == 1 || Integer.valueOf(txtfecha1.getText().substring(3, 5)) == 3 || Integer.valueOf(txtfecha1.getText().substring(3, 5)) == 5 || Integer.valueOf(txtfecha1.getText().substring(3, 5)) == 7 || Integer.valueOf(txtfecha1.getText().substring(3, 5)) == 8 || Integer.valueOf(txtfecha1.getText().substring(3, 5)) == 10 || Integer.valueOf(txtfecha1.getText().substring(3, 5)) == 12) {
                            txtfecha1.transferFocus();
                        } else {
                            if (Integer.valueOf(txtfecha1.getText().substring(3, 5)) == 4 || Integer.valueOf(txtfecha1.getText().substring(3, 5)) == 6 || Integer.valueOf(txtfecha1.getText().substring(3, 5)) == 9 || Integer.valueOf(txtfecha1.getText().substring(3, 5)) == 11) {
                                if (Integer.valueOf(txtfecha1.getText().substring(0, 2)) <= 30) {
                                    txtfecha1.transferFocus();
                                }
                            } else {
                                if (Integer.valueOf(txtfecha1.getText().substring(3, 5)) == 2) {
                                    if (Integer.valueOf(txtfecha1.getText().substring(6, 10)) % 4 == 0) {
                                        if (Integer.valueOf(txtfecha1.getText().substring(0, 2)) <= 29) {
                                            txtfecha1.transferFocus();
                                        }
                                    } else {
                                        if (Integer.valueOf(txtfecha1.getText().substring(0, 2)) <= 28) {
                                            txtfecha1.transferFocus();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }//GEN-LAST:event_txtfecha1KeyPressed

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked

    }//GEN-LAST:event_formMouseClicked

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        new Alta_Seccion(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        new Alta_Medico(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void txtdnipacienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdnipacienteKeyReleased
        TableRowSorter sorter = new TableRowSorter(modelPaciente);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtdnipaciente.getText() + ".*"));
        tablapacientes.setRowSorter(sorter);
    }//GEN-LAST:event_txtdnipacienteKeyReleased

    private void txttelfijoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelfijoKeyReleased
        if (!isNumeric(txttelfijo.getText())) {
            txttelfijo.setText("");
        }
    }//GEN-LAST:event_txttelfijoKeyReleased

    private void txttelcelularKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelcelularKeyReleased
        if (!isNumeric(txttelcelular.getText())) {
            txttelcelular.setText("");
        }
    }//GEN-LAST:event_txttelcelularKeyReleased

    private void txtdireccionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdireccionKeyReleased
        String texto = txtdireccion.getText().toUpperCase();
        txtdireccion.setText(texto);
    }//GEN-LAST:event_txtdireccionKeyReleased

    private void txtlocalidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlocalidadKeyReleased

    }//GEN-LAST:event_txtlocalidadKeyReleased

    private void tablapacientesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablapacientesKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tablapacientesKeyPressed

    private void txtpacientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpacientesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpacientesActionPerformed

    private void txtpacientesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpacientesKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpacientesKeyPressed

    private void txtpacientesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpacientesKeyReleased
///        filtrarpacientes3(txtpacientes.getText());
        TableRowSorter sorter = new TableRowSorter(modelPaciente);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtpacientes.getText() + ".*"));
        tablapacientes.setRowSorter(sorter);
    }//GEN-LAST:event_txtpacientesKeyReleased

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        int opcion = JOptionPane.showConfirmDialog(this, "Desea actualizar la base de datos?", "Mensaje", JOptionPane.YES_NO_OPTION);
        if (opcion == 0) {
            new Proceso(null, true).setVisible(true);
        }
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void txtdnipacienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdnipacienteKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            btnborrarpaciente2.doClick();
        }

        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            //     tabcolegio.setSelectedIndex(1);
        }

        if (evt.getKeyCode() == KeyEvent.VK_F3) {

            jTabbedPane3.setSelectedIndex(0);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F4) {

            jTabbedPane3.setSelectedIndex(1);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {

            jTabbedPane3.setSelectedIndex(2);
            txtmedico.requestFocus();

        }
        if (evt.getKeyCode() == KeyEvent.VK_F6) {

            jTabbedPane3.setSelectedIndex(3);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F7) {

            //   jTabbedPane2.setSelectedIndex(0);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F8) {

        }
        if (evt.getKeyCode() == KeyEvent.VK_F9) {

            ///    jTabbedPane2.setSelectedIndex(2);
//
        }
        if (evt.getKeyCode() == KeyEvent.VK_F10) {

            /// jTabbedPane2.setSelectedIndex(3);
        }

    }//GEN-LAST:event_txtdnipacienteKeyPressed

    private void UtilitarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_UtilitarioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            btnborrarpaciente2.doClick();
        }

        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            ///   tabcolegio.setSelectedIndex(1);
        }

        if (evt.getKeyCode() == KeyEvent.VK_F3) {

            jTabbedPane3.setSelectedIndex(0);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F4) {

            jTabbedPane3.setSelectedIndex(1);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {

            jTabbedPane3.setSelectedIndex(2);
            txtmedico.requestFocus();

        }
        if (evt.getKeyCode() == KeyEvent.VK_F6) {

            jTabbedPane3.setSelectedIndex(3);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F7) {

            ///    jTabbedPane2.setSelectedIndex(0);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F8) {

        }
        if (evt.getKeyCode() == KeyEvent.VK_F9) {

            ///   jTabbedPane2.setSelectedIndex(2);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F10) {

            ///   jTabbedPane2.setSelectedIndex(3);
        }
    }//GEN-LAST:event_UtilitarioKeyPressed

    private void PacientesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PacientesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            btnborrarpaciente2.doClick();
        }

        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            ///  tabcolegio.setSelectedIndex(1);
        }

        if (evt.getKeyCode() == KeyEvent.VK_F3) {

            jTabbedPane3.setSelectedIndex(0);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F4) {

            jTabbedPane3.setSelectedIndex(1);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {

            jTabbedPane3.setSelectedIndex(2);
            txtmedico.requestFocus();

        }
        if (evt.getKeyCode() == KeyEvent.VK_F6) {

            jTabbedPane3.setSelectedIndex(3);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F7) {

            ///   jTabbedPane2.setSelectedIndex(0);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F8) {

        }
        if (evt.getKeyCode() == KeyEvent.VK_F9) {

            ///  jTabbedPane2.setSelectedIndex(2);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F10) {

            ///   jTabbedPane2.setSelectedIndex(3);
        }
    }//GEN-LAST:event_PacientesKeyPressed

    private void FacturaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_FacturaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            btnborrarpaciente2.doClick();
        }

        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            ///   tabcolegio.setSelectedIndex(1);
        }

        if (evt.getKeyCode() == KeyEvent.VK_F3) {

            jTabbedPane3.setSelectedIndex(0);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F4) {

            jTabbedPane3.setSelectedIndex(1);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {

            jTabbedPane3.setSelectedIndex(2);
            txtmedico.requestFocus();

        }
        if (evt.getKeyCode() == KeyEvent.VK_F6) {

            jTabbedPane3.setSelectedIndex(3);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F7) {

            ///  jTabbedPane2.setSelectedIndex(0);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F8) {

        }
        if (evt.getKeyCode() == KeyEvent.VK_F9) {

            ///  jTabbedPane2.setSelectedIndex(2);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F10) {

            //   jTabbedPane2.setSelectedIndex(3);
        }
    }//GEN-LAST:event_FacturaKeyPressed

    private void OrdenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_OrdenKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            btnborrarpaciente2.doClick();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            //   tabcolegio.setSelectedIndex(1);
        }

        if (evt.getKeyCode() == KeyEvent.VK_F3) {

            jTabbedPane3.setSelectedIndex(0);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F4) {

            jTabbedPane3.setSelectedIndex(1);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {

            jTabbedPane3.setSelectedIndex(2);
            txtmedico.requestFocus();

        }
        if (evt.getKeyCode() == KeyEvent.VK_F6) {

            jTabbedPane3.setSelectedIndex(3);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F7) {

            ///   jTabbedPane2.setSelectedIndex(0);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F8) {

        }
        if (evt.getKeyCode() == KeyEvent.VK_F9) {

            ///   jTabbedPane2.setSelectedIndex(2);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F10) {

            ///   jTabbedPane2.setSelectedIndex(3);
//
        }
    }//GEN-LAST:event_OrdenKeyPressed

    private void jTabbedPane3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTabbedPane3KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            btnborrarpaciente2.doClick();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            ///   tabcolegio.setSelectedIndex(1);
        }

        if (evt.getKeyCode() == KeyEvent.VK_F3) {

            jTabbedPane3.setSelectedIndex(0);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F4) {

            jTabbedPane3.setSelectedIndex(1);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {

            txtmedico.requestFocus();
            jTabbedPane3.setSelectedIndex(2);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F6) {

            jTabbedPane3.setSelectedIndex(3);

        }
        if (evt.getKeyCode() == KeyEvent.VK_F7) {

            //  jTabbedPane2.setSelectedIndex(0);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F8) {

        }
        if (evt.getKeyCode() == KeyEvent.VK_F9) {

            ///  jTabbedPane2.setSelectedIndex(2);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F10) {

            ///  jTabbedPane2.setSelectedIndex(3);
        }
    }//GEN-LAST:event_jTabbedPane3KeyPressed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        new Derivaciones(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void txtedadpacienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtedadpacienteKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtedadpacienteKeyReleased

    private void txtnombrepacienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombrepacienteKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombrepacienteKeyReleased

    private void txtfechanacActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfechanacActionPerformed
        if (txtfechanac.getText().equals("  -  -    ")) {
            txtfechanac.setText("01-01-1950");
            String fechanac = txtfechanac.getText();
            txtedadpaciente.setText(edad(revertir(fechanac)));
            txtfechanac.transferFocus();
        }
    }//GEN-LAST:event_txtfechanacActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        new Alta_Materiales(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void btnsalir7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir7ActionPerformed
        jTabbedPane3.setSelectedIndex(0);
    }//GEN-LAST:event_btnsalir7ActionPerformed


    private void txtdnipacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdnipacienteActionPerformed
        banderapaciente = 0;
        id_paciente = 0;
        int band = 0, band2 = 0;
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        String dni = txtdnipaciente.getText();
        if (!dni.equals("")) {
            try {
                String sSQL = "SELECT personas.apellido,personas.nombre, personas.domicilio, personas.sexo, localidad.nombre_localidad, localidad.id_localidad FROM personas INNER JOIN localidad ON localidad.id_localidad = personas.id_localidad WHERE dni=" + dni;
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sSQL);
                while (rs.next()) {
                    txtapellidopaciente.setText(rs.getString(1));
                    txtnombrepaciente.setText(rs.getString(2));
                    txtdireccion.setText(rs.getString(3));
                    if (rs.getString(4).equals("M")) {
                        cbosexo.setSelectedIndex(1);
                    }
                    if (rs.getString(4).equals("F")) {
                        cbosexo.setSelectedIndex(0);
                    }
                    txtlocalidad.setText(rs.getString(5));
                    id_localidad = rs.getInt(6);
                    txttelfijo.setText("");
                    txttelcelular.setText("");
                    txtmail.setText("");
                    band = 1;
                    banderapaciente = 1;
                    btnPatologias.setEnabled(true);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
            if (band == 1) {
                try {
                    String sSQL = "SELECT id_Pacientes, telefono, celular, mail, fecha_nacimiento FROM pacientes WHERE personas_dni=" + dni;
                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sSQL);
                    while (rs.next()) {
                        txttelfijo.setText(rs.getString("telefono"));
                        txttelcelular.setText(rs.getString("celular"));
                        txtmail.setText(rs.getString("mail"));
                        id_paciente = rs.getInt("id_Pacientes");
                        System.out.println("revertirfecha_1(rs.getString(fecha_nacimiento)) " + revertirfecha_1(rs.getString("fecha_nacimiento")));
                        txtfechanac.setText(revertirfecha_1(rs.getString("fecha_nacimiento")));
                        String fechanac = txtfechanac.getText();
                        txtedadpaciente.setText(edad(revertir(fechanac)));
                        txtfechanac.transferFocus();
                        band2 = 1;
                        banderapaciente = 2;
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
                if (band2 == 0) {
                    banderapaciente = 1;
                    txttelfijo.setText("");
                    txttelcelular.setText("");
                    txtmail.setText("");
                    txtfechanac.setText("01-01-1950");
                    txtedadpaciente.setText("");
                    txtobrasocialpaciente.setText("");
                    txtnumafiliadopaciente.setText("");
                    //cbosexo.setSelectedIndex(0);
                    txttelfijo.requestFocus();
                } else {
                    txtobrasocialpaciente.setText("");
                    txtnumafiliadopaciente.setText("");
                    txtobrasocialpaciente.requestFocus();
                }
            } else {
                banderapaciente = 0;
                // cargarlocalidad();
                txtapellidopaciente.setText("");
                txtnombrepaciente.setText("");
                txtlocalidad.setText("");
                txtdireccion.setText("");
                txttelfijo.setText("");
                txttelcelular.setText("");
                txtmail.setText("");
                txtfechanac.setText("");
                txtedadpaciente.setText("");
                txtobrasocialpaciente.setText("");
                txtnumafiliadopaciente.setText("");
                // cbosexo.setSelectedIndex(0);
                txtapellidopaciente.requestFocus();
            }
        }

        try {
            cn.close();
        } catch (SQLException ex) {
            Logger.getLogger(MainB.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_txtdnipacienteActionPerformed

    private void btnborrarpacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnborrarpacienteActionPerformed
        banderapaciente = 0;
        bandera_paciente = 0;
        limpiarDatosPaciente();

    }//GEN-LAST:event_btnborrarpacienteActionPerformed

    void limpiarDatosPaciente() {
        txtdnipaciente.setText("");
        txtapellidopaciente.setText("");
        txtnombrepaciente.setText("");
        cbosexo.setSelectedIndex(0);
        txtlocalidad.setText("");
        txtdireccion.setText("");
        txttelfijo.setText("");
        txttelcelular.setText("");
        txtmail.setText("");
        id_localidad = 2358;
        txtfechanac.setText("");
        txtedadpaciente.setText("");
        txtobrasocialpaciente.setText("");
        txtnumafiliadopaciente.setText("");
        txtdnipaciente.requestFocus();
    }

    void limpiarDatosPacienteModuloPracticas() {
        txtprotocolo.setText("");
        txtfecha2.setText("");
        txtnombreafiliado1.setText("");
        txtfecha3.setText("");
        txtmedico.setText("");
        cboservicio.setSelectedIndex(0);
        txtcama.setText("");
        txtnumorden2.setText("");
        txtfecha1.setText("");
        txttipo.setText("");
        txtprecio.setText("");
        txtreciennacido.setText("");
        chkcoseguro.setSelected(false);
        borrartabla();
        cargatotalespracticas();
        jTabbedPane3.setSelectedIndex(1);
        jTabbedPane3.setEnabledAt(2, false);
        jTabbedPane3.setEnabledAt(1, true);
        id_paciente = 0;
        id_historia_clinica = 0;
        id_orden = 0;
        id_obra_social = 0;

    }

    void cargarpaciente() {
        String sSQL = "";
        String numero = "";
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        sSQL = "SELECT MAX(id_pacientes) AS id_pacientes FROM pacientes";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.last();
            if (rs.getInt("id_pacientes") != 0) {
                id_paciente = rs.getInt("id_pacientes");
            } else {
                id_paciente = 1;
            }
            ////
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    boolean verifica_campos_afiliado() {
        boolean respuesta = false;
        if (!txtdnipaciente.getText().equals("")
                && !txtapellidopaciente.getText().equals("")
                && !txtnombrepaciente.getText().equals("")
                && !txtobrasocialpaciente.getText().equals("")
                && !txtnumafiliadopaciente.getText().equals("")) {
            respuesta = true;
        } else {
            respuesta = false;
        }
        return respuesta;
    }

    private void btncargarordenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncargarordenActionPerformed
        if (bandera_paciente == 1 && verifica_campos_afiliado() && !txtnumafiliadopaciente.getText().equals("") && !txtobrasocialpaciente.getText().equals("")) {

            jTabbedPane3.setEnabledAt(2, true);
            jTabbedPane3.setSelectedIndex(2);

            observacionesOrdenes = "";
            //new ObservacionOrdenes(null, true).setVisible(true);

            txtmedico.requestFocus();
            cargarhistoriaclinica();
            jPanel13.transferFocus();

        } else {
            JOptionPane.showMessageDialog(null, "El paciente no está dado de alta");
        }
    }//GEN-LAST:event_btncargarordenActionPerformed

    void cursor() {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.pack();
    }

    void cursor2() {
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.pack();
    }

    public static String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "";
    }

    int cargarobrasocial(String codigo) {
        int respuesta = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        System.out.println("codigo os" + codigo);
        String sSQL = "SELECT id_obrasocial FROM obrasocial where Int_codigo_obrasocial=" + codigo + " and estado_obrasocial=1";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                respuesta = rs.getInt("id_obrasocial");
            }
            cn.close();
        } catch (Exception e) {
            respuesta = 0;
            JOptionPane.showMessageDialog(null, "No se pudo conectar al servidor... Verifique su conexión a internet");
        }
        System.out.println("id_obra_social CBT" + respuesta);
        return respuesta;
    }

    double precio_practicas_cbt(double practica) {
        double precio = 0.0;

        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        try {
            Statement St = cn.createStatement();
            ResultSet Rs = St.executeQuery("SELECT codigo_practica,preciototal,determinacion,codigo_fac_practicas_obrasocial FROM obrasocial_tiene_practicasnbu  WHERE id_obrasocial=" + id_obra_social);
            while (Rs.next()) {
                precio = Rs.getDouble(2);

            }
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }

        return precio;
    }

    void grabar_orden() {
        respuestapractica = "";
        int subsidio = 0, bandera_osde = 1, bandera_sw = 1, bandera_boreal = 1, bandera_medife = 1;
        double COSEGURO = 0.0;
        String coseguro;
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        cargarhora();
        int i2 = 0;
        try {
            if (!txtnombrepaciente.getText().equals("") && !txtdnipaciente.getText().equals("") && !txtnumafiliadopaciente.getText().equals("")) {
                if (tablapracticas.getRowCount() != 0) {

                    double total = 0.0, total2 = 0.0, totalordenes = 0.0, seña = 0.0;
                    String precio, servicio, num_orden = "", id_practica, tipo_orden = "", mes, año, cod_practca = "", cod_fac_practca, planes = "", coseguros = "";
                    String sSQL = "", ssQL = "", nombre_practca, aux, nombreRN;
                    int n, cama = 0, n2, n3, i = 0, banderaorden = 0;
                    if (txtcama.getText().equals("")) {
                        cama = 0;
                    } else {
                        cama = Integer.valueOf(txtcama.getText());
                    }

                    if (txtprecio.getText().equals("")) {
                        coseguro = "0";
                    } else {
                        coseguro = txtprecio.getText();
                    }

                    fecha = txtfecha1.getText();
                    servicio = cboservicio.getSelectedItem().toString();
                    if (!txttipo.getText().equals("")) {
                        tipo_orden = txttipo.getText();
                    }
                    nombreRN = txtreciennacido.getText();
                    n2 = tablapracticas.getRowCount();
                    edad = txtedadpaciente.getText();
                    /////////////////TIEMPO DE PROCESAMIENTO////////////////////////////////////////////////////////
                    int h = 0;
                    int aux2 = 0, var2 = 0;
                    while (h < n2) {
                        aux2 = Integer.valueOf(tablapracticas.getValueAt(h, 10).toString());
                        if (aux2 > var2) {
                            var2 = aux2;
                        }
                        h++;
                    }
                    imprime_fila_mesada();
                    System.out.println("Formularios.MainB.grabar_orden()");
                    tiempo_procesamiento = var2;
                    //////////////////////////////////////////////////////////////////////

                    if (banderaorden == 0) {
                        if (banderamodifica == 0) {
                            ////////////////////VALIDACION ONLINE/////////////////////////////////////////
                            /////////////////////OSDE////////////////////////////////
                            if (obra.substring(0, obra.indexOf(" ")).equals("3100")) {
                                String mensajenuevo = "", habilitado;
                                int cantidad = 0;
                                //////////////////////////////////////////////////////////////
                                cargarobrasocial("3100");
                                if (id_obra_social != 0) {
                                    if (tablapracticas.getRowCount() != 0) {
                                        int n4, w = 0;
                                        String cod_practca_osde, nombre_practca_osde;
                                        n4 = tablapracticas.getRowCount();
                                        if (n4 != 0) {
                                            while (w < n4) {
                                                planes = planes + "00";
                                                coseguros = coseguros + "00000.0";
                                                cod_practca = cod_practca + tablapracticas.getValueAt(cantidad, 2).toString().substring(0, 6);
                                                cod_practca_osde = tablapracticas.getValueAt(w, 11).toString();
                                                nombre_practca_osde = tablapracticas.getValueAt(w, 3).toString();
                                                mensajenuevo = mensajenuevo + "<DetalleProcedimientos><NroItem>" + (w + 1) + "</NroItem><CodPrestacion>" + cod_practca_osde + "</CodPrestacion><CodAlternativo></CodAlternativo><TipoPrestacion>" + OsdeAfiliado.tipo_orden + "</TipoPrestacion><ArancelPrestacion>0</ArancelPrestacion><CantidadSolicitada>1</CantidadSolicitada><DescripcionPrestacion>" + nombre_practca_osde + "</DescripcionPrestacion></DetalleProcedimientos>";
                                                w++;
                                                cantidad++;
                                            }
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(null, "No hay practicas en la tabla...");
                                    }
                                    /////////////////////////////////////////////////////////////////////////////////
                                    mensajepractica = "<Mensaje><EncabezadoMensaje><VersionMsj>1.0</VersionMsj><TipoTransaccion>02L</TipoTransaccion><IdMsj>" + hora + "</IdMsj><InicioTrx><FechaTrx>" + fecha + "</FechaTrx><HoraTrx>" + hora + "</HoraTrx></InicioTrx><Financiador><CodigoFinanciador>11</CodigoFinanciador><CuitFinanciador>30546741253</CuitFinanciador></Financiador><Prestador><CuitPrestador>" + cuil + "</CuitPrestador><RazonSocial>" + Enviar_Facturacion.matricula + "</RazonSocial></Prestador></EncabezadoMensaje><EncabezadoAtencion><Efector/><Prescriptor/><Credencial><NumeroCredencial>" + txtnumafiliadopaciente.getText() + "</NumeroCredencial></Credencial><Preautorizacion/><Documentacion/><Atencion/><Diagnostico/><CodFinalizacionTratamiento/><MensajeParaFinanciador/></EncabezadoAtencion>" + mensajenuevo + "</Mensaje>";
                                    HttpOsdePractica http2 = new HttpOsdePractica();
                                    System.out.println("GRABAR Send");
                                    try {
                                        http2.sendGet();
                                    } catch (Exception ex) {
                                        JOptionPane.showMessageDialog(this, ex);
                                    }
                                    int pos = respuestapractica.indexOf("MensajeDisplay");
                                    int pos2 = respuestapractica.indexOf("/MensajeDisplay");
                                    if (respuestapractica.substring(pos + 15, pos + 17).equals("OK")) {
                                        habilitado = respuestapractica.substring(pos + 15, pos + 17);
                                        mensajenuevo = "";
                                        int pos3 = respuestapractica.indexOf("<NroReferencia>");
                                        int pos4 = respuestapractica.indexOf("</NroReferencia>");
                                        num_orden = respuestapractica.substring(pos3 + 15, pos4);
                                        int contador = 0;
                                        while (contador < tablapracticas.getRowCount()) {
                                            tablapracticas.setValueAt(num_orden, contador, 1);
                                            contador++;
                                        }
                                        bandera_osde = 1;
                                    } else {
                                        habilitado = "ERROR";
                                        bandera_osde = 0;
                                    }
                                }
                                /////////////////////////////////////////////////////////////////
                                if (bandera_osde == 1) {
                                    /////////////////////////////////////////////////////
                                    txtnumorden2.setText(num_orden);

                                    //////////////////////////////////////////////////////////////////
                                    valida_orden_colegio orden_colegio = new valida_orden_colegio();
                                    int respuesta = orden_colegio.valida(
                                            Integer.valueOf(periodo),
                                            txtapellidopaciente.getText() + " " + txtnombrepaciente.getText(),
                                            txtdnipaciente.getText(),
                                            txtnumafiliadopaciente.getText(),
                                            Integer.valueOf(matricula_medico),
                                            num_orden,
                                            fechaonline,
                                            Double.valueOf(txttotal3.getText()),
                                            fecha,
                                            hora2,
                                            ip,
                                            id_obra_social,
                                            ElegirBioquimicos.id_colegiado,
                                            cantidad,
                                            cod_practca,
                                            COSEGURO,
                                            fechaonline,
                                            1,
                                            "",
                                            planes,
                                            coseguros);
                                    System.out.println("OSDE 5");
                                    if (respuesta == 1) {
                                        JOptionPane.showMessageDialog(null, "Nro. Transaccion: " + num_orden.substring(3, 9));
                                        int contador = 0;
                                        while (contador < tablapracticas.getRowCount()) {
                                            tablapracticas.setValueAt(num_orden, contador, 1);
                                            contador++;
                                        }
                                    } else {
                                        System.out.println("Anulacion Osde");
                                        mensajeanulacion = "<Mensaje><EncabezadoMensaje><VersionMsj>1.0</VersionMsj><NroReferenciaCancel>" + num_orden + "</NroReferenciaCancel><TipoTransaccion>04A</TipoTransaccion><IdMsj>" + hora + "</IdMsj><InicioTrx><FechaTrx>" + fecha + "</FechaTrx><HoraTrx>" + hora + "</HoraTrx></InicioTrx><Financiador><CodigoFinanciador>11</CodigoFinanciador><CuitFinanciador>30546741253</CuitFinanciador></Financiador><Prestador><CuitPrestador>" + cuil + "</CuitPrestador></Prestador></EncabezadoMensaje><EncabezadoAtencion><Efector/><Prescriptor/><Credencial><NumeroCredencial>" + txtnumafiliadopaciente.getText() + "</NumeroCredencial></Credencial><Atencion><FechaAtencion>" + fecha + "</FechaAtencion></Atencion></EncabezadoAtencion></Mensaje>";
                                        HttpOsdeAnulacion http = new HttpOsdeAnulacion();
                                        System.out.println("Testing 3 - Send Http GET request");
                                        try {
                                            cursor2();
                                            http.sendGet();
                                            bandera_osde = 0;
                                            JOptionPane.showMessageDialog(null, "La orden no fue validada");
                                        } catch (Exception ex) {
                                            cursor2();
                                            bandera_osde = 0;
                                            JOptionPane.showMessageDialog(null, ex);

                                        }
                                    }
                                }
                            }
                            /////////////////////SWISS MEDICAL////////////////////////////////////////////////////////////////////////////////////////
                            if (obra.substring(0, obra.indexOf(" ")).equals("10070")) {
                                String mensajenuevo = "", habilitado;
                                //////////////////////////////////////////////////////////////
                                cargarobrasocial("10070");
                                if (id_obra_social != 0) {
                                    if (tablapracticas.getRowCount() != 0) {
                                        int w2 = 0;
                                        String cod_practca_sw, nombre_practca_sw;

                                        if (tablapracticas.getRowCount() != 0) {
                                            while (w2 < tablapracticas.getRowCount()) {
                                                System.out.println("tablapracticas.getValueAt(w2, 2).toString()" + tablapracticas.getValueAt(w2, 2).toString().substring(0, 6));
                                                cod_practca_sw = tablapracticas.getValueAt(w2, 2).toString().substring(0, 6);
                                                nombre_practca_sw = tablapracticas.getValueAt(w2, 3).toString();
                                                mensajenuevo = mensajenuevo + "<DetalleProcedimientos><NroItem>" + (w2 + 1) + "</NroItem><CodPrestacion>" + cod_practca_sw + "</CodPrestacion><CodAlternativo></CodAlternativo><TipoPrestacion>" + tipo_orden + "</TipoPrestacion><ArancelPrestacion>0</ArancelPrestacion><CantidadSolicitada>1</CantidadSolicitada><DescripcionPrestacion>" + nombre_practca_sw + "</DescripcionPrestacion></DetalleProcedimientos>";
                                                w2++;
                                            }
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(null, "No hay practicas en la tabla...");
                                    }
                                    /////////////////////////////////////////////////////////////////////////////////
                                    mensajepractica = "<Mensaje><EncabezadoMensaje><VersionMsj>1.0</VersionMsj><TipoTransaccion>02L</TipoTransaccion><IdMsj>" + hora + "</IdMsj><InicioTrx><FechaTrx>" + fechaonline + "</FechaTrx><HoraTrx>" + hora + "</HoraTrx></InicioTrx><Financiador><CodigoFinanciador>26</CodigoFinanciador><CuitFinanciador>30654855168</CuitFinanciador></Financiador><Prestador><CuitPrestador>" + cuil + "</CuitPrestador><RazonSocial>" + Enviar_Facturacion.matricula + "</RazonSocial></Prestador></EncabezadoMensaje><EncabezadoAtencion><Efector/><Prescriptor/><Credencial><NumeroCredencial>" + txtnumafiliadopaciente.getText() + "</NumeroCredencial></Credencial><Preautorizacion/><Documentacion/><Atencion/><Diagnostico/><CodFinalizacionTratamiento/><MensajeParaFinanciador/></EncabezadoAtencion>" + mensajenuevo + "</Mensaje>";
                                    //  mensajepractica = "<Mensaje><EncabezadoMensaje><VersionMsj>1.0</VersionMsj><TipoTransaccion>02L</TipoTransaccion><IdMsj>" + hora + "</IdMsj><InicioTrx><FechaTrx>" + fechaosde + "</FechaTrx><HoraTrx>" + hora + "</HoraTrx></InicioTrx><Financiador><CodigoFinanciador>26</CodigoFinanciador><CuitFinanciador>30654855168</CuitFinanciador></Financiador><Prestador><CuitPrestador>" + cuit + "</CuitPrestador><RazonSocial>" + nombre_colegiado + "</RazonSocial></Prestador></EncabezadoMensaje><EncabezadoAtencion><Efector/><Prescriptor/><Credencial><NumeroCredencial>" + txtnumafiliado.getText() + "</NumeroCredencial></Credencial><Preautorizacion/><Documentacion/><Atencion/><Diagnostico/><CodFinalizacionTratamiento/><MensajeParaFinanciador/></EncabezadoAtencion>" + mensajenuevo + "</Mensaje>";
                                    Httpswmpractica http2 = new Httpswmpractica();
                                    System.out.println("GRABAR 2 - Send SWISS");
                                    System.out.println("Matricula" + Enviar_Facturacion.matricula);
                                    System.out.println(Enviar_Facturacion.usuario);
                                    System.out.println(Enviar_Facturacion.contraseña);
                                    System.out.println("cuil " + cuil);
                                    System.out.println("hora" + hora);
                                    System.out.println("fecha " + fecha);
                                    System.out.println("Afiliado" + txtnumafiliadopaciente.getText());
                                    try {
                                        http2.sendGet();
                                    } catch (Exception ex) {
                                        JOptionPane.showMessageDialog(this, ex);
                                    }
                                    int pos = respuestapractica.indexOf("MensajeDisplay");
                                    int pos2 = respuestapractica.indexOf("</MensajeDisplay");
                                    if (respuestapractica.substring(pos + 15, pos + 17).equals("OK")) {
                                        habilitado = respuestapractica.substring(pos + 15, pos + 17);
                                        mensajenuevo = "";
                                        int pos3 = respuestapractica.indexOf("<CodAutorizacion>");
                                        int pos4 = respuestapractica.indexOf("</CodAutorizacion>");
                                        num_orden = respuestapractica.substring(pos3 + 17, pos4);
                                        JOptionPane.showMessageDialog(null, "Nro. Transaccion: " + num_orden);
                                        int contador = 0;
                                        while (contador < tablapracticas.getRowCount()) {
                                            tablapracticas.setValueAt(num_orden, contador, 1);
                                            contador++;
                                        }
                                        bandera_sw = 1;
                                    } else {
                                        habilitado = "ERROR";
                                        bandera_sw = 0;
                                    }
                                }
                            }
                            ////////////////////BOREAL///////////////////////////////////////////////////////////////////////////////////////
                            if (obra.equals("50015 - BOREAL")) {
                                if (!txtmedico.getText().equals("")) {
                                    cursor();
                                    String practicas[] = new String[tablapracticas.getRowCount()];
                                    ///busco la matricula del medico
                                    String cadena = txtmedico.getText(), cod = "";
                                    System.out.println("boreal 1 " + cadena.length());
                                    i = 0;
                                    while (i < cadena.length()) {
                                        if (String.valueOf(cadena.charAt(i)).equals("-")) {
                                            i = cadena.length();
                                        } else {
                                            cod = cod + cadena.charAt(i);
                                        }
                                        i++;
                                    }
                                    int matriculamedico = Integer.valueOf(cod); //////////////////////////////////////////////////////////////
                                    /////////////////////////////////////////////////
                                    int cantidad = 0, cantidad_boreal = 0;
                                    System.out.println("boreal 2");
                                    if (tablapracticas.getRowCount() != 0) {
                                        String cod_practca_boreal;
                                        while (cantidad < tablapracticas.getRowCount()) {
                                            cod_practca_boreal = tablapracticas.getValueAt(cantidad, 2).toString().substring(0, 6);
                                            if (!cod_practca_boreal.equals("660001")) {
                                                practicas[cantidad] = cod_practca_boreal;
                                                totalordenes = totalordenes + Double.valueOf(tablapracticas.getValueAt(cantidad, 4).toString());
                                                cod_practca = cod_practca + cod_practca_boreal;
                                                cantidad_boreal++;
                                            }
                                            cantidad++;
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(null, "No hay practicas en la tabla...");
                                    }
                                    cargarobrasocial("50015");
                                    if (id_obra_social != 0) {
                                        System.out.println("boreal 3");
                                        String emisor = "CBT" + completarceros(Enviar_Facturacion.matricula, 9);
                                        Contraseña_Boreal contraseña = new Contraseña_Boreal();
                                        String clave = contraseña.Boreal_Contraseña();
                                        String num_afiliado = txtnumafiliadopaciente.getText().substring(0, 8);
                                        String tipo_afiliado = txtnumafiliadopaciente.getText().substring(9, 10);
                                        //////////////////////////////////////////////////////////////////
                                        System.out.println("valida boreal");
                                        Valida_Online orden = new Valida_Online();
                                        bandera_boreal = orden.Boreal_online(practicas, cantidad_boreal, emisor, clave, cuil, num_afiliado, tipo_afiliado, fecha2);
                                        System.out.println("boreal 4");
                                        /////////////////////////////////////////////////////////////////
                                        if (bandera_boreal == 1) {
                                            /////////////////////////////////////////////////////
                                            txtnumorden2.setText(orden.get_num_orden());
                                            txtprecio.setText(orden.get_coseguro());

                                            //////////////////////////////////////////////////////////////////
                                            valida_orden_colegio orden_colegio = new valida_orden_colegio();
                                            int respuesta = orden_colegio.valida(
                                                    Integer.valueOf(periodo),
                                                    txtapellidopaciente.getText() + " " + txtnombrepaciente.getText(),
                                                    txtdnipaciente.getText(),
                                                    txtnumafiliadopaciente.getText(),
                                                    matriculamedico,
                                                    orden.get_num_orden(),
                                                    fechaonline,
                                                    totalordenes,
                                                    fecha,
                                                    hora2,
                                                    ip,
                                                    id_obra_social,
                                                    ElegirBioquimicos.id_colegiado,
                                                    cantidad,
                                                    cod_practca,
                                                    Double.valueOf(orden.get_coseguro()),
                                                    fechaonline,
                                                    1,
                                                    "",
                                                    orden.get_plancadena(),
                                                    orden.get_cosegurocadena());
                                            System.out.println("boreal 5");
                                            if (respuesta == 1) {
                                                int contador = 0;
                                                while (contador < tablapracticas.getRowCount()) {
                                                    tablapracticas.setValueAt(orden.get_num_orden(), contador, 1);
                                                    contador++;
                                                }
                                            } else {
                                                cursor2();
                                                //////////////////////////////////////////////////////////////////
                                                System.out.println("Anula boreal");
                                                Anula_Online boreal = new Anula_Online();
                                                bandera_boreal = boreal.Boreal_online(emisor, clave, cuil, orden.get_num_orden());
                                                System.out.println("boreal 6");
                                                /////////////////////////////////////////////////////////////////
                                                JOptionPane.showMessageDialog(null, "La orden no pudo ser grabada");
                                            }
                                        }
                                    }
                                    cursor2();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Debe ingresar los campos obligatorios para la validación online");
                                }
                            }
                            /////////////MEDIFE//////////////////////////////////////////////////////////
                            if (obra.equals("512 - MEDIFE - ONLINE- PRE PAGA C.M.C.  S.A.")) {
                                if (!txtmedico.getText().equals("")) {
                                    cursor();
                                    String practicas[] = new String[tablapracticas.getRowCount()];
                                    ///busco la matricula del medico
                                    String cadena = txtmedico.getText(), cod = "";
                                    System.out.println("medife 1 " + cadena.length());
                                    i = 0;
                                    while (i < cadena.length()) {
                                        if (String.valueOf(cadena.charAt(i)).equals("-")) {
                                            i = cadena.length();
                                        } else {
                                            cod = cod + cadena.charAt(i);
                                        }
                                        i++;
                                    }
                                    int matriculamedico = Integer.valueOf(cod); //////////////////////////////////////////////////////////////
                                    /////////////////////////////////////////////////
                                    int cantidad = 0;
                                    System.out.println("medife 2");

                                    if (id_obra_social != 0) {
                                        if (tablapracticas.getRowCount() != 0) {
                                            String cod_practca_medife;
                                            String cod_practica_CBT;
                                            while (cantidad < tablapracticas.getRowCount()) {
                                                cod_practca_medife = tablapracticas.getValueAt(cantidad, 11).toString();
                                                cod_practica_CBT = tablapracticas.getValueAt(cantidad, 2).toString().substring(0, 6);
                                                System.out.println("cod_practca_medife " + cod_practca_medife);
                                                practicas[cantidad] = cod_practca_medife;
                                                totalordenes = totalordenes + Double.valueOf(tablapracticas.getValueAt(cantidad, 4).toString());
                                                cod_practca = cod_practca + cod_practica_CBT;
                                                cantidad++;
                                            }
                                        } else {
                                            JOptionPane.showMessageDialog(null, "No hay practicas en la tabla...");
                                        }
                                        System.out.println("medife 3");
                                        String emisor = "CBT" + completarceros(Enviar_Facturacion.matricula, 9);
                                        String num_afiliado = txtnumafiliadopaciente.getText();
                                        //////////////////////////////////////////////////////////////////
                                        System.out.println("valida medife");
                                        Valida_Online orden = new Valida_Online();
                                        bandera_medife = orden.Medife_online(practicas, cantidad, emisor, cuil, num_afiliado, fecha2);
                                        System.out.println("medife 4");
                                        //////////////////////////////////// //////////////
                                        if (bandera_medife == 1) {
                                            /////////////////////////////////////////////////////
                                            txtnumorden2.setText(orden.get_num_orden());
                                            txtprecio.setText(orden.get_coseguro());
                                            //////////////////////////////////////////////////////////////////
                                            valida_orden_colegio orden_colegio = new valida_orden_colegio();
                                            int respuesta = orden_colegio.valida(
                                                    Integer.valueOf(periodo),
                                                    txtapellidopaciente.getText() + " " + txtnombrepaciente.getText(),
                                                    txtdnipaciente.getText(),
                                                    txtnumafiliadopaciente.getText(),
                                                    matriculamedico,
                                                    orden.get_num_orden(),
                                                    fechaonline,
                                                    totalordenes,
                                                    fecha,
                                                    hora2,
                                                    ip,
                                                    cargarobrasocial("512"),
                                                    ElegirBioquimicos.id_colegiado,
                                                    cantidad,
                                                    cod_practca,
                                                    Double.valueOf(orden.get_coseguro()),
                                                    fechaonline,
                                                    1,
                                                    "",
                                                    orden.get_plancadena(),
                                                    orden.get_cosegurocadena());
                                            System.out.println("medife 5");
                                            if (respuesta == 1) {
                                                int contador = 0;
                                                while (contador < tablapracticas.getRowCount()) {
                                                    tablapracticas.setValueAt(orden.get_num_orden(), contador, 1);
                                                    contador++;
                                                }
                                            } else {
                                                cursor2();
                                                //////////////////////////////////////////////////////////////////
                                                System.out.println("Anula medife");
                                                Anula_Online medife = new Anula_Online();
                                                medife.Medife_online(txtnumafiliadopaciente.getText(), cuil, orden.get_num_orden());
                                                System.out.println("medife 6");
                                                /////////////////////////////////////////////////////////////////
                                                JOptionPane.showMessageDialog(null, "La orden no pudo ser grabada");
                                            }
                                        }
                                    }
                                    cursor2();

                                }
                            }
                            ///////////////////////////////////////////////////////////////////////////////
                            if (bandera_osde == 1 && bandera_sw == 1 && bandera_boreal == 1) {
                                ////////////////////////   ORDENES   /////////////////////////
                                n2 = tablapracticas.getRowCount();
                                int j = 0;
                                if (n2 != 0) {
                                    num_orden = "0";
                                    cargarhistoriaclinica();
                                    id_orden = Integer.valueOf(tablapracticas.getValueAt(0, 0).toString());
                                    while (j < n2) {
                                        if (!num_orden.equals(tablapracticas.getValueAt(j, 1).toString())) {
                                            num_orden = tablapracticas.getValueAt(j, 1).toString();
                                            /////////////////////////////////////////////////////////////////////
                                            sSQL = "INSERT INTO ordenes(periodo,numero_orden,total,estado_orden,fecha,"
                                                    + "id_Usuarios,id_medicos,id_especialidades,id_Pacientes,servicio, cama, tipo_orden,"
                                                    + " id_obrasocial, nombre_recien_nacido,hora,fecha_de_autorizacion,seña,precio_coseguro,observaciones)"
                                                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                            try {
                                                cargarorden();
                                                ////////////sumo totales//////////////////////////////
                                                i = 0;
                                                n2 = tablapracticas.getRowCount();
                                                if (n2 != 0) {
                                                    while (i < n2) {
                                                        if (num_orden.equals(tablapracticas.getValueAt(i, 1).toString())) {
                                                            total = Double.valueOf(tablapracticas.getValueAt(i, 4).toString());
                                                            total2 = Double.valueOf(tablapracticas.getValueAt(i, 5).toString());
                                                            totalordenes = totalordenes + total + total2;
                                                        }
                                                        i++;
                                                    }
                                                }
                                                PreparedStatement pst = cn.prepareStatement(sSQL);
                                                pst.setString(1, periodo);
                                                pst.setString(2, num_orden);
                                                pst.setDouble(3, Redondear(totalordenes));
                                                pst.setInt(4, 0);
                                                pst.setString(5, revertir(txtfecha1.getText()));
                                                pst.setInt(6, id_usuario);

                                                ///////////////////// Verifico si el campo txtmedico es vacio 
                                                if (txtmedico.getText().equals("")) {
                                                    ///1094 id sin datos medicos
                                                    pst.setInt(7, 1094);
                                                } else {
                                                    pst.setInt(7, id_medico);
                                                }
                                                pst.setInt(8, id_especialidad);
                                                pst.setInt(9, id_paciente);
                                                pst.setString(10, servicio);
                                                pst.setInt(11, cama);
                                                pst.setString(12, tipo_orden);
                                                pst.setInt(13, id_obra_social);
                                                pst.setString(14, nombreRN);
                                                pst.setString(15, hora);
                                                pst.setString(16, revertir(fecha));
//                                                if (!txttotal5.getText().equals("")) {
//                                              pst.setDouble(17, Double.valueOf(txttotal5.getText()));
//                                            }else {
//                                                    pst.setDouble(17, 0.00);
//                                            }
                                                System.out.println("Formularios.MainB.grabar_orden() seña:" + seña_publica);
                                                pst.setDouble(17, seña_publica);
                                                pst.setString(18, coseguro);
                                                pst.setString(19, observacionesOrdenes);
                                                n = pst.executeUpdate();
                                                if (n > 0) {
                                                    System.out.println("modifica la orden");
                                                    //////////////////////////////////////////////////detalle de orden////////////////////
                                                    try {
                                                        i = 0;
                                                        n2 = tablapracticas.getRowCount();
                                                        if (n2 != 0) {
                                                            while (i < n2) {
                                                                if (num_orden.equals(tablapracticas.getValueAt(i, 1).toString())) {
                                                                    String SQL = "INSERT INTO ordenes_tienen_practicas( id_practicas, id_ordenes, precio_practica, cod_practica_fac,factura)"
                                                                            + "VALUES(?,?,?,?,?)";
                                                                    PreparedStatement st = cn.prepareStatement(SQL);
                                                                    id_practica = tablapracticas.getValueAt(i, 6).toString();

                                                                    total = Double.valueOf(tablapracticas.getValueAt(i, 4).toString());
                                                                    if (tablapracticas.getValueAt(i, 11) != null) {
                                                                        cod_fac_practca = tablapracticas.getValueAt(i, 11).toString();
                                                                    } else {
                                                                        cod_fac_practca = tablapracticas.getValueAt(i, 2).toString();
                                                                    }
                                                                    int fac = 0;
                                                                    if (!tablapracticas.getValueAt(i, 8).toString().equals("true")) {
                                                                        fac = 0;
                                                                    } else {
                                                                        fac = 1;
                                                                    }
                                                                    int imprime;
                                                                    if (!tablapracticas.getValueAt(i, 9).toString().equals("true")) {
                                                                        imprime = 0;
                                                                    } else {
                                                                        imprime = 1;
                                                                    }
                                                                    st.setString(1, id_practica);
                                                                    st.setInt(2, id_orden);
                                                                    st.setDouble(3, Redondear(total));
                                                                    st.setString(4, cod_fac_practca);
                                                                    st.setInt(5, fac);
                                                                    n3 = st.executeUpdate();

                                                                    if (n3 > 0) {
                                                                        /////////////////////////////////////resultados//////////////////////
                                                                        String sSQL5 = "SELECT analisis.id_analisis from analisis where analisis.id_practicas=" + id_practica;
                                                                        Statement st5 = cn.createStatement();
                                                                        ResultSet rs5 = st5.executeQuery(sSQL5);
                                                                        /////////////////////////////////////////////////////////////////////////

                                                                        while (rs5.next()) {

                                                                            String SQL2 = "INSERT INTO resultados( id_analisis, id_practicas, id_ordenes, id_Usuarios, id_medicos, id_especialidades, id_Pacientes, resultado, observacion,imprimir_nombre)"
                                                                                    + "VALUES(?,?,?,?,?,?,?,?,?,?)";
                                                                            PreparedStatement st2 = cn.prepareStatement(SQL2);
                                                                            // JOptionPane.showMessageDialog(null, rs5.getString(1));
                                                                            st2.setString(1, rs5.getString(1));
                                                                            st2.setString(2, id_practica);
                                                                            st2.setInt(3, id_orden);
                                                                            st2.setInt(4, id_usuario);
                                                                            st2.setInt(5, id_medico);
                                                                            st2.setInt(6, id_especialidad);
                                                                            st2.setInt(7, id_paciente);
                                                                            st2.setString(8, "-");
                                                                            st2.setString(9, "");
                                                                            System.out.println("imprime:" + imprime);
                                                                            st2.setInt(10, imprime);
                                                                            st2.executeUpdate();
                                                                        }
                                                                    }
                                                                }
                                                                i++;
                                                            }
                                                        }
                                                    } catch (SQLException ex) {
                                                        JOptionPane.showMessageDialog(null, ex);
                                                    }
                                                }
                                                //////////////////////////////////////
                                                String SQL3 = "INSERT INTO historia_clinica( idhistoria_clinica, descripcion, id_ordenes, fecha_entrega)"
                                                        + "VALUES(?,?,?,?)";
                                                PreparedStatement st3 = cn.prepareStatement(SQL3);
                                                st3.setInt(1, id_historia_clinica);
                                                st3.setString(2, txttipo.getText());
                                                st3.setInt(3, id_orden);
                                                st3.setString(4, revertir(txtfecha1.getText()));
                                                st3.executeUpdate();
                                                ///////////////SEÑA////////////////////////////
                                                try {
                                                    //////////////////////////////////////
                                                    String SQL4 = "INSERT INTO seña(seña,idhistoria_clinica)"
                                                            + "VALUES(?,?)";
                                                    PreparedStatement st4 = cn.prepareStatement(SQL4);
//                                                if (!txttotal5.getText().equals("")) {
//                                                    st4.setDouble(1, Double.valueOf(txttotal5.getText()));
//                                                } else {
//                                                    st4.setDouble(1, 0.00);
//                                                }
                                                    System.out.println("en tabla seña-:" + seña_publica);
                                                    st4.setDouble(1, seña_publica);
                                                    st4.setInt(2, id_historia_clinica);
                                                    st4.executeUpdate();
                                                    ///////////////////////////////////////////
                                                } catch (Exception e) {
                                                    JOptionPane.showMessageDialog(null, e);
                                                }

                                                ///////////////CAJA////////////////////////////
                                                try {
                                                    //////////////////////////////////////
                                                    String SQL5 = "INSERT INTO caja(monto,fecha,idHistoriaClinica)"
                                                            + "VALUES(?,?,?)";
                                                    PreparedStatement st5 = cn.prepareStatement(SQL5);
                                                    if (!txttotal5.getText().equals("")) {
                                                        st5.setDouble(1, Double.valueOf(txttotal5.getText()) + Double.valueOf(txttotal2.getText()));
                                                    } else {
                                                        st5.setDouble(1, 0.00);
                                                    }
                                                    st5.setString(2, revertir(fecha));
                                                    st5.setInt(3, id_historia_clinica);
                                                    st5.executeUpdate();
                                                    ///////////////////////////////////////////
                                                } catch (Exception e) {
                                                    JOptionPane.showMessageDialog(null, e);
                                                }
                                            } catch (SQLException ex) {
                                                JOptionPane.showMessageDialog(null, ex);
                                            }
                                            ///////////////////////////////////////////////////////////////////////

                                        }
                                        num_orden = tablapracticas.getValueAt(j, 1).toString();
                                        j++;
                                    }
                                }
                                ///////////////////panel 1////////////////////////////////////
                                banderapaciente = 0;
                                txtdnipaciente.setText("");
                                txtapellidopaciente.setText("");
                                txtnombrepaciente.setText("");
                                txtlocalidad.setText("");
                                txtdireccion.setText("");
                                txttelfijo.setText("");
                                txttelcelular.setText("");
                                txtmail.setText("");
                                txtfechanac.setText("");
                                txtedadpaciente.setText("");
                                txtobrasocialpaciente.setText("");
                                txtnumafiliadopaciente.setText("");
                                cbosexo.setSelectedIndex(0);
                                bandera_paciente = 0;
                                //////////////panel 2////////////////
                                txtmedico.setText("");
                                cboservicio.setSelectedIndex(0);
                                txtcama.setText("");
                                txtnumorden2.setText("");
                                txtfecha1.setText("");
                                txttipo.setText("");
                                chkcoseguro.setSelected(false);
                                txtprecio.setText("");
                                txtinstrucciones.setText("");
                                borrartabla();
                                jTabbedPane3.setSelectedIndex(0);
                                jTabbedPane3.setEnabledAt(2, false);
                                cargartablapacientes();
                                cargatotalespracticas();
                                btncargarorden.setEnabled(false);
                                observacionesOrdenes = "";
                                seña_publica = 0.0;
                            }
                        } else {
                            ////////////////////////////////MODIFICA//////////////////detalle de orden////////////////////                            
                            i = 0;
                            n2 = tablapracticas.getRowCount();
                            double total_orden = 0;
                            int bandera_nueva_orden = 0;
                            if (n2 != 0) {
                                while (i < n2) {
                                    System.out.println("i=" + i);
                                    System.out.println("tablapracticas.getValueAt(i, 12).toString() " + tablapracticas.getValueAt(i, 12).toString());
                                    if (Integer.valueOf(tablapracticas.getValueAt(i, 12).toString()) != 0) {
                                        System.out.println("Formularios.MainB.grabar_orden().modifica.id_orden");
                                        try {
                                            String sSQL3 = "SELECT id_practicas,ordenes_tienen_practicas.id_ordenes FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes = ordenes.id_ordenes INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes WHERE ordenes.estado_orden=0 and historia_clinica.idhistoria_clinica =" + id_historia_clinica + " and ordenes_tienen_practicas.id_practicas=" + tablapracticas.getValueAt(i, 6).toString();

                                            Statement st3 = cn.createStatement();
                                            ResultSet rs3 = st3.executeQuery(sSQL3);
                                            if (!rs3.next()) {
                                                String SQL = "INSERT INTO ordenes_tienen_practicas( id_practicas, id_ordenes, precio_practica, cod_practica_fac,factura)"
                                                        + "VALUES(?,?,?,?,?)";

                                                PreparedStatement st = cn.prepareStatement(SQL);
                                                id_practica = tablapracticas.getValueAt(i, 6).toString();
                                                total = Double.valueOf(tablapracticas.getValueAt(i, 4).toString());
                                                if (tablapracticas.getValueAt(i, 11) != null) {
                                                    cod_fac_practca = tablapracticas.getValueAt(i, 11).toString();
                                                } else {
                                                    cod_fac_practca = tablapracticas.getValueAt(i, 11).toString();
                                                }

                                                //JOptionPane.showMessageDialog(null, "codigo de facturacion:  " + Integer.valueOf(tablapracticas.getValueAt(i, 11).toString()));
                                                int fac = 0;
                                                if (tablapracticas.getValueAt(i, 8).toString().equals("true")) {
                                                    fac = 1;
                                                } else {
                                                    fac = 0;
                                                }
                                                System.out.println("imprimir 2");
                                                int imprime;
                                                if (!tablapracticas.getValueAt(i, 9).toString().equals("true")) {

                                                    imprime = 0;
                                                    System.out.println("imprime" + imprime);
                                                } else {
                                                    System.out.println("no imprime");
                                                    imprime = 1;
                                                }
                                                //JOptionPane.showMessageDialog(null, id_practica);
                                                st.setString(1, id_practica);
                                                st.setInt(2, Integer.valueOf(tablapracticas.getValueAt(i, 12).toString()));
                                                st.setDouble(3, Redondear(total));
                                                st.setString(4, cod_fac_practca);
                                                st.setInt(5, fac);
                                                n3 = st.executeUpdate();
                                                if (n3 > 0) {
                                                    /////////////////////////////////////resultados//////////////////////
                                                    String sSQL5 = "SELECT analisis.id_analisis from analisis where analisis.id_practicas=" + id_practica;
                                                    Statement st5 = cn.createStatement();
                                                    ResultSet rs5 = st5.executeQuery(sSQL5);
                                                    //  JOptionPane.showMessageDialog(null, id_practica);
                                                    /////////////////////////////////////////////////////////////////////////
                                                    while (rs5.next()) {
                                                        // JOptionPane.showMessageDialog(null, "id analisis: " + rs5.getString(1));
                                                        //JOptionPane.showMessageDialog(null, "id analisis: " + Integer.valueOf(tablapracticas.getValueAt(i, 12).toString()));
                                                        String SQL2 = "INSERT INTO resultados( id_analisis, id_practicas, id_ordenes, id_Usuarios, id_medicos, id_especialidades, id_Pacientes, resultado, observacion,imprimir_nombre)"
                                                                + "VALUES(?,?,?,?,?,?,?,?,?,?)";
                                                        PreparedStatement st2 = cn.prepareStatement(SQL2);
                                                        st2.setString(1, rs5.getString(1));
                                                        st2.setString(2, id_practica);
                                                        st2.setInt(3, Integer.valueOf(tablapracticas.getValueAt(i, 12).toString()));
                                                        st2.setInt(4, id_usuario);
                                                        ///////////////////// Verifico si el campo txtmedico es vacio 
                                                        if (txtmedico.getText().equals("")) {
                                                            ///1094 id sin datos medicos
                                                            st2.setInt(5, 1094);
                                                        } else {
                                                            st2.setInt(5, id_medico);;
                                                        }
                                                        st2.setInt(6, id_especialidad);
                                                        st2.setInt(7, id_paciente);
                                                        st2.setString(8, "-");
                                                        st2.setString(9, "");
                                                        st2.setInt(10, imprime);
                                                        st2.executeUpdate();
                                                    }
                                                }
                                            } else {
                                                String SQL = "UPDATE ordenes_tienen_practicas SET factura=? "
                                                        + " WHERE id_ordenes=" + tablapracticas.getValueAt(i, 12).toString() + " AND id_practicas=" + rs3.getInt(1);
                                                PreparedStatement pst = cn.prepareStatement(SQL);
                                                int fac = 0;
                                                if (tablapracticas.getValueAt(i, 8).toString().equals("true")) {
                                                    fac = 1;
                                                } else {
                                                    fac = 0;
                                                }
                                                pst.setInt(1, fac);
                                                n3 = pst.executeUpdate();

                                                System.out.println("modifica 1-:" + seña_publica);

                                                String SQLresultado = "UPDATE resultados SET imprimir_nombre=?  "
                                                        + " WHERE id_ordenes=" + tablapracticas.getValueAt(i, 12).toString() + " AND id_practicas=" + rs3.getInt(1);
                                                PreparedStatement pst1 = cn.prepareStatement(SQLresultado);
                                                int imprime = 0;
                                                if (!tablapracticas.getValueAt(i, 9).toString().equals("true")) {
                                                    ;
                                                    System.out.println("imprime" + imprime);
                                                } else {
                                                    System.out.println("no imprime");
                                                    imprime = 1;
                                                }
                                                pst1.setInt(1, imprime);
                                                pst1.executeUpdate();

                                            }

                                        } catch (SQLException ex) {
                                            JOptionPane.showMessageDialog(null, ex);
                                        }

                                    } else {

                                        bandera_nueva_orden = 1;
                                    }
                                    ///}
                                    i++;
                                }

                                //////////////////////////////////////////////////////////////
                                if (bandera_nueva_orden == 1) {
                                    System.out.println("modifica... agrega....." + bandera_nueva_orden);
                                    int cantidad = 0, j = 0, k = 0;
                                    if (tablapracticas.getRowCount() != 0) {

                                        while (j < tablapracticas.getRowCount()) {
                                            if (Integer.valueOf(tablapracticas.getValueAt(j, 12).toString()) == 0) {
                                                cantidad = cantidad + 1;
                                            }
                                            j++;
                                        }
                                    }

                                    System.out.println("cantidad: " + cantidad);
                                    int plan_ss[] = new int[cantidad];
                                    String coseguro_ss[] = new String[cantidad];
                                    int id_practicas[] = new int[cantidad];
                                    int id_ordenes[] = new int[cantidad];
                                    String precio_practicas[] = new String[cantidad];
                                    String cod_fac[] = new String[cantidad];
                                    int factura[] = new int[cantidad];
                                    j = 0;
                                    k = 0;
                                    if (cantidad != 0) {
                                        while (j < tablapracticas.getRowCount()) {
                                            if (Integer.valueOf(tablapracticas.getValueAt(j, 12).toString()) == 0) {
                                                plan_ss[k] = 00;
                                                coseguro_ss[k] = "00000.0";
                                                id_practicas[k] = Integer.valueOf(tablapracticas.getValueAt(j, 6).toString());
                                                System.out.println("id_practicas " + tablapracticas.getValueAt(j, 6).toString());
                                                id_ordenes[k] = Integer.valueOf(tablapracticas.getValueAt(j, 12).toString());
                                                System.out.println("id_ordenes " + tablapracticas.getValueAt(j, 12).toString());
                                                precio_practicas[k] = tablapracticas.getValueAt(j, 4).toString();
                                                System.out.println("precio_practicas " + tablapracticas.getValueAt(j, 4).toString());
                                                if (tablapracticas.getValueAt(j, 11) != null) {
                                                    cod_fac[k] = tablapracticas.getValueAt(j, 11).toString();
                                                }
                                                System.out.println("cod_fac");
                                                if (tablapracticas.getValueAt(j, 8).toString().equals("true")) {
                                                    factura[k] = 1;
                                                    System.out.println("factura");
                                                } else {
                                                    factura[k] = 0;
                                                    System.out.println("factura");
                                                }
                                                total = Double.valueOf(tablapracticas.getValueAt(j, 4).toString());
                                                System.out.println("total");
                                                k++;
                                            }
                                            j++;
                                            System.out.println("J " + j);
                                        }
                                        System.out.println("Antes");
                                        Cargar_orden validar = new Cargar_orden();
                                        System.out.println(periodo);
                                        System.out.println(txtnumorden2.getText());
                                        System.out.println(total);
                                        System.out.println(txtfecha1.getText());
                                        System.out.println(id_usuario);
                                        System.out.println(id_medico);
                                        System.out.println(id_especialidad);
                                        System.out.println(id_paciente);
                                        System.out.println(servicio);
                                        System.out.println(cama);
                                        System.out.println(tipo_orden);
                                        System.out.println(id_obra_social);
                                        System.out.println(txtreciennacido.getText());
                                        System.out.println(hora);
                                        System.out.println(fecha);
                                        System.out.println(seña_publica);
                                        int respuesta = validar.cargar(
                                                Integer.valueOf(periodo),
                                                txtnumorden2.getText(),
                                                total,
                                                txtfecha1.getText(),
                                                id_usuario,
                                                id_medico,
                                                id_especialidad,
                                                id_paciente,
                                                servicio,
                                                String.valueOf(cama),
                                                tipo_orden,
                                                id_obra_social,
                                                txtreciennacido.getText(),
                                                hora,
                                                fecha,
                                                seña_publica,
                                                coseguro_ss,
                                                plan_ss,
                                                id_practicas,
                                                id_ordenes,
                                                precio_practicas,
                                                cod_fac,
                                                factura,
                                                id_historia_clinica);
                                        System.out.println("Respuesta: " + respuesta);
                                    }
                                }
                                /////////////////////////////////////////
                                int j = 0;
                                String numero_orden_1 = "";
                                String idorden;
                                idorden = tablapracticas.getValueAt(j, 12).toString();
                                while (j < n2) {
                                    if (idorden.equals(tablapracticas.getValueAt(j, 12).toString()) && !idorden.equals("0")) {
                                        total_orden = total_orden + Redondear(Double.valueOf(tablapracticas.getValueAt(j, 4).toString()));
                                        System.out.println("total_orden: " + total_orden);
                                        numero_orden_1 = tablapracticas.getValueAt(j, 1).toString();
                                    } else if (!idorden.equals("0")) {
                                        ////////////////////////////ordenes////////////////////////////////////////////////////////
                                        String sSQL3 = "UPDATE ordenes SET numero_orden=?, total=?, fecha=?, id_Usuarios=?, id_medicos=?,"
                                                + "id_especialidades=?, id_Pacientes=?, servicio=?, cama=?,"
                                                + "tipo_orden=?,"
                                                + "id_obrasocial=?, precio_coseguro=?, nombre_recien_nacido=?,hora=?,observaciones=?, seña=?"
                                                + " WHERE id_ordenes=" + idorden;
                                        PreparedStatement pst = cn.prepareStatement(sSQL3);
                                        pst.setString(1, numero_orden_1);
                                        // JOptionPane.showMessageDialog(null, "Graba el numero antes del else: "+numero_orden_1);
                                        pst.setDouble(2, total_orden);
                                        pst.setString(3, revertir(txtfecha1.getText()));
                                        pst.setInt(4, id_usuario);
                                        pst.setInt(5, id_medico);
                                        pst.setInt(6, id_especialidad);
                                        pst.setInt(7, id_paciente);
                                        pst.setString(8, servicio);
                                        pst.setInt(9, cama);
                                        pst.setString(10, tipo_orden);
                                        pst.setInt(11, id_obra_social);
                                        if (!txtprecio.getText().equals("")) {
                                            pst.setDouble(12, Double.valueOf(txtprecio.getText()));
                                        } else {
                                            pst.setDouble(12, 0.0);
                                        }
                                        pst.setString(13, nombreRN);
                                        pst.setString(14, hora);
                                        pst.setString(15, observacionesOrdenes);
                                        pst.setDouble(16, seña_publica);
                                        pst.executeUpdate();
                                        total_orden = 0;
                                        idorden = tablapracticas.getValueAt(j, 12).toString();
                                        total_orden = total_orden + Redondear(Double.valueOf(tablapracticas.getValueAt(j, 4).toString()));
                                        numero_orden_1 = tablapracticas.getValueAt(j, 1).toString();
                                        ////////////////////////////////////////////

                                        System.out.println("modifica 2-:" + seña_publica);

                                    }
                                    j++;
                                }

                                if (j == n2 && !idorden.equals("0")) {
                                    ////////////////////////////ordenes////////////////////////////////////////////////////////
                                    //JOptionPane.showMessageDialog(null, numero_orden_1);
                                    String sSQL3 = "UPDATE ordenes SET numero_orden=?, total=?, fecha=?, id_Usuarios=?, id_medicos=?,"
                                            + "id_especialidades=?, id_Pacientes=?, servicio=?, cama=?,"
                                            + "tipo_orden=?,"
                                            + "id_obrasocial=?, precio_coseguro=?, nombre_recien_nacido=?,hora=?,observaciones=?, seña=?"
                                            + " WHERE id_ordenes=" + idorden;
                                    PreparedStatement pst = cn.prepareStatement(sSQL3);
                                    pst.setString(1, numero_orden_1);
                                    pst.setDouble(2, total_orden);
                                    pst.setString(3, revertir(txtfecha1.getText()));
                                    pst.setInt(4, id_usuario);
                                    pst.setInt(5, id_medico);
                                    pst.setInt(6, id_especialidad);
                                    pst.setInt(7, id_paciente);
                                    pst.setString(8, servicio);
                                    pst.setInt(9, cama);
                                    pst.setString(10, tipo_orden);
                                    pst.setInt(11, id_obra_social);
                                    if (!txtprecio.getText().equals("")) {
                                        pst.setDouble(12, Double.valueOf(txtprecio.getText()));
                                    } else {
                                        pst.setDouble(12, 0.0);
                                    }
                                    pst.setString(13, nombreRN);
                                    pst.setString(14, hora);
                                    pst.setString(15, observacionesOrdenes);
                                    pst.setDouble(16, seña_publica);
                                    pst.executeUpdate();

                                    System.out.println("modifica 3-:" + seña_publica);
                                }
                            }
                            banderamodifica = 0;
                            banderapaciente = 0;
                            txtdnipaciente.setText("");
                            txtapellidopaciente.setText("");
                            txtnombrepaciente.setText("");
                            txtlocalidad.setText("");
                            txtdireccion.setText("");
                            txttelfijo.setText("");
                            txttelcelular.setText("");
                            txtmail.setText("");
                            txtfechanac.setText("");
                            bandera_paciente = 0;
                            txtedadpaciente.setText("");
                            txtobrasocialpaciente.setText("");
                            txtnumafiliadopaciente.setText("");
                            cbosexo.setSelectedIndex(0);
                            //////////////panel 2////////////////
                            txtmedico.setText("");
                            cboservicio.setSelectedIndex(0);
                            txtcama.setText("");
                            txtnumorden2.setText("");
                            txtfecha1.setText("");
                            txttipo.setText("");
                            chkcoseguro.setSelected(false);
                            txtprecio.setText("");
                            txtinstrucciones.setText("");
                            borrartabla();
                            jTabbedPane3.setSelectedIndex(0);
                            jTabbedPane3.setEnabledAt(2, false);
                            cargartablapacientes();
                            cargatotalespracticas();
                            observacionesOrdenes = "";
                            seña_publica = 0.0;
                        }
                    } else {
                        i = 0;
                        num_orden = tablapracticas.getValueAt(i, 1).toString();
                        n2 = tablapracticas.getRowCount();
                        while (n2 > i) {
                            aux = tablapracticas.getValueAt(i, 1).toString();
                            if (num_orden != aux) {
                                banderaorden = 1;
                                break;
                            } else {
                                banderaorden = 0;
                            }
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No hay practicas en la tabla...");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe ingresar datos sobre el afiliado");
            }
            ////
            cn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "Error en la base de datos");
        }
    }

    private void btngrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btngrabarActionPerformed
        grabar_orden();
    }//GEN-LAST:event_btngrabarActionPerformed

    public String revertir(String entrada) {
        System.out.println("Formularios.MainB.revertir()");
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }
        String salida = "";
        int i = 0;
        /////Año/////
        for (i = 6; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ///Mes///
        for (i = 3; i <= 4; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ////Dia////
        for (i = 0; i <= 1; i++) {
            salida = salida + entrada.charAt(i);
        }

        return salida;

    }

    /*   public String revertirfecha(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }

        String salida = "";
        int i = 0;
        ////Dia////
        for (i = 8; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }

        ///Mes///
        for (i = 5; i <= 6; i++) {
            salida = salida + entrada.charAt(i);
        }
        /////Año/////
        for (i = 0; i <= 3; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";

        salida = salida + "-";

        return salida;

    }*/
    public String revertirfecha_1(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }

        String salida = "";
        int i = 0;
        ////Dia////
        for (i = 8; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ///Mes///
        for (i = 5; i <= 6; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        /////Año/////
        for (i = 0; i <= 3; i++) {
            salida = salida + entrada.charAt(i);
        }
        System.out.println("salida " + salida);
        return salida;

    }

    public String edad(String fechnac) {
        System.out.println("fechnac " + fechnac);
        if ((null == fechnac) || (fechnac.length() <= 1)) {
            return fechnac;
        }

        String años1 = "", meses1 = "", dias1 = "", años2 = "", meses2 = "", dias2 = "", salida = "";
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
        int edad = 0, i = 0, año1 = 0, año2 = 0, mes1 = 0, mes2 = 0, dia1 = 0, dia2 = 0;
        System.out.println("fecha " + fecha);
        /////Año///// 1985-06-21
        for (i = 0; i <= 3; i++) {
            años1 = años1 + fechnac.charAt(i);
            años2 = años2 + fecha.charAt(i);
        }
        System.out.println("años1 " + años1);
        año1 = Integer.valueOf(años1).intValue();
        System.out.println("años2 " + años2);
        año2 = Integer.valueOf(años2).intValue();
        if (año1 < año2) {
            edad = año2 - año1;
        } else {
            if (año1 == año2) {
                edad = 0;
            } else {
                JOptionPane.showMessageDialog(null, "Fecha erronea");
            }

        }
        ///Mes///
        for (i = 5; i <= 6; i++) {
            meses1 = meses1 + fechnac.charAt(i);
            meses2 = meses2 + fecha.charAt(i);
        }
        mes1 = Integer.valueOf(meses1).intValue();
        mes2 = Integer.valueOf(meses2).intValue();
        if (mes1 > mes2) {
            edad = edad - 1;
        } else {
            if (mes1 == mes2) {
                ////Dia////
                for (i = 8; i <= 9; i++) {
                    dias1 = dias1 + fechnac.charAt(i);
                    dias2 = dias2 + fecha.charAt(i);
                }

                dia1 = Integer.valueOf(dias1).intValue();
                dia2 = Integer.valueOf(dias2).intValue();
                if (dia1 > dia2) {
                    edad = edad - 1;
                } else {
                    edad = edad + 1;
                }
            }
        }
        if (edad == 0) {
            edad = mes2 - mes1;
            txtedadpaciente.setText(txtedadpaciente.getText());
            jLabel48.setText("Meses");
        } else {
            txtedadpaciente.setText(txtedadpaciente.getText());
            jLabel48.setText("Años");
        }
        salida = String.valueOf(edad).toString();
        return salida;
    }

    public void validaOS() {

        borrarpractica();
        ///int band = 0;
        obra = txtobrasocialpaciente.getText();

        if (!obra.equals("")) {
            int i = 0;
            while (i < contadorobrasocial) {
                if (obra.equals(obrasocial[i])) {
                    id_obra_social = idobrasocial[i];
                    nbu = añonbu[i];
                    contadorobra = i;
                    System.out.println(obra.substring(0, obra.indexOf(" ")));
                    if (obra.substring(0, obra.indexOf(" ")).equals("3100")) {
                        ////////////////////////////////////////////////////////////////////////////////
                        String sql = "SELECT nombre, apellido, cuit, matricula, usuario, contraseña,id_colegiado FROM laboratorios";
                        ConexionMySQLLocal cc = new ConexionMySQLLocal();
                        Connection cn = cc.Conectar();
                        try {
                            Statement st = cn.createStatement();
                            ResultSet rs = st.executeQuery(sql);
                            rs.last();
                            if (rs.getRow() == 1) {
                                cuil = rs.getString(3);
                                Enviar_Facturacion.matricula = rs.getString(4);
                                Enviar_Facturacion.usuario = rs.getString(5);
                                Enviar_Facturacion.contraseña = rs.getString(6);
                                System.out.println(cuil);
                                id_colegiado = rs.getInt(7);
                            } else {
                                facturacion = 3;
                                new ElegirBioquimicos(null, true).setVisible(true);
                                System.out.println("cuil " + cuil);
                                System.out.println("id_colegiados " + id_colegiado);
                            }
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, ex);
                            JOptionPane.showMessageDialog(null, "Error en la base de datos");

                        }
                    } else {

                        if (obra.substring(0, obra.indexOf(" ")).equals("50015")) {
                            ////////////////////////////////////////////////////////////////////////////////
                            String sql = "SELECT nombre, apellido, cuit, matricula, usuario, contraseña,id_colegiado FROM laboratorios";
                            ConexionMySQLLocal cc = new ConexionMySQLLocal();
                            Connection cn = cc.Conectar();
                            try {
                                Statement st = cn.createStatement();
                                ResultSet rs = st.executeQuery(sql);
                                rs.last();
                                if (rs.getRow() == 1) {
                                    cuil = rs.getString(3);
                                    Enviar_Facturacion.matricula = rs.getString(4);
                                    Enviar_Facturacion.usuario = rs.getString(5);
                                    Enviar_Facturacion.contraseña = rs.getString(6);
                                    ElegirBioquimicos.id_colegiado = rs.getInt(7);
                                    System.out.println(cuil);
                                } else {
                                    facturacion = 3;
                                    new ElegirBioquimicos(null, true).setVisible(true);
                                    System.out.println("cuil " + cuil);
                                    System.out.println("id_colegiados " + id_colegiado);
                                }
                            } catch (SQLException ex) {
                                JOptionPane.showMessageDialog(null, ex);
                                JOptionPane.showMessageDialog(null, "Error en la base de datos");

                            }
                            ///////////////////////////////////////////////////////////////////////////////
                            /* new BorealAfiliado(this, true).setVisible(true);
                            if (BorealAfiliado.habilitado.equals("OK")) {
                                txtnumafiliadopaciente.setText(BorealAfiliado.Codigo_afiliado);
                                //////////////comprobar si es afiliado del lab////////////////////////
                                try {
                                    String sSQL = "SELECT  numero_afiliado FROM pacientes_tienen_obrasociales WHERE id_Pacientes=" + id_paciente + " AND id_obrasocial=" + id_obra_social;
                                    Statement st = cn.createStatement();
                                    ResultSet rs = st.executeQuery(sSQL);
                                    while (rs.next()) {
                                        //// txtnumafiliadopaciente.setText(rs.getString("numero_afiliado"));
                                        banderapaciente = 3;
                                        borrartabla();
                                    }
                                    cn.close();
                                } catch (Exception e) {
                                    JOptionPane.showMessageDialog(null, e);
                                }
                                contadorobra = i;
                                break;

                            }*/
                        } else {
                            ////////////"9000 - ASOCIACION MUTUAL SANCOR"/////////////////////////////////////////////////////
                            if (obra.substring(0, obra.indexOf(" ")).equals("9000")) {
                                ////////////////////////////////////////////////////////////////////////////////
                                String sql = "SELECT nombre, apellido, cuit, matricula, usuario, contraseña FROM laboratorios";
                                ConexionMySQLLocal cc = new ConexionMySQLLocal();
                                Connection cn = cc.Conectar();
                                try {
                                    Statement st = cn.createStatement();
                                    ResultSet rs = st.executeQuery(sql);
                                    rs.last();
                                    if (rs.getRow() == 1) {
                                        cuil = rs.getString(3);
                                        Enviar_Facturacion.matricula = rs.getString(4);
                                        Enviar_Facturacion.usuario = rs.getString(5);
                                        Enviar_Facturacion.contraseña = rs.getString(6);
                                        System.out.println(cuil);
                                    } else {
                                        facturacion = 3;
                                        new ElegirBioquimicos(null, true).setVisible(true);
                                        System.out.println("cuil " + cuil);
                                        System.out.println("id_colegiados " + id_colegiado);
                                    }
                                } catch (SQLException ex) {
                                    JOptionPane.showMessageDialog(null, ex);
                                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");

                                }
                            } else {
                                if (obra.substring(0, obra.indexOf(" ")).equals("1805")) {
                                    ////////////////////////////////////////////////////////////////////////////////
                                    String sql = "SELECT nombre, apellido, cuit, matricula, usuario, contraseña FROM laboratorios";
                                    ConexionMySQLLocal cc = new ConexionMySQLLocal();
                                    Connection cn = cc.Conectar();
                                    try {
                                        Statement st = cn.createStatement();
                                        ResultSet rs = st.executeQuery(sql);
                                        rs.last();
                                        if (rs.getRow() == 1) {
                                            cuil = rs.getString(3);
                                            Enviar_Facturacion.matricula = rs.getString(4);
                                            Enviar_Facturacion.usuario = rs.getString(5);
                                            Enviar_Facturacion.contraseña = rs.getString(6);
                                            System.out.println(cuil);
                                        } else {
                                            facturacion = 3;
                                            new ElegirBioquimicos(null, true).setVisible(true);
                                            System.out.println("cuil " + cuil);
                                            System.out.println("id_colegiados " + id_colegiado);
                                        }
                                    } catch (SQLException ex) {
                                        JOptionPane.showMessageDialog(null, ex);
                                        JOptionPane.showMessageDialog(null, "Error en la base de datos...");

                                    }
                                } else {
                                    if (obra.substring(0, obra.indexOf(" ")).equals("10070")) {
                                        ////////////////////////////////////////////////////////////////////////////////
                                        String sql = "SELECT nombre, apellido, cuit, matricula, usuario, contraseña FROM laboratorios";
                                        ConexionMySQLLocal cc = new ConexionMySQLLocal();
                                        Connection cn = cc.Conectar();
                                        try {
                                            Statement st = cn.createStatement();
                                            ResultSet rs = st.executeQuery(sql);
                                            rs.last();
                                            if (rs.getRow() == 1) {
                                                cuil = rs.getString(3);
                                                Enviar_Facturacion.matricula = rs.getString(4);
                                                Enviar_Facturacion.usuario = rs.getString(5);
                                                Enviar_Facturacion.contraseña = rs.getString(6);
                                                System.out.println(cuil);
                                            } else {
                                                facturacion = 3;
                                                new ElegirBioquimicos(null, true).setVisible(true);
                                                System.out.println("cuil " + cuil);
                                                System.out.println("id_colegiados " + id_colegiado);
                                            }
                                        } catch (SQLException ex) {
                                            JOptionPane.showMessageDialog(null, ex);
                                            JOptionPane.showMessageDialog(null, "Error en la base de datos...");

                                        }
                                    } else {
                                        if (obra.substring(0, obra.indexOf(" ")).equals("512")) {
                                            ////////////////////////////////////////////////////////////////////////////////
                                            String sql = "SELECT nombre, apellido, cuit, matricula, usuario, contraseña,id_colegiado FROM laboratorios";
                                            ConexionMySQLLocal cc = new ConexionMySQLLocal();
                                            Connection cn = cc.Conectar();
                                            try {
                                                Statement st = cn.createStatement();
                                                ResultSet rs = st.executeQuery(sql);
                                                rs.last();
                                                if (rs.getRow() == 1) {
                                                    cuil = rs.getString(3);
                                                    Enviar_Facturacion.matricula = rs.getString(4);
                                                    Enviar_Facturacion.usuario = rs.getString(5);
                                                    Enviar_Facturacion.contraseña = rs.getString(6);
                                                    ElegirBioquimicos.id_colegiado = rs.getInt(7);
                                                    System.out.println(cuil);
                                                } else {
                                                    facturacion = 3;
                                                    new ElegirBioquimicos(null, true).setVisible(true);
                                                    System.out.println("cuil " + cuil);
                                                    System.out.println("id_colegiados " + id_colegiado);
                                                }
                                            } catch (SQLException ex) {
                                                JOptionPane.showMessageDialog(null, ex);
                                                JOptionPane.showMessageDialog(null, "Error en la base de datos");

                                            }
                                            ///////////////////////////////////////////////////////////////////////////////
                                            /*  new BorealAfiliado(this, true).setVisible(true);
                                            if (BorealAfiliado.habilitado.equals("OK")) {
                                                txtnumafiliadopaciente.setText(BorealAfiliado.Codigo_afiliado);
                                                //////////////comprobar si es afiliado del lab////////////////////////
                                                try {
                                                    String sSQL = "SELECT  numero_afiliado FROM pacientes_tienen_obrasociales WHERE id_Pacientes=" + id_paciente + " AND id_obrasocial=" + id_obra_social;
                                                    Statement st = cn.createStatement();
                                                    ResultSet rs = st.executeQuery(sSQL);
                                                    while (rs.next()) {
                                                        //// txtnumafiliadopaciente.setText(rs.getString("numero_afiliado"));
                                                        banderapaciente = 3;
                                                        borrartabla();
                                                    }
                                                    cn.close();
                                                } catch (Exception e) {
                                                    JOptionPane.showMessageDialog(null, e);
                                                }
                                                contadorobra = i;
                                                break;

                                            }*/
                                        } else {
                                            ConexionMySQLLocal cc = new ConexionMySQLLocal();
                                            Connection cn = cc.Conectar();
                                            try {
                                                String sSQL = "SELECT  numero_afiliado FROM pacientes_tienen_obrasociales WHERE id_Pacientes=" + id_paciente + " AND id_obrasocial=" + id_obra_social;
                                                Statement st = cn.createStatement();
                                                ResultSet rs = st.executeQuery(sSQL);
                                                while (rs.next()) {
                                                    txtnumafiliadopaciente.setText(rs.getString("numero_afiliado"));
                                                    banderapaciente = 3;
                                                    borrartabla();
                                                }
                                                cn.close();
                                            } catch (Exception e) {
                                                JOptionPane.showMessageDialog(null, e);
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                i++;
            }
            txtnumafiliadopaciente.requestFocus();
        }
    }

    public void validaAfiliado() {

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        String dni, ap, nom, fn, dir, sexo, numaf, tel, mail, cel, obs, loc;
        String sSQL = "", sSQL2 = "", sSQL3 = "";
        dni = txtdnipaciente.getText();
        ap = txtapellidopaciente.getText();
        nom = txtnombrepaciente.getText();
        if (!txtfechanac.getText().equals("  -  -    ")) {
            fn = revertir(txtfechanac.getText());
        } else {
            fn = "1900-01-01";
        }
        if (cbosexo.getSelectedItem().toString().equals("Masculino")) {
            sexo = "M";
        } else {
            sexo = "F";
        }
        loc = txtlocalidad.getText();
        dir = txtdireccion.getText();
        if (!txttelfijo.getText().equals("")) {
            tel = txttelfijo.getText();
        } else {
            tel = "0";
        }
        if (!txttelcelular.getText().equals("")) {
            cel = txttelcelular.getText();
        } else {
            cel = "0";
        }
        mail = txtmail.getText();
        obs = txtobrasocialpaciente.getText();
        numaf = txtnumafiliadopaciente.getText();
        try {
            String sSQLaf = "SELECT  numero_afiliado FROM pacientes_tienen_obrasociales WHERE id_Pacientes=" + id_paciente + " AND id_obrasocial=" + id_obra_social;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQLaf);
            System.out.println("numero_afiliado 1");
            while (rs.next()) {
                System.out.println("numero_afiliado 2");
                if (!rs.wasNull()) {
                    System.out.println("numero_afiliado 3");
                    txtnumafiliadopaciente.setText(rs.getString("numero_afiliado"));
                    banderapaciente = 3;
                    borrartabla();
                }

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        System.out.println("banderapaciente: " + banderapaciente);
        if (banderapaciente == 0) {
            if (!dni.equals("") && !ap.equals("") && !nom.equals("") && !obs.equals("") && !numaf.equals("")) {
                try {
                    sSQL = "INSERT INTO personas(dni, apellido, nombre, "
                            + "domicilio, sexo, id_localidad, id_provincia)"
                            + "VALUES(?,?,?,?,?,?,?)";

                    PreparedStatement pst = cn.prepareStatement(sSQL);
                    pst.setString(1, dni);
                    pst.setString(2, ap);
                    pst.setString(3, nom);
                    pst.setString(4, dir);
                    pst.setString(5, sexo);
                    pst.setInt(6, id_localidad);
                    pst.setInt(7, 25);
                    int n = pst.executeUpdate();
                    if (n > 0) {
                        sSQL2 = "INSERT INTO pacientes(telefono,celular, mail, fecha_nacimiento, "
                                + "personas_dni)"
                                + "VALUES(?,?,?,?,?)";

                        PreparedStatement pst2 = cn.prepareStatement(sSQL2);
                        pst2.setString(1, tel);
                        pst2.setString(2, cel);
                        pst2.setString(3, mail);
                        pst2.setString(4, fn);
                        pst2.setString(5, dni);
                        int n2 = pst2.executeUpdate();
                        if (n2 > 0) {
                            cargarpaciente();
                            sSQL3 = "INSERT INTO pacientes_tienen_obrasociales(id_Pacientes, id_obrasocial, numero_afiliado)"
                                    + "VALUES(?,?,?)";

                            PreparedStatement pst3 = cn.prepareStatement(sSQL3);
                            pst3.setInt(1, id_paciente);
                            pst3.setInt(2, id_obra_social);
                            pst3.setString(3, numaf);
                            int n3 = pst3.executeUpdate();
                            if (n3 > 0) {
                                txtnombreafiliado1.setText(ap + " " + nom);
                                cargarperiodo();
                                // JOptionPane.showMessageDialog(null, banderapaciente);
                                // cargarpracticas();
                                cargarpracticasparticular();
                                cargarpracticaconobra();
                                // jTabbedPane3.setSelectedIndex(2);
                                //txtmedico.requestFocus();
                                txtnumafiliadopaciente.transferFocus();
                                txtmedico.requestFocus();
                            }
                        }
                    }

                    /////
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Error 1. Ocurrio un problema... Intente nuevamente llenando los datos del paciente");
                    JOptionPane.showMessageDialog(null, e);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe llenar los datos del paciente...");
            }
        } /////////////////////////////////////////////////////////

        if (banderapaciente == 1) {
            if (!dni.equals("") && !ap.equals("") && !nom.equals("") && !obs.equals("") && !numaf.equals("")) {
                //if (!dni.equals("") && !ap.equals("") && !nom.equals("") && !fn.equals("") && !sexo.equals("") && !loc.equals("") && !dir.equals("") && !tel.equals("") && !mail.equals("") && !cel.equals("") && !obs.equals("") && !numaf.equals("")) {
                try {
                    sSQL2 = "INSERT INTO pacientes(telefono,celular, mail, fecha_nacimiento, "
                            + "personas_dni)"
                            + "VALUES(?,?,?,?,?)";
                    PreparedStatement pst2 = cn.prepareStatement(sSQL2);
                    pst2.setString(1, tel);
                    pst2.setString(2, cel);
                    pst2.setString(3, mail);
                    pst2.setString(4, fn);
                    pst2.setString(5, dni);

                    int n2 = pst2.executeUpdate();
                    System.out.println("paciente 1");
                    if (n2 > 0) {
                        cargarpaciente();
                        System.out.println("paciente 2");
                        sSQL3 = "INSERT INTO pacientes_tienen_obrasociales(id_Pacientes, id_obrasocial, numero_afiliado)"
                                + "VALUES(?,?,?)";

                        PreparedStatement pst3 = cn.prepareStatement(sSQL3);
                        pst3.setInt(1, id_paciente);
                        pst3.setInt(2, id_obra_social);
                        pst3.setString(3, numaf);
                        int n3 = pst3.executeUpdate();
                        System.out.println("paciente 3");
                        if (n3 > 0) {
                            System.out.println("paciente 4");
                            txtnombreafiliado1.setText(ap + " " + nom);
                            cargarperiodo();
                            ///  JOptionPane.showMessageDialog(null, banderapaciente);
                            cargarpracticaconobra();
                            //cargarpracticas();
                            cargarpracticasparticular();
                            //  jTabbedPane3.setSelectedIndex(2);
                            // txtmedico.requestFocus();
                            txtnumafiliadopaciente.transferFocus();
                            txtmedico.requestFocus();
                        }
                    }
                    ////
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Error 2. Ocurrio un problema... Intente nuevamente llenando los datos del paciente");
                    JOptionPane.showMessageDialog(null, e);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe llenar los datos del paciente...");
            }
        }
        if (banderapaciente == 2) {
            if (!dni.equals("") && !ap.equals("") && !nom.equals("") && !obs.equals("") && !numaf.equals("")) {
                //if (!dni.equals("") && !ap.equals("") && !nom.equals("") && !fn.equals("") && !sexo.equals("") && !loc.equals("") && !dir.equals("") && !tel.equals("") && !mail.equals("") && !cel.equals("") && !obs.equals("") && !numaf.equals("")) {
                try {
                    sSQL3 = "INSERT INTO pacientes_tienen_obrasociales(id_Pacientes, id_obrasocial, numero_afiliado)"
                            + "VALUES(?,?,?)";

                    PreparedStatement pst3 = cn.prepareStatement(sSQL3);
                    pst3.setInt(1, id_paciente);
                    pst3.setInt(2, id_obra_social);
                    pst3.setString(3, numaf);
                    int n3 = pst3.executeUpdate();
                    if (n3 > 0) {
                        txtnombreafiliado1.setText(ap + " " + nom);
                        cargarperiodo();
                        /// JOptionPane.showMessageDialog(null, banderapaciente);
                        cargarpracticaconobra();
                        //cargarpracticas();
                        cargarpracticasparticular();
                        // jTabbedPane3.setSelectedIndex(2);
                        //txtmedico.requestFocus();
                        txtnumafiliadopaciente.transferFocus();
                        txtmedico.requestFocus();
                    }
                    ///
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Error 3. Ocurrio un problema... Intente nuevamente llenando los daotos del paciente");
                    JOptionPane.showMessageDialog(null, e);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe llenar los datos del paciente...");
            }
        }
        if (banderapaciente == 3) {
            txtnombreafiliado1.setText(ap + " " + nom);
            cargarperiodo();
            cargarpracticaconobra();
            //cargarpracticas();
            cargarpracticasparticular();
            txtnumafiliadopaciente.transferFocus();
            //txtmedico.requestFocus();

        }

        try {
            cn.close();

        } catch (SQLException ex) {
            Logger.getLogger(MainB.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void txtapellidopacienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtapellidopacienteKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtapellidopaciente.transferFocus();
        }
    }//GEN-LAST:event_txtapellidopacienteKeyPressed

    private void txtnombrepacienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombrepacienteKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtnombrepaciente.transferFocus();
        }
    }//GEN-LAST:event_txtnombrepacienteKeyPressed

    private void cbosexoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbosexoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cbosexo.transferFocus();
        }
    }//GEN-LAST:event_cbosexoKeyPressed

    private void txtlocalidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlocalidadKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

        }
    }//GEN-LAST:event_txtlocalidadKeyPressed

    private void txtdireccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdireccionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtdireccion.transferFocus();
        }
    }//GEN-LAST:event_txtdireccionKeyPressed

    private void txttelfijoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelfijoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txttelfijo.transferFocus();
        }
    }//GEN-LAST:event_txttelfijoKeyPressed

    private void txttelcelularKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelcelularKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txttelcelular.transferFocus();
        }
    }//GEN-LAST:event_txttelcelularKeyPressed

    private void txtmailKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmailKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtmail.transferFocus();
        }
    }//GEN-LAST:event_txtmailKeyPressed

    private void txtfechanacKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfechanacKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txtfechanac.getText().equals("  -  -    ")) {
                txtfechanac.setText("01-01-1950");
                String fechanac = txtfechanac.getText();
                txtedadpaciente.setText(edad(revertir(fechanac)));
                txtfechanac.transferFocus();
            } else {
                String fechanac = txtfechanac.getText();
                txtedadpaciente.setText(edad(revertir(fechanac)));
                txtfechanac.transferFocus();
            }

        }
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            if (txtfechanac.getText().equals("  -  -    ")) {//
                txtfechanac.setText("01-01-1950");
                String fechanac = txtfechanac.getText();
                txtedadpaciente.setText(edad(revertir(fechanac)));
                txtfechanac.transferFocus();
            } else {
                String fechanac = txtfechanac.getText();
                txtedadpaciente.setText(edad(revertir(fechanac)));
                txtfechanac.transferFocus();
            }
        }
    }//GEN-LAST:event_txtfechanacKeyPressed

    private void txtobrasocialpacienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobrasocialpacienteKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtobrasocialpaciente.getText().equals("")) {
                if (txtnumafiliadopaciente.getText().equals("")) {
                    txtnumafiliadopaciente.setText("0");
                }
                txtnumafiliadopaciente.requestFocus();
            }
        }
    }//GEN-LAST:event_txtobrasocialpacienteKeyPressed

    private void txtmedicoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmedicoKeyPressed
        /* if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

         }*/
    }//GEN-LAST:event_txtmedicoKeyPressed

    private void txtmedicoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmedicoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmedicoKeyReleased

    private void cboservicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboservicioActionPerformed

    }//GEN-LAST:event_cboservicioActionPerformed

    private void txtnumorden2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnumorden2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnumorden2ActionPerformed

    private void txtnumorden2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnumorden2KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtfecha1.select(0, 0);
            txtnumorden2.transferFocus();
        }
    }//GEN-LAST:event_txtnumorden2KeyPressed

    private void txtnumorden2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnumorden2KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnumorden2KeyReleased

    private void btnaceptar3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptar3ActionPerformed
        new Proxima_Orden(this, true).setVisible(true);
        if (!numorden.equals("")) {
            txtnumorden2.setText(numorden);
            txtpractica2.requestFocus();
        }
    }//GEN-LAST:event_btnaceptar3ActionPerformed

    private void cboservicioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cboservicioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (cboservicio.getSelectedItem().toString().equals("Ambulatorio")) {
                txtnumorden2.requestFocus();
            } else {
                jLabel31.setEnabled(true);
                txtcama.setEnabled(true);
                txtcama.requestFocus();
            }
        }
    }//GEN-LAST:event_cboservicioKeyPressed

    private void chkcoseguroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chkcoseguroKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (chkcoseguro.isSelected() == true) {
                jLabel56.setEnabled(true);
                txtprecio.setEnabled(true);
            } else {
                jLabel56.setEnabled(false);
                txtprecio.setEnabled(false);
            }
        }
        chkcoseguro.transferFocus();
    }//GEN-LAST:event_chkcoseguroKeyPressed

    private void txtlocalidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtlocalidadActionPerformed
        int band = 0;
        String loc = txtlocalidad.getText();
        if (!loc.equals("")) {

            int i = 0;
            while (i < contadorlocalidad) {
                if (loc.equals(nombrelocalidad[i])) {
                    id_localidad = idlocalidad[i];
                    contadorlocalidad = i;
                    break;
                }
                i++;
            }
            txtlocalidad.transferFocus();
        }
    }//GEN-LAST:event_txtlocalidadActionPerformed

    private void txttotal2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotal2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotal2ActionPerformed

    private void txttotal3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotal3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotal3ActionPerformed

    private void txttotal4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotal4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotal4ActionPerformed

    private void txtfecha1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfecha1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfecha1ActionPerformed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        facturacion = 0;
        new ElegirBioquimicos(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void btnsalir9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir9ActionPerformed
        jTabbedPane3.setSelectedIndex(0);
    }//GEN-LAST:event_btnsalir9ActionPerformed

    private void btnsalir11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir11ActionPerformed
        btnborrarpaciente.doClick();
        jTabbedPane3.setSelectedIndex(0);
        jTabbedPane3.setEnabledAt(2, false);
        observacionesOrdenes = "";
        seña_publica = 0.0;
    }//GEN-LAST:event_btnsalir11ActionPerformed

    private void jMenuItem16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem16ActionPerformed
        new Analisis(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem16ActionPerformed


    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        new Usuarios(this, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    void imprime_fila_mesada() {
        /////////////////FORMATO DE MESADA ///////////////////////////////////////////////////////
        int h = 0, m = 0;
        int aux2 = 0, var2 = 0;
        while (h < tablapracticas.getRowCount()) {
            if (tablapracticas.getValueAt(h, 9).toString().equals("false")) {
                System.out.println("informe_mesada_false " + Integer.valueOf(tablapracticas.getValueAt(h, 6).toString()));
                informe_mesada_false[m] = Integer.valueOf(tablapracticas.getValueAt(h, 6).toString());
                m++;
            }
            h++;
        }
        indice_if_false = m;
        System.out.println("indice_if_false " + indice_if_false);
    }

    void imprimir_tabla_practicas() {
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        try {
            String sql7 = "Select horientacion from reportes where id_reportes=1";
            Statement St7 = cn.createStatement();
            ResultSet rs7 = St7.executeQuery(sql7);
            rs7.next();
            int mesada = rs7.getInt(1);
            //////////////////////////////////////////////////////////////////
            JDialog viewer = new JDialog(new javax.swing.JFrame(), "Reporte", true);
            viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
            viewer.setSize(800, 600);
            viewer.setLocationRelativeTo(null);
            System.out.println("previsualizacion definida");
            JasperReport report = null;
            ///////////////////////////////////////////////////////////////////
            Map parametros = new HashMap();
            parametros.put("id_protocolo", completarceros(String.valueOf(id_historia_clinica), 10));
            parametros.put("fecha_entrega", agregartiempo(fecha, tiempo_procesamiento));
            parametros.put("edad", edad);
            if (mesada == 0) {
                report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Informe_Mesada.jasper"));
            } else if (mesada == 1) {
                report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Informe_Mesada_2.jasper"));
            } else if (mesada == 2) {
                report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Informe_Mesada_3.jasper"));
            } else if (mesada == 3) {
                report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Informe_Mesada_4.jasper"));
            } else if (mesada == 4) {
                report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Informe_Mesada_5.jasper"));
            } else {
                report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Informe_Mesada_6.jasper"));
            }

            JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, cn);
            JasperViewer jv = new JasperViewer(jPrint, false);
            viewer.getContentPane().add(jv.getContentPane());
            viewer.setVisible(true);

            cn.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }


    private void btngrabarimprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btngrabarimprimirActionPerformed
        grabar_orden();
        imprimir_tabla_practicas();

    }//GEN-LAST:event_btngrabarimprimirActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        facturacion = 1;
        new ElegirBioquimicos(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void btnsalir17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir17ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalir17ActionPerformed

    private void btnsalir18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir18ActionPerformed
        jTabbedPane3.setSelectedIndex(0);
    }//GEN-LAST:event_btnsalir18ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new Protocolos(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void ResultadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResultadosActionPerformed
        int totalRow = tablapacientes.getRowCount();
        if (totalRow != -1) {
            Informe.id_orden = Integer.valueOf(tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString());
            String fech = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 5).toString();
            System.out.println("fech " + fech);
            fechaPaciente = invertir(fech);
            System.out.println("fech " + fechaPaciente);
            nombre_paciente = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 1).toString() + " " + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 2).toString();
            mail = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 13).toString();
            System.out.println("id_orden " + Informe.id_orden + " fechaPaciente " + fechaPaciente + " nombre_paciente " + nombre_paciente);
            new Resultados(null, true).setVisible(true);
        }
    }//GEN-LAST:event_ResultadosActionPerformed

    private void modificaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificaActionPerformed
        if (banderamodifica == 1) {
            btnborrar2.doClick();
        }
        borrarmedicos();
        cargarmatricula();
        DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
        int fila = 0, bandera_limpiar = 0;
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        try {
            String sSQL = "SELECT personas.apellido,personas.nombre, personas.domicilio, personas.sexo, localidad.nombre_localidad FROM personas INNER JOIN localidad ON localidad.id_localidad = personas.id_localidad WHERE dni=" + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 3).toString();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            if (rs.next()) {
                txtdnipaciente.setText(tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 3).toString());
                //  JOptionPane.showMessageDialog(null, "1");
                txtapellidopaciente.setText(rs.getString(1));
                // JOptionPane.showMessageDialog(null, "2");
                txtnombrepaciente.setText(rs.getString(2));
                //JOptionPane.showMessageDialog(null, "3");
                txtlocalidad.setText(rs.getString(5));
                //JOptionPane.showMessageDialog(null, "4");
                txtdireccion.setText(rs.getString(3));
                //JOptionPane.showMessageDialog(null, "5");
                if (rs.getString(4).equals("M")) {
                    cbosexo.setSelectedIndex(0);
                    //  JOptionPane.showMessageDialog(null, "6");
                } else {
                    cbosexo.setSelectedIndex(1);
                    //    JOptionPane.showMessageDialog(null, "6");
                }
                txtnombreafiliado1.setText(rs.getString("personas.apellido") + " " + rs.getString("personas.nombre"));
                // JOptionPane.showMessageDialog(null, "7");
                txttelfijo.setText("");
                // JOptionPane.showMessageDialog(null, "8");
                txttelcelular.setText("");
                //JOptionPane.showMessageDialog(null, "9");
                txtmail.setText("");
                //JOptionPane.showMessageDialog(null, "10");
                banderapaciente = 1;
                //JOptionPane.showMessageDialog(null, "11");
            }
            ///
        } catch (Exception e) {
            bandera_limpiar = 1;
            JOptionPane.showMessageDialog(null, e);
        }
        try {
            String sSQL = "SELECT id_Pacientes, telefono, celular, mail, fecha_nacimiento FROM pacientes WHERE personas_dni=" + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 3).toString();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            if (rs.next()) {
                txttelfijo.setText(rs.getString("telefono"));
                //JOptionPane.showMessageDialog(null, "12");
                txttelcelular.setText(rs.getString("celular"));
                //JOptionPane.showMessageDialog(null, "13");
                // JOptionPane.showMessageDialog(null, "14");
                txtmail.setText(rs.getString("mail"));
                //JOptionPane.showMessageDialog(null, "15");
                id_paciente = rs.getInt("id_Pacientes");
                //JOptionPane.showMessageDialog(null, "16");
                txtfechanac.setText(revertirfecha_1(rs.getString("fecha_nacimiento")));
                //JOptionPane.showMessageDialog(null, "17");
                String fechanac = txtfechanac.getText();
                // JOptionPane.showMessageDialog(null, "18");
                txtedadpaciente.setText(edad(revertir(fechanac)));
                // JOptionPane.showMessageDialog(null, "19");
                txtfechanac.transferFocus();
                // JOptionPane.showMessageDialog(null, "20");
                id_obra_social = Integer.valueOf(tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 10).toString());
                System.out.println("id_obrasocial: " + id_obra_social);
                // JOptionPane.showMessageDialog(null, "21");
                banderapaciente = 2;

            }
            ////
        } catch (Exception e) {
            bandera_limpiar = 1;
            JOptionPane.showMessageDialog(null, e);
        }
        try {
            String sSQL = "SELECT  numero_afiliado , obrasocial.Int_codigo_obrasocial, obrasocial.razonsocial_obrasocial FROM pacientes_tienen_obrasociales inner join obrasocial on obrasocial.id_obrasocial=pacientes_tienen_obrasociales.id_obrasocial WHERE pacientes_tienen_obrasociales.id_Pacientes=" + id_paciente + " AND pacientes_tienen_obrasociales.id_obrasocial=" + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 10).toString();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            if (rs.next()) {
                System.out.println("txtnumafiliadopaciente: " + rs.getString(1));
                System.out.println("txtobrasocialpaciente: " + rs.getString(2) + " - " + rs.getString(3));
                txtnumafiliadopaciente.setText(rs.getString(1));
                // JOptionPane.showMessageDialog(null, "21");
                txtobrasocialpaciente.setText(rs.getString(2) + " - " + rs.getString(3));
                // JOptionPane.showMessageDialog(null, "22");
                banderapaciente = 3;
                borrarpractica();
                ///cargarpracticaconobra();
                ///borrartabla();
                id_obra_social = Integer.valueOf(tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 10).toString());
                // JOptionPane.showMessageDialog(null, "23");
                // JOptionPane.showMessageDialog(null,"antes de cargar"+ id_obra_social);
                cargarpracticaconobra();
                //cargarpracticas();
                cargarpracticasparticular();
            }
            /////
        } catch (Exception e) {
            bandera_limpiar = 1;
            JOptionPane.showMessageDialog(null, e);
        }
        try {
//            String sSQL = "SELECT ordenes.*, medicos.apellido, medicos.nombre, medicos.matricula,seña.seña FROM ordenes INNER JOIN medicos ON medicos.id_medicos = ordenes.id_medicos INNER JOIN medicos_tienen_especialidades ON medicos_tienen_especialidades.id_medicos = ordenes.id_medicos INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes WHERE ordenes.estado_orden=0 and historia_clinica.idhistoria_clinica =" + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString();

            String sSQL = "SELECT ordenes.*, medicos.apellido, medicos.nombre, medicos.matricula,seña.seña \n"
                    + "FROM ordenes INNER JOIN medicos ON medicos.id_medicos = ordenes.id_medicos \n"
                    + "INNER JOIN medicos_tienen_especialidades ON medicos_tienen_especialidades.id_medicos = ordenes.id_medicos \n"
                    + "INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes \n"
                    + "Left join seña using (idhistoria_clinica)\n"
                    + "WHERE ordenes.estado_orden=0 and historia_clinica.idhistoria_clinica =" + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString();

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            if (rs.next()) {
                if (!rs.getString(3).equals("?")) {
                    id_medico = rs.getInt(8);
                    //     JOptionPane.showMessageDialog(null, "24");
                    id_paciente = rs.getInt(10);
                    //     JOptionPane.showMessageDialog(null, rs.getString("medicos.matricula"));
                    //     JOptionPane.showMessageDialog(null, rs.getString("medicos.apellido"));
                    //     JOptionPane.showMessageDialog(null, rs.getString("medicos.nombre"));
                    txtmedico.setText(rs.getString("medicos.matricula") + "-" + rs.getString("medicos.apellido") + " " + rs.getString("medicos.nombre"));
                    //     JOptionPane.showMessageDialog(null, "26");
                    cboservicio.setSelectedItem(rs.getString(11));
                    //     JOptionPane.showMessageDialog(null, "27");
                    txtcama.setText(rs.getString(12));
                    //     JOptionPane.showMessageDialog(null, "28");
                    txtnumorden2.setText(rs.getString(3));
                    //     JOptionPane.showMessageDialog(null, "29");
                    txtfecha1.setText(revertirfecha_1(rs.getString(6)));
                    //     JOptionPane.showMessageDialog(null, "30");
                    txtfecha2.setText(revertirfecha_1(rs.getString(6)));
                    //     JOptionPane.showMessageDialog(null, "31");
                    txttipo.setText(rs.getString(13));
                    //     JOptionPane.showMessageDialog(null, "32");
                    txtprecio.setText(rs.getString(19));
                    //     JOptionPane.showMessageDialog(null, "33");
                    txtreciennacido.setText(rs.getString(20));
                    //     JOptionPane.showMessageDialog(null, "34");
                    id_especialidad = rs.getInt(9);
                    //     JOptionPane.showMessageDialog(null, "35");
                    System.out.println(rs.getString(23));
                    txttotal5.setText(rs.getString(23));

                    observacionesOrdenes = (rs.getString("observaciones"));
                    if (observacionesOrdenes == null) {
                        observacionesOrdenes = "";
                    }
                    String señas = rs.getString("seña");
                    if (señas != null) {
                        seña_publica = rs.getDouble("seña");
                    } else {
                        seña_publica = 0.0;
                    }

                    System.out.println("Formularios.MainB.modificaActionPerformed():" + observacionesOrdenes);
                } else {
                    bandera_limpiar = 1;
                }
            }
            /////
        } catch (Exception e) {
            bandera_limpiar = 1;
            JOptionPane.showMessageDialog(null, e);
        }
        try {
            String sSQL3 = "select * from vista_ordenes_tiene_practicas WHERE estado_orden=0 and idhistoria_clinica =" + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString();
            //String sSQL3 = "SELECT ordenes.numero_orden, practicas.codigo_practica ,practicas.determinacion_practica , ordenes_tienen_practicas.*,practicas.precio1,practicas.precio2,practicas.precio3,practicas.precio4, practicas.tipo_informe FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes = ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas = ordenes_tienen_practicas.id_practicas INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes WHERE ordenes.estado_orden=0 and historia_clinica.idhistoria_clinica =" + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString();
            Statement st3 = cn.createStatement();
            ResultSet rs3 = st3.executeQuery(sSQL3);
            while (rs3.next()) {
                Object nuevo[] = {
                    fila + 1, "", ""};
                temp.addRow(nuevo);
                tablapracticas.setValueAt(rs3.getString(1), fila, 1);
                tablapracticas.setValueAt(rs3.getString(2), fila, 2);
                tablapracticas.setValueAt(rs3.getString(3), fila, 3);
                tablapracticas.setValueAt(rs3.getString(6), fila, 4);
                tablapracticas.setValueAt(0, fila, 5);
                tablapracticas.setValueAt(rs3.getString(4), fila, 6);
                tablapracticas.setValueAt(rs3.getString(11), fila, 7);
                if (rs3.getInt(8) == 1) {
                    tablapracticas.setValueAt(true, fila, 8);
                } else {
                    tablapracticas.setValueAt(false, fila, 8);
                }
                if (rs3.getInt("codigo_practica") == 660475 || rs3.getInt("codigo_practica") == 660711) {
                    System.out.println("ingresa al 475 o al 711: " + rs3.getInt("codigo_practica"));
                    tablapracticas.setValueAt(false, fila, 9);
                } else {
                    tablapracticas.setValueAt(true, fila, 9);
                }
                tablapracticas.setValueAt(rs3.getString(15), fila, 10);
                tablapracticas.setValueAt(rs3.getString(7), fila, 11);
                tablapracticas.setValueAt(rs3.getString(5), fila, 12);
                fila++;
                filamodifica = rs3.getString(5);
                ////////////////////////////////////////////////////////////////////                    
                tablapracticas.getColumnModel().getColumn(11).setMaxWidth(0);
                tablapracticas.getColumnModel().getColumn(11).setMinWidth(0);
                tablapracticas.getColumnModel().getColumn(11).setPreferredWidth(0);
                ////////////////////////////////////////////////////////////////////                
            }
            /////
        } catch (Exception e) {
            bandera_limpiar = 1;
            JOptionPane.showMessageDialog(null, e);
        }
        if (bandera_limpiar == 1) {
            JOptionPane.showMessageDialog(null, "No se pudo acceder para hacer la modificación");
            btnborrar2.doClick();
        } else {
            jTabbedPane3.setEnabledAt(2, true);
            jTabbedPane3.setEnabledAt(1, false);
            jTabbedPane3.setSelectedIndex(2);
            txtmedico.requestFocus();
            banderamodifica = 1;
            System.out.println(tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString());
            id_historia_clinica = Integer.valueOf(tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString());
            txtprotocolo.setText(completarceros(tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString(), 10));
            cargatotalespracticas();
            txtpractica2.setText("");
        }
        try {
            cn.close();

        } catch (SQLException ex) {
            Logger.getLogger(MainB.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_modificaActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        new Alta_Bioquimicos(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void btnimprimirdjj1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimirdjj1ActionPerformed
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        new PeriodoImprime(this, true).setVisible(true);
        DecimalFormat df = new DecimalFormat("0.00");
        if (!periododjj.equals("") || !PeriodoImprime.mes.equals("") || !PeriodoImprime.año.equals("")) {
            /////////////////////////////imprime reporte////////////////////////////////
            int n, aux = 0, i = 0, j = 0, pacientes = 0, practicas = 0, band = 0;
            LinkedList<camposordenes> Resultados = new LinkedList<camposordenes>();
            Resultados.clear();
            String orden = "";
            int cantidad = tablaordenes.getRowCount(), contador = 0;
            int cantidad2 = tabladetalle.getRowCount(), cantidad3 = 0, k = 0;
            ///DecimalFormat df = new DecimalFormat("0.00");
            progreso.setMaximum(cantidad);
            double pesos = 0, centavos = 0;
            double totalpesospracticas = 0, total;
            System.out.println(ObraSocial + " - " + tablaordenes.getRowCount());
            if (ObraSocial != null) {
                if (!ObraSocial.equals("") && tablaordenes.getRowCount() != 0) {
                    total = 0.0;
                    while (i < cantidad) {
                        int id_ordenes = 0;
                        camposordenes tipo;
                        j = aux;
                        while (j < cantidad2) {
                            if (tablaordenes.getValueAt(i, 7).equals(true) && id_ordenes == 0) {
                                System.out.println(tabladetalle.getValueAt(j, 5).toString());
                                tipo = new camposordenes(
                                        tablaordenes.getValueAt(i, 8).toString(),
                                        tablaordenes.getValueAt(i, 3).toString(),
                                        tablaordenes.getValueAt(i, 4).toString(),
                                        tabladetalle.getValueAt(j, 1).toString(),
                                        tabladetalle.getValueAt(j, 2).toString(),
                                        tabladetalle.getValueAt(j, 3).toString(),
                                        tabladetalle.getValueAt(j, 4).toString(),
                                        tablaordenes.getValueAt(i, 5).toString().substring(8, 10) + "/" + tablaordenes.getValueAt(i, 5).toString().substring(5, 7) + "/" + tablaordenes.getValueAt(i, 5).toString().substring(0, 4));
                                Resultados.add(tipo);
                                id_ordenes = Integer.valueOf(tablaordenes.getValueAt(i, 0).toString());
                                pacientes = pacientes + 1;
                                total = Math.round(total);
                                total = total + Math.round(Double.valueOf(tabladetalle.getValueAt(j, 3).toString()));
                                practicas = practicas + 1;
                            } else {
                                if (tablaordenes.getValueAt(i, 7).equals(true) && id_ordenes == Integer.valueOf(tabladetalle.getValueAt(j, 0).toString())) {

                                    tipo = new camposordenes(
                                            "''",
                                            "''",
                                            tablaordenes.getValueAt(i, 4).toString(),
                                            tabladetalle.getValueAt(j, 1).toString(),
                                            tabladetalle.getValueAt(j, 2).toString(),
                                            tabladetalle.getValueAt(j, 3).toString(),
                                            tabladetalle.getValueAt(j, 4).toString(),
                                            tablaordenes.getValueAt(i, 5).toString().substring(8, 10) + "/" + tablaordenes.getValueAt(i, 5).toString().substring(5, 7) + "/" + tablaordenes.getValueAt(i, 5).toString().substring(0, 4));
                                    Resultados.add(tipo);
                                    total = Math.round(total);
                                    total = total + Math.round(Double.valueOf(tabladetalle.getValueAt(j, 3).toString()));
                                    practicas = practicas + 1;
                                } else {
                                    aux = j;
                                    j = cantidad2 - 1;
                                }
                            }
                            System.out.println(total);
                            contador++;
                            progreso.setValue(contador);
                            j++;
                        }
                        i++;
                    }
                    String mes = PeriodoImprime.mes, año = PeriodoImprime.año, cadena = PeriodoImprime.mes;
                    //String mes = "", año = tablaordenes.getValueAt(0, 1).toString().substring(0, 4), cadena = tablaordenes.getValueAt(0, 1).toString().substring(4, 6);
                    if (cadena.equals("01")) {
                        mes = "Enero";
                    }
                    if (cadena.equals("02")) {
                        mes = "Febrero";
                    }
                    if (cadena.equals("03")) {
                        mes = "Marzo";
                    }
                    if (cadena.equals("04")) {
                        mes = "Abril";
                    }
                    if (cadena.equals("05")) {
                        mes = "Mayo";
                    }
                    if (cadena.equals("06")) {
                        mes = "Junio";
                    }
                    if (cadena.equals("07")) {
                        mes = "Julio";
                    }
                    if (cadena.equals("08")) {
                        mes = "Agosto";
                    }
                    if (cadena.equals("09")) {
                        mes = "Septiembre";
                    }
                    if (cadena.equals("10")) {
                        mes = "Octubre";
                    }
                    if (cadena.equals("11")) {
                        mes = "Noviembre";
                    }
                    if (cadena.equals("12")) {
                        mes = "Diciembre";
                    }
                    facturacion = 2;
                    new ElegirBioquimicos(this, true).setVisible(true);
                    String periodo3 = mes + " " + año;

                    centavos = Redondearcentavos(total);
                    pesos = Redondear(Redondear(total) - Redondear(centavos) / 100);
                    String centa = String.valueOf(centavos);
                    /// centa.substring(band)
                    total_centavos_letras = NumberToLetterConverter.convertNumberToLetter(centavos);
                    total_pesos_letras = NumberToLetterConverter.convertNumberToLetter(pesos);
                    try {
                        String sql2 = "SELECT logo,nombre,direccion,telefono,mail FROM reportes";
                        Statement st2 = cn.createStatement();
                        ResultSet rs2 = st2.executeQuery(sql2);
                        if (rs2.next()) {
                            byte[] img = rs2.getBytes(1);
                            if (img != null) {
                                try {
                                    imagen = ConvertirImagen(img);
                                } catch (IOException ex) {
                                    JOptionPane.showMessageDialog(null, ex);
                                }
                                Icon icon = new ImageIcon(imagen);
                                txtlogo.setIcon(icon);
                            } else {
                                txtlogo.setIcon(null);
                            }
                            Nombre_Lab.nombrelab = rs2.getString(2);
                            Nombre_Lab.direccionlab = rs2.getString(3);
                            Nombre_Lab.telefonolab = rs2.getString(4);
                            Nombre_Lab.maillab = rs2.getString(5);
                        }
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                    try {
                        JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ordenes.jasper"));
                        ///////////////////////////////////////////////////C:\Users\Lucas\Documents\NetBeansProjects\colegio bioquimicos\src\Reportes
                        Map parametros = new HashMap();
                        parametros.put("foto", imagen);
                        parametros.put("nombre", Nombre_Lab.nombrelab);
                        parametros.put("direccion", Nombre_Lab.direccionlab);
                        parametros.put("telefono", Nombre_Lab.telefonolab);
                        parametros.put("mail", Nombre_Lab.maillab);
                        parametros.put("matricula", matricula_colegiado);
                        parametros.put("matricula", matricula_colegiado);
                        parametros.put("matricula", matricula_colegiado);
                        parametros.put("periodo", periodo3);
                        parametros.put("fecha", fecha2);
                        parametros.put("obra_social", ObraSocial);
                        parametros.put("num_obra_social", CodObra);
                        parametros.put("laboratorio", nombre_colegiado);
                        parametros.put("domicilio_lab", direccion);
                        parametros.put("localidad", lugar);
                        parametros.put("pacientes", String.valueOf(pacientes));
                        parametros.put("practicas", String.valueOf(practicas));
                        parametros.put("total_letras_pesos", total_pesos_letras + " PESOS");
                        parametros.put("total_letras_centavos", total_centavos_letras + " CENTAVOS");
                        parametros.put("total", String.valueOf(total));
                        JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados));
                        JasperExportManager.exportReportToPdfFile(jPrint, "C:\\PDF-FACTURACION\\" + periodo + "-" + matricula_colegiado + "-" + CodObra + ".pdf");
                        JasperViewer.viewReport(jPrint, false);
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                    try {
                        Process p = Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL " + "C:\\Descargas-CBT\\" + periodo + "-" + CodObra + "-" + matricula_colegiado + ".pdf");
                    } catch (Exception evvv) {
                        JOptionPane.showMessageDialog(null, "No se puede abrir el archivo de ayuda, probablemente fue borrado", "ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "El resumen debe ser hecho por obra social");
            }
        }

        try {
            cn.close();

        } catch (SQLException ex) {
            Logger.getLogger(MainB.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_btnimprimirdjj1ActionPerformed

    public double Redondeardosdigitos(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    private void fechainicialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechainicialKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            fechainicial.transferFocus();
            fechafinal.select(0, 0);
        }
    }//GEN-LAST:event_fechainicialKeyPressed

    private void fechafinalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fechafinalActionPerformed


    }//GEN-LAST:event_fechafinalActionPerformed

    String completarceros(String v, int d) {
        if (v.length() < d) {
            String ceros = "";
            for (int i = v.length(); i < d; i++) {
                ceros += "0";
            }
            v = ceros + v;
        }
        return v;
    }

    private void fechafinalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechafinalKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnFiltrar.requestFocus();
        }
    }//GEN-LAST:event_fechafinalKeyPressed


    private void tablaordenesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaordenesMouseClicked
        if (tablaordenes.getSelectedColumn() == 7) {
            clic_practicatodas();
            cargatotalesordenesfacturacion();
        }
    }//GEN-LAST:event_tablaordenesMouseClicked

    private void tablaordenesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaordenesKeyPressed
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        try {
            ////////////////////////////ordenes////////////////////////////////////////////////////////
            String sSQL3 = "UPDATE ordenes SET numero_orden=? "
                    + " WHERE id_ordenes=" + tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0).toString();
            PreparedStatement pst = cn.prepareStatement(sSQL3);
            pst.setString(1, tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 4).toString());
            int i = pst.executeUpdate();
            if (i > 0) {
                JOptionPane.showMessageDialog(null, "Se modifico con exito...");
            }
            cn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_tablaordenesKeyPressed

    private void txttotal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotal1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotal1ActionPerformed

    private void txtordenes1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtordenes1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtordenes1ActionPerformed

    private void txtordenes1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtordenes1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtordenes1KeyPressed

    void filtrarordenes(String valor) {
        //estos seran los titulos de la tabla.
        String[] Titulo = {"Orden", "Periodo", "Obra Social", "Nombre Afiliado", "N° Orden", "Fecha Orden", "Total"};
        String[] Registros = new String[7];
        String sql = "SELECT ordenes.id_ordenes, ordenes.periodo, obrasocial.codigo_obrasocial, personas.apellido, ordenes.numero_orden, ordenes.fecha, round(ordenes.total,2) FROM ordenes INNER JOIN pacientes ON pacientes.id_Pacientes = ordenes.id_Pacientes INNER JOIN personas ON personas.dni = pacientes.personas_dni INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.estado_orden=0 and Date(fecha) BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "'AND CONCAT(id_ordenes, ' ',numero_orden ,' ',personas.dni,' ',personas.apellido)"
                + "LIKE '%" + valor + "%'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();

        try {
            /////////////////////////////////////////////////////
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                ///  if (id_usuario == rs.getInt("id_colegiados")) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                model.addRow(Registros);

                tablaordenes.setModel(model);
                /////////////////////////////////////////////////////////////
                //  tablaordenes.getColumnModel().getColumn(0).setMaxWidth(0);
                //  tablaordenes.getColumnModel().getColumn(0).setMinWidth(0);
                // tablaordenes.getColumnModel().getColumn(0).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////
                alinear();
                tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
                // tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
                // }
            }
            /////
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        ////////////////////////////////////////////////////////////
        String[] titulos2 = {"N° Orden", "cod_practica"};//estos seran los titulos de la tabla.            
        String[] datos2 = new String[2];
        model = new DefaultTableModel(null, titulos2) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        try {

            Statement St = cn.createStatement();
            ResultSet Rs = St.executeQuery("SELECT ordenes.id_ordenes, practicas.codigo_practica FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas INNER JOIN pacientes ON pacientes.id_Pacientes = ordenes.id_Pacientes INNER JOIN personas ON personas.dni = pacientes.personas_dni  WHERE ordenes.estado_orden=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' AND CONCAT(ordenes.id_ordenes, ' ',numero_orden ,' ',personas.dni,' ',personas.apellido)"
                    + "LIKE '%" + valor + "%'");
            String estado = "";
            while (Rs.next()) {
                datos2[0] = Rs.getString(1);
                datos2[1] = Rs.getString(2);
                model.addRow(datos2);
            }
            tabladetalle.setModel(model);
            /////////////////////////////////////////////////////////////
            //   tablaordenes.getColumnModel().getColumn(0).setMaxWidth(0);
            ///     tablaordenes.getColumnModel().getColumn(0).setMinWidth(0);
            //  tablaordenes.getColumnModel().getColumn(0).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////////
            ////
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);

        }

        try {
            cn.close();
            ////////////////////////////////////////////////////////////

        } catch (SQLException ex) {
            Logger.getLogger(MainB.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void txtordenes1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtordenes1KeyReleased
        /*  if (!txtordenes1.getText().equals("")) {
         filtrarordenes(txtordenes1.getText());
         // 
         }*/

        TableRowSorter sorter = new TableRowSorter(model3);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtordenes1.getText() + ".*"));
        tablaordenes.setRowSorter(sorter);
    }//GEN-LAST:event_txtordenes1KeyReleased

    private void txttotalordenesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotalordenesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotalordenesActionPerformed

    private void txtmedicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmedicoActionPerformed
        if (!txtmedico.getText().equals("")) {
            ConexionMySQLLocal mysql = new ConexionMySQLLocal();
            Connection cn = mysql.Conectar();
            int band = 0;
            String cod = "", nom = "", cadena = txtmedico.getText();
            int i = 0, j = 1;
            //
            while (i < cadena.length()) {
                if (String.valueOf(cadena.charAt(i)).equals("-")) {
                    j = i + 2;
                    i = cadena.length();
                } else {
                    cod = cod + cadena.charAt(i);
                }
                i++;
            }

            if (isNumeric(cod)) {
                matricula_medico = cod;
                try {
                    String sSQLmedico = "SELECT id_medicos, matricula, nombre, apellido FROM medicos where matricula=" + matricula_medico + " and estado=1";
                    Statement stmedico = cn.createStatement();
                    ResultSet rsmedico = stmedico.executeQuery(sSQLmedico);
                    if (!rsmedico.wasNull()) {
                        rsmedico.next();
                        id_medico = rsmedico.getInt(1);
                        try {
                            String sSQL4 = "SELECT id_especialidades FROM medicos_tienen_especialidades where id_medicos=" + id_medico;
                            Statement st4 = cn.createStatement();
                            ResultSet rs4 = st4.executeQuery(sSQL4);
                            if (!rs4.wasNull()) {
                                rs4.next();
                                id_especialidad = rs4.getInt(1);
                                band = 1;
                            }
                        } catch (Exception e) {
                            ////  JOptionPane.showMessageDialog(null, e);
                        }
                    }
                    /////
                } catch (Exception e) {
                    ////  JOptionPane.showMessageDialog(null, e+"+");
                }
                txtmedico.transferFocus();

                if (band == 0) {
                    new Agregar_Medico(this, true).setVisible(true);
                    borrarmedicos();
                    cargarmatricula2();
                    txtmedico.setText(medico);
                    cboservicio.requestFocus();

                }
            } else {
                new Agregar_Medico(this, true).setVisible(true);
                borrarmedicos();
                cargarmatricula2();
                txtmedico.setText(medico);
                cboservicio.requestFocus();

            }
            try {
                cn.close();

            } catch (SQLException ex) {
                Logger.getLogger(MainB.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            txtmedico.transferFocus();
            id_medico = 1;
        }

    }//GEN-LAST:event_txtmedicoActionPerformed

    void cargarmatricula2() {
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        textAutoAcompletermatricula2.removeAllItems();
        String sSQL = "SELECT medicos.id_medicos,medicos.nombre, medicos.apellido, medicos_tienen_especialidades.matricula FROM medicos INNER JOIN medicos_tienen_especialidades ON medicos_tienen_especialidades.id_medicos = medicos.id_medicos where estado=1";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            contadormedico = 0;
            while (rs.next()) {
                idmedico[contadormedico] = rs.getInt(1);
                textAutoAcompletermatricula2.addItem(rs.getInt(4) + "-" + rs.getString(3) + " " + rs.getString(2));
                contadormedico++;
            }
            ////
            cn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        textAutoAcompletermatricula2.setMode(0);
        textAutoAcompletermatricula2.setCaseSensitive(false);
    }

    void borrartablaordenes() {
        DefaultTableModel temp = (DefaultTableModel) tablaordenes.getModel();
        int a = temp.getRowCount() - 1;  //Índices van de 0 a n-1
        for (int i = a; i >= 0; i--) {
            temp.removeRow(i);
        }
    }

    private void btncancelar3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelar3ActionPerformed
        borrartablaordenes();
        progreso.setValue(0);
        txttotal1.setText("");
        txttotalordenes.setText("0");
    }//GEN-LAST:event_btncancelar3ActionPerformed

    private void btnobrasociales6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnobrasociales6ActionPerformed
        jTabbedPane3.setSelectedIndex(1);
    }//GEN-LAST:event_btnobrasociales6ActionPerformed

    private void btnobrasociales7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnobrasociales7ActionPerformed
        fechainicial.setLocation(0, 0);
        jTabbedPane3.setSelectedIndex(3);

    }//GEN-LAST:event_btnobrasociales7ActionPerformed

    private void btnobrasociales8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnobrasociales8ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnobrasociales8ActionPerformed

    private void txtreciennacidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtreciennacidoActionPerformed
        txtreciennacido.transferFocus();        // TODO add your handling code here:
    }//GEN-LAST:event_txtreciennacidoActionPerformed

    private void txtreciennacidoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtreciennacidoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtreciennacidoKeyReleased

    private void txtreciennacidoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtreciennacidoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtreciennacidoKeyPressed

    private void chkcoseguroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkcoseguroActionPerformed
        if (chkcoseguro.isSelected() == true) {
            jLabel56.setEnabled(true);
            txtprecio.setEnabled(true);
            chkcoseguro.transferFocus();
        } else {
            jLabel56.setEnabled(false);
            txtprecio.setEnabled(false);
            chkcoseguro.transferFocus();
        }
    }//GEN-LAST:event_chkcoseguroActionPerformed

    private void txtprecioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtprecioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtprecio.transferFocus();
        }
    }//GEN-LAST:event_txtprecioKeyPressed

    private void txtprotocoloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtprotocoloActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtprotocoloActionPerformed

    private void txtprotocoloKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtprotocoloKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtprotocoloKeyReleased

    private void txtprotocoloKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtprotocoloKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtprotocoloKeyPressed

    private void txtdnipacienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtdnipacienteMouseClicked
        txtdnipaciente.selectAll();
    }//GEN-LAST:event_txtdnipacienteMouseClicked

    private void txtobrasocialpacienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtobrasocialpacienteMouseClicked
        /// txtobrasocialpaciente.selectAll();
    }//GEN-LAST:event_txtobrasocialpacienteMouseClicked

    private void fechainicialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fechainicialMouseClicked
        fechainicial.selectAll();
    }//GEN-LAST:event_fechainicialMouseClicked

    private void fechafinalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fechafinalMouseClicked
        fechafinal.selectAll();
    }//GEN-LAST:event_fechafinalMouseClicked

    private void txtmedicoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtmedicoMouseClicked
        txtmedico.selectAll();
    }//GEN-LAST:event_txtmedicoMouseClicked

    private void txtnumorden2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtnumorden2MouseClicked
        txtnumorden2.selectAll();
    }//GEN-LAST:event_txtnumorden2MouseClicked

    private void txtfecha1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtfecha1MouseClicked
        txtfecha1.selectAll();
    }//GEN-LAST:event_txtfecha1MouseClicked

    private void btnborrar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnborrar2ActionPerformed
        banderapaciente = 0;
        bandera_paciente = 0;
        limpiarDatosPaciente();
        limpiarDatosPacienteModuloPracticas();
        banderamodifica = 0;
        ///cargartablapacientes();
        txtdnipaciente.requestFocus();
        observacionesOrdenes = "";
        seña_publica = 0.0;
        ///////////////////////////////////
    }//GEN-LAST:event_btnborrar2ActionPerformed

    private void btnmodificarpacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarpacienteActionPerformed
        if (!txtdnipaciente.getText().equals("")) {
            ConexionMySQLLocal mysql = new ConexionMySQLLocal();
            Connection cn = mysql.Conectar();
            String sSQL7;
            sSQL7 = "UPDATE personas SET domicilio=?, id_localidad=?   WHERE dni='" + txtdnipaciente.getText() + "'";
            try {
                PreparedStatement pst2 = cn.prepareStatement(sSQL7);
                pst2.setString(1, txtdireccion.getText());
                pst2.setInt(2, id_localidad);
                int n5 = pst2.executeUpdate();
                if (n5 > 0) {
                    bandera_paciente = 1;
                    String sSQL6;
                    sSQL6 = "UPDATE pacientes SET telefono=?, celular=?,mail=?,fecha_nacimiento=?  WHERE personas_dni='" + txtdnipaciente.getText() + "'";
                    try {
                        PreparedStatement pst = cn.prepareStatement(sSQL6);
                        pst.setString(1, txttelfijo.getText());
                        pst.setString(2, txttelcelular.getText());
                        pst.setString(3, txtmail.getText());
                        pst.setString(4, revertir(txtfechanac.getText()));
                        n5 = pst.executeUpdate();
                        if (n5 > 0) {
                            bandera_paciente = 2;
                            String sSQL5;
                            sSQL5 = "UPDATE pacientes_tienen_obrasociales SET numero_afiliado=? WHERE id_Pacientes=" + id_paciente + " and id_obrasocial=" + id_obra_social;
                            try {
                                PreparedStatement pst3 = cn.prepareStatement(sSQL5);
                                pst3.setString(1, txtnumafiliadopaciente.getText());
                                n5 = pst3.executeUpdate();
                                if (n5 > 0) {
                                    bandera_paciente = 3;
                                }
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(null, "Ocurrió un problema, no se pudo actualizar...Error 8866");
                            }
                        }

                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Ocurrió un problema, no se pudo actualizar...Error 8871");
                    }
                }
                if (bandera_paciente == 1 || bandera_paciente == 2 || bandera_paciente == 3) {
                    JOptionPane.showMessageDialog(null, "Se actualizó correctamente");
                }
                cn.close();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ocurrió un problema, no se pudo actualizar...Error 8876");
            }
            ////////////////////////////////////////////////////////////////
        }
        ////////////////////////////////////////////////////////
    }//GEN-LAST:event_btnmodificarpacienteActionPerformed

    private void cargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cargarActionPerformed
        DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
        int fila = 0;
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();

        try {
            String sSQL = "SELECT personas.apellido,personas.nombre, personas.domicilio, personas.sexo, localidad.nombre_localidad, localidad.id_localidad FROM personas INNER JOIN localidad ON localidad.id_localidad = personas.id_localidad WHERE dni=" + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 3).toString();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                txtdnipaciente.setText(tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 3).toString());
                txtapellidopaciente.setText(rs.getString(1));
                txtnombrepaciente.setText(rs.getString(2));
                txtlocalidad.setText(rs.getString(5));
                id_localidad = rs.getInt(6);
                txtdireccion.setText(rs.getString(3));
                if (rs.getString(4).equals("M")) {
                    cbosexo.setSelectedIndex(1);
                } else {
                    cbosexo.setSelectedIndex(0);
                }////personas.apellido,personas.nombre
                txtnombreafiliado1.setText(rs.getString("personas.apellido") + " " + rs.getString("personas.nombre"));
                txttelfijo.setText("");
                txttelcelular.setText("");
                txtmail.setText("");
                banderapaciente = 1;
            }
            ////
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        try {
            String sSQL = "SELECT id_Pacientes, telefono, celular, mail, fecha_nacimiento FROM pacientes WHERE personas_dni=" + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 3).toString();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                txttelfijo.setText(rs.getString("telefono"));
                txttelcelular.setText(rs.getString("celular"));
                txtmail.setText(rs.getString("mail"));
                id_paciente = rs.getInt("id_Pacientes");
                txtfechanac.setText(revertirfecha_1(rs.getString("fecha_nacimiento")));
                String fechanac = txtfechanac.getText();
                txtedadpaciente.setText(edad(revertir(fechanac)));
                txtfechanac.transferFocus();
                id_obra_social = Integer.valueOf(tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 10).toString());
                banderapaciente = 2;

            }
            ///
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        try {
            String sSQL = "SELECT  numero_afiliado , obrasocial.Int_codigo_obrasocial, obrasocial.razonsocial_obrasocial FROM pacientes_tienen_obrasociales inner join obrasocial on obrasocial.id_obrasocial=pacientes_tienen_obrasociales.id_obrasocial WHERE pacientes_tienen_obrasociales.id_Pacientes=" + id_paciente + " AND pacientes_tienen_obrasociales.id_obrasocial=" + tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 10).toString();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                txtnumafiliadopaciente.setText(rs.getString(1));
                txtobrasocialpaciente.setText(rs.getString(2) + " - " + rs.getString(3));
                banderapaciente = 3;
                borrarpractica();
                cargarpracticaconobra();
                borrartabla();
            }
            ////
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        if (!txtapellidopaciente.getText().equals("") && !txtdnipaciente.getText().equals("") && !txtfechanac.getText().equals("") && !txtnombrepaciente.getText().equals("") && !txtlocalidad.getText().equals("") && !txtobrasocialpaciente.getText().equals("") && !txtnumafiliadopaciente.getText().equals("")) {
            btncargarorden.setEnabled(true);
        }

        try {
            cn.close();

        } catch (SQLException ex) {
            Logger.getLogger(MainB.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        btnPatologias.setEnabled(true);
    }//GEN-LAST:event_cargarActionPerformed

    private void borrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_borrarActionPerformed
        int borrar = 0;

        DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
        if (tablapracticas.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila...");
        } else {
            if (banderamodifica == 1) {
                ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                Connection cn = mysql.Conectar();
                try {
                    String sSQL = "SELECT resultado FROM resultados WHERE id_ordenes=" + filamodifica + " AND id_practicas=" + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 6).toString();
                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sSQL);
                    while (rs.next()) {
                        if (!rs.getString(1).equals("-")) {
                            int opcion = JOptionPane.showConfirmDialog(this, "La practica tiene resultados cargados, desea eliminarla?", "Mensaje", JOptionPane.YES_NO_OPTION);
                            if (opcion != 0) {
                                borrar = 1;
                                rs.last();
                            }
                        }
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }

                if (borrar == 0) {
                    try {
                        PreparedStatement pst3 = cn.prepareStatement("DELETE FROM resultados WHERE id_ordenes =" + filamodifica + " AND id_practicas=" + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 6).toString());
                        pst3.executeUpdate();

                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                    }
                    try {
                        PreparedStatement pst4 = cn.prepareStatement("DELETE FROM ordenes_tienen_practicas WHERE id_ordenes =" + filamodifica + " AND id_practicas=" + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 6).toString());
                        pst4.executeUpdate();
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Error en la base de datos..." + e);
                    }
                    try {
                        cn.close();

                    } catch (SQLException ex) {
                        Logger.getLogger(MainB.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                    temp.removeRow(tablapracticas.getSelectedRow());
                    cargatotalespracticas();
                }
            } else {
                temp.removeRow(tablapracticas.getSelectedRow());
                cargatotalespracticas();
            }

        }

    }//GEN-LAST:event_borrarActionPerformed

    private void txtmedicoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtmedicoFocusLost
        ///  txtmedico.requestFocus();
    }//GEN-LAST:event_txtmedicoFocusLost

    private void btnobservacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnobservacionesActionPerformed
//        fecha = txtfecha1.getText();
//        imprimir_tabla_practicas();
        new ObservacionOrdenes(null, true).setVisible(true);

    }//GEN-LAST:event_btnobservacionesActionPerformed

    private void btnborrar4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnborrar4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnborrar4ActionPerformed

    private void btnobrasociales9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnobrasociales9ActionPerformed
        new Historias_Clinicas(null, true).setVisible(true);
    }//GEN-LAST:event_btnobrasociales9ActionPerformed

    private void txtfecha2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfecha2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfecha2ActionPerformed

    private void txtfecha2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfecha2KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfecha2KeyReleased

    private void txtfecha2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfecha2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfecha2KeyPressed

    private void txttipoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttipoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txttipo.getText().equals("Recíen Nacidos.") || txttipo.getText().equals("VIH.")) {
                txtreciennacido.setEnabled(true);
                if (txttipo.getText().equals("Recíen Nacidos.")) {
                    jLabel58.setText("Nombre RN:");
                } else {
                    jLabel58.setText("Nombre Codif:");
                }
                jLabel58.setEnabled(true);
                txttipo.transferFocus();
            } else {
                txtreciennacido.setEnabled(false);
                jLabel58.setText("");
                jLabel58.setEnabled(false);
                txttipo.transferFocus();
            }
        }
    }//GEN-LAST:event_txttipoKeyPressed

    private void txttotal5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotal5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotal5ActionPerformed

    private void txtpractica2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpractica2KeyPressed

    }//GEN-LAST:event_txtpractica2KeyPressed

    private void txtpractica2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpractica2KeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ADD) {
            txtpractica2.setText("");
            btngrabar.doClick();
        }
        if (evt.getKeyCode() == KeyEvent.VK_SUBTRACT) {
            txtpractica2.setText("");
            btngrabarimprimir.doClick();
        }
        if (evt.getKeyCode() == KeyEvent.VK_MULTIPLY) {
            txtpractica2.setText("");
            btnobservaciones.doClick();
        }
    }//GEN-LAST:event_txtpractica2KeyReleased

    int verificar_orden(int id_historia_clinica, String orden) {
        int id_orden = 0;
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        try {
            System.out.println("Formularios.MainB.verificar_orden()");
            String sSQL = "SELECT\n"
                    + "ordenes.id_ordenes\n"
                    + "FROM\n"
                    + "  ((((ordenes\n"
                    + "    JOIN pacientes ON ((pacientes.id_Pacientes = ordenes.id_Pacientes)))\n"
                    + "    JOIN personas ON ((personas.dni = pacientes.personas_dni)))\n"
                    + "    JOIN historia_clinica ON ((historia_clinica.id_ordenes = ordenes.id_ordenes)))\n"
                    + "    LEFT JOIN seña ON ((historia_clinica.idhistoria_clinica = seña.idhistoria_clinica)))\n"
                    + "WHERE\n"
                    + "  (ordenes.estado_orden = 0) and historia_clinica.idhistoria_clinica=" + id_historia_clinica + " and ordenes.numero_orden='" + orden + "'\n"
                    + "GROUP BY\n"
                    + "historia_clinica.idhistoria_clinica\n"
                    + "ORDER BY ordenes.id_ordenes";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            if (!rs.next()) {

                id_orden = 0;
                //   JOptionPane.showMessageDialog(null, id_orden);

            } else {
                id_orden = rs.getInt(1);
                //  JOptionPane.showMessageDialog(null, id_orden);
            }
            cn.close();
        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);
        }
        return id_orden;
    }

    /*  void cargacomboprecioparticular(String id_practica) {
        ComboParticular.removeAllItems();
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        try {
            String sSQL = "SELECT practicas.codigo_practica, practicas.determinacion_practica, practicas_nbu.id_practicas, practicas.instrucciones, practicas.precio1, practicas.precio2, practicas.precio3, practicas.precio4, practicas.tiempo_procesamiento, obrasocial_tiene_practicas_nbu.preciototal FROM practicas_nbu INNER JOIN practicas ON practicas.id_practicas = practicas_nbu.id_practicas INNER JOIN obrasocial_tiene_practicas_nbu ON obrasocial_tiene_practicas_nbu.id_practicasnbu = practicas_nbu.id_practicasnbu WHERE obrasocial_tiene_practicas_nbu.id_obrasocial=96 and practicas.id_practicas=" + id_practica;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                ComboParticular.addItem(rs.getString(10));
                ComboParticular.addItem(rs.getString(5));
                ComboParticular.addItem(rs.getString(6));
                ComboParticular.addItem(rs.getString(7));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }*/

    private void txtpractica2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpractica2ActionPerformed
        if (!txtpractica2.getText().equals("")) {
            String cod = "", nom = "", cadena = txtpractica2.getText(), numorden = txtnumorden2.getText();
            int codigo = 0;
            int n = tablapracticas.getRowCount();
            int i = 0, j = 1;

            ///cod/// 0000 - aaaaaaaaa
            while (i < cadena.length()) {
                if (String.valueOf(cadena.charAt(i)).equals("-")) {
                    j = i + 2;
                    i = cadena.length();
                } else {
                    cod = cod + cadena.charAt(i);
                }
                i++;
            }
            ///nom/// 0000 - aaaaaaaaa
            while (j < cadena.length()) {
                nom = nom + cadena.charAt(j);
                j++;
            }
            DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
            ////////////////////////////////////
            codigo = Integer.valueOf(cod.substring(0, 6));
            if (banderamodifica == 0) {
                int band = 0;
                if (!cadena.equals("")) {
                    i = 0;
                    j = 0;
                    int fila = 0;
                    while (i < contadorj) {
                        if (cadena.equals(practica[i])) {//comparo con practica particular
                            System.out.println("esta en el particular");
                            while (j < contadorj2) {
                                if (practica2[j].equals(practica[i])) {//compraro con practica particular y la de la obra social                                
                                    System.out.println("esta en la obra social");
                                    if (n != 0) {
                                        if (!txtnumorden2.getText().equals("")) {
                                            fila = n;
                                            Object nuevo[] = {
                                                fila + 1, "", ""};
                                            temp.addRow(nuevo);
                                            System.out.println("---->1");
                                            tablapracticas.setValueAt(numorden, fila, 1);
                                            tablapracticas.setValueAt(cod, fila, 2);
                                            tablapracticas.setValueAt(nom, fila, 3);
                                            tablapracticas.setValueAt(preciopractica2[j], fila, 4);
                                            tablapracticas.setValueAt(preciopractica[j], fila, 5);
                                            //cargacomboprecioparticular(idpractica2[j]);
                                            tablapracticas.setValueAt(idpractica2[j], fila, 6);
                                            txtinstrucciones.setText(instruciones2[j]);//instruciones
                                            System.out.println("tiempo2: " + tiempo2[j]);
                                            tablapracticas.setValueAt(tiempo2[j], fila, 10);
                                            tablapracticas.setValueAt(true, fila, 8);

                                            if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                tablapracticas.setValueAt(false, fila, 9);
                                            } else {
                                                tablapracticas.setValueAt(true, fila, 9);
                                            }

                                            tablapracticas.setValueAt(preciopracticaparticular5[j], fila, 7);
                                            tablapracticas.setValueAt(practica_fac[j], fila, 11);
                                            /// tablapracticas.setValueAt(0, fila, 5);

                                        } else {
                                            fila = n;
                                            Object nuevo[] = {
                                                fila + 1, "", ""};
                                            temp.addRow(nuevo);
                                            System.out.println("---->2");
                                            tablapracticas.setValueAt("1", fila, 1);
                                            tablapracticas.setValueAt(cod, fila, 2);
                                            tablapracticas.setValueAt(nom, fila, 3);
                                            tablapracticas.setValueAt(preciopractica2[j], fila, 4);
                                            tablapracticas.setValueAt(preciopractica[j], fila, 5);
                                            //cargacomboprecioparticular(idpractica2[j]);
                                            tablapracticas.setValueAt(idpractica2[j], fila, 6);
                                            txtinstrucciones.setText(instruciones2[j]);
                                            System.out.println("tiempo2: " + tiempo2[j]);
                                            tablapracticas.setValueAt(tiempo2[j], fila, 10);
                                            tablapracticas.setValueAt(preciopracticaparticular5[j], fila, 7);
                                            tablapracticas.setValueAt(true, fila, 8);
                                            if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                tablapracticas.setValueAt(false, fila, 9);
                                            } else {
                                                tablapracticas.setValueAt(true, fila, 9);
                                            }
                                            tablapracticas.setValueAt(practica_fac[j], fila, 11);
                                            ///tablapracticas.setValueAt(0, fila, 5);
                                        }

                                    } else {
                                        if (!txtnumorden2.getText().equals("")) {
                                            Object nuevo[] = {
                                                "1", "", ""};
                                            temp.addRow(nuevo);
                                            System.out.println("---->3");
                                            tablapracticas.setValueAt(numorden, fila, 1);
                                            tablapracticas.setValueAt(cod, 0, 2);
                                            tablapracticas.setValueAt(nom, 0, 3);
                                            tablapracticas.setValueAt(preciopractica2[j], 0, 4);
                                            tablapracticas.setValueAt(preciopractica[j], 0, 5);
                                            //cargacomboprecioparticular(idpractica2[j]);
                                            tablapracticas.setValueAt(idpractica2[j], 0, 6);
                                            txtinstrucciones.setText(instruciones2[j]);
                                            System.out.println("tiempo2: " + tiempo2[j]);
                                            tablapracticas.setValueAt(tiempo2[j], 0, 10);
                                            tablapracticas.setValueAt(preciopracticaparticular5[j], 0, 7);
                                            tablapracticas.setValueAt(true, 0, 8);

                                            if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                tablapracticas.setValueAt(false, fila, 9);
                                            } else {
                                                tablapracticas.setValueAt(true, fila, 9);
                                            }
                                            tablapracticas.setValueAt(practica_fac[j], 0, 11);
                                            //tablapracticas.setValueAt(0, 0, 5);
                                        } else {
                                            Object nuevo[] = {
                                                "1", "", ""};
                                            temp.addRow(nuevo);
                                            System.out.println("---->4");
                                            tablapracticas.setValueAt("1", 0, 1);
                                            tablapracticas.setValueAt(cod, 0, 2);
                                            tablapracticas.setValueAt(nom, 0, 3);
                                            tablapracticas.setValueAt(preciopractica2[j], 0, 4);
                                            tablapracticas.setValueAt(preciopractica[j], 0, 5);
                                            //cargacomboprecioparticular(idpractica2[j]);
                                            tablapracticas.setValueAt(idpractica2[j], 0, 6);
                                            txtinstrucciones.setText(instruciones2[j]);
                                            System.out.println("tiempo2: " + tiempo2[j]);
                                            tablapracticas.setValueAt(tiempo2[j], 0, 10);
                                            tablapracticas.setValueAt(preciopracticaparticular5[j], 0, 7);
                                            tablapracticas.setValueAt(true, 0, 8);
                                            if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                tablapracticas.setValueAt(false, fila, 9);
                                            } else {
                                                tablapracticas.setValueAt(true, fila, 9);
                                            }
                                            tablapracticas.setValueAt(practica_fac[j], 0, 11);
                                            //tablapracticas.setValueAt(0, 0, 5);
                                        }
                                    }
                                    band = 1;
                                }
                                j++;
                            }
                            if (band == 0) {

                                i = 0;
                                while (i < contadorj) {
                                    if (cadena.equals(practica[i])) {
                                        if (n != 0) {
                                            if (!txtnumorden2.getText().equals("")) {
                                                fila = n;
                                                Object nuevo[] = {
                                                    fila + 1, "", ""};
                                                temp.addRow(nuevo);
                                                System.out.println("---->5");
                                                tablapracticas.setValueAt(numorden, fila, 1);
                                                tablapracticas.setValueAt(cod, fila, 2);
                                                tablapracticas.setValueAt(nom, fila, 3);
                                                tablapracticas.setValueAt(preciopractica[i], fila, 5);
                                                //cargacomboprecioparticular(idpractica2[j]);
                                                tablapracticas.setValueAt(0.00, fila, 4);
                                                tablapracticas.setValueAt(idpractica[i], fila, 6);
                                                txtinstrucciones.setText(instruciones[i]);//instruciones
                                                System.out.println("tiempo2: " + tiempo2[j]);
                                                tablapracticas.setValueAt(tiempo[i], fila, 10);
                                                tablapracticas.setValueAt(false, fila, 8);
                                                if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                    tablapracticas.setValueAt(false, fila, 9);
                                                } else {
                                                    tablapracticas.setValueAt(true, fila, 9);
                                                }
                                                tablapracticas.setValueAt(preciopracticaparticular4[i], fila, 7);
                                                tablapracticas.setValueAt(practica_fac[i], fila, 11);
                                                ///tablapracticas.setValueAt(0, fila, 5);
                                                //,
                                            } else {
                                                fila = n;
                                                Object nuevo[] = {
                                                    fila + 1, "", ""};
                                                temp.addRow(nuevo);
                                                System.out.println("---->6");
                                                tablapracticas.setValueAt("1", fila, 1);
                                                tablapracticas.setValueAt(cod, fila, 2);
                                                tablapracticas.setValueAt(nom, fila, 3);
                                                tablapracticas.setValueAt(preciopractica[i], fila, 5);
                                                //cargacomboprecioparticular(idpractica2[j]);
                                                tablapracticas.setValueAt(0.00, fila, 4);
                                                tablapracticas.setValueAt(idpractica[i], fila, 6);
                                                txtinstrucciones.setText(instruciones[i]);
                                                System.out.println("tiempo2: " + tiempo2[j]);
                                                tablapracticas.setValueAt(tiempo[i], fila, 10);
                                                tablapracticas.setValueAt(preciopracticaparticular4[i], fila, 7);
                                                tablapracticas.setValueAt(false, fila, 8);
                                                if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                    tablapracticas.setValueAt(false, fila, 9);
                                                } else {
                                                    tablapracticas.setValueAt(true, fila, 9);
                                                }
                                                tablapracticas.setValueAt(practica_fac[i], fila, 11);
                                                ///tablapracticas.setValueAt(0, fila, 5);
                                            }

                                        } else {
                                            if (!txtnumorden2.getText().equals("")) {
                                                Object nuevo[] = {
                                                    "1", "", ""};
                                                temp.addRow(nuevo);
                                                System.out.println("---->7");
                                                tablapracticas.setValueAt(numorden, fila, 1);
                                                tablapracticas.setValueAt(cod, 0, 2);
                                                tablapracticas.setValueAt(nom, 0, 3);
                                                tablapracticas.setValueAt(preciopractica[i], 0, 5);
                                                //cargacomboprecioparticular(idpractica2[j]);
                                                tablapracticas.setValueAt(0.00, 0, 4);
                                                tablapracticas.setValueAt(idpractica[i], 0, 6);
                                                txtinstrucciones.setText(instruciones[i]);
                                                System.out.println("tiempo2: " + tiempo2[j]);
                                                tablapracticas.setValueAt(tiempo[i], 0, 10);
                                                tablapracticas.setValueAt(preciopracticaparticular4[i], 0, 7);
                                                tablapracticas.setValueAt(false, 0, 8);
                                                if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                    tablapracticas.setValueAt(false, fila, 9);
                                                } else {
                                                    tablapracticas.setValueAt(true, fila, 9);
                                                }
                                                tablapracticas.setValueAt(practica_fac[i], 0, 11);
                                                ///tablapracticas.setValueAt(0, 0, 5);
                                            } else {
                                                Object nuevo[] = {
                                                    "1", "", ""};
                                                temp.addRow(nuevo);
                                                System.out.println("---->8");
                                                tablapracticas.setValueAt("1", 0, 1);
                                                tablapracticas.setValueAt(cod, 0, 2);
                                                tablapracticas.setValueAt(nom, 0, 3);
                                                tablapracticas.setValueAt(preciopractica[i], 0, 5);
                                                //cargacomboprecioparticular(idpractica2[j]);
                                                tablapracticas.setValueAt(0.00, 0, 4);
                                                tablapracticas.setValueAt(idpractica[i], 0, 6);
                                                txtinstrucciones.setText(instruciones[i]);
                                                tablapracticas.setValueAt(tiempo[i], 0, 10);
                                                System.out.println("tiempo2: " + tiempo2[j]);
                                                tablapracticas.setValueAt(preciopracticaparticular4[i], 0, 7);
                                                tablapracticas.setValueAt(false, 0, 8);
                                                if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                    tablapracticas.setValueAt(false, fila, 9);
                                                } else {
                                                    tablapracticas.setValueAt(true, fila, 9);
                                                }
                                                tablapracticas.setValueAt(practica_fac[i], 0, 11);
                                                //tablapracticas.setValueAt(0, 0, 5);
                                            }
                                        }
                                        band = 2;
                                    }
                                    i++;
                                }
                            }
                        }
                        i++;
                        ////////////////////////////////////////////////////////////////////
                        tablapracticas.getColumnModel().getColumn(6).setMaxWidth(0);
                        tablapracticas.getColumnModel().getColumn(6).setMinWidth(0);
                        tablapracticas.getColumnModel().getColumn(6).setPreferredWidth(0);
                        ////////////////////////////////////////////////////////////////////
                        tablapracticas.getColumnModel().getColumn(7).setMaxWidth(0);
                        tablapracticas.getColumnModel().getColumn(7).setMinWidth(0);
                        tablapracticas.getColumnModel().getColumn(7).setPreferredWidth(0);
                        ////////////////////////////////////////////////////////////////////
                        tablapracticas.getColumnModel().getColumn(10).setMaxWidth(0);
                        tablapracticas.getColumnModel().getColumn(10).setMinWidth(0);
                        tablapracticas.getColumnModel().getColumn(10).setPreferredWidth(0);
                        ////////////////////////////////////////////////////////////////////                    
                        tablapracticas.getColumnModel().getColumn(11).setMaxWidth(0);
                        tablapracticas.getColumnModel().getColumn(11).setMinWidth(0);
                        tablapracticas.getColumnModel().getColumn(11).setPreferredWidth(0);
                        ////////////////////////////////////////////////////////////////////
                        ////////////////////////////////////////////////////////////////////                    
                        tablapracticas.getColumnModel().getColumn(12).setMaxWidth(0);
                        tablapracticas.getColumnModel().getColumn(12).setMinWidth(0);
                        tablapracticas.getColumnModel().getColumn(12).setPreferredWidth(0);

                        tablapracticas.getColumnModel().getColumn(0).setPreferredWidth(30);
                        tablapracticas.getColumnModel().getColumn(1).setPreferredWidth(30);
                        tablapracticas.getColumnModel().getColumn(2).setPreferredWidth(20);
                        tablapracticas.getColumnModel().getColumn(3).setPreferredWidth(270);
                        tablapracticas.getColumnModel().getColumn(4).setPreferredWidth(20);
                        tablapracticas.getColumnModel().getColumn(5).setPreferredWidth(40);
                        tablapracticas.getColumnModel().getColumn(8).setPreferredWidth(10);
                        tablapracticas.getColumnModel().getColumn(9).setPreferredWidth(15);
                        ///////Ultima Fila///////
                        Rectangle r = tablapracticas.getCellRect(tablapracticas.getRowCount() - 1, 0, true);
                        tablapracticas.scrollRectToVisible(r);
                        tablapracticas.getSelectionModel().setSelectionInterval(tablapracticas.getRowCount() - 1, tablapracticas.getRowCount() - 1);
                        //////////////////////////

                    }
                    if (band == 0) {

                        i = 0;
                        while (i < contadorj) {
                            if (cadena.equals(practica[i])) {
                                if (n != 0) {
                                    if (!txtnumorden2.getText().equals("")) {
                                        fila = n;
                                        Object nuevo[] = {
                                            fila + 1, "", ""};
                                        temp.addRow(nuevo);
                                        System.out.println("---->9");
                                        tablapracticas.setValueAt(numorden, fila, 1);
                                        tablapracticas.setValueAt(cod, fila, 2);
                                        tablapracticas.setValueAt(nom, fila, 3);
                                        tablapracticas.setValueAt(preciopractica[i], fila, 5);
                                        //cargacomboprecioparticular(idpractica2[j]);
                                        tablapracticas.setValueAt(0.00, fila, 4);
                                        tablapracticas.setValueAt(idpractica[i], fila, 6);
                                        txtinstrucciones.setText(instruciones[i]);//instruciones
                                        System.out.println("tiempo2: " + tiempo2[j]);
                                        tablapracticas.setValueAt(tiempo[i], fila, 10);
                                        tablapracticas.setValueAt(false, fila, 8);
                                        if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                            tablapracticas.setValueAt(false, fila, 9);
                                        } else {
                                            tablapracticas.setValueAt(true, fila, 9);
                                        }
                                        tablapracticas.setValueAt(preciopracticaparticular4[i], fila, 7);
                                        tablapracticas.setValueAt(practica_fac[i], fila, 11);
                                        tablapracticas.setValueAt(0, fila, 12);
                                        ///tablapracticas.setValueAt(0, fila, 5);
                                        //,
                                    } else {
                                        fila = n;
                                        Object nuevo[] = {
                                            fila + 1, "", ""};
                                        temp.addRow(nuevo);
                                        System.out.println("---->10");
                                        tablapracticas.setValueAt("1", fila, 1);
                                        tablapracticas.setValueAt(cod, fila, 2);
                                        tablapracticas.setValueAt(nom, fila, 3);
                                        tablapracticas.setValueAt(preciopractica[i], fila, 5);
                                        //cargacomboprecioparticular(idpractica2[j]);
                                        tablapracticas.setValueAt(0.00, fila, 4);
                                        tablapracticas.setValueAt(idpractica[i], fila, 6);
                                        txtinstrucciones.setText(instruciones[i]);
                                        System.out.println("tiempo2: " + tiempo2[j]);
                                        tablapracticas.setValueAt(tiempo[i], fila, 10);
                                        tablapracticas.setValueAt(preciopracticaparticular4[i], fila, 7);
                                        tablapracticas.setValueAt(false, fila, 8);
                                        if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                            tablapracticas.setValueAt(false, fila, 9);
                                        } else {
                                            tablapracticas.setValueAt(true, fila, 9);
                                        }
                                        tablapracticas.setValueAt(practica_fac[i], fila, 11);
                                        tablapracticas.setValueAt(0, fila, 12);
                                    }

                                } else {
                                    if (!txtnumorden2.getText().equals("")) {
                                        Object nuevo[] = {
                                            "1", "", ""};
                                        temp.addRow(nuevo);
                                        System.out.println("---->11");
                                        tablapracticas.setValueAt(numorden, fila, 1);
                                        tablapracticas.setValueAt(cod, 0, 2);
                                        tablapracticas.setValueAt(nom, 0, 3);
                                        tablapracticas.setValueAt(preciopractica[i], 0, 5);
                                        //cargacomboprecioparticular(idpractica2[j]);
                                        tablapracticas.setValueAt(0.00, 0, 4);
                                        tablapracticas.setValueAt(idpractica[i], 0, 6);
                                        txtinstrucciones.setText(instruciones[i]);
                                        System.out.println("tiempo2: " + tiempo2[j]);
                                        tablapracticas.setValueAt(tiempo[i], 0, 10);
                                        tablapracticas.setValueAt(preciopracticaparticular4[i], 0, 7);
                                        tablapracticas.setValueAt(false, 0, 8);
                                        if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                            tablapracticas.setValueAt(false, fila, 9);
                                        } else {
                                            tablapracticas.setValueAt(true, fila, 9);
                                        }
                                        tablapracticas.setValueAt(practica_fac[i], 0, 11);
                                        tablapracticas.setValueAt(0, fila, 12);
                                    } else {
                                        Object nuevo[] = {
                                            "1", "", ""};
                                        temp.addRow(nuevo);
                                        System.out.println("---->12");
                                        tablapracticas.setValueAt("1", 0, 1);
                                        tablapracticas.setValueAt(cod, 0, 2);
                                        tablapracticas.setValueAt(nom, 0, 3);
                                        tablapracticas.setValueAt(preciopractica[i], 0, 5);
                                        //cargacomboprecioparticular(idpractica2[j]);
                                        tablapracticas.setValueAt(0.00, 0, 4);
                                        tablapracticas.setValueAt(idpractica[i], 0, 6);
                                        txtinstrucciones.setText(instruciones[i]);
                                        tablapracticas.setValueAt(tiempo[i], 0, 10);
                                        System.out.println("tiempo2: " + tiempo2[j]);
                                        tablapracticas.setValueAt(preciopracticaparticular4[i], 0, 7);
                                        tablapracticas.setValueAt(false, 0, 8);
                                        if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                            tablapracticas.setValueAt(false, fila, 9);
                                        } else {
                                            tablapracticas.setValueAt(true, fila, 9);
                                        }
                                        tablapracticas.setValueAt(practica_fac[i], 0, 11);
                                        tablapracticas.setValueAt(0, fila, 12);
                                    }
                                }
                                band = 2;
                            }
                            i++;
                        }
                    }
                }
            } else {////////////////////////////////modifica////////////////////////

                int band = 0;
                if (!cadena.equals("")) {
                    i = 0;
                    j = 0;
                    //numorden = tablapracticas.getValueAt(n - 1, 1).toString();
                    //JOptionPane.showMessageDialog(null, "contador:" + numorden);
                    System.out.println("cadena: " + cadena);
                    int fila = 0, k = 0, bandera = 0, orden_id = 0;
                    while (i < contadorj) {
                        if (cadena.equals(practica[i])) {
                            //JOptionPane.showMessageDialog(null, "contador:" + contadorj);
                            while (j < contadorj2) {
                                System.out.println("practica2[j] " + practica2[j]);
                                System.out.println("practica[j] " + practica[i]);
                                if (practica2[j].equals(practica[i])) {//compraro con practica particular y la de la obra social                                  
                                    if (n != 0) {
                                        if (!txtnumorden2.getText().equals("")) {
                                            fila = n;
                                            while (n > k && bandera == 0) {
                                                numorden = tablapracticas.getValueAt(k, 1).toString();
                                                //     JOptionPane.showMessageDialog(null, "orden:" + numorden);
                                                if (txtnumorden2.getText().equals(numorden)) {
                                                    bandera = 1;
                                                }
                                                k++;
                                            }
                                            if (bandera == 1) {
                                                Object nuevo[] = {
                                                    fila + 1, "", ""};
                                                temp.addRow(nuevo);
                                                System.out.println("---->13");
                                                tablapracticas.setValueAt(numorden, fila, 1);
                                                tablapracticas.setValueAt(cod, fila, 2);
                                                tablapracticas.setValueAt(nom, fila, 3);
                                                tablapracticas.setValueAt(preciopractica2[j], fila, 4);
                                                tablapracticas.setValueAt(preciopractica[j], fila, 5);
                                                tablapracticas.setValueAt(idpractica2[j], fila, 6);
                                                txtinstrucciones.setText(instruciones2[j]);//instruciones
                                                System.out.println("tiempo2: " + tiempo2[j]);
                                                tablapracticas.setValueAt(tiempo2[j], fila, 10);
                                                tablapracticas.setValueAt(true, fila, 8);

                                                if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                    tablapracticas.setValueAt(false, fila, 9);
                                                } else {
                                                    tablapracticas.setValueAt(true, fila, 9);
                                                }

                                                tablapracticas.setValueAt(preciopracticaparticular5[j], fila, 7);
                                                tablapracticas.setValueAt(practica_fac[j], fila, 11);
                                                //tablapracticas.setValueAt(0, fila, 5);
                                                System.out.println("--->13_1");
                                                orden_id = verificar_orden(Integer.valueOf(txtprotocolo.getText()), txtnumorden2.getText());
                                                System.out.println("--->13_2");
                                                tablapracticas.setValueAt(orden_id, fila, 12);

                                            } else {
                                                Object nuevo[] = {
                                                    fila + 1, "", ""};
                                                temp.addRow(nuevo);
                                                System.out.println("---->14");
                                                tablapracticas.setValueAt(txtnumorden2.getText(), fila, 1);
                                                tablapracticas.setValueAt(cod, fila, 2);
                                                tablapracticas.setValueAt(nom, fila, 3);
                                                tablapracticas.setValueAt(preciopractica2[j], fila, 4);
                                                tablapracticas.setValueAt(preciopractica[j], fila, 5);
                                                tablapracticas.setValueAt(idpractica2[j], fila, 6);
                                                txtinstrucciones.setText(instruciones2[j]);//instruciones
                                                System.out.println("tiempo2: " + tiempo2[j]);
                                                tablapracticas.setValueAt(tiempo2[j], fila, 10);
                                                tablapracticas.setValueAt(true, fila, 8);

                                                if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                    tablapracticas.setValueAt(false, fila, 9);
                                                } else {
                                                    tablapracticas.setValueAt(true, fila, 9);
                                                }

                                                tablapracticas.setValueAt(preciopracticaparticular5[j], fila, 7);
                                                tablapracticas.setValueAt(practica_fac[j], fila, 11);
                                                ///tablapracticas.setValueAt(0, fila, 5);
                                                tablapracticas.setValueAt(0, fila, 12);

                                            }
                                        } else {
                                            // if (!txtnumorden2.getText().equals("")) {
                                            Object nuevo[] = {
                                                fila + 1, "", ""};
                                            temp.addRow(nuevo);
                                            System.out.println("---->15");
                                            tablapracticas.setValueAt(txtnumorden2.getText(), fila, 1);
                                            tablapracticas.setValueAt(cod, fila, 2);
                                            tablapracticas.setValueAt(nom, fila, 3);
                                            tablapracticas.setValueAt(preciopractica2[j], fila, 4);
                                            tablapracticas.setValueAt(preciopractica[j], fila, 5);
                                            tablapracticas.setValueAt(idpractica2[j], fila, 6);
                                            txtinstrucciones.setText(instruciones2[j]);//instruciones
                                            tablapracticas.setValueAt(tiempo2[j], fila, 10);
                                            System.out.println("tiempo2: " + tiempo2[j]);
                                            tablapracticas.setValueAt(true, fila, 8);

                                            if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                tablapracticas.setValueAt(false, fila, 9);
                                            } else {
                                                tablapracticas.setValueAt(true, fila, 9);
                                            }

                                            tablapracticas.setValueAt(preciopracticaparticular5[j], fila, 7);
                                            tablapracticas.setValueAt(practica_fac[j], fila, 11);
                                            ///tablapracticas.setValueAt(0, fila, 5);
                                            tablapracticas.setValueAt(0, fila, 12);
                                            // }
                                        }

                                    } else {
                                        if (!txtnumorden2.getText().equals("")) {
                                            Object nuevo[] = {
                                                "1", "", ""};
                                            temp.addRow(nuevo);
                                            System.out.println("---->16");
                                            tablapracticas.setValueAt(txtnumorden2.getText(), fila, 1);
                                            tablapracticas.setValueAt(cod, fila, 2);
                                            tablapracticas.setValueAt(nom, fila, 3);
                                            tablapracticas.setValueAt(preciopractica2[j], fila, 4);
                                            tablapracticas.setValueAt(preciopractica[j], fila, 5);
                                            tablapracticas.setValueAt(idpractica2[j], fila, 6);
                                            txtinstrucciones.setText(instruciones2[j]);//instruciones
                                            System.out.println("tiempo2: " + tiempo2[j]);
                                            tablapracticas.setValueAt(tiempo2[j], fila, 10);
                                            tablapracticas.setValueAt(true, fila, 8);

                                            if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                tablapracticas.setValueAt(false, fila, 9);
                                            } else {
                                                tablapracticas.setValueAt(true, fila, 9);
                                            }

                                            tablapracticas.setValueAt(preciopracticaparticular5[j], fila, 7);
                                            tablapracticas.setValueAt(practica_fac[j], fila, 11);
                                            ///tablapracticas.setValueAt(0, fila, 5);
                                            tablapracticas.setValueAt(verificar_orden(Integer.valueOf(txtprotocolo.getText()), txtnumorden2.getText()), fila, 12);

                                        } else {
                                            Object nuevo[] = {
                                                "1", "", ""};
                                            temp.addRow(nuevo);
                                            System.out.println("---->17");
                                            tablapracticas.setValueAt("1", fila, 1);
                                            tablapracticas.setValueAt(cod, fila, 2);
                                            tablapracticas.setValueAt(nom, fila, 3);
                                            tablapracticas.setValueAt(preciopractica2[j], fila, 4);
                                            tablapracticas.setValueAt(preciopractica[j], fila, 5);
                                            tablapracticas.setValueAt(idpractica2[j], fila, 6);
                                            txtinstrucciones.setText(instruciones2[j]);//instruciones
                                            tablapracticas.setValueAt(tiempo2[j], fila, 10);
                                            System.out.println("tiempo2: " + tiempo2[j]);
                                            tablapracticas.setValueAt(true, fila, 8);

                                            if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                                tablapracticas.setValueAt(false, fila, 9);
                                            } else {
                                                tablapracticas.setValueAt(true, fila, 9);
                                            }

                                            tablapracticas.setValueAt(preciopracticaparticular5[j], fila, 7);
                                            tablapracticas.setValueAt(practica_fac[j], fila, 11);
                                            ///tablapracticas.setValueAt(0, fila, 5);
                                            tablapracticas.setValueAt(0, fila, 12);

                                        }
                                    }
                                    band = 1;
                                }
                                j++;
                            }
                        }
                        i++;
                        /*////////////////////////////////////////////////////////////////////                    
                        tablapracticas.getColumnModel().getColumn(11).setMaxWidth(0);
                        tablapracticas.getColumnModel().getColumn(11).setMinWidth(0);
                        tablapracticas.getColumnModel().getColumn(11).setPreferredWidth(0);*/
                        ////////////////////////////////////////////////////////////////////
                        tablapracticas.getColumnModel().getColumn(0).setPreferredWidth(30);
                        tablapracticas.getColumnModel().getColumn(1).setPreferredWidth(30);
                        tablapracticas.getColumnModel().getColumn(2).setPreferredWidth(20);
                        tablapracticas.getColumnModel().getColumn(3).setPreferredWidth(270);
                        tablapracticas.getColumnModel().getColumn(4).setPreferredWidth(20);
                        tablapracticas.getColumnModel().getColumn(5).setPreferredWidth(40);
                        tablapracticas.getColumnModel().getColumn(7).setPreferredWidth(30);
                        tablapracticas.getColumnModel().getColumn(8).setPreferredWidth(10);
                        tablapracticas.getColumnModel().getColumn(9).setPreferredWidth(15);
                        ///////Ultima Fila///////
                        Rectangle r = tablapracticas.getCellRect(tablapracticas.getRowCount() - 1, 0, true);
                        tablapracticas.scrollRectToVisible(r);
                        tablapracticas.getSelectionModel().setSelectionInterval(tablapracticas.getRowCount() - 1, tablapracticas.getRowCount() - 1);
                        //////////////////////////
                    }
                    if (band == 0) {

                        i = 0;
                        while (i < contadorj) {
                            if (cadena.equals(practica[i])) {
                                if (n != 0) {
                                    if (!txtnumorden2.getText().equals("")) {
                                        fila = n;
                                        Object nuevo[] = {
                                            fila + 1, "", ""};
                                        temp.addRow(nuevo);
                                        System.out.println("---->18");
                                        tablapracticas.setValueAt(numorden, fila, 1);
                                        tablapracticas.setValueAt(cod, fila, 2);
                                        tablapracticas.setValueAt(nom, fila, 3);
                                        tablapracticas.setValueAt(preciopractica[i], fila, 5);
                                        //cargacomboprecioparticular(idpractica2[j]);
                                        tablapracticas.setValueAt(0.00, fila, 4);
                                        tablapracticas.setValueAt(idpractica[i], fila, 6);
                                        txtinstrucciones.setText(instruciones[i]);//instruciones
                                        System.out.println("tiempo2: " + tiempo2[j]);
                                        tablapracticas.setValueAt(tiempo[i], fila, 10);
                                        tablapracticas.setValueAt(false, fila, 8);
                                        if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                            tablapracticas.setValueAt(false, fila, 9);
                                        } else {
                                            tablapracticas.setValueAt(true, fila, 9);
                                        }
                                        tablapracticas.setValueAt(preciopracticaparticular4[i], fila, 7);
                                        tablapracticas.setValueAt(cod, fila, 11);
                                        tablapracticas.setValueAt(0, fila, 12);
                                        //,
                                    } else {
                                        fila = n;
                                        Object nuevo[] = {
                                            fila + 1, "", ""};
                                        temp.addRow(nuevo);
                                        System.out.println("---->19");
                                        tablapracticas.setValueAt("1", fila, 1);
                                        tablapracticas.setValueAt(cod, fila, 2);
                                        tablapracticas.setValueAt(nom, fila, 3);
                                        tablapracticas.setValueAt(preciopractica[i], fila, 5);
                                        //cargacomboprecioparticular(idpractica2[j]);
                                        tablapracticas.setValueAt(0.00, fila, 4);
                                        tablapracticas.setValueAt(idpractica[i], fila, 6);
                                        txtinstrucciones.setText(instruciones[i]);
                                        System.out.println("tiempo2: " + tiempo2[j]);
                                        tablapracticas.setValueAt(tiempo[i], fila, 10);
                                        tablapracticas.setValueAt(preciopracticaparticular4[i], fila, 7);
                                        tablapracticas.setValueAt(false, fila, 8);
                                        if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                            tablapracticas.setValueAt(false, fila, 9);
                                        } else {
                                            tablapracticas.setValueAt(true, fila, 9);
                                        }
                                        tablapracticas.setValueAt(practica_fac[i], fila, 11);
                                        ///tablapracticas.setValueAt(0, fila, 5);
                                    }

                                } else {
                                    if (!txtnumorden2.getText().equals("")) {
                                        Object nuevo[] = {
                                            "1", "", ""};
                                        temp.addRow(nuevo);
                                        System.out.println("---->20");
                                        tablapracticas.setValueAt(numorden, fila, 1);
                                        tablapracticas.setValueAt(cod, 0, 2);
                                        tablapracticas.setValueAt(nom, 0, 3);
                                        tablapracticas.setValueAt(preciopractica[i], 0, 5);
                                        //cargacomboprecioparticular(idpractica2[j]);
                                        tablapracticas.setValueAt(0.00, 0, 4);
                                        tablapracticas.setValueAt(idpractica[i], 0, 6);
                                        txtinstrucciones.setText(instruciones[i]);
                                        System.out.println("tiempo2: " + tiempo2[j]);
                                        tablapracticas.setValueAt(tiempo[i], 0, 10);
                                        tablapracticas.setValueAt(preciopracticaparticular4[i], 0, 7);
                                        tablapracticas.setValueAt(false, 0, 8);
                                        if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                            tablapracticas.setValueAt(false, fila, 9);
                                        } else {
                                            tablapracticas.setValueAt(true, fila, 9);
                                        }
                                        tablapracticas.setValueAt(practica_fac[i], 0, 11);
                                        ///tablapracticas.setValueAt(0, 0, 5);
                                    } else {
                                        Object nuevo[] = {
                                            "1", "", ""};
                                        temp.addRow(nuevo);
                                        System.out.println("---->21");
                                        tablapracticas.setValueAt("1", 0, 1);
                                        tablapracticas.setValueAt(cod, 0, 2);
                                        tablapracticas.setValueAt(nom, 0, 3);
                                        tablapracticas.setValueAt(preciopractica[i], 0, 5);
                                        //cargacomboprecioparticular(idpractica2[j]);
                                        tablapracticas.setValueAt(0.00, 0, 4);
                                        tablapracticas.setValueAt(idpractica[i], 0, 6);
                                        txtinstrucciones.setText(instruciones[i]);
                                        tablapracticas.setValueAt(tiempo[i], 0, 10);
                                        System.out.println("tiempo2: " + tiempo2[j]);
                                        tablapracticas.setValueAt(preciopracticaparticular4[i], 0, 7);
                                        tablapracticas.setValueAt(false, 0, 8);
                                        if (codigo == 660475 || codigo == 660711 || codigo == 660911) {
                                            tablapracticas.setValueAt(false, fila, 9);
                                        } else {
                                            tablapracticas.setValueAt(true, fila, 9);
                                        }
                                        tablapracticas.setValueAt(practica_fac[i], 0, 11);
                                        //tablapracticas.setValueAt(0, 0, 5);
                                    }
                                }
                                band = 2;
                            }
                            i++;
                        }
                    }
                }
            }
            cargatotalespracticas();
            txtpractica2.setText("");
        }

    }//GEN-LAST:event_txtpractica2ActionPerformed

    private void btnborrar5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnborrar5ActionPerformed
        new Señas(null, true).setVisible(true);
        txttotal5.setText(String.valueOf(seña_publica));
        CargaTotalesPrecios();
    }//GEN-LAST:event_btnborrar5ActionPerformed

    private void btnborrar6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnborrar6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnborrar6ActionPerformed

    private void btnborrar7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnborrar7ActionPerformed
        ///////////////////informe_mesada///////////////////////////
        LinkedList<informe_mesada> Resultados_mesada = new LinkedList<informe_mesada>();
        Resultados_mesada.clear();
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            String id_orden = "", laboratorio = "", paciente = "", fecha_realizacion = "", fercha_entrega = "";
            String analisis1 = "", analisis2 = "", analisis3 = "", seccion = "", titulo = "";
            int flag = 0, estado_nombre;
            String sql2 = "SELECT distinct(historia_clinica.idhistoria_clinica), analisis.codigo_interno, secciones.nombre, titulos.nombre, practicas.determinacion_practica, analisis.estado_titulo\n"
                    + "FROM ordenes \n"
                    + "INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes \n"
                    + "INNER JOIN ordenes_tienen_practicas on ordenes.id_ordenes=ordenes_tienen_practicas.id_ordenes \n"
                    + "INNER JOIN analisis on analisis.id_practicas=ordenes_tienen_practicas.id_practicas \n"
                    + "INNER JOIN practicas on practicas.id_practicas=analisis.id_practicas\n"
                    + "INNER JOIN secciones on secciones.id_secciones=analisis.id_secciones \n"
                    + "LEFT JOIN titulos on titulos.id_titulo=analisis.id_titulo\n"
                    + " WHERE ordenes.estado_orden=0 and historia_clinica.idhistoria_clinica=" + id_historia_clinica + " order by secciones.prioridad, practicas.determinacion_practica, titulos.nombre";
            Statement St = cn.createStatement();
            ResultSet rs = St.executeQuery(sql2);
            informe_mesada datos;
            while (rs.next()) {

                /// JOptionPane.showMessageDialog(null, rs.getString("analisis.estado_titulo"));
                // estado_nombre = rs.getInt("analisis.estado_titulo");
                if (flag == 0) {
                    if (rs.getInt("analisis.estado_titulo") == 0) {
                        titulo = "";
                    } else {
                        titulo = rs.getString(4);
                    }
                    analisis1 = rs.getString(2);
                    seccion = rs.getString(3);
                    flag = 1;
                } else if (seccion.equals(rs.getString(3)) && (titulo.equals(rs.getString(4)) || rs.getInt("analisis.estado_titulo") == 0)) {
                    if (analisis1.equals("")) {
                        analisis1 = rs.getString(2);
                    } else if (analisis2.equals("")) {
                        analisis2 = rs.getString(2);
                    } else if (analisis3.equals("")) {
                        analisis3 = rs.getString(2);
                        datos = new informe_mesada(titulo, seccion, analisis1, analisis2, analisis3);
                        analisis1 = "";
                        analisis2 = "";
                        analisis3 = "";
                        Resultados_mesada.add(datos);
                    }
                } else if (seccion.equals(rs.getString(3))) {
                    if (analisis1.equals("")) {
                        if (rs.getInt("analisis.estado_titulo") == 0) {
                            titulo = "";

                        } else {
                            titulo = rs.getString(4);
                        }
                        titulo = rs.getString(4);
                        analisis1 = rs.getString(2);
                    } else if (analisis2.equals("")) {
                        datos = new informe_mesada(titulo, seccion, analisis1, analisis2, analisis3);
                        if (rs.getInt("analisis.estado_titulo") == 0) {
                            titulo = "";

                        } else {
                            titulo = rs.getString(4);
                        }
                        titulo = rs.getString(4);
                        analisis1 = rs.getString(2);
                        analisis2 = "";
                        analisis3 = "";
                        Resultados_mesada.add(datos);
                    }
                } else {
                    if (analisis1.equals("")) {
                        seccion = rs.getString(3);
                        if (rs.getInt("analisis.estado_titulo") == 0) {

                            titulo = "";
                        } else {
                            titulo = rs.getString(4);
                        }
                        titulo = rs.getString(4);
                        analisis1 = rs.getString(2);
                    } else if (analisis2.equals("")) {
                        datos = new informe_mesada(titulo, seccion, analisis1, analisis2, analisis3);
                        seccion = rs.getString(3);
                        if (rs.getInt("analisis.estado_titulo") == 0) {

                            titulo = "";
                        } else {
                            titulo = rs.getString(4);
                        }
                        titulo = rs.getString(4);
                        analisis1 = rs.getString(2);
                        analisis2 = "";
                        analisis3 = "";
                        Resultados_mesada.add(datos);
                    }

                }
                // datos = new informe_mesada(rs.getString(3), rs.getString(4), rs.getString(2), rs.getString(2), rs.getString(2));
                // datos = new informe_mesada("1","2","3","4","5");
            }
            if (!analisis1.equals("") || !analisis2.equals("") || !analisis3.equals("")) {
                datos = new informe_mesada(titulo, seccion, analisis1, analisis2, analisis3);
                Resultados_mesada.add(datos);
                analisis1 = "";
                analisis2 = "";
                analisis3 = "";

            }

            String sql4 = "SELECT distinct(historia_clinica.idhistoria_clinica), personas.apellido, personas.nombre, pacientes.fecha_nacimiento, pacientes.telefono, pacientes.celular, ordenes.fecha, ordenes.observaciones, ordenes.tipo_orden, ordenes.servicio, historia_clinica.descripcion \n"
                    + "FROM ordenes \n"
                    + "INNER JOIN pacientes ON pacientes.id_Pacientes = ordenes.id_Pacientes \n"
                    + "INNER JOIN personas ON personas.dni = pacientes.personas_dni \n"
                    + "INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes where historia_clinica.idhistoria_clinica=" + id_historia_clinica + " WHERE ordenes.estado_orden=0";

            Statement St1 = cn.createStatement();
            ResultSet rs1 = St1.executeQuery(sql4);
            rs1.next();
            //////////////////////////////////////////////////////////////////////
            String sql6 = "Select nombre_lab, horario_inicio, horario_fin, dias from licencia";
            Statement St6 = cn.createStatement();
            ResultSet rs6 = St6.executeQuery(sql6);
            rs6.next();
            //////////////////////////////////////////////////////////////////
            String sql7 = "Select horientacion,nombre from reportes";
            Statement St7 = cn.createStatement();
            ResultSet rs7 = St7.executeQuery(sql7);
            rs7.next();
            ///////////////////////////////////////////////////////////////////
            if (rs7.getString(1).equals("Predeterminado")) {
                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/informe_mesada_predefi.jasper"));
                Map parametros = new HashMap();
                parametros.put("id_orden", completarceros(rs1.getString("historia_clinica.idhistoria_clinica"), 10));
                parametros.put("laboratorio", rs7.getString(2) + "   " + rs6.getString(2) + " a " + rs6.getString(3) + " de " + rs6.getString(4));
                parametros.put("paciente", rs1.getString("personas.apellido") + ", " + rs1.getString("personas.nombre"));
                parametros.put("fecha_realizacion", rs1.getString("ordenes.fecha"));
                parametros.put("fercha_entrega", agregartiempo(fecha, tiempo_procesamiento));
                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados_mesada));
                //JasperViewer.viewReport(jPrint, true);
                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Informes_Pacientes\\" + " informe_Mesada" + "-" + completarceros(rs1.getString("historia_clinica.idhistoria_clinica"), 10) + "-" + rs1.getString("personas.apellido") + ".pdf");
                JasperPrintManager.printReport(jPrint, false);
            }
            if (rs7.getString(1).equals("A4")) {
                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/informe_mesada_.jasper"));
                Map parametros = new HashMap();
                parametros.put("id_orden", completarceros(rs1.getString("historia_clinica.idhistoria_clinica"), 10));
                parametros.put("laboratorio", rs7.getString(2) + "   " + rs6.getString(2) + " a " + rs6.getString(3) + " de " + rs6.getString(4));
                parametros.put("paciente", rs1.getString("personas.apellido") + ", " + rs1.getString("personas.nombre"));
                parametros.put("fecha_realizacion", rs1.getString("ordenes.fecha"));
                parametros.put("fercha_entrega", agregartiempo(fecha, tiempo_procesamiento));
                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados_mesada));
                //JasperViewer.viewReport(jPrint, true);
                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Informes_Pacientes\\" + " informe_Mesada" + "-" + completarceros(rs1.getString("historia_clinica.idhistoria_clinica"), 10) + "-" + rs1.getString("personas.apellido") + ".pdf");
                JasperPrintManager.printReport(jPrint, false);
            }
            if (rs7.getString(1).equals("A5")) {
                JasperReport report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/informe_mesada_A5.jasper"));
                Map parametros = new HashMap();
                parametros.put("id_orden", completarceros(rs1.getString("historia_clinica.idhistoria_clinica"), 10));
                parametros.put("laboratorio", rs7.getString(2) + "   " + rs6.getString(2) + " a " + rs6.getString(3) + " de " + rs6.getString(4));
                parametros.put("paciente", rs1.getString("personas.apellido") + ", " + rs1.getString("personas.nombre"));
                parametros.put("fecha_realizacion", rs1.getString("ordenes.fecha"));
                parametros.put("fercha_entrega", agregartiempo(fecha, tiempo_procesamiento));
                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(Resultados_mesada));
                //JasperViewer.viewReport(jPrint, true);
                JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Informes_Pacientes\\" + " informe_Mesada" + "-" + completarceros(rs1.getString("historia_clinica.idhistoria_clinica"), 10) + "-" + rs1.getString("personas.apellido") + ".pdf");
                JasperPrintManager.printReport(jPrint, false);
            }
            //
            cn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_btnborrar7ActionPerformed

    private void btnborrar8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnborrar8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnborrar8ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        new Plantillas(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        if (banderacarga == 0) {
            cargaprovincia();
            cargalocalidad();
            banderacarga = 1;
        }
        new AgregarObraSocial(this, true).setVisible(true);
        borrarobrasocial();
        recargarobrasosial();
        cargarobrasocial();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        new Tabla_PracticasNBU(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        new Tabla_ObrasSociales(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void PrincipalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PrincipalKeyPressed

    }//GEN-LAST:event_PrincipalKeyPressed

    private void btnborrarpaciente2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnborrarpaciente2ActionPerformed
        limpiarDatosPaciente();
        new BuscarPersonas(null, true).setVisible(true);
        if (!documento_afiliado.equals("")) {
            txtdnipaciente.setText(documento_afiliado);
            txtdnipacienteActionPerformed(evt);
        } else {
            txtdnipaciente.requestFocus();
        }
    }//GEN-LAST:event_btnborrarpaciente2ActionPerformed

    private void jMenuItem17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem17ActionPerformed
        new Analisis(null, true).setVisible(true);
    }//GEN-LAST:event_jMenuItem17ActionPerformed

    private void btnPatologiasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPatologiasActionPerformed
        if (bandera_paciente == 1 || bandera_paciente == 2 || bandera_paciente == 3) {
            idPaciente = id_paciente;
            new PatologiasPorPaciente(null, true).setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "El paciente no está dado de alta");
        }
    }//GEN-LAST:event_btnPatologiasActionPerformed

    private void txtnumafiliadopacienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnumafiliadopacienteKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            bandera_paciente = 1;
            System.out.println("Formularios.MainB.txtnumafiliadopacienteKeyPressed()");
            validaAfiliado();
            btncargarorden.setEnabled(true);
            btncargarorden.requestFocus();

        }

    }//GEN-LAST:event_txtnumafiliadopacienteKeyPressed

    private void btnFiltrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFiltrarActionPerformed
        if (jCheckBox1.isSelected()) {
            String fechinicial = fechainicial.getText();
            if (fechinicial != null) {
                //  String finicial = formato.format(fechinicial.getTime());
                String fechfinal = fechafinal.getText();
                if (fechfinal != null) {
                    //  String ffinal = formato.format(fechfinal.getTime());
                    // if (finicial.compareTo(ffinal) <= 0) {
                    ///if ((fecha.compareTo(fechfinal) >= 0) && (fecha.compareTo(fechinicial) >= 0)) {
                    bandera_online = 0;
                    new ImprimirObraSocial(this, true).setVisible(true);
                    if (ObraSocial != null) {
                        borrartablaordenes();
                        iniciarSplash();
                        hilo = new HiloOrdenes(progreso);
                        hilo.start();
                        hilo = null;
                    } else {
                        JOptionPane.showMessageDialog(null, "No hay obra social");
                    }
                    ///}
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe completar todos los datos...");
            }
        } else {
            String fechinicial = fechainicial.getText();
            if (fechinicial != null) {
                //  String finicial = formato.format(fechinicial.getTime());
                String fechfinal = fechafinal.getText();
                if (fechfinal != null) {
                    borrartablaordenes();
                    iniciarSplash();
                    hilo = new HiloOrdenes(progreso);
                    hilo.start();
                    hilo = null;
                    //                  }
                    ///}
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe completar todos los datos...");
            }
        }
    }//GEN-LAST:event_btnFiltrarActionPerformed

    private void txtnumafiliadopacienteFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtnumafiliadopacienteFocusGained

        if (!txtnumafiliadopaciente.getText().equals("")) {

            txtnumafiliadopaciente.selectAll();
        }


    }//GEN-LAST:event_txtnumafiliadopacienteFocusGained

    private void txtobrasocialpacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtobrasocialpacienteActionPerformed

        int band = 0;
        obra = txtobrasocialpaciente.getText();
        if (!obra.equals("")) {

            int i = 0;
            while (i < contadorobrasocial) {

                if (obra.equals(obrasocial[i])) {
                    validaOS();
                    if (obra.equals("50015 - BOREAL")
                            || obra.equals("10070 - SWISS MEDICAL GROUP S.A. - ONLINE")
                            || obra.equals("3100 - OSDE")
                            || obra.equals("9000 - ASOCIACION MUTUAL SANCOR")
                            || obra.equals("1805 - SUBSIDIO DE SALUD - ONLINE")
                            || obra.equals("1806 - SUBSIDIO DE SALUD - AUTORIZACION - ONLINE")
                            || obra.equals("3102 - OSDE - OFFLINE")
                            || obra.equals("512 - MEDIFE - ONLINE- PRE PAGA C.M.C.  S.A.")
                            || obra.equals("37701 - JERARQUICOS SALUD - EMP. BNA - ONLINE")) {
                        //|| obra.equals("37701 - JERARQUICOS SALUD - EMP. BNA - ONLINE")) {//SUBSIDIO DE SALUD - AUTORIZACION - ONLINE
                        /////////////////3102 - OSDE - OFFLINE//////////////////////////////////////////////////////////////////////////////
                        if (obra.equals("3102 - OSDE - OFFLINE")) {
                            new osdeOffline(this, true).setVisible(true);
                            id_obra_social = idobrasocial[i];
                            band = 3;
                            contadorobra = i;
                            break;
                        }
                        /////////////////50015 - BOREAL
                        if (obra.equals("50015 - BOREAL")) {
                            System.out.println("boreal online");
                            new BorealAfiliado(this, true).setVisible(true);
                            if (BorealAfiliado.habilitado.equals("OK")) {
                                txtnumafiliadopaciente.setText(BorealAfiliado.Codigo_afiliado);
                                id_obra_social = idobrasocial[i];
                                band = 2;
                                contadorobra = i;
                                txtnumafiliadopaciente.requestFocus();
                                break;
                            }
                        }
                        /////////////////3100 - OSDE
                        if (obra.equals("3100 - OSDE")) {
                            new OsdeAfiliado(this, true).setVisible(true);
                            if (OsdeAfiliado.habilitado.equals("OK")) {
                                txtnumafiliadopaciente.setText(OsdeAfiliado.Codigo_afiliado);
                                id_obra_social = idobrasocial[i];
                                band = 2;
                                contadorobra = i;
                                txtnumafiliadopaciente.requestFocus();
                                break;
                            }
                        }
                        /////////////////9000 - ASOCIACION MUTUAL SANCOR
                        if (obra.equals("9000 - ASOCIACION MUTUAL SANCOR")) {
                            new SancorAfiliado(this, true).setVisible(true);//http://e.sancorsalud.com.ar/APAWE_SSA_V2.aspx?wsdl
                            if (SancorAfiliado.habilitado.equals("OK")) {
                                txtnumafiliadopaciente.setText(SancorAfiliado.Codigo_afiliado);
                                id_obra_social = idobrasocial[i];
                                band = 2;
                                contadorobra = i;
                                txtnumafiliadopaciente.requestFocus();
                                break;
                            }
                        }

                        /////////////////10070 - SWISS MEDICAL GROUP S.A. - ONLINE
                        if (obra.equals("10070 - SWISS MEDICAL GROUP S.A. - ONLINE")) {
                            new SwissAfiliado(this, true).setVisible(true);
                            if (SwissAfiliado.habilitado.equals("OK")) {
                                txtnumafiliadopaciente.setText(SwissAfiliado.Codigo_afiliado);
                                id_obra_social = idobrasocial[i];
                                band = 2;
                                contadorobra = i;
                                txtnumafiliadopaciente.requestFocus();
                                break;
                            }
                        }
                        /////////////////512 - MEDIFE - ONLINE- PRE PAGA C.M.C.  S.A.///////
                        if (obra.equals("512 - MEDIFE - ONLINE- PRE PAGA C.M.C.  S.A.")) {
                            new MedifeAfiliado(this, true).setVisible(true);
                            if (MedifeAfiliado.habilitado.equals("OK")) {
                                txtnumafiliadopaciente.setText(MedifeAfiliado.Codigo_afiliado);
                                id_obra_social = idobrasocial[i];
                                band = 2;
                                contadorobra = i;
                                txtnumafiliadopaciente.requestFocus();
                                break;
                            }
                        }
                        /////////////////37700 - JERARQUICOS SALUD - EMPLEADOS BANCOS NACIONAL////////
                        if (obra.equals("37701 - JERARQUICOS SALUD - EMP. BNA - ONLINE")) {
                            try {
                                new JerarquicosAfiliado(this, true).setVisible(true);

                            } catch (DatatypeConfigurationException ex) {
                                Logger.getLogger(MainB.class
                                        .getName()).log(Level.SEVERE, null, ex);
                            }
                            if (JerarquicosAfiliado.habilitado.equals("OK")) {
                                txtnumafiliadopaciente.setText(JerarquicosAfiliado.Codigo_afiliado);
                                id_obra_social = idobrasocial[i];
                                band = 2;
                                contadorobra = i;
                                txtnumafiliadopaciente.requestFocus();
                                break;
                            }
                        }
                    } else {
                        id_obra_social = idobrasocial[i];
                        band = 1;
                        contadorobra = i;
                        break;
                    }
                }
                i++;
            }

        }
        cargarperiodo();
    }//GEN-LAST:event_txtobrasocialpacienteActionPerformed

    private void txtnumafiliadopacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnumafiliadopacienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnumafiliadopacienteActionPerformed

    private void tablapracticasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablapracticasKeyPressed
        int borrar = 0;
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cargatotalespracticas();
            CargaTotalesPrecios();
        }

        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            tablapracticas.transferFocus();
            evt.consume();
        }

        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
            if (tablapracticas.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila...");
            } else {
                if (banderamodifica == 1) {
                    ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                    Connection cn = mysql.Conectar();
                    try {
                        String sSQL = "SELECT resultado FROM resultados WHERE id_ordenes=" + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 12).toString() + " AND id_practicas=" + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 6).toString();
                        Statement st = cn.createStatement();
                        ResultSet rs = st.executeQuery(sSQL);
                        while (rs.next()) {
                            if (!rs.getString(1).equals("-")) {
                                int opcion = JOptionPane.showConfirmDialog(this, "La practica tiene resultados cargados, desea eliminarla?", "Mensaje", JOptionPane.YES_NO_OPTION);
                                if (opcion != 0) {
                                    borrar = 1;
                                    rs.last();
                                }
                            }
                        }
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }

                    if (borrar == 0) {
                        try {
                            PreparedStatement pst3 = cn.prepareStatement("DELETE FROM resultados WHERE id_ordenes =" + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 12).toString() + " AND id_practicas=" + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 6).toString());
                            pst3.executeUpdate();

                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                        }
                        try {
                            PreparedStatement pst4 = cn.prepareStatement("DELETE FROM ordenes_tienen_practicas WHERE id_ordenes =" + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 12).toString() + " AND id_practicas=" + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 6).toString());
                            pst4.executeUpdate();
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, "Error en la base de datos..." + e);
                        }
                        temp.removeRow(tablapracticas.getSelectedRow());
                        cargatotalespracticas();
                    }
                    try {
                        cn.close();

                    } catch (SQLException ex) {
                        Logger.getLogger(MainB.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    temp.removeRow(tablapracticas.getSelectedRow());
                    cargatotalespracticas();
                }

            }
        }
    }//GEN-LAST:event_tablapracticasKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        limpiarDatosPaciente();
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        int numero = (int) (Math.random() * 70000000) + 1;
        System.out.println("numero al azar: " + numero);
        String sql = "SELECT dni\n"
                + "FROM personas\n"
                + "WHERE dni=" + numero + "  and dni IS NOT NULL";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.wasNull() == false) {
                txtdnipaciente.setText(String.valueOf(numero));
            }
            txtdnipaciente.requestFocus();
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos");

        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtfecha3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfecha3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfecha3ActionPerformed

    private void txtfecha3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfecha3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfecha3KeyPressed

    private void txtfecha3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfecha3KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfecha3KeyReleased

    private void btnimprimirdjj2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimirdjj2ActionPerformed

        if (this.tablaordenes.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "La tabla está vacía");
            return;
        } else {
            JFileChooser chooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
            chooser.setFileFilter(filter);
            chooser.setDialogTitle("Guardar Archivo");
            chooser.setMultiSelectionEnabled(false);
            chooser.setAcceptAllFileFilterUsed(false);
            if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                List<JTable> tb = new ArrayList<>();
                List<String> nom = new ArrayList<>();
                tb.add(tablaordenes);
                nom.add("Periodo" + Enviar_Facturacion.periodobioq);
                String archivo = chooser.getSelectedFile().toString().concat(".xls");
                try {
                    Clases.Exportar e = new Exportar(new File(archivo), tb, nom);
                    if (e.export()) {
                        JOptionPane.showMessageDialog(null, "Los datos se guardaron correctamente", "", JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
                }
            }
        }
    }//GEN-LAST:event_btnimprimirdjj2ActionPerformed

    private void txtfechaPacienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtfechaPacienteMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfechaPacienteMouseClicked

    private void txtfechaPacienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfechaPacienteKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtfechaPaciente.transferFocus();
            txtfechaPaciente2.select(0, 0);
        }
    }//GEN-LAST:event_txtfechaPacienteKeyPressed

    private void txtfechaPaciente2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtfechaPaciente2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfechaPaciente2MouseClicked

    private void txtfechaPaciente2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfechaPaciente2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtfechaPaciente2ActionPerformed

    private void txtfechaPaciente2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfechaPaciente2KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String fechinicial = txtfechaPaciente.getText();
            if (fechinicial != null) {
                //  String finicial = formato.format(fechinicial.getTime());
                String fechfinal = txtfechaPaciente2.getText();
                if (fechfinal != null) {
                    borrartablapacientes();
                    iniciarSplash();//
                    hiloPacientesPorFecha = new HiloBusquedaPacientesPorFecha(progreso);
                    hiloPacientesPorFecha.start();
                    hiloPacientesPorFecha = null;
                    //                  }
                    ///}
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe completar todos los datos...");
            }
        }
    }//GEN-LAST:event_txtfechaPaciente2KeyPressed

    private void btnnomenclador10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador10KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador10KeyPressed

    private void btnnomenclador10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador10ActionPerformed

        new Precio_obrasocial(null, true).setVisible(true);        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador10ActionPerformed

    private void btnnomenclador9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador9KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador9KeyPressed

    private void btnnomenclador9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador9ActionPerformed
        new Usuarios(null, true).setVisible(true);
    }//GEN-LAST:event_btnnomenclador9ActionPerformed

    private void btnsalir15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir15ActionPerformed
        new Practicas(null, true).setVisible(true);
    }//GEN-LAST:event_btnsalir15ActionPerformed

    private void btnnomenclador1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador1KeyPressed

    private void btnnomenclador1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador1ActionPerformed
        new Tabla_PracticasNBU(null, true).setVisible(true);
    }//GEN-LAST:event_btnnomenclador1ActionPerformed

    private void btnnomenclador5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador5KeyPressed

    private void btnnomenclador5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador5ActionPerformed
        new Alta_Materiales(null, true).setVisible(true);
    }//GEN-LAST:event_btnnomenclador5ActionPerformed

    private void btnsalir13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir13ActionPerformed
        new Informe(null, true).setVisible(true);
    }//GEN-LAST:event_btnsalir13ActionPerformed

    private void btnsalir12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir12ActionPerformed
        new Historico(null, true).setVisible(true);
    }//GEN-LAST:event_btnsalir12ActionPerformed

    private void btnnomenclador2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador2KeyPressed

    private void btnnomenclador2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador2ActionPerformed
        facturacion = 0;
        new ElegirBioquimicos(null, true).setVisible(true);
    }//GEN-LAST:event_btnnomenclador2ActionPerformed

    private void btnsalir16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir16ActionPerformed
        new Proceso(null, true).setVisible(true);
    }//GEN-LAST:event_btnsalir16ActionPerformed

    private void btnnomenclador8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador8KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador8KeyPressed

    private void btnnomenclador8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador8ActionPerformed
        facturacion = 1;
        new ElegirBioquimicos(null, true).setVisible(true);
    }//GEN-LAST:event_btnnomenclador8ActionPerformed

    private void btnobrasociales4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnobrasociales4ActionPerformed
        /*facturacion = 1;
        new ElegirBioquimicos(null, true).setVisible(true);*/
        try {
            Desktop.getDesktop().browse(new URI("C:/Descargas-CBT/"));
            //dispose();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "No se ha podido cargar la ruta");
        }
    }//GEN-LAST:event_btnobrasociales4ActionPerformed

    private void btnnomenclador7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador7KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador7KeyPressed

    private void btnnomenclador7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador7ActionPerformed
        new Historias_Clinicas(null, true).setVisible(true);
    }//GEN-LAST:event_btnnomenclador7ActionPerformed

    private void btnobrasociales3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnobrasociales3ActionPerformed
        new Alta_Medico(null, true).setVisible(true);
    }//GEN-LAST:event_btnobrasociales3ActionPerformed

    private void btnsalir14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir14ActionPerformed
        try {
            Desktop.getDesktop().browse(new URI("C:/Informes_Pacientes/"));
            //dispose();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "No se ha podido cargar la ruta");
        }
    }//GEN-LAST:event_btnsalir14ActionPerformed

    private void btnnomenclador6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador6KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador6KeyPressed

    private void btnnomenclador6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador6ActionPerformed
        new Ayuda(null, true).setVisible(true);
    }//GEN-LAST:event_btnnomenclador6ActionPerformed

    private void btnnomenclador4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador4KeyPressed

    private void btnnomenclador4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador4ActionPerformed
        new Analisis(null, true).setVisible(true);
    }//GEN-LAST:event_btnnomenclador4ActionPerformed

    private void btnobrasociales2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnobrasociales2ActionPerformed
        new Pacientes(null, true).setVisible(true);
        cargartablapacientes();
    }//GEN-LAST:event_btnobrasociales2ActionPerformed

    private void btnnomenclador3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador3KeyPressed

    private void btnnomenclador3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador3ActionPerformed
        new Alta_Bioquimicos(null, true).setVisible(true);
    }//GEN-LAST:event_btnnomenclador3ActionPerformed

    private void btnsalir8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir8ActionPerformed
        new Alta_Seccion(null, true).setVisible(true);
    }//GEN-LAST:event_btnsalir8ActionPerformed

    private void btnobrasociales1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnobrasociales1ActionPerformed
        new Tabla_ObrasSociales(null, true).setVisible(true);
    }//GEN-LAST:event_btnobrasociales1ActionPerformed

    private void btnobrasociales5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnobrasociales5ActionPerformed
        new FechaInforme(null, true).setVisible(true);

        if (!fechai.equals("")) {
            ConexionMySQLLocal mysql = new ConexionMySQLLocal();
            Connection cn = mysql.Conectar();
            System.out.println("fecha inicial: " + fechai + " Fecha final " + fechao);
            try {
                JDialog viewer = new JDialog(new javax.swing.JFrame(), "Reporte", true);
                viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
                viewer.setSize(800, 600);
                viewer.setLocationRelativeTo(null);
                System.out.println("previsualizacion definida");
                JasperReport report = null;
                ///////////////////////////////////////////////////////////////////
                Map parametros = new HashMap();
                parametros.put("fechai", fechai);
                parametros.put("fechao", fechao);

                //            parametros.put("fecha_entrega", agregartiempo(fecha, tiempo_procesamiento));
//            parametros.put("edad", edad);
                report = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Informe_diario_Practicas.jasper"));

                JasperPrint jPrint = JasperFillManager.fillReport(report, parametros, cn);
                JasperViewer jv = new JasperViewer(jPrint, false);
                viewer.getContentPane().add(jv.getContentPane());
                viewer.setVisible(true);

            } catch (JRException ex) {
                Logger.getLogger(MainB.class
                        .getName()).log(Level.SEVERE, null, ex);
            }

        }
    }//GEN-LAST:event_btnobrasociales5ActionPerformed

    void ElegirBioquimico() {
        ////////////////////////////////////////////////////////////////////////////////
        String sql = "SELECT nombre, apellido, cuit, matricula, usuario, contraseña,id_colegiado FROM laboratorios";
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.last();
            if (rs.getRow() == 1) {
                cuil = rs.getString(3);
                Enviar_Facturacion.matricula = rs.getString(4);
                Enviar_Facturacion.usuario = rs.getString(5);
                Enviar_Facturacion.contraseña = rs.getString(6);
                System.out.println(cuil);
                ElegirBioquimicos.id_colegiado = rs.getInt(7);
            } else {
                facturacion = 3;
                new ElegirBioquimicos(null, true).setVisible(true);
                System.out.println(cuil);
            }

            cn.close();

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos");

        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Anular;
    private javax.swing.JScrollPane Bioquimicos;
    private javax.swing.JPanel Factura;
    private javax.swing.JMenuItem Modificar;
    private javax.swing.JPanel Orden;
    private javax.swing.JPanel Pacientes;
    private javax.swing.JMenuItem Practicas;
    private javax.swing.JPanel Principal;
    private javax.swing.JMenuItem Resultados;
    private javax.swing.JPanel Utilitario;
    private javax.swing.JMenuItem borrar;
    private javax.swing.JButton btnFiltrar;
    private javax.swing.JButton btnPatologias;
    private javax.swing.JButton btnaceptar3;
    private javax.swing.JButton btnborrar2;
    private javax.swing.JButton btnborrar4;
    private javax.swing.JButton btnborrar5;
    private javax.swing.JButton btnborrar6;
    private javax.swing.JButton btnborrar7;
    private javax.swing.JButton btnborrar8;
    private javax.swing.JButton btnborrarpaciente;
    private javax.swing.JButton btnborrarpaciente2;
    private javax.swing.JButton btncancelar3;
    private javax.swing.JButton btncargarorden;
    private javax.swing.JButton btngrabar;
    private javax.swing.JButton btngrabarimprimir;
    private javax.swing.JButton btnimprimirdjj1;
    private javax.swing.JButton btnimprimirdjj2;
    private javax.swing.JButton btnmodificarpaciente;
    private javax.swing.JButton btnnomenclador1;
    private javax.swing.JButton btnnomenclador10;
    private javax.swing.JButton btnnomenclador2;
    private javax.swing.JButton btnnomenclador3;
    private javax.swing.JButton btnnomenclador4;
    private javax.swing.JButton btnnomenclador5;
    private javax.swing.JButton btnnomenclador6;
    private javax.swing.JButton btnnomenclador7;
    private javax.swing.JButton btnnomenclador8;
    private javax.swing.JButton btnnomenclador9;
    private javax.swing.JButton btnobrasociales1;
    private javax.swing.JButton btnobrasociales10;
    private javax.swing.JButton btnobrasociales2;
    private javax.swing.JButton btnobrasociales3;
    private javax.swing.JButton btnobrasociales4;
    private javax.swing.JButton btnobrasociales5;
    private javax.swing.JButton btnobrasociales6;
    private javax.swing.JButton btnobrasociales7;
    private javax.swing.JButton btnobrasociales8;
    private javax.swing.JButton btnobrasociales9;
    private javax.swing.JButton btnobservaciones;
    private javax.swing.JButton btnsalir11;
    private javax.swing.JButton btnsalir12;
    private javax.swing.JButton btnsalir13;
    private javax.swing.JButton btnsalir14;
    private javax.swing.JButton btnsalir15;
    private javax.swing.JButton btnsalir16;
    private javax.swing.JButton btnsalir17;
    private javax.swing.JButton btnsalir18;
    private javax.swing.JButton btnsalir7;
    private javax.swing.JButton btnsalir8;
    private javax.swing.JButton btnsalir9;
    private javax.swing.JMenuItem cargar;
    private javax.swing.JComboBox cboservicio;
    private javax.swing.JComboBox cbosexo;
    private javax.swing.JCheckBox chkcoseguro;
    private javax.swing.JFormattedTextField fechafinal;
    private javax.swing.JFormattedTextField fechainicial;
    private javax.swing.JButton jButton1;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu jPopupMenu2;
    private javax.swing.JPopupMenu jPopupMenu3;
    private javax.swing.JPopupMenu jPopupMenu4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JMenu jcbt;
    private javax.swing.JMenu jdatos;
    private javax.swing.JMenu jinformes;
    private javax.swing.JMenuItem modifica;
    private javax.swing.JProgressBar progreso;
    private javax.swing.JTable tabladetalle;
    private javax.swing.JTable tablaordenes;
    private javax.swing.JTable tablapacientes;
    private javax.swing.JTable tablapracticas;
    private javax.swing.JTextField txtapellidopaciente;
    private javax.swing.JFormattedTextField txtcama;
    private javax.swing.JTextArea txtdescripcion;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtdnipaciente;
    private javax.swing.JTextField txtedadpaciente;
    private javax.swing.JFormattedTextField txtfecha1;
    private javax.swing.JTextField txtfecha2;
    private javax.swing.JTextField txtfecha3;
    private javax.swing.JFormattedTextField txtfechaPaciente;
    private javax.swing.JFormattedTextField txtfechaPaciente2;
    private javax.swing.JFormattedTextField txtfechanac;
    private javax.swing.JTextArea txtinstrucciones;
    private javax.swing.JTextField txtlocalidad;
    private javax.swing.JLabel txtlogo;
    private javax.swing.JTextField txtmail;
    private javax.swing.JTextField txtmedico;
    private javax.swing.JTextField txtnombreafiliado1;
    private javax.swing.JTextField txtnombrepaciente;
    private javax.swing.JTextField txtnumafiliadopaciente;
    private javax.swing.JTextField txtnumorden2;
    private javax.swing.JTextField txtobrasocialpaciente;
    private javax.swing.JTextField txtordenes1;
    private javax.swing.JTextField txtpacientes;
    private javax.swing.JTextField txtpractica2;
    private javax.swing.JFormattedTextField txtprecio;
    private javax.swing.JTextField txtprotocolo;
    private javax.swing.JTextField txtreciennacido;
    private javax.swing.JTextField txttelcelular;
    private javax.swing.JTextField txttelfijo;
    private javax.swing.JTextField txttipo;
    private javax.swing.JTextField txttotal1;
    private javax.swing.JTextField txttotal2;
    private javax.swing.JTextField txttotal3;
    private javax.swing.JTextField txttotal4;
    private javax.swing.JTextField txttotal5;
    private javax.swing.JTextField txttotalordenes;
    // End of variables declaration//GEN-END:variables

}
