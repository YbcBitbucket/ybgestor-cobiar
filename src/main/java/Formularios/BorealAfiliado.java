package Formularios;

import Clases.Contraseña_Boreal;
import static Formularios.Enviar_Facturacion.matricula;
import static Formularios.MainB.cuil;
import ClienteBoreal.WsBorealExecuteResponse;
import java.awt.Cursor;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class BorealAfiliado extends javax.swing.JDialog {

    public static String habilitado = "", nombreafiliado = "", dni = "", Codigo_afiliado = "";

    String idmsj = "", hora = "", fechaosde = "", pasaporte = "", mensaje = "", respuesta = "";

    public BorealAfiliado(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Afiliado Boreal");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtafiliado = new javax.swing.JFormattedTextField();
        jButton1 = new javax.swing.JButton();

        jLabel2.setText("jLabel2");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ingrese el numero de Afiliado", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Numero:");

        txtafiliado.setForeground(new java.awt.Color(0, 102, 204));
        try {
            txtafiliado.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("########/#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtafiliado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtafiliado.setNextFocusableComponent(jButton1);
        txtafiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtafiliadoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtafiliado, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtafiliado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728951 - electricity lightning.png"))); // NOI18N
        jButton1.setText("Validar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    String completarceros(String v, int d) {
        String ceros = "";
        if (v.length() < d) {
            for (int i = v.length(); i < d; i++) {
                ceros += "0";
            }
            v = ceros + v;
        }
        return v;
    }

    void cursor() {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.pack();
    }

    void cursor2() {
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.pack();
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        cursor();
        String mensajepractica = "", respuestapractica;
        //////////////////////////////////////////////////////////////        
        String emisor = "CBT" + completarceros(matricula, 9);
        Contraseña_Boreal contraseña = new Contraseña_Boreal();
        String clave = contraseña.Boreal_Contraseña();
        /////////////////////////////////////////////////////////////////////////////////
        
        
        mensajepractica = "<Boreal><Mensaje><Canal>ID</Canal><SitioEmisor>" + emisor + "</SitioEmisor><Receptor><Nombre>BOREAL</Nombre><ID>222023</ID><Tipo>IIN</Tipo></Receptor><MsgTipo><Tipo>ZQI</Tipo><Evento>Z01</Evento><Estructura>ZQI_Z01</Estructura></MsgTipo></Mensaje><Seguridad><Usuario>cobitucws</Usuario><Clave>" + clave + "</Clave></Seguridad><Prestador><PrestadorId>" + cuil + "</PrestadorId><PrestadorTipoIdent>CU</PrestadorTipoIdent></Prestador><Afiliado><AfiliadoNroCredencial>" + txtafiliado.getText().substring(0, 8) + "</AfiliadoNroCredencial><AfiliadoGf>" + txtafiliado.getText().substring(9, 10) + "</AfiliadoGf><TipoIdentificador>HC</TipoIdentificador></Afiliado></Boreal>";
        //mensajepractica = "<Boreal><Mensaje><Canal>ID</Canal><SitioEmisor>" + emisor + "</SitioEmisor><Receptor><Nombre>BOREAL</Nombre><ID>222023</ID><Tipo>IIN</Tipo></Receptor><MsgTipo><Tipo>ZQI</Tipo><Evento>Z01</Evento><Estructura>ZQI_Z01</Estructura></MsgTipo></Mensaje><Seguridad><Usuario>cobitucws</Usuario><Clave>" + clave + "</Clave></Seguridad><Prestador><PrestadorId>" + cuil + "</PrestadorId><PrestadorTipoIdent>CU</PrestadorTipoIdent></Prestador><Afiliado><AfiliadoNroCredencial>" + txtafiliado.getText().substring(0, 8) + "</AfiliadoNroCredencial><AfiliadoGf></AfiliadoGf><TipoIdentificador>DU</TipoIdentificador></Afiliado></Boreal>";
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////            
        System.out.println("Testing 1 - Send Http GET request");
        System.out.println(mensajepractica);
        ///////////////////////////////////////////////////////////////////////////////////////////////
        ClienteBoreal.WsBorealExecute servicio = new ClienteBoreal.WsBorealExecute();
        servicio.setIngresoxml(mensajepractica);
        respuestapractica = execute(servicio).getEgresoxml();
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        System.out.println("Testing 2 - Get Http GET request");
        System.out.println(respuestapractica);
        ////////////////////////////////////////////////////////////////////////////////////////////////
        int pos = respuestapractica.indexOf("<AutEstadoId>");
        int pos2 = respuestapractica.indexOf("</AutEstadoId>");
        /// JOptionPane.showMessageDialog(null, respuestapractica.substring(pos + 13, pos2));
        if (respuestapractica.substring(pos + 13, pos2).equals("B000")) {            
            nombreafiliado = respuestapractica.substring(respuestapractica.indexOf("<AfiliadoNombre>") + 16, respuestapractica.indexOf("</AfiliadoNombre>"));
            Codigo_afiliado = txtafiliado.getText();
            habilitado = "OK";
            //////////////////////////////////////
            Mensaje_Afiliado.afiliado=nombreafiliado;
            Mensaje_Afiliado.dni="";
            Mensaje_Afiliado.credencial=Codigo_afiliado;
            Mensaje_Afiliado.plan=respuestapractica.substring(respuestapractica.indexOf("<AfiliadoPlanDes>") + 17, respuestapractica.indexOf("</AfiliadoPlanDes>"));;
            //////////////////////////////////////////////
            new Mensaje_Afiliado(null, true).setVisible(true);
            this.dispose();
        } else {
            habilitado = "ERROR";
            int pos3 = respuestapractica.indexOf("<AutObs>");
            int pos4 = respuestapractica.indexOf("</AutObs>");
            JOptionPane.showMessageDialog(null, respuestapractica.substring(pos3 + 8, pos4));
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtafiliadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtafiliadoActionPerformed
        txtafiliado.transferFocus();        // TODO add your handling code here:
    }//GEN-LAST:event_txtafiliadoActionPerformed
      
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JFormattedTextField txtafiliado;
    // End of variables declaration//GEN-END:variables

    private static WsBorealExecuteResponse execute(ClienteBoreal.WsBorealExecute parameters) {
        ClienteBoreal.WsBoreal service = new ClienteBoreal.WsBoreal();
        ClienteBoreal.WsBorealSoapPort port = service.getWsBorealSoapPort();
        return port.execute(parameters);
    }
}
