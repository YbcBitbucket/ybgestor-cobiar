package Formularios;

import Clases.ConexionMySQL;
import Clases.ConexionMySQLLocal;
import Clases.Escape;
import Clases.Exportar;
import static Formularios.Login.periodo_colegiado;
import static Formularios.MainB.idobraimprime;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class Enviar_Facturacion extends javax.swing.JDialog {

    HiloOrdenes hilo;
    HiloOrdenesImportar hilo4;
    HiloOrdenesXobrasocial hilo2;
    DefaultTableModel model2, model3;
    public static String bioquimico = "", matricula = "", usuario, contraseña, periodobioq, observacion = "";
    String fecha;
    String hora = "", ip2 = "";
    int id_orden, id_colegiados, tipo_orden = 1;
    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;

    public Enviar_Facturacion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Enviar facturación");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        this.setLocationRelativeTo(null);
        labelbioq.setText("Bioq. " + bioquimico);
        labelmat1.setText("Matricula: " + matricula);
        labelperiodo.setText("Periodo: " + periodobioq);
        cargarfecha();
        fechainicial.requestFocus();
        fechainicial.select(0, 0);
        dobleclickordenes();

        Escape.funcionescape(this);

    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }

    void dobleclickordenes() {
        tablaordenes.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    Detalle_Practicas.num_orden = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 4).toString();
                    Detalle_Practicas.id_orden = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0).toString();
                    Detalle_Practicas.afiliado = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 3).toString();
                    Detalle_Practicas.servicio = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 2).toString();
                    Detalle_Practicas.fecha = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 5).toString();
                    Detalle_Practicas.total = tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 6).toString();
                    new Detalle_Practicas(null, true).setVisible(true);
                }
            }
        });
    }

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);

        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);

        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    double calculaprecio(int id_orden) {
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        double total = 0.00;
        try {
            Statement precio = cn.createStatement();
            ResultSet totalr;
            totalr = precio.executeQuery("SELECT sum(precio_practica) FROM ordenes \n"
                    + "INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes \n"
                    + "INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas \n"
                    + "WHERE ordenes_tienen_practicas.factura=1 \n"
                    + "and ordenes_tienen_practicas.estado=1 \n"
                    + "and ordenes.estado_orden=0 \n"
                    + "and ordenes.estado_enviado=0 \n"
                    + "and ordenes.id_ordenes=" + id_orden);
            totalr.next();
            if (totalr.getDouble(1) >= 0) {
                total = Redondear(totalr.getDouble(1));
            } else {
                total = 0.0;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        return total;
    }

    public class HiloOrdenes extends Thread {

        JProgressBar progreso;

        public HiloOrdenes(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            model2 = (DefaultTableModel) tablaordenes.getModel();
            ConexionMySQLLocal cc = new ConexionMySQLLocal();
            Connection cn = cc.Conectar();
            try {
                Statement St = cn.createStatement();
                ResultSet Rs;
                if (chkobrasocial.isSelected()) {
                    Rs = St.executeQuery("SELECT * FROM vista_ordenes_2 WHERE estado_orden=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' AND id_obrasocial=" + idobraimprime + " ORDER BY id_ordenes");
                } else {
                    Rs = St.executeQuery("SELECT * FROM vista_ordenes_2 WHERE estado_orden=0 and tipodefacturaciondirectaocolegio='COLEGIO' and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' ORDER BY id_ordenes");
                }

                String estado = "";
                while (Rs.next()) {
                    if (Rs.getBoolean(8) == false) {
                        if (Rs.getBoolean(8) == true) {
                            estado = "Enviado";
                        } else {
                            estado = "No Enviado";
                            Object nuevo[] = {
                                Rs.getInt(1),
                                Rs.getString(2),
                                Rs.getString(3),
                                Rs.getString(4) + " " + Rs.getString(9),
                                Rs.getString(5),
                                Rs.getString(6),
                                calculaprecio(Rs.getInt(1)),
                                true,
                                estado,
                                Rs.getString(9),
                                Rs.getString(10),
                                Rs.getString(11),
                                Rs.getString(12)};
                            model2.addRow(nuevo);
                        }

                    }
                }
                tablaordenes.setModel(model2);
                /////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(9).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(9).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(9).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(10).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(10).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(10).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(11).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(11).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(11).setPreferredWidth(0);
                //////////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);
                ////////////////////////////////////////////////////////////////////////
                progreso.setValue(1000);
                //cargatotales();
                txtordenes1.requestFocus();
                txtordenes1.setEnabled(true);
                cargatotalesordenesfacturacion();
                cn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public class HiloOrdenesXobrasocial extends Thread {

        JProgressBar progreso;

        public HiloOrdenesXobrasocial(JProgressBar progreso1) {
            super();///WHERE ordenes.id_obrasocial=" + idobraimprime + "
            this.progreso = progreso1;
        }

        public void run() {

            model2 = (DefaultTableModel) tablaordenes.getModel();
            ConexionMySQLLocal cc = new ConexionMySQLLocal();
            Connection cn = cc.Conectar();
            try {
                Statement St = cn.createStatement();
                ///ResultSet Rs = St.executeQuery("SELECT ordenes.id_ordenes, ordenes.periodo, obrasocial.codigo_obrasocial, personas.apellido, ordenes.numero_orden, ordenes.fecha, round(ordenes.total,2), ordenes.estado_orden, ordenes.estado_enviado,personas.nombre,pacientes_tienen_obrasociales.numero_afiliado, personas.dni,medicos.matricula FROM ordenes INNER JOIN pacientes ON pacientes.id_Pacientes = ordenes.id_Pacientes INNER JOIN personas ON personas.dni = pacientes.personas_dni INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial INNER JOIN pacientes_tienen_obrasociales ON (pacientes_tienen_obrasociales.id_Pacientes = ordenes.id_Pacientes and pacientes_tienen_obrasociales.id_obrasocial = ordenes.id_obrasocial) INNER JOIN medicos ON medicos.id_medicos = ordenes.id_medicos WHERE ordenes.id_obrasocial=" + idobraimprime + " AND ordenes.estado_orden=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' ORDER BY id_ordenes");
                ResultSet Rs;
                if (chkobrasocial.isSelected()) {
                    Rs = St.executeQuery("SELECT * FROM vista_ordenes WHERE fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' AND id_obrasocial=" + idobraimprime);
                } else {
                    Rs = St.executeQuery("SELECT * FROM vista_ordenes WHERE fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "'");
                }
                String estado = "";
                while (Rs.next()) {
                    if (Rs.getBoolean(8) == false) {
                        if (Rs.getBoolean(9) == true) {
                            estado = "Enviado";
                        } else {
                            estado = "No Enviado";
                            Object nuevo[] = {
                                Rs.getString(1),
                                Rs.getString(2),
                                Rs.getString(3),
                                Rs.getString(4) + " " + Rs.getString(10),
                                Rs.getString(5),
                                Rs.getString(6),
                                Rs.getString(7),
                                true,
                                estado,
                                Rs.getString(10),
                                Rs.getString(11),
                                Rs.getString(12),
                                Rs.getString(13),};
                            model2.addRow(nuevo);
                        }

                    }
                }
                tablaordenes.setModel(model2);
                tablaordenes.getColumnModel().getColumn(9).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(9).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(9).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(10).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(10).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(10).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(11).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(11).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(11).setPreferredWidth(0);
                //////////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(12).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(12).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(12).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////////////
                progreso.setValue(1000);
                cargatotales();
                txtordenes1.requestFocus();
                txtordenes1.setEnabled(true);
                cargatotalesordenesfacturacion();
                /// cn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
            ////////////////////////////////////////////////////////////

            String[] titulos2 = {"N° Orden", "cod_practica", "1", "2", "3", "4"};//estos seran los titulos de la tabla.            
            String[] datos2 = new String[6];
            model3 = new DefaultTableModel(null, titulos2) {
                ////Celdas no editables////////
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            try {
                Statement St = cn.createStatement();
                Statement total = cn.createStatement();
                ResultSet totalr;
                ResultSet Rs;
                if (chkobrasocial.isSelected()) {
                    Rs = St.executeQuery("SELECT ordenes.id_ordenes, practicas.codigo_practica, practicas.determinacion_practica,precio_practica,cod_practica_fac FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas  WHERE ordenes.estado_orden=0 and ordenes.estado_enviado=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' AND ordenes.id_obrasocial=" + idobraimprime);
                    totalr = total.executeQuery("SELECT sum(precio_practica) FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas  WHERE ordenes.estado_orden=0 and ordenes.estado_enviado=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' AND ordenes.id_obrasocial=" + idobraimprime);
                } else {
                    Rs = St.executeQuery("SELECT ordenes.id_ordenes, practicas.codigo_practica, practicas.determinacion_practica,precio_practica,cod_practica_fac FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas  WHERE ordenes.estado_orden=0 and ordenes.estado_enviado=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "'");
                    totalr = total.executeQuery("SELECT sum(precio_practica) FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas  WHERE ordenes.estado_orden=0 and ordenes.estado_enviado=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "'");
                }
                while (Rs.next()) {
                    datos2[0] = Rs.getString(1);///id orden
                    datos2[1] = Rs.getString(2);////cod practica    
                    datos2[2] = Rs.getString(3);////determinacion
                    datos2[3] = Rs.getString(4);////precio practica
                    datos2[4] = Rs.getString(5);////cod fac prac
                    datos2[5] = "idorden" + Rs.getString(1);
                    model3.addRow(datos2);
                }
                totalr.next();
                txttotal1.setText((String.valueOf(Redondear(totalr.getDouble(1)))));
                tabladetalle.setModel(model3);
                /////////////////////////////////////////////////////////////
                tabladetalle.getColumnModel().getColumn(2).setMaxWidth(0);
                tabladetalle.getColumnModel().getColumn(2).setMinWidth(0);
                tabladetalle.getColumnModel().getColumn(2).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////
                tabladetalle.getColumnModel().getColumn(3).setMaxWidth(0);
                tabladetalle.getColumnModel().getColumn(3).setMinWidth(0);
                tabladetalle.getColumnModel().getColumn(3).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tabladetalle.getColumnModel().getColumn(4).setMaxWidth(0);
                tabladetalle.getColumnModel().getColumn(4).setMinWidth(0);
                tabladetalle.getColumnModel().getColumn(4).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tabladetalle.getColumnModel().getColumn(5).setMaxWidth(0);
                tabladetalle.getColumnModel().getColumn(5).setMinWidth(0);
                tabladetalle.getColumnModel().getColumn(5).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                ///

                /*String[] titulos2 = {"N° Orden", "cod_practica"};//estos seran los titulos de la tabla.            
            String[] datos2 = new String[2];
            model3 = new DefaultTableModel(null, titulos2) {
                ////Celdas no editables////////
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            try {
                Statement St = cn.createStatement();
                ResultSet Rs = St.executeQuery("SELECT ordenes.id_ordenes, practicas.codigo_practica FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas  WHERE factura=1 and ordenes.estado_orden=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "'");
                
                String estado = "";
                while (Rs.next()) {
                    datos2[0] = Rs.getString(1);
                    datos2[1] = Rs.getString(2);
                    model3.addRow(datos2);
                }
                tabladetalle.setModel(model3);*/
                /////////////////////////////////////////////////////////////
                //   tablaordenes.getColumnModel().getColumn(0).setMaxWidth(0);
                ///     tablaordenes.getColumnModel().getColumn(0).setMinWidth(0);
                //  tablaordenes.getColumnModel().getColumn(0).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                ///cn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);

            }
            ////////////////////////////////////////////////////////////
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }
        }

    }

    public void cargatotalesordenesfacturacion() {
        int totalRow = tablaordenes.getRowCount(), contador = 0;
        totalRow -= 1;
        for (int i = 0; i <= (totalRow); i++) {
            if (tablaordenes.getValueAt(i, 7).toString().equals("true")) {
                contador++;
            }
        }
        txttotalordenes.setText((String.valueOf(contador)));
    }

    void cargatotales() {
        double total = 0.00, sumatoria = 0.0;

        DecimalFormat df = new DecimalFormat("0.00");
        //AQUI SE SUMAN LOS VALORES DE CADA FILA PARA COLOCARLO EN EL CAMPO DE TOTAL
        int totalRow = tablaordenes.getRowCount();
        totalRow -= 1;
        for (int i = 0; i <= (totalRow); i++) {
            if (tablaordenes.getValueAt(i, 7).toString().equals("true")) {
                String x = tablaordenes.getValueAt(i, 6).toString();
                sumatoria = Double.valueOf(x);
                total = total + sumatoria;
            }
        }
        //txttotalordenes.setText((String.valueOf(totalRow + 1)));

        txttotal1.setText((String.valueOf(Redondear(total))));

    }

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel11 = new javax.swing.JPanel();
        labelperiodo = new javax.swing.JLabel();
        labelbioq = new javax.swing.JLabel();
        labelmat1 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaordenes = new javax.swing.JTable();
        jLabel25 = new javax.swing.JLabel();
        txttotal1 = new javax.swing.JTextField();
        txtordenes1 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txttotalordenes = new javax.swing.JTextField();
        progreso = new javax.swing.JProgressBar();
        jScrollPane5 = new javax.swing.JScrollPane();
        tabladetalle = new javax.swing.JTable();
        btnenviar = new javax.swing.JButton();
        btnexportar = new javax.swing.JButton();
        btnsalir5 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        fechainicial = new javax.swing.JFormattedTextField();
        fechafinal = new javax.swing.JFormattedTextField();
        chkobrasocial = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Facturante", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        labelperiodo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelperiodo.setForeground(new java.awt.Color(0, 102, 204));

        labelbioq.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelbioq.setForeground(new java.awt.Color(0, 102, 204));

        labelmat1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelmat1.setForeground(new java.awt.Color(0, 102, 204));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelbioq, javax.swing.GroupLayout.DEFAULT_SIZE, 289, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelmat1, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelperiodo, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelbioq, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelperiodo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelmat1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Seleccionar Ordenes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablaordenes.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaordenes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Orden", "Periodo", "Obra Social", "Afiliado", "N° Orden", "Fecha Orden ", "Total", "Enviar", "Estado", "Nom Af", "N Af", "Dni", "Mat"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, true, false, true, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaordenes.setOpaque(false);
        tablaordenes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaordenesMouseClicked(evt);
            }
        });
        tablaordenes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaordenesKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(tablaordenes);

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel25.setText("Total: $");

        txttotal1.setEditable(false);
        txttotal1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txttotal1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txttotal1.setBorder(null);
        txttotal1.setOpaque(false);
        txttotal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotal1ActionPerformed(evt);
            }
        });

        txtordenes1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtordenes1.setForeground(new java.awt.Color(0, 102, 102));
        txtordenes1.setToolTipText("Buscar por dni paciente o num paciente o nom paciente o num orden");
        txtordenes1.setEnabled(false);
        txtordenes1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtordenes1ActionPerformed(evt);
            }
        });
        txtordenes1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtordenes1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtordenes1KeyReleased(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel26.setText("Total Ordenes a Enviar:");

        txttotalordenes.setEditable(false);
        txttotalordenes.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txttotalordenes.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txttotalordenes.setBorder(null);
        txttotalordenes.setOpaque(false);
        txttotalordenes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotalordenesActionPerformed(evt);
            }
        });

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setMaximum(1000);
        progreso.setString("");

        tabladetalle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tabladetalle.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabladetalle.setOpaque(false);
        jScrollPane5.setViewportView(tabladetalle);

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotalordenes, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(txtordenes1)
                        .addGap(470, 470, 470))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jScrollPane3)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtordenes1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txttotal1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel25))
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel26)
                            .addComponent(txttotalordenes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(progreso, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btnenviar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnenviar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/logocbt.png"))); // NOI18N
        btnenviar.setMnemonic('i');
        btnenviar.setText("Enviar a CBT");
        btnenviar.setToolTipText("[Alt + i]");
        btnenviar.setPreferredSize(new java.awt.Dimension(80, 23));
        btnenviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnenviarActionPerformed(evt);
            }
        });

        btnexportar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnexportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728900 - application browser.png"))); // NOI18N
        btnexportar.setMnemonic('d');
        btnexportar.setText("Exportar a Excel");
        btnexportar.setToolTipText("[Alt + d]");
        btnexportar.setPreferredSize(new java.awt.Dimension(80, 23));
        btnexportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnexportarActionPerformed(evt);
            }
        });

        btnsalir5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir5.setMnemonic('s');
        btnsalir5.setText("Volver");
        btnsalir5.setToolTipText("[Alt + s]");
        btnsalir5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir5ActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtrar por Fecha", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 102))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Desde:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Hasta:");

        fechainicial.setForeground(new java.awt.Color(0, 102, 204));
        try {
            fechainicial.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        fechainicial.setToolTipText("");
        fechainicial.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        fechainicial.setDropMode(javax.swing.DropMode.INSERT);
        fechainicial.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        fechainicial.setNextFocusableComponent(fechafinal);
        fechainicial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fechainicialActionPerformed(evt);
            }
        });
        fechainicial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechainicialKeyPressed(evt);
            }
        });

        fechafinal.setForeground(new java.awt.Color(0, 102, 204));
        try {
            fechafinal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        fechafinal.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        fechafinal.setDropMode(javax.swing.DropMode.INSERT);
        fechafinal.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        fechafinal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fechafinalActionPerformed(evt);
            }
        });
        fechafinal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechafinalKeyPressed(evt);
            }
        });

        chkobrasocial.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chkobrasocial.setForeground(new java.awt.Color(51, 51, 51));
        chkobrasocial.setText("Por Obra Social");
        chkobrasocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkobrasocialActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(chkobrasocial, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(fechainicial, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(fechafinal, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(fechafinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(fechainicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(chkobrasocial)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnenviar, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnsalir5, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnsalir5)
                        .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnenviar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /* public javax.swing.JProgressBar getjProgressBar2() {
     return progreso3;
     }*/
    void borrartabla() {
        DefaultTableModel temp = (DefaultTableModel) tablaordenes.getModel();
        int a = temp.getRowCount() - 1;  //Índices van de 0 a n-1
        for (int i = a; i >= 0; i--) {
            temp.removeRow(i);
        }
    }

    String invertir(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }
        String salida = "";
        int i = 0;
        /////Año/////
        for (i = 6; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ///Mes///
        for (i = 3; i <= 4; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "-";
        ////Dia////
        for (i = 0; i <= 1; i++) {
            salida = salida + entrada.charAt(i);
        }

        return salida;

    }

    String revertir(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }
        String salida = "";
        int i = 0;
        ////Dia////
        for (i = 8; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "/";
        ///Mes///
        for (i = 5; i <= 6; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "/";
        /////Año/////
        for (i = 0; i <= 3; i++) {
            salida = salida + entrada.charAt(i);
        }

        return salida;

    }


    private void tablaordenesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaordenesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
            clic_practica();
            cargatotales();
            cargatotalesordenesfacturacion();
        }
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_UP) {
            clic_practica();
            cargatotales();
            cargatotalesordenesfacturacion();
        }
    }//GEN-LAST:event_tablaordenesKeyPressed

    private void txttotal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotal1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotal1ActionPerformed

    private void txtordenes1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtordenes1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtordenes1ActionPerformed

    private void txtordenes1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtordenes1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtordenes1KeyPressed

    private void txtordenes1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtordenes1KeyReleased
        TableRowSorter sorter = new TableRowSorter(model2);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtordenes1.getText() + ".*"));
        tablaordenes.setRowSorter(sorter);
    }//GEN-LAST:event_txtordenes1KeyReleased

    private void btnsalir5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir5ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalir5ActionPerformed

    private void txttotalordenesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotalordenesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotalordenesActionPerformed

    private void btnexportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnexportarActionPerformed
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        int i = 1, bandera = 0;
        int i2 = 4;
        String mes = "";
        new Periodo(null, true).setVisible(true);
        if (!Enviar_Facturacion.periodobioq.equals("")) {
            while (i2 < 6) {
                mes = mes + String.valueOf(Enviar_Facturacion.periodobioq).charAt(i2);
                i2++;
            }
            DefaultTableModel model2 = (DefaultTableModel) tablaordenes.getModel();
            try {
                Statement St = cn.createStatement();
                ResultSet Rs = St.executeQuery("SELECT ordenes.id_ordenes, ordenes.periodo, obrasocial.codigo_obrasocial, personas.apellido, ordenes.numero_orden, ordenes.fecha, round(ordenes.total,2), ordenes.estado_orden FROM ordenes INNER JOIN pacientes ON pacientes.id_Pacientes = ordenes.id_Pacientes INNER JOIN personas ON personas.dni = pacientes.personas_dni INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.estado_orden=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "'");
                while (Rs.next()) {
                    Object nuevo[] = {
                        Rs.getString(1),
                        Rs.getString(2),
                        Rs.getString(3),
                        Rs.getString(4),
                        Rs.getString(5),
                        Rs.getString(6),
                        Rs.getString(7),
                        Rs.getBoolean(8)};
                    model2.addRow(nuevo);
                }
                tablaordenes.setModel(model2);
                /////////////////////////////////////////////////////////////
                //   tablaordenes.getColumnModel().getColumn(0).setMaxWidth(0);
                ///     tablaordenes.getColumnModel().getColumn(0).setMinWidth(0);
                //  tablaordenes.getColumnModel().getColumn(0).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                tablaordenes.getColumnModel().getColumn(1).setMaxWidth(0);
                tablaordenes.getColumnModel().getColumn(1).setMinWidth(0);
                tablaordenes.getColumnModel().getColumn(1).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////
                alinear();
                tablaordenes.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);
                tablaordenes.getColumnModel().getColumn(7).setCellRenderer(alinearCentro);
                progreso.setValue(100);
                cargatotales();
                cargatotalesordenesfacturacion();
                int opcion = 0;
                if (opcion == 0) {
                    if (this.tablaordenes.getRowCount() == 0) {
                        JOptionPane.showMessageDialog(null, "La tabla está vacía");
                        return;
                    }
                    JFileChooser chooser = new JFileChooser();
                    FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
                    chooser.setFileFilter(filter);
                    chooser.setDialogTitle("Guardar Archivo");
                    chooser.setMultiSelectionEnabled(false);
                    chooser.setAcceptAllFileFilterUsed(false);
                    if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                        List<JTable> tb = new ArrayList<>();
                        List<String> nom = new ArrayList<>();
                        tb.add(tablaordenes);
                        nom.add("Periodo" + Enviar_Facturacion.periodobioq);
                        String archivo = chooser.getSelectedFile().toString().concat(".xls");
                        try {
                            Clases.Exportar e = new Exportar(new File(archivo), tb, nom);
                            if (e.export()) {

                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
                        }
                    }
                } else {

                }
                ///    cn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
            ////////////////////////////////////////////////////////////
            String[] titulos2 = {"Id_ordenes", "cod_practica"};//estos seran los titulos de la tabla.            
            String[] datos2 = new String[2];
            model3 = new DefaultTableModel(null, titulos2) {
                ////Celdas no editables////////
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            try {
                Statement St = cn.createStatement();
                ResultSet Rs = St.executeQuery("SELECT ordenes.id_ordenes, practicas.codigo_practica FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas  WHERE ordenes.estado_orden=0 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "'");
                String estado = "";
                while (Rs.next()) {
                    datos2[0] = Rs.getString(1);
                    datos2[1] = Rs.getString(2);
                    model3.addRow(datos2);
                }
                tabladetalle.setModel(model3);
                /////////////////////////////////////////////////////////////
                //   tablaordenes.getColumnModel().getColumn(0).setMaxWidth(0);
                ///     tablaordenes.getColumnModel().getColumn(0).setMinWidth(0);
                //  tablaordenes.getColumnModel().getColumn(0).setPreferredWidth(0);
                /////////////////////////////////////////////////////////////////

                if (this.tabladetalle.getRowCount() == 0) {
                    JOptionPane.showMessageDialog(null, "La tabla está vacía");
                    return;
                } else {
                    JFileChooser chooser = new JFileChooser();
                    FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
                    chooser.setFileFilter(filter);
                    chooser.setDialogTitle("Guardar Archivo");
                    chooser.setMultiSelectionEnabled(false);
                    chooser.setAcceptAllFileFilterUsed(false);
                    if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                        List<JTable> tb = new ArrayList<>();
                        List<String> nom = new ArrayList<>();
                        tb.add(tabladetalle);
                        nom.add("Periodo" + Enviar_Facturacion.periodobioq);
                        String archivo = chooser.getSelectedFile().toString().concat(".xls");
                        try {
                            Clases.Exportar e = new Exportar(new File(archivo), tb, nom);
                            if (e.export()) {
                                JOptionPane.showMessageDialog(null, "Los datos se guardaron correctamente", "", JOptionPane.INFORMATION_MESSAGE);
                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
                        }
                    }
                }
                cn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);

            }
            ////////////////////////////////////////////////////////////
        }
    }//GEN-LAST:event_btnexportarActionPerformed

    void cargarfecha() {
        ////////////////////////////////////////////////////////////////////////
        try {
            String thisIp = InetAddress.getLocalHost().getHostAddress();
            String thisname = InetAddress.getLocalHost().getHostName();
            ip2 = thisIp + "-" + thisname;
        } catch (Exception e) {
            e.printStackTrace();
        }
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
        /////////////////////////////////////////////////////////////////////
        SimpleDateFormat formatoTiempo = new SimpleDateFormat("HHmmss");
        java.util.Date currentDate1 = new java.util.Date();
        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(currentDate1);
        hora = formatoTiempo.format(currentDate1);
        fechainicial.setText(fecha);
        fechafinal.setText(fecha);
    }


    private void fechainicialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fechainicialActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_fechainicialActionPerformed

    private void fechainicialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechainicialKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            fechainicial.transferFocus();
            fechafinal.select(0, 0);
        }
    }//GEN-LAST:event_fechainicialKeyPressed

    private void fechafinalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechafinalKeyPressed

    }//GEN-LAST:event_fechafinalKeyPressed


    private void fechafinalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fechafinalActionPerformed
        if (chkobrasocial.isSelected()) {
            String fechinicial = fechainicial.getText();
            if (fechinicial != null) {
                String fechfinal = fechafinal.getText();
                if (fechfinal != null) {
                    System.out.println((fechfinal.compareTo(fechfinal)));
                    if ((fechfinal.compareTo(fechfinal) >= 0)) {

                        new ImprimirObraSocial(null, true).setVisible(true);
                        borrartabla();
                        iniciarSplash();
                        hilo = new HiloOrdenes(progreso);
                        hilo.start();
                        hilo = null;
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe completar todos los datos...");
            }
        } else {
            String fechinicial = fechainicial.getText();
            if (fechinicial != null) {
                String fechfinal = fechafinal.getText();
                if (fechfinal != null) {
                    if ((fechfinal.compareTo(fechfinal) >= 0)) {
                        borrartabla();
                        iniciarSplash();
                        hilo = new HiloOrdenes(progreso);
                        hilo.start();
                        hilo = null;
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe completar todos los datos...");
            }
        }
    }//GEN-LAST:event_fechafinalActionPerformed

    void clic_practica() {
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        ////////////////////////////////////////////////////////////
        String[] titulos2 = {"N° Orden", "cod_practica", "1", "2", "3", "4"};//estos seran los titulos de la tabla.            
        String[] datos2 = new String[6];
        model3 = new DefaultTableModel(null, titulos2) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        try {
            Statement St = cn.createStatement();
            Statement total = cn.createStatement();
            ResultSet totalr;
            ResultSet Rs;
            if (chkobrasocial.isSelected()) {
                Rs = St.executeQuery("SELECT ordenes.id_ordenes, practicas.codigo_practica, practicas.determinacion_practica,precio_practica,cod_practica_fac FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas  WHERE ordenes.estado_orden=0 and ordenes.estado_enviado=0 and ordenes_tienen_practicas.factura=1 and ordenes_tienen_practicas.estado=1 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' AND ordenes.id_obrasocial=" + idobraimprime + " and ordenes.id_ordenes=" + tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0));
                totalr = total.executeQuery("SELECT sum(precio_practica) FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas  WHERE ordenes.estado_orden=0 and ordenes.estado_enviado=0 and ordenes_tienen_practicas.factura=1 and ordenes_tienen_practicas.estado=1 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' AND ordenes.id_obrasocial=" + idobraimprime + " and ordenes.id_ordenes=" + tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0));
            } else {
                Rs = St.executeQuery("SELECT ordenes.id_ordenes, practicas.codigo_practica, practicas.determinacion_practica,precio_practica,cod_practica_fac FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas  WHERE ordenes.estado_orden=0 and ordenes.estado_enviado=0 and ordenes_tienen_practicas.factura=1 and ordenes_tienen_practicas.estado=1 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "'" + " and ordenes.id_ordenes=" + tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0));
                totalr = total.executeQuery("SELECT sum(precio_practica) FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas  WHERE ordenes.estado_orden=0 and ordenes.estado_enviado=0 and ordenes_tienen_practicas.factura=1 and ordenes_tienen_practicas.estado=1 and fecha BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "'" + " and ordenes.id_ordenes=" + tablaordenes.getValueAt(tablaordenes.getSelectedRow(), 0));
            }
            String estado = "";
            while (Rs.next()) {
                datos2[0] = Rs.getString(1);///id orden
                datos2[1] = Rs.getString(2);////cod practica    
                datos2[2] = Rs.getString(3);////determinacion
                datos2[3] = Rs.getString(4);////precio practica
                datos2[4] = Rs.getString(5);////cod fac prac
                datos2[5] = "idorden" + Rs.getString(1);
                model3.addRow(datos2);
            }
            totalr.next();
            txttotal1.setText((String.valueOf(Redondear(totalr.getDouble(1)))));
            tabladetalle.setModel(model3);
            /////////////////////////////////////////////////////////////
            tabladetalle.getColumnModel().getColumn(2).setMaxWidth(0);
            tabladetalle.getColumnModel().getColumn(2).setMinWidth(0);
            tabladetalle.getColumnModel().getColumn(2).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            tabladetalle.getColumnModel().getColumn(3).setMaxWidth(0);
            tabladetalle.getColumnModel().getColumn(3).setMinWidth(0);
            tabladetalle.getColumnModel().getColumn(3).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////////
            tabladetalle.getColumnModel().getColumn(4).setMaxWidth(0);
            tabladetalle.getColumnModel().getColumn(4).setMinWidth(0);
            tabladetalle.getColumnModel().getColumn(4).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////////
            tabladetalle.getColumnModel().getColumn(5).setMaxWidth(0);
            tabladetalle.getColumnModel().getColumn(5).setMinWidth(0);
            tabladetalle.getColumnModel().getColumn(5).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////////
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        ////////////////////////////////////////////////////////////
    }

    private void tablaordenesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaordenesMouseClicked
        clic_practica();
        ///cargatotales();
        cargatotalesordenesfacturacion();
    }//GEN-LAST:event_tablaordenesMouseClicked

    private void btnenviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnenviarActionPerformed
        ////////////////////////////////////////////
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        String sSQL2 = "SELECT periodos,id_colegiados FROM colegiados WHERE matricula_colegiado=" + "'" + matricula + "' and tipo_profesional='B'";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL2);
            while (rs.next()) {
                periodo_colegiado = rs.getInt("periodos");
                id_colegiados = rs.getInt("id_colegiados");
            }
            cn.close();
            iniciarSplash2();
            hilo4 = new HiloOrdenesImportar(progreso);
            hilo4.start();
            hilo4 = null;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error 1 " + e);
        }

    }//GEN-LAST:event_btnenviarActionPerformed

    private void chkobrasocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkobrasocialActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkobrasocialActionPerformed

    public javax.swing.JProgressBar getjProgressBar2() {
        return progreso;
    }

    public void iniciarSplash2() {
        this.getjProgressBar2().setBorderPainted(false);
        this.getjProgressBar2().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar2().setStringPainted(true);
    }

    public double Redondearentero(double numero) {
        return Math.round(numero);
    }

    public class HiloOrdenesImportar extends Thread {

        JProgressBar progreso;

        public HiloOrdenesImportar(JProgressBar progreso3) {
            super();
            this.progreso = progreso3;

        }

        public void run() {
            ConexionMySQL mysql = new ConexionMySQL();
            Connection cn = mysql.Conectar();
            ConexionMySQLLocal mysql2 = new ConexionMySQLLocal();
            Connection cn2 = mysql2.Conectar();
            int cantidad;
            String errores = "";
            ////////////////////////////////////////////
            int indice = 1; //indice contara los numeros primos que vamos encontrando    
            int i2 = 0, nbu, id_obra_social = 0;
            int p = 0, i4 = 0, bandera_orden;
            int n4 = tablaordenes.getRowCount();
            progreso.setMaximum(n4);
            progreso.setValue(1);
            if (n4 != 0) {
                bandera_orden = 0;
                if (Redondearentero(n4 * 5 / 136) != 0) {
                    JOptionPane.showMessageDialog(null, "El proceso durará aproximadamente " + Redondearentero(n4 * 5 / 136) + " minutos...");
                } else {
                    JOptionPane.showMessageDialog(null, "El proceso durará unos segundos...");
                }
                try {
                    String sSQL4 = "SELECT estado FROM periodos ";
                    Statement st4 = cn.createStatement();
                    ResultSet rs4 = st4.executeQuery(sSQL4);
                    rs4.next();
                    if (rs4.getBoolean("estado") == true) {

                        while (i4 < n4) {
                            if (tablaordenes.getValueAt(i4, 7).toString().equals("true") && !tablaordenes.getValueAt(i4, 8).toString().equals("Enviado")) {
                                if (tablaordenes.getRowCount() != 0) {
                                    //  orden", "Periodo", "Obra Social", "N° Afiliado", "N° Orden", "Fecha Orden", "Total", "Estado"};//estos seran los titulos de la tabla.            
                                    double total = 0.0, totalordenes = 0.0;
                                    String num_afiliado, precio, nom_afiliado, dni_afiliado, fecha_orden, matricula_presc, num_orden;
                                    String sSQL = "", ssQL = "", plan = "", cod_practica = "", coseguro = "", fecha_coseguro = "";
                                    int n, n2, n3, i, j, bandera = 0, obra_social, id_orden_local;
                                    num_afiliado = tablaordenes.getValueAt(i4, 10).toString();
                                    nom_afiliado = tablaordenes.getValueAt(i4, 3).toString() + " " + tablaordenes.getValueAt(i4, 9).toString();
                                    dni_afiliado = tablaordenes.getValueAt(i4, 11).toString();
                                    fecha_orden = tablaordenes.getValueAt(i4, 5).toString();
                                    matricula_presc = tablaordenes.getValueAt(i4, 12).toString();
                                    obra_social = Integer.valueOf(tablaordenes.getValueAt(i4, 2).toString());
                                    num_orden = tablaordenes.getValueAt(i4, 4).toString();
                                    id_orden_local = Integer.valueOf(tablaordenes.getValueAt(i4, 0).toString());
                                    /////////////////////////////////////
                                    i = 0;
                                    if (bandera == 0) {
                                        ////////////////////////ordenes/////////////////////////  
                                        int ContadorPractica = 0;
                                        try {
                                            Statement St = cn2.createStatement();
                                            ResultSet Rs = St.executeQuery("SELECT practicas.codigo_practica  FROM ordenes INNER JOIN ordenes_tienen_practicas ON ordenes_tienen_practicas.id_ordenes=ordenes.id_ordenes INNER JOIN practicas ON practicas.id_practicas=ordenes_tienen_practicas.id_practicas  WHERE factura=1 and ordenes.id_ordenes=" + Integer.valueOf(tablaordenes.getValueAt(i4, 0).toString()));
                                            while (Rs.next()) {
                                                cod_practica = cod_practica + Rs.getString(1);
                                                coseguro = coseguro + "00000.0";
                                                plan = plan + "00";

                                                ContadorPractica++;
                                            }
                                        } catch (SQLException ex) {
                                            JOptionPane.showMessageDialog(null, ex);
                                        }
                                        totalordenes = Double.valueOf(tablaordenes.getValueAt(i4, 6).toString());
                                        //////////////PROCEDURE///////////////////////////////
                                        try {
                                            /////////////////////////////////////////////////////////////////
                                            String sSQL3 = "SELECT id_obrasocial FROM obrasocial WHERE codigo_obrasocial=" + obra_social;
                                            Statement st3 = cn.createStatement();
                                            ResultSet rs3 = st3.executeQuery(sSQL3);
                                            try {
                                                rs3.next();
                                                id_obra_social = rs3.getInt("id_obrasocial");
                                            } catch (Exception e) {
                                                JOptionPane.showMessageDialog(null, e + " 2");
                                            }/*
                                            N _periodo INT (6),
                                            IN _nombre VARCHAR (100),
                                            IN _dni VARCHAR (40),
                                            IN _numero_afiliado VARCHAR (40),
                                            IN _matricula VARCHAR (40),
                                            IN _numero_orden VARCHAR (40),
                                            IN _fecha_realizacion VARCHAR(10),
                                            IN _total VARCHAR (40),
                                            IN _estado INT (1),
                                            IN _fecha VARCHAR (10),
                                            IN _hora VARCHAR (8),
                                            IN _ip VARCHAR (40),
                                            IN _id_obrasocial INT (11),
                                            IN _id_colegiado INT (11),
                                            IN _id_validador INT (11),
                                            IN _cantidad INT (2),
                                            IN _codigo_nbu VARCHAR (252),
                                            OUT _transaccion INT(11),
                                            IN _coseguro VARCHAR (20),
                                            IN _fecha_coseguro VARCHAR (10),
                                            IN _tipo INT (1),
                                            IN _observacion VARCHAR (255),
                                            IN _codigo_plan VARCHAR (255),
                                            IN _coseguro_practicas VARCHAR (255)
                                             */
                                            ////CALL cargar_orden (201606,'euge','31589460','64454564545','4564','12345',' 01/07/2016','350.25',1,'01/07/2016','130947','192.192.192.192',24,1173,7,2,'660001660002',@_transaccion);
                                            CallableStatement SP_cargar_orden = null;
                                            SP_cargar_orden = cn.prepareCall("{CALL cargar_orden2 (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                                            SP_cargar_orden.setString(1, periodobioq);
                                            SP_cargar_orden.setString(2, nom_afiliado);
                                            SP_cargar_orden.setString(3, dni_afiliado);
                                            SP_cargar_orden.setString(4, num_afiliado);
                                            SP_cargar_orden.setString(5, matricula_presc);
                                            SP_cargar_orden.setString(6, num_orden);
                                            SP_cargar_orden.setString(7, revertir(fecha_orden));
                                            SP_cargar_orden.setDouble(8, Redondear(totalordenes));
                                            SP_cargar_orden.setString(9, "1");
                                            SP_cargar_orden.setString(10, fecha);
                                            SP_cargar_orden.setString(11, hora);
                                            SP_cargar_orden.setString(12, ip2);
                                            SP_cargar_orden.setInt(13, id_obra_social);
                                            SP_cargar_orden.setInt(14, id_colegiados);
                                            SP_cargar_orden.setString(15, null);
                                            SP_cargar_orden.setInt(16, ContadorPractica);
                                            SP_cargar_orden.setString(17, cod_practica);
                                            SP_cargar_orden.setString(18, "@_transaccion");
                                            SP_cargar_orden.setString(19, "0.0");
                                            SP_cargar_orden.setString(20, fecha_coseguro);
                                            SP_cargar_orden.setInt(21, tipo_orden);
                                            SP_cargar_orden.setString(22, observacion);
                                            SP_cargar_orden.setInt(21, tipo_orden);
                                            SP_cargar_orden.setString(22, observacion);
                                            SP_cargar_orden.setString(23, plan);
                                            SP_cargar_orden.setString(24, coseguro);
                                            boolean d = SP_cargar_orden.execute();
                                            if (d == true) {
                                                try {
                                                    String sSQL8;
                                                    sSQL8 = "UPDATE ordenes SET estado_enviado=?  WHERE id_ordenes=" + (id_orden_local);
                                                    PreparedStatement pst3 = cn2.prepareStatement(sSQL8);
                                                    pst3.setInt(1, 1);
                                                    pst3.executeUpdate();
                                                    progreso.setValue(i4);
                                                } catch (Exception e) {
                                                    JOptionPane.showMessageDialog(null, e + "error 3");
                                                }
                                            }
                                        } catch (Exception e) {
                                            JOptionPane.showMessageDialog(null, e);
                                        }
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "No hay practicas en la tabla...");
                                    i4 = n4;
                                }
                                bandera_orden = 1;

                            }
                            indice++;
                            i4++;
                        }
                        progreso.setValue(1000);
                    } else {
                        JOptionPane.showMessageDialog(null, "Período finalizado. Debe verificar que Período desea cargar");
                    }
                    ///cn.close();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                    JOptionPane.showMessageDialog(null, "Error en la base de datos");
                }
                progreso.setValue(indice); //aumentamos la barra 
                if (bandera_orden == 1) {
                    JOptionPane.showMessageDialog(null, "Proceso realizado con exito...");
                    dispose();
                    ///////////////////////////////////////////////////
                }

            }
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    boolean isNumeric(String cadena) {
        try {
            Double.parseDouble(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    void traeorden() {
        String sSQL = "";
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();

        sSQL = "SELECT MAX(id_orden) AS id_orden FROM ordenes";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.next();
            if (rs.getInt("id_orden") != 0) {
                id_orden = rs.getInt("id_orden");
            }
            //// cn.close();
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnenviar;
    private javax.swing.JButton btnexportar;
    private javax.swing.JButton btnsalir5;
    private javax.swing.JCheckBox chkobrasocial;
    private javax.swing.JFormattedTextField fechafinal;
    private javax.swing.JFormattedTextField fechainicial;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel labelbioq;
    private javax.swing.JLabel labelmat1;
    private javax.swing.JLabel labelperiodo;
    private javax.swing.JProgressBar progreso;
    private javax.swing.JTable tabladetalle;
    private javax.swing.JTable tablaordenes;
    private javax.swing.JTextField txtordenes1;
    private javax.swing.JTextField txttotal1;
    private javax.swing.JTextField txttotalordenes;
    // End of variables declaration//GEN-END:variables
}
