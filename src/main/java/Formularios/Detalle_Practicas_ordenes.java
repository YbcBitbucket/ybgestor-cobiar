package Formularios;

import Clases.ConexionMySQLLocal;
import static Clases.Escape.funcionescape;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Detalle_Practicas_ordenes extends javax.swing.JDialog {

    DefaultTableModel model;
    public static String id_orden, afiliado, servicio, fecha, total, num_orden, hora, historia_clinica;

    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;

    public Detalle_Practicas_ordenes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Detalle");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());

        this.setLocationRelativeTo(null);
        //cargartabla(id_orden);
        System.out.println(id_orden);
        cargartabla(id_orden);
        txtorden.setText(id_orden);
        txtafiliado.setText(afiliado);
        txtfecha.setText(fecha);
        // cargartotales();
        funcionescape(this);
        this.setResizable(false);
    }

    void cargartotales() {
        double total1 = 0.00, desc, sumatoria;
        //AQUI SE SUMAN LOS VALORES DE LA COLUMNAS
        int totalRow = tablapracticas.getRowCount();
        totalRow -= 1;
        for (int i = 0; i <= (totalRow); i++) {
            String x = tablapracticas.getValueAt(i, 2).toString();
            sumatoria = Double.valueOf(x).doubleValue();
            total1 = total1 + sumatoria;
        }
    }

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    void cargartabla(String valor) {
        String[] Titulo = {"Cod", "Practica", "Resultado", "N° de Orden", "O.Social"};
        String[] Registros = new String[5];
        //String sql = "SELECT ordenes_tienen_practicas.id_ordenes, practicas.codigo_practica, practicas.determinacion_practica, precio_practica FROM ordenes_tienen_practicas INNER JOIN practicas ON practicas.id_practicas = ordenes_tienen_practicas.id_practicas  WHERE ordenes_tienen_practicas.id_ordenes=" + valor;
        String sql = "SELECT ordenes_tienen_practicas.id_ordenes, practicas.codigo_practica, practicas.determinacion_practica, numero_orden,razonsocial_obrasocial,factura,Int_codigo_obrasocial,pacientes_tienen_obrasociales.numero_afiliado\n"
                + "FROM ordenes_tienen_practicas \n"
                + "INNER JOIN ordenes ON ordenes_tienen_practicas.id_ordenes = ordenes.id_ordenes \n"
                + "INNER JOIN practicas ON practicas.id_practicas = ordenes_tienen_practicas.id_practicas \n"
                + "INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes \n"
                + "INNER JOIN obrasocial on obrasocial.id_obrasocial=ordenes.id_obrasocial\n"
                + "INNER JOIN pacientes_tienen_obrasociales ON pacientes_tienen_obrasociales.id_obrasocial=ordenes.id_obrasocial and pacientes_tienen_obrasociales.id_Pacientes=ordenes.id_Pacientes\n"
                + "WHERE ordenes_tienen_practicas.id_ordenes=" + valor;
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                if (column == 3) {
                    return true;
                } else {
                    return false;
                }
            }
        };
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {

                Registros[0] = rs.getString(2);
                Registros[1] = rs.getString(3);
                Registros[2] = rs.getString(4);
                //////////N° de Orden
                Registros[3] = rs.getString("numero_orden");
                if (rs.getInt("factura") == 1) {
                    Registros[4] = rs.getString("razonsocial_obrasocial");

                } else {
                    Registros[4] = "PARTICULAR";
                }
                txtnumafiliado.setText(rs.getString("numero_afiliado"));
                model.addRow(Registros);

            }
            tablapracticas.setModel(model);
            //  tablapracticas.setAutoCreateRowSorter(true);
            /////////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(2).setMaxWidth(0);
            tablapracticas.getColumnModel().getColumn(2).setMinWidth(0);
            tablapracticas.getColumnModel().getColumn(2).setPreferredWidth(0);
            /////ajustar ancho de columna///////
            tablapracticas.getColumnModel().getColumn(0).setPreferredWidth(50);
            tablapracticas.getColumnModel().getColumn(1).setPreferredWidth(200);
            tablapracticas.getColumnModel().getColumn(3).setPreferredWidth(80);
            tablapracticas.getColumnModel().getColumn(4).setPreferredWidth(200);
            /////////////////////////////////////////////////////////////*/
            alinear();
            tablapracticas.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
            tablapracticas.getColumnModel().getColumn(1).setCellRenderer(alinearIzquierda);
            tablapracticas.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
            tablapracticas.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
            //////////////////////////////////////////////////////////////////
            /// cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);

        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);

        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablapracticas = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtorden = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtafiliado = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtfecha = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtnumafiliado = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Practicas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        jPanel1.setToolTipText("");

        tablapracticas.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablapracticas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tablapracticas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablapracticasKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablapracticas);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("N° Protocolo:");

        txtorden.setEditable(false);
        txtorden.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtorden.setForeground(new java.awt.Color(0, 102, 204));
        txtorden.setBorder(null);
        txtorden.setOpaque(false);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Afiliado:");

        txtafiliado.setEditable(false);
        txtafiliado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtafiliado.setForeground(new java.awt.Color(0, 102, 204));
        txtafiliado.setBorder(null);
        txtafiliado.setOpaque(false);
        txtafiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtafiliadoActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Fecha:");

        txtfecha.setEditable(false);
        txtfecha.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtfecha.setForeground(new java.awt.Color(0, 102, 204));
        txtfecha.setBorder(null);
        txtfecha.setOpaque(false);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Nº Afiliado:");

        txtnumafiliado.setEditable(false);
        txtnumafiliado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtnumafiliado.setForeground(new java.awt.Color(0, 102, 204));
        txtnumafiliado.setBorder(null);
        txtnumafiliado.setOpaque(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 614, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtnumafiliado))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtafiliado))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtfecha, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtorden, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtnumafiliado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtafiliado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txtorden, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(txtfecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablapracticasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablapracticasKeyPressed
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        try {
            ////////////////////////////ordenes////////////////////////////////////////////////////////
            String sSQL3 = "UPDATE ordenes SET numero_orden=? "
                    + " WHERE id_ordenes=" + id_orden;
            PreparedStatement pst = cn.prepareStatement(sSQL3);
            pst.setString(1, tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 3).toString());
            int i = pst.executeUpdate();
            if (i > 0) {
                JOptionPane.showMessageDialog(null, "Se modificó con exito...");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_tablapracticasKeyPressed

    private void txtafiliadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtafiliadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtafiliadoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablapracticas;
    private javax.swing.JTextField txtafiliado;
    private javax.swing.JTextField txtfecha;
    private javax.swing.JTextField txtnumafiliado;
    private javax.swing.JTextField txtorden;
    // End of variables declaration//GEN-END:variables
}
