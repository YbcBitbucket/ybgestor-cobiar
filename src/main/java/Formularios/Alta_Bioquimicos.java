package Formularios;

import Clases.ConexionMySQLLocal;
import Clases.Escape;
import Clases.solomayusculas;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Alta_Bioquimicos extends javax.swing.JDialog {

    DefaultTableModel model;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    int id_bioquimico;
    public int bmodificar = 0;

    public Alta_Bioquimicos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Alta bioquímicos");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        cargartabla("");
        eventotabla();
        dobleclick();
        Escape.funcionescape(this);
        this.setLocationRelativeTo(null);
        txtnombre.setDocument(new solomayusculas());
        txtapellido.setDocument(new solomayusculas());
        txtdireccion.setDocument(new solomayusculas());
        
    }

    void dobleclick() {
        tablamedico.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    id_bioquimico = Integer.valueOf(tablamedico.getValueAt(tablamedico.getSelectedRow(), 0).toString());
                    txtnombre.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 2).toString());
                    txtapellido.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 1).toString());
                    txtmatricula.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 3).toString());
                    txtdireccion.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 4).toString());
                    txttelefono.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 5).toString());
                    txtcuil.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 6).toString());
                    txtcelular.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 7).toString());
                    txtdni.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 8).toString());
                    txtusuario.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 9).toString());
                    txtcontraseña.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 10).toString());
                    txtMailLab.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 11).toString());
                    txtcontraseña1.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 12).toString());
                    bmodificar = 1;
                }
            }
        });
    }

    void cargartabla(String valor) {
        String[] Titulo = {"id_laboratorios", "Apellido", "Nombre", "Matricula", "", "", "", "", "", "", "", "", ""};
        String[] Registros = new String[13];
        String sql = "SELECT id_laboratorios, nombre, apellido, matricula, direccion, telefono, cuit, celular, dni, usuario, contraseña, mail_direccion, mail_contraseña FROM laboratorios WHERE concat(apellido,'', nombre,'',matricula) LIKE '%" + valor + "%'";

        int i = 0;
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString("id_laboratorios");
                Registros[1] = rs.getString("apellido");
                Registros[2] = rs.getString("nombre");
                Registros[3] = rs.getString("matricula");
                if (rs.getString("direccion") != null) {
                    Registros[4] = rs.getString("direccion");
                } else {
                    Registros[4] = "";
                }
                if (rs.getString("telefono") != null) {
                    Registros[5] = rs.getString("telefono");
                } else {
                    Registros[5] = "";
                }
                if (rs.getString("cuit") != null) {
                    Registros[6] = rs.getString("cuit");
                } else {
                    Registros[6] = "";
                }
                if (rs.getString("celular") != null) {
                    Registros[7] = rs.getString("celular");
                } else {
                    Registros[7] = "";
                }
                if (rs.getString("dni") != null) {
                    Registros[8] = rs.getString("dni");
                } else {
                    Registros[8] = "";
                }
                Registros[9] = rs.getString("usuario");
                Registros[10] = rs.getString("contraseña");
                Registros[11] = rs.getString("mail_direccion");
                Registros[12] = rs.getString("mail_contraseña");
                model.addRow(Registros);
            }
            tablamedico.setModel(model);
            tablamedico.setAutoCreateRowSorter(true);
            /////////////////////////////////////////////////////////////
            tablamedico.getColumnModel().getColumn(0).setMaxWidth(0);
            tablamedico.getColumnModel().getColumn(0).setMinWidth(0);
            tablamedico.getColumnModel().getColumn(0).setPreferredWidth(0);

            tablamedico.getColumnModel().getColumn(4).setMaxWidth(0);
            tablamedico.getColumnModel().getColumn(4).setMinWidth(0);
            tablamedico.getColumnModel().getColumn(4).setPreferredWidth(0);

            tablamedico.getColumnModel().getColumn(5).setMaxWidth(0);
            tablamedico.getColumnModel().getColumn(5).setMinWidth(0);
            tablamedico.getColumnModel().getColumn(5).setPreferredWidth(0);

            tablamedico.getColumnModel().getColumn(6).setMaxWidth(0);
            tablamedico.getColumnModel().getColumn(6).setMinWidth(0);
            tablamedico.getColumnModel().getColumn(6).setPreferredWidth(0);

            tablamedico.getColumnModel().getColumn(7).setMaxWidth(0);
            tablamedico.getColumnModel().getColumn(7).setMinWidth(0);
            tablamedico.getColumnModel().getColumn(7).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            tablamedico.getColumnModel().getColumn(8).setMaxWidth(0);
            tablamedico.getColumnModel().getColumn(8).setMinWidth(0);
            tablamedico.getColumnModel().getColumn(8).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            tablamedico.getColumnModel().getColumn(9).setMaxWidth(0);
            tablamedico.getColumnModel().getColumn(9).setMinWidth(0);
            tablamedico.getColumnModel().getColumn(9).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            tablamedico.getColumnModel().getColumn(10).setMaxWidth(0);
            tablamedico.getColumnModel().getColumn(10).setMinWidth(0);
            tablamedico.getColumnModel().getColumn(10).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            tablamedico.getColumnModel().getColumn(11).setMaxWidth(0);
            tablamedico.getColumnModel().getColumn(11).setMinWidth(0);
            tablamedico.getColumnModel().getColumn(11).setPreferredWidth(0);
            //////////////////////////////////////////////////////////////////
            tablamedico.getColumnModel().getColumn(12).setMaxWidth(0);
            tablamedico.getColumnModel().getColumn(12).setMinWidth(0);
            tablamedico.getColumnModel().getColumn(12).setPreferredWidth(0);
            alinear();
            tablamedico.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
            tablamedico.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
            tablamedico.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);

            //////////////////////////
            ////cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        }
    }

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    void eventotabla() {
        tablamedico.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        ListSelectionModel rowSM = tablamedico.getSelectionModel();

        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                //Ignore extra messages.
                if (e.getValueIsAdjusting()) {
                    return;
                }

                ListSelectionModel lsm = (ListSelectionModel) e.getSource();

                if (lsm.isSelectionEmpty()) {
                } else {

                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtapellido = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtmatricula = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtcuil = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtdni = new javax.swing.JTextField();
        txttelefono = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtcelular = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtusuario = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtMailLab = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtcontraseña = new javax.swing.JPasswordField();
        txtcontraseña1 = new javax.swing.JPasswordField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablamedico = new javax.swing.JTable();
        btnaceptar = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Alta Bioquimicos"));

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Nombre:");

        txtnombre.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.setNextFocusableComponent(txtapellido);
        txtnombre.setPreferredSize(new java.awt.Dimension(200, 21));
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Apellido:");

        txtapellido.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtapellido.setForeground(new java.awt.Color(0, 102, 204));
        txtapellido.setMinimumSize(new java.awt.Dimension(200, 21));
        txtapellido.setNextFocusableComponent(txtdni);
        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtapellidoKeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("telefono:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Matrícula:");

        txtmatricula.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmatricula.setForeground(new java.awt.Color(0, 102, 204));
        txtmatricula.setNextFocusableComponent(txtcelular);
        txtmatricula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmatriculaKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("direccion:");

        txtdireccion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdireccion.setForeground(new java.awt.Color(0, 102, 204));
        txtdireccion.setMinimumSize(new java.awt.Dimension(200, 21));
        txtdireccion.setNextFocusableComponent(txtmatricula);
        txtdireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdireccionKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Dni:");

        txtcuil.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcuil.setForeground(new java.awt.Color(0, 102, 204));
        txtcuil.setMinimumSize(new java.awt.Dimension(200, 21));
        txtcuil.setNextFocusableComponent(txtdireccion);
        txtcuil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcuilKeyPressed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Cuil:");

        txtdni.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdni.setForeground(new java.awt.Color(0, 102, 204));
        txtdni.setNextFocusableComponent(txtcuil);
        txtdni.setPreferredSize(new java.awt.Dimension(200, 21));
        txtdni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdniKeyPressed(evt);
            }
        });

        txttelefono.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txttelefono.setForeground(new java.awt.Color(0, 102, 204));
        txttelefono.setNextFocusableComponent(txtusuario);
        txttelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttelefonoKeyPressed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Celular:");

        txtcelular.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcelular.setForeground(new java.awt.Color(0, 102, 204));
        txtcelular.setNextFocusableComponent(txttelefono);
        txtcelular.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcelularKeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Contraseña:");

        txtusuario.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtusuario.setForeground(new java.awt.Color(0, 102, 204));
        txtusuario.setMinimumSize(new java.awt.Dimension(200, 21));
        txtusuario.setNextFocusableComponent(txtcontraseña);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("Usuario CBT:");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("Contr. Mail:");

        txtMailLab.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtMailLab.setForeground(new java.awt.Color(0, 102, 204));
        txtMailLab.setMinimumSize(new java.awt.Dimension(200, 21));
        txtMailLab.setNextFocusableComponent(txtcontraseña1);

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText("Mail Lab:");

        txtcontraseña.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcontraseña.setForeground(new java.awt.Color(0, 102, 204));
        txtcontraseña.setNextFocusableComponent(txtMailLab);
        txtcontraseña.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcontraseñaKeyPressed(evt);
            }
        });

        txtcontraseña1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcontraseña1.setForeground(new java.awt.Color(0, 102, 204));
        txtcontraseña1.setNextFocusableComponent(btnaceptar);
        txtcontraseña1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcontraseña1KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMailLab, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtusuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtnombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtapellido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtdni, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtcuil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtdireccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtmatricula)
                    .addComponent(txtcelular)
                    .addComponent(txttelefono)
                    .addComponent(txtcontraseña, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtcontraseña1, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(39, 39, 39)
                                    .addComponent(txtmatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5)))
                            .addGap(18, 18, 18)
                            .addComponent(txtcelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(39, 39, 39))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1)
                                .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel3)
                                        .addComponent(txtapellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(18, 18, 18)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel6)
                                        .addComponent(txtdni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel8))
                                    .addGap(18, 18, 18)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel7)
                                        .addComponent(txtcuil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel4))))))
                    .addComponent(txttelefono, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtusuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(txtcontraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtMailLab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(txtcontraseña1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tablamedico.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablamedico.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablamedico);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnaceptar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728934 - enter login right.png"))); // NOI18N
        btnaceptar.setText("Aceptar");
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir.setText("Volver");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(353, 353, 353)
                        .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnsalir, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        String nombre, apellido, matricula, direccion, cuit, telefono, celular, dni;
        String sSQL = "";
        nombre = txtnombre.getText();
        apellido = txtapellido.getText();
        matricula = txtmatricula.getText();
        direccion = txtdireccion.getText();
        if (!txtcuil.getText().equals("")) {
            cuit = txtcuil.getText();
        } else {
            cuit = "0";
        }
        if (!txttelefono.getText().equals("")) {
            telefono = txttelefono.getText();
        } else {
            telefono = "0";
        }
        if (!txtcelular.getText().equals("")) {
            celular = txtcelular.getText();
        } else {
            celular = "0";
        }
        if (!txtdni.getText().equals("")) {
            dni = txtdni.getText();
        } else {
            dni = "0";
        }
        int control = 0;

        if (!"".equals(nombre) && !"".equals(apellido) && !"".equals(matricula)) {
            //String mail =txtusuario1.getText().substring(txtusuario1.getText().indexOf("@"),txtusuario1.getText().indexOf(".com"));

            if (bmodificar == 0) {

                ///////////////////agregar datos//////////////////////////////
                try {
                    String sql = "SELECT matricula FROM laboratorios";
                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sql);
                    int contador = 0; //inicio
                    while (rs.next()) {
                        contador++;
                    }
                    rs.beforeFirst();// fin
                    if (contador != 0) {
                        while (rs.next()) {

                            if (matricula.equals(rs.getString("matricula"))) {

                                JOptionPane.showMessageDialog(null, "La matrícula ya existe");
                                txtmatricula.requestFocus();
                                txtmatricula.selectAll();

                                control = 1;
                                break;

                            }
                        }
                    }
                    rs.beforeFirst();
                    if (control == 0) {
                        sSQL = "INSERT INTO laboratorios  (nombre, apellido, matricula, direccion, telefono, cuit, celular, dni,usuario, contraseña, mail_direccion, mail_contraseña) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                        PreparedStatement pst = cn.prepareStatement(sSQL);
                        pst.setString(1, nombre);
                        pst.setString(2, apellido);
                        pst.setString(3, matricula);
                        pst.setString(4, direccion);
                        pst.setString(5, telefono);
                        pst.setString(6, cuit);
                        pst.setString(7, celular);
                        pst.setString(8, dni);
                        pst.setString(9, txtusuario.getText());
                        pst.setString(10, txtcontraseña.getText());
                        pst.setString(11, txtMailLab.getText());
                        pst.setString(12, txtcontraseña1.getText());
                        int n = pst.executeUpdate();
                        if (n > 0) {
                            JOptionPane.showMessageDialog(null, "Bioquímico cargado con éxito...");
                            cargartabla("");                            
                        }
                    }
                  ////  cn.close();
                } catch (SQLException ex) {

                    JOptionPane.showMessageDialog(null, ex);
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");

                }
            } else {
                /////////////////////////////////////////////
                try {

                    String sSQL3 = "UPDATE laboratorios SET nombre=?, apellido=?, matricula=?, direccion=?, telefono=?, cuit=?, celular=?, dni=?,usuario=?, contraseña=?, mail_direccion=?, mail_contraseña=? WHERE id_laboratorios=" + tablamedico.getValueAt(tablamedico.getSelectedRow(), 0).toString();
                    PreparedStatement pst = cn.prepareStatement(sSQL3);
                    pst.setString(1, nombre);
                    pst.setString(2, apellido);
                    pst.setString(3, matricula);
                    pst.setString(4, direccion);
                    pst.setString(5, telefono);
                    pst.setString(6, cuit);
                    pst.setString(7, celular);
                    pst.setString(8, dni);
                    pst.setString(9, txtusuario.getText());
                    pst.setString(10, txtcontraseña.getText());
                    pst.setString(11, txtMailLab.getText());
                    pst.setString(12, txtcontraseña1.getText());
                    int n = pst.executeUpdate();
                    if (n > 0) {
                        JOptionPane.showMessageDialog(null, "Bioquímico modificado con éxito...");
                    }
                    txtnombre.setText("");
                    txtapellido.setText("");
                    txtmatricula.setText("");
                    txtcelular.setText("");
                    txtcuil.setText("");
                    txtdireccion.setText("");
                    txtdni.setText("");
                    txttelefono.setText("");
                    txtcontraseña.setText("");
                    txtusuario.setText("");
                    txtcontraseña1.setText("");
                    txtMailLab.setText("");
                    cargartabla("");
                   /// cn.close();
                } catch (SQLException ex) {

                    JOptionPane.showMessageDialog(null, ex);
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");

                }
                /////////////////////////////////////////////
            }
            
            ////////////////////////modifica datos/////////////////////////////////
        } else {
            JOptionPane.showMessageDialog(null, "Debe completar los campos obligatorios");
        }
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void txtnombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtnombre.transferFocus();
        }
    }//GEN-LAST:event_txtnombreKeyPressed

    private void txtapellidoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtapellidoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtapellido.transferFocus();
        }
    }//GEN-LAST:event_txtapellidoKeyPressed

    private void txtdniKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdniKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtdni.transferFocus();
        }
    }//GEN-LAST:event_txtdniKeyPressed

    private void txtcuilKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcuilKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtcuil.transferFocus();
        }
    }//GEN-LAST:event_txtcuilKeyPressed

    private void txtdireccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdireccionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtdireccion.transferFocus();
        }
    }//GEN-LAST:event_txtdireccionKeyPressed

    private void txtmatriculaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmatriculaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtmatricula.transferFocus();
        }
    }//GEN-LAST:event_txtmatriculaKeyPressed

    private void txtcelularKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcelularKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtcelular.transferFocus();
        }
    }//GEN-LAST:event_txtcelularKeyPressed

    private void txttelefonoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelefonoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txttelefono.transferFocus();
        }
    }//GEN-LAST:event_txttelefonoKeyPressed

    private void txtcontraseñaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcontraseñaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtcontraseña.transferFocus();
        }
    }//GEN-LAST:event_txtcontraseñaKeyPressed

    private void txtcontraseña1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcontraseña1KeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtcontraseña1.transferFocus();
        }
    }//GEN-LAST:event_txtcontraseña1KeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablamedico;
    private javax.swing.JTextField txtMailLab;
    private javax.swing.JTextField txtapellido;
    private javax.swing.JTextField txtcelular;
    private javax.swing.JPasswordField txtcontraseña;
    private javax.swing.JPasswordField txtcontraseña1;
    private javax.swing.JTextField txtcuil;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtdni;
    private javax.swing.JTextField txtmatricula;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextField txttelefono;
    private javax.swing.JTextField txtusuario;
    // End of variables declaration//GEN-END:variables
}
