package Formularios;

import Clases.ConexionMySQLLocal;
import Clases.Escape;
import static Formularios.Resultados.idpaciente;
import static Formularios.Resultados.referencia;
import static Formularios.Resultados.resultado;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.table.DefaultTableModel;

public class Resultado extends javax.swing.JDialog {

    public static String nombre = "";
    DefaultTableModel modelo;
    int idresultados[] = new int[1000];
    String[] tooltips = new String[50];

    class MyComboBoxRenderer extends BasicComboBoxRenderer {

        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
                if (-1 < index) {
                    list.setToolTipText(tooltips[index]);
                }
            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            setFont(list.getFont());
            setText((value == null) ? "" : value.toString());
            return this;
        }
    }

    public Resultado(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Agregar resultado");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        Escape.funcionescape(this);
        this.setLocationRelativeTo(null);
        jlabelnidad.setText(Resultados.unidad);
        txtnombre.setText(nombre);
        txt_valores_referencia.setText(referencia);
        cargarcomboresutados();
        cargarpatologias();
        //cargarpaciente();
        dobleclick();
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                Resultados.resultado = "";
            }
        });
        /// JOptionPane.showMessageDialog(null, resultado);
        txtresultado.setText(resultado);

        txtresultado.requestFocus();
        txtresultado.selectAll();
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

    }

    void cargarcomboresutados() {
        int i = 0, j = 0;
        String sql = "SELECT id, nombre FROM tipo_resultados where id_practicas =" + Resultados.idpratica + " and id_analisis=" + Resultados.idanalisis + " and estado=1";
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        cbotiporesultado.removeAllItems();
        cbotiporesultado.addItem("...");
        tooltips[j] = ("...");
        j++;
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                idresultados[i] = (rs.getInt(1));
                cbotiporesultado.addItem(rs.getString(2));
                tooltips[j] = rs.getString(2);
                i++;
                j++;
            }
            cbotiporesultado.setRenderer(new MyComboBoxRenderer());
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        }
    }

    void cargarpatologias() {
        int i = 0;
        String sql = "SELECT nombre FROM patologias where id_paciente =" + idpaciente;
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                //if(i==0){
                if (!jTextArea1.getText().equals("")) {
                    jTextArea1.setText(jTextArea1.getText() + " - " + rs.getString(1));
                } else {
                    jTextArea1.setText(rs.getString(1));
                }
                i++;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        }
    }

    void dobleclick() {
        tablaResultadosAnteriores.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    txtresultado.setText(tablaResultadosAnteriores.getValueAt(tablaResultadosAnteriores.getSelectedRow(), 1).toString());
                    Resultados.resultado = txtresultado.getText();
                    dispose();
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaResultadosAnteriores = new javax.swing.JTable();
        txtnombre = new javax.swing.JLabel();
        btnprincipal = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        cbotiporesultado = new javax.swing.JComboBox();
        jScrollPane3 = new javax.swing.JScrollPane();
        txt_valores_referencia = new javax.swing.JTextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jlabelnidad = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtresultado = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        btnprincipal1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(876, 460));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Resultados Anteriores", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(153, 153, 153))); // NOI18N

        tablaResultadosAnteriores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tablaResultadosAnteriores);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        txtnombre.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));

        btnprincipal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnprincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnprincipal.setMnemonic('p');
        btnprincipal.setText("Volver");
        btnprincipal.setMaximumSize(new java.awt.Dimension(110, 41));
        btnprincipal.setMinimumSize(new java.awt.Dimension(110, 43));
        btnprincipal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnprincipalActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Tipos de Resultados", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(153, 153, 153))); // NOI18N

        cbotiporesultado.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cbotiporesultado.setForeground(new java.awt.Color(0, 102, 204));
        cbotiporesultado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cbotiporesultadoMouseClicked(evt);
            }
        });
        cbotiporesultado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbotiporesultadoActionPerformed(evt);
            }
        });
        cbotiporesultado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cbotiporesultadoKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbotiporesultado, 0, 464, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbotiporesultado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        txt_valores_referencia.setColumns(20);
        txt_valores_referencia.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txt_valores_referencia.setForeground(new java.awt.Color(255, 0, 0));
        txt_valores_referencia.setLineWrap(true);
        txt_valores_referencia.setRows(5);
        txt_valores_referencia.setBorder(javax.swing.BorderFactory.createTitledBorder("Referencias"));
        txt_valores_referencia.setCaretColor(new java.awt.Color(255, 255, 255));
        txt_valores_referencia.setMaximumSize(new java.awt.Dimension(220, 75));
        jScrollPane3.setViewportView(txt_valores_referencia);

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jTextArea1.setForeground(new java.awt.Color(0, 102, 204));
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setBorder(javax.swing.BorderFactory.createTitledBorder("Patologías"));
        jScrollPane1.setViewportView(jTextArea1);

        jlabelnidad.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jlabelnidad.setForeground(new java.awt.Color(51, 51, 51));

        txtresultado.setColumns(20);
        txtresultado.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        txtresultado.setLineWrap(true);
        txtresultado.setRows(5);
        txtresultado.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultado"));
        txtresultado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtresultadoKeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(txtresultado);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Unidad:");

        btnprincipal1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnprincipal1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728901 - archive office.png"))); // NOI18N
        btnprincipal1.setMnemonic('p');
        btnprincipal1.setText("Resultados Anteriores");
        btnprincipal1.setMaximumSize(new java.awt.Dimension(110, 41));
        btnprincipal1.setMinimumSize(new java.awt.Dimension(110, 43));
        btnprincipal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnprincipal1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3)
                            .addComponent(jScrollPane4)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jlabelnidad, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnprincipal1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtnombre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 854, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnprincipal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jlabelnidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnprincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnprincipal1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    void cargarpaciente() {

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        String Titulo[] = {"Nº", "Resultado", "Fecha",};
        String sSQL = "SELECT * from vista_resultados_anteriores WHERE id_pacientes=" + Resultados.idpaciente + " and id_analisis=" + Resultados.idanalisis;

        System.out.println(Resultados.idpaciente);
        System.out.println(Resultados.idanalisis);

        String[] Registros = new String[3];
        int i = 1;
        modelo = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }

        };
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                //   if(!rs.last()){
                System.out.println(rs.getString(2));
                Registros[0] = String.valueOf(i);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(1);
                modelo.addRow(Registros);
                i++;

                // }
            }
            tablaResultadosAnteriores.setModel(modelo);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    private void btnprincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnprincipalActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnprincipalActionPerformed

    private void cbotiporesultadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbotiporesultadoMouseClicked

    }//GEN-LAST:event_cbotiporesultadoMouseClicked

    private void cbotiporesultadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbotiporesultadoActionPerformed
        if (!txtresultado.getText().equals("-")) {
            txtresultado.setText(txtresultado.getText() + ", " + cbotiporesultado.getSelectedItem().toString());
            txtresultado.requestFocus();
            txtresultado.selectAll();
        } else {
            txtresultado.setText(cbotiporesultado.getSelectedItem().toString());
            txtresultado.requestFocus();
            txtresultado.selectAll();
        }
    }//GEN-LAST:event_cbotiporesultadoActionPerformed

    private void cbotiporesultadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cbotiporesultadoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!txtresultado.getText().equals("-")) {
                txtresultado.setText(txtresultado.getText() + ", " + cbotiporesultado.getSelectedItem().toString());
                txtresultado.requestFocus();
                txtresultado.selectAll();
            } else {
                txtresultado.setText(cbotiporesultado.getSelectedItem().toString());
                txtresultado.requestFocus();
                txtresultado.selectAll();
            }
            dispose();
        }
    }//GEN-LAST:event_cbotiporesultadoKeyPressed

    private void txtresultadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtresultadoKeyPressed
        int varlor_predeterminado = 0;

        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            varlor_predeterminado = 1;
        }
        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            varlor_predeterminado = 2;
        }
        if (evt.getKeyCode() == KeyEvent.VK_F3) {
            varlor_predeterminado = 3;
        }
        if (evt.getKeyCode() == KeyEvent.VK_F4) {
            varlor_predeterminado = 4;
        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            varlor_predeterminado = 5;
        }
        if (evt.getKeyCode() == KeyEvent.VK_F6) {
            varlor_predeterminado = 6;
        }
        if (evt.getKeyCode() == KeyEvent.VK_F7) {
            varlor_predeterminado = 7;
        }

        if (varlor_predeterminado != 0) {
            if (cbotiporesultado.getItemCount() != 1 && cbotiporesultado.getItemCount() > varlor_predeterminado) {
                cbotiporesultado.setSelectedIndex(varlor_predeterminado);
                if (!txtresultado.getText().equals("-")) {
                    Resultados.imprime_analisis = varlor_predeterminado;
                    Resultados.resultado = cbotiporesultado.getSelectedItem().toString();
                }
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(null, "No existen valores predeterminados");
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txtresultado.getText().equals(ABORT)) {
                Resultados.resultado = txtresultado.getText();
            }
            if (!txtresultado.getText().equals("-")) {
                Resultados.imprime_analisis = 1;
                Resultados.resultado = txtresultado.getText();
            }
            this.dispose();
        }
    }//GEN-LAST:event_txtresultadoKeyPressed

    private void btnprincipal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnprincipal1ActionPerformed
        if (tablaResultadosAnteriores.getRowCount() == 0) {
            cargarpaciente();
        }
    }//GEN-LAST:event_btnprincipal1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnprincipal;
    private javax.swing.JButton btnprincipal1;
    private javax.swing.JComboBox cbotiporesultado;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel jlabelnidad;
    private javax.swing.JTable tablaResultadosAnteriores;
    private javax.swing.JTextArea txt_valores_referencia;
    private javax.swing.JLabel txtnombre;
    private javax.swing.JTextArea txtresultado;
    // End of variables declaration//GEN-END:variables
}
