package Formularios;

import Clases.ConexionMySQLLocal;
import Clases.Escape;
import static Formularios.MainB.facturacion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class ElegirBioquimicos extends javax.swing.JDialog {

    public static int id_orden, id_colegiado;
    String usua, contra, dire, lug;

    public ElegirBioquimicos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Bioquímicos");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        this.setLocationRelativeTo(null);
        Escape.funcionescape(this);
        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        txtbuscar = new javax.swing.JTextField();
        txtapellido = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        txtmatricula = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtcuil = new javax.swing.JTextField();
        btnimprimir2 = new javax.swing.JButton();
        btnimprimir3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Elegir Bioquimico", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Buscar Bioquimo:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Apellido:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Nombre:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Matricula:");

        txtbuscar.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        txtbuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        txtapellido.setEditable(false);
        txtapellido.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        txtapellido.setForeground(new java.awt.Color(0, 102, 204));
        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtapellidoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtapellidoKeyReleased(evt);
            }
        });

        txtnombre.setEditable(false);
        txtnombre.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnombreKeyReleased(evt);
            }
        });

        txtmatricula.setEditable(false);
        txtmatricula.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        txtmatricula.setForeground(new java.awt.Color(0, 102, 204));
        txtmatricula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmatriculaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmatriculaKeyReleased(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Cuil:");

        txtcuil.setEditable(false);
        txtcuil.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        txtcuil.setForeground(new java.awt.Color(0, 102, 204));
        txtcuil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcuilKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcuilKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                    .addComponent(txtapellido, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                    .addComponent(txtnombre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                    .addComponent(txtmatricula, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtcuil))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtapellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtmatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtcuil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btnimprimir2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728934 - enter login right.png"))); // NOI18N
        btnimprimir2.setMnemonic('a');
        btnimprimir2.setText("Aceptar");
        btnimprimir2.setToolTipText("[Alt + a]");
        btnimprimir2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimir2ActionPerformed(evt);
            }
        });

        btnimprimir3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnimprimir3.setMnemonic('v');
        btnimprimir3.setText("Volver");
        btnimprimir3.setToolTipText("[Alt + v]");
        btnimprimir3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimir3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnimprimir2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnimprimir3)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnimprimir2)
                    .addComponent(btnimprimir3))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        if (!txtbuscar.getText().equals("")) {
            cargarbioquimico(txtbuscar.getText());
        } else {
            txtcuil.setText("");
            txtapellido.setText("");
            txtnombre.setText("");
            txtmatricula.setText("");
        }
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed

    }//GEN-LAST:event_txtbuscarKeyPressed

    private void txtapellidoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtapellidoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtapellidoKeyReleased

    private void txtapellidoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtapellidoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtapellidoKeyPressed

    private void txtnombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyReleased

    private void txtnombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyPressed

    private void txtmatriculaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmatriculaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmatriculaKeyReleased

    private void txtmatriculaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmatriculaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmatriculaKeyPressed

    private void btnimprimir3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimir3ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnimprimir3ActionPerformed

    private void btnimprimir2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimir2ActionPerformed
        if (!txtmatricula.getText().equals("")) {
            if (facturacion == 0) {
                Enviar_Facturacion.bioquimico = txtapellido.getText() + " " + txtnombre.getText();
                Enviar_Facturacion.matricula = txtmatricula.getText();
                Enviar_Facturacion.usuario = usua;
                Enviar_Facturacion.contraseña = contra;
                new Periodo(null, true).setVisible(true);
                if (!Enviar_Facturacion.periodobioq.equals("")) {
                    new Enviar_Facturacion(null, true).setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(null, "No selecciono un periodo...");
                }
                this.dispose();
            }
            if (facturacion == 1) {
                Enviar_Facturacion.bioquimico = txtapellido.getText() + " " + txtnombre.getText();
                Enviar_Facturacion.matricula = txtmatricula.getText();
                Enviar_Facturacion.usuario = usua;
                Enviar_Facturacion.contraseña = contra;
                new Facturacion(null, true).setVisible(true);
                this.dispose();
            }
            if (facturacion == 2) {
                Login.matricula_colegiado = txtmatricula.getText();
                Login.nombre_colegiado = txtapellido.getText() + " " + txtnombre.getText();
                MainB.direccion = dire;
                MainB.lugar = lug;
                this.dispose();
            }
            if (facturacion == 3) {//validacion online
                Enviar_Facturacion.matricula = txtmatricula.getText();
                Enviar_Facturacion.usuario = usua;
                Enviar_Facturacion.contraseña = contra;
                MainB.cuil = txtcuil.getText();
                this.dispose();
            }

        }
    }//GEN-LAST:event_btnimprimir2ActionPerformed

    private void txtcuilKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcuilKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcuilKeyReleased

    private void txtcuilKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcuilKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcuilKeyPressed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarActionPerformed

    void cargarbioquimico(String valor) {
        double total = 0;
        int i = 0;
       /* String sql = "SELECT nombre, apellido, cuit, matricula, usuario, contraseña,direccion,lugar,id_colegiado FROM laboratorios WHERE CONCAT(nombre, ' ', apellido, ' ', matricula)"
                + "LIKE '%" + valor + "%'";*/
        String sql = "SELECT nombre, apellido, cuit, matricula, usuario, contraseña,direccion,lugar,id_colegiado FROM laboratorios WHERE CONCAT(nombre, ' ', apellido, ' ', matricula)"
                + "LIKE '%" + valor + "%'";
       
        //SELECT ordenes.id_ordenes, personas.apellido, personas.nombre, pacientes.fecha_nacimiento, pacientes.telefono, pacientes.celular, ordenes.fecha, ordenes.observaciones, ordenes.tipo_orden, ordenes.servicio, historia_clinica.descripcion FROM ordenes INNER JOIN pacientes ON pacientes.id_Pacientes = ordenes.id_Pacientes INNER JOIN personas ON personas.dni = pacientes.personas_dni INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes WHERE CONCAT(personas_dni, ' ', personas.apellido, ' ',personas.nombre) LIKE 'ROBLES'
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                total = 0;
                txtcuil.setText(rs.getString(3));
                txtmatricula.setText(rs.getString(4));
                txtapellido.setText(rs.getString(2));
                txtnombre.setText(rs.getString(1));
                usua = rs.getString(5);
                contra = rs.getString(6);
                dire = rs.getString(7);
                lug = rs.getString(8);
                id_colegiado = rs.getInt(9);
                i++;
            }
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");

        }

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnimprimir2;
    private javax.swing.JButton btnimprimir3;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txtapellido;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txtcuil;
    private javax.swing.JTextField txtmatricula;
    private javax.swing.JTextField txtnombre;
    // End of variables declaration//GEN-END:variables
}
