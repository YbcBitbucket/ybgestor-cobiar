package Formularios;

import Clases.ConexionMySQLLocal;
import Clases.Escape;
import static Formularios.Login.id_usuario;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

public class AgregarObraSocial extends javax.swing.JDialog {

    public static String raz, id = "", año, codfac, nombre, direccion, provincia, localidad, cp, http, mail1, mail2, mail3, nomref, sfechaalta, sfechabaja, facps, fnn, fp, fpp, idi, d998, spp, tc, tf, cos, cuit, telefono, fax, celref, uniar, ndecontrato, porcacargoaf, pordedesc, tipofac, tipoiva;
    public static int ide, bandera_agrega = 0;
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
    Date fechinicial;
    Date fechfinal;
    public int idnbu[] = new int[100];
    HiloOrdenes hilo;

    public AgregarObraSocial(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Agregar Obra Social");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        this.setLocationRelativeTo(null);
        jTabbedPane1.setEnabledAt(1, false);
        sifac.setSelected(true);
        sinome.setSelected(true);
        cupon.setSelected(true);
        sipaciente.setSelected(true);
        siempre.setSelected(true);
        incluye.setSelected(true);
        sisub.setSelected(true);
        sitienecat.setSelected(true);
        directa.setSelected(true);
        cbonbu.removeAllItems();
        cargacombo();
        Escape.funcionescape(this);
        for (int i = 0; i < MainB.contadorprovincia; i++) {
            cboprovincia.addItem(MainB.nombreprovincia[i]);
        }
        AutoCompleteDecorator.decorate(cboprovincia);
        txtdireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txthttp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toLowerCase(c));
                }
            }
        });
        txtncontrato.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtporcacargoafiliado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtrazonsoc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

        txtreferente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
    }

    public class HiloOrdenes extends Thread {

        JProgressBar progreso;

        public HiloOrdenes(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            año = cbonbu.getSelectedItem().toString();

            /////////////////////////////////////////////////////
            int bandera = 0, registro = 0, multip;
            btnagregar.setEnabled(false);
            btnatras.setEnabled(false);
            btncancelar1.setEnabled(false);
            if (!año.equals("")) {
                if (!txtunidadarancel.getText().equals("")) {
                    try {
                        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                        Connection cn = mysql.Conectar();
                        /////////////////////////////////////////////////////
                        String sqlid = "SELECT id_obrasocial FROM obrasocial";
                        Statement stid = cn.createStatement();
                        ResultSet rsid = stid.executeQuery(sqlid);

                        String sqlnbu = "SELECT id_nbu FROM nbu where añonbu=" + año;
                        Statement stnbu = cn.createStatement();
                        ResultSet rsnbu = stnbu.executeQuery(sqlnbu);
                        rsnbu.next();
                        año = rsnbu.getString("id_nbu");

                        while (rsid.next()) {
                            rsid.last();
                            ide = rsid.getInt("id_obrasocial") + 1;
                        }
                        cos = txtcodobrasoc.getText();
                        raz = txtrazonsoc.getText();
                        nombre = txtnombre.getText();
                        direccion = txtdireccion.getText();
                        cuit = txtcuit.getText();
                        provincia = cboprovincia.getSelectedItem().toString();
                        localidad = cbolocalidad.getSelectedItem().toString();
                        cp = txtcp.getText();
                        telefono = txttelefono.getText();
                        fax = txtfax.getText();
                        http = txthttp.getText();
                        mail1 = txtmail1.getText();
                        mail2 = txtmail2.getText();
                        mail3 = txtmail3.getText();
                        codfac = txtcodfac.getText();
                        if (fechalta.getDate() != null) {
                            Date fechinicial = fechalta.getDate();
                            sfechaalta = formato.format(fechinicial.getTime());
                        } else {
                            sfechaalta = null;
                        }
                        if (fechabaja.getDate() != null) {
                            fechfinal = fechabaja.getDate();
                            sfechabaja = formato.format(fechfinal.getTime());
                        } else {
                            sfechabaja = null;
                        }
                        nomref = txtreferente.getText();
                        celref = txtcelreferente.getText();
                        uniar = txtunidadarancel.getText();
                        ndecontrato = txtncontrato.getText();
                        porcacargoaf = txtporcacargoafiliado.getText();
                        pordedesc = txtporcdedesc.getText();
                        tipofac = cbotipofac.getSelectedItem().toString().toUpperCase();
                        tipoiva = cbotipoiva.getSelectedItem().toString().toUpperCase();
                        if (sifac.isSelected()) {
                            facps = "SI";
                        } else {
                            facps = "NO";
                        }
                        if (sinome.isSelected()) {
                            fnn = "SI";
                        } else {
                            fnn = "NO";
                        }
                        if (cupon.isSelected()) {
                            fp = "CUPON";
                        } else {
                            if (ingreso.isSelected()) {
                                fp = "INGRESO";
                            } else {
                                fp = "ORDEN";
                            }
                        }
                        if (sipaciente.isSelected()) {
                            fpp = "SI";
                        } else {
                            fpp = "NO";
                        }
                        if (siempre.isSelected()) {
                            idi = "SIEMPRE";
                        } else {
                            if (altacomp.isSelected()) {
                                idi = "ALTA COMPLEJIDAD";
                            } else {
                                idi = "NUNCA";
                            }
                        }
                        if (incluye.isSelected()) {
                            d998 = "INCLUYE";
                        } else {
                            if (noincluye.isSelected()) {
                                d998 = "NO INCLUYE";
                            } else {
                                d998 = "DISCRIMINA";
                            }
                        }
                        if (sisub.isSelected()) {
                            spp = "SI";
                        } else {
                            spp = "NO";
                        }
                        if (sitienecat.isSelected()) {
                            tc = "SI";
                        } else {
                            tc = "NO";
                        }
                        if (directa.isSelected()) {
                            tf = "DIRECTA";
                        } else {
                            tf = "COLEGIO";
                        }
                        String sqlos = "SELECT id_obrasocial FROM obrasocial WHERE codigo_obrasocial='" + cos + "' OR razonsocial_obrasocial='" + raz + "'";
                        Statement stos = cn.createStatement();
                        ResultSet rsos = stos.executeQuery(sqlos);
                        while (rsos.next()) {
                            id = rsos.getString("id_obrasocial");
                        }
                        rsos.beforeFirst(); // fin
                        if (id.equals("") && ide != 0) {
                            String sSQL = "INSERT INTO obrasocial(celularreferente_obrasocial, codigofacturacion_obrasocial, codigofacliq_obrasocial, codigo_obrasocial, codigopostal_obrasocial, cuit_obrasocial, direccion_obrasocial, facturaaltacomplejidad, facturanonomenclados, facturapor, facturaporpaciente, fax_obrasocial, fechadealta_obrasocial, fechadebaja_obrasocial, http_obrasocial, importeunidaddearancel_obrasocial, importeunidaddegasto_obrasocial, imprimedobleinforme, d998, localidad_obrasocial, mail1_obrasocial, mail2_obrasocial, mail3_obrasocial, nombrereferente_obrasocial, nombre_obrasocial, nresolucioningreso_obrasocial, ntablaaranceles_obrasocial, porcentajeafiliado_obrasocial, porcentajedescuento_obrasocial, provincia_obrasocial, razonsocial_obrasocial, subtotalporpaciente, telefono_obrasocial, tienecategorizacion, tipodefacturacion, tipodefacturaciondirectaocolegio, tipoiva, añonbu,Int_codigo_obrasocial,estado_obrasocial) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                            PreparedStatement pst = cn.prepareStatement(sSQL);
                            pst.setString(1, celref);
                            pst.setString(2, codfac);
                            pst.setString(3, "");
                            pst.setString(4, cos);
                            pst.setString(5, cp);
                            pst.setString(6, cuit);
                            pst.setString(7, direccion);
                            pst.setString(8, facps);
                            pst.setString(9, fnn);
                            pst.setString(10, fp);
                            pst.setString(11, fpp);
                            pst.setString(12, fax);
                            pst.setString(13, sfechaalta);
                            pst.setString(14, sfechabaja);
                            pst.setString(15, http);
                            pst.setString(16, uniar);
                            pst.setString(17, "");
                            pst.setString(18, idi);
                            pst.setString(19, d998);
                            pst.setString(20, localidad);
                            pst.setString(21, mail1);
                            pst.setString(22, mail2);
                            pst.setString(23, mail3);
                            pst.setString(24, nomref);
                            pst.setString(25, nombre);
                            pst.setString(26, ndecontrato);
                            pst.setString(27, "");
                            pst.setString(28, porcacargoaf);
                            pst.setString(29, pordedesc);
                            pst.setString(30, provincia);
                            pst.setString(31, raz);
                            pst.setString(32, spp);
                            pst.setString(33, telefono);
                            pst.setString(34, tc);
                            pst.setString(35, tipofac);
                            pst.setString(36, tf);
                            pst.setString(37, tipoiva);
                            pst.setString(38, año);
                            pst.setString(39, cos);
                            pst.setInt(40, 1);
                            int n = pst.executeUpdate();
                            if (n > 0) {
                                int nbu = Integer.valueOf(cbonbu.getSelectedItem().toString());

                                String sqlp = "select DISTINCT(practicas_nbu.id_practicasnbu), practicas_nbu.id_practicas, practicas_nbu.id_nbu, practicas_nbu.unidadbioquimica_practica, practicas.codigo_practica FROM practicas_nbu INNER JOIN practicas ON practicas.id_practicas = practicas_nbu.id_practicas INNER JOIN NBU USING(id_nbu) WHERE añonbu=" + nbu;
                                Statement stp = cn.createStatement();
                                ResultSet rsp = stp.executeQuery(sqlp);
                                JOptionPane.showMessageDialog(null, "Esto tardará unos segundos...espere por favor");
                                while (rsp.next()) {
                                    progreso.setValue(0);
                                    String sSQL1 = "INSERT INTO obrasocial_tiene_practicas_nbu(id_obrasocial, id_practicasnbu, codigo_fac_practicas_obrasocial, unidadbioquimica, importeunidaddearancel_obrasocial, preciofijo, preciototal, id_nbu, id_usuarios) VALUES(?,?,?,?,?,?,?,?,?)";
                                    PreparedStatement pst1 = cn.prepareStatement(sSQL1);
                                    pst1.setInt(1, ide);
                                    pst1.setString(2, rsp.getString(1));
                                    pst1.setString(3, rsp.getString(5));
                                    pst1.setString(4, rsp.getString(4));///unidad bioq
                                    pst1.setString(5, uniar);///importeobs
                                    pst1.setString(6, "0");///precio fijo
                                    double total = Redondear(Double.valueOf(uniar) * rsp.getDouble(4));
                                    pst1.setDouble(7, total);
                                    pst1.setInt(8, rsp.getInt(3));
                                    pst1.setInt(9, id_usuario);
                                    n = pst1.executeUpdate();
                                    if (n > 0) {
                                        progreso.setValue(100);
                                        pausa(10);
                                    }
                                }
                                rsp.close();
                                rsnbu.close();
                                bandera_agrega = 1;
                                JOptionPane.showMessageDialog(null, "Se cargó exitosamente la Obra Social, ahora se listaran las practicas...");
                                new Tabla_PracticasNBU(null, true).setVisible(true);
                                btnagregar.setEnabled(true);
                                btnatras.setEnabled(true);
                                btncancelar1.setEnabled(true);
                            }
                            /// cn.close();
                        } else {
                            JOptionPane.showMessageDialog(null, "La Obra Social ya se encuentra en la base de datos");
                            btnagregar.setEnabled(true);
                            btnatras.setEnabled(true);
                            btncancelar1.setEnabled(true);
                        }
                        ///////////////////////////////////////String.valueOf(multip)//////////////////////////////////////////
                    } catch (SQLException ex) {
                        Logger.getLogger(AgregarObraSocial.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Debe ingresar la Unidad de Importe de Arancel");
                    btnagregar.setEnabled(true);
                    btnatras.setEnabled(true);
                    btncancelar1.setEnabled(true);
                }
            } else {
                JOptionPane.showMessageDialog(null, "No seleccionó el año");
                btnagregar.setEnabled(true);
                btnatras.setEnabled(true);
                btncancelar1.setEnabled(true);
            }
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        ObraSocial = new javax.swing.JPanel();
        btnsiguiente = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtcodobrasoc = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtrazonsoc = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtcuit = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtcp = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txttelefono = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtfax = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txthttp = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtmail1 = new javax.swing.JTextField();
        txtmail2 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        txtmail3 = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        fechalta = new com.toedter.calendar.JDateChooser();
        fechabaja = new com.toedter.calendar.JDateChooser();
        jLabel30 = new javax.swing.JLabel();
        txtreferente = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        txtcelreferente = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtcodfac = new javax.swing.JTextField();
        cboprovincia = new javax.swing.JComboBox();
        cbolocalidad = new javax.swing.JComboBox();
        ObraSocial1 = new javax.swing.JPanel();
        btnagregar = new javax.swing.JButton();
        btncancelar1 = new javax.swing.JButton();
        btnatras = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        txtunidadarancel = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtncontrato = new javax.swing.JTextField();
        txtporcacargoafiliado = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtporcdedesc = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        sifac = new javax.swing.JCheckBox();
        jLabel25 = new javax.swing.JLabel();
        nofac = new javax.swing.JCheckBox();
        jLabel26 = new javax.swing.JLabel();
        sinome = new javax.swing.JCheckBox();
        nonome = new javax.swing.JCheckBox();
        jLabel32 = new javax.swing.JLabel();
        cupon = new javax.swing.JCheckBox();
        ingreso = new javax.swing.JCheckBox();
        jLabel33 = new javax.swing.JLabel();
        sipaciente = new javax.swing.JCheckBox();
        nopaciente = new javax.swing.JCheckBox();
        jLabel34 = new javax.swing.JLabel();
        siempre = new javax.swing.JCheckBox();
        altacomp = new javax.swing.JCheckBox();
        nunca = new javax.swing.JCheckBox();
        jLabel35 = new javax.swing.JLabel();
        incluye = new javax.swing.JCheckBox();
        noincluye = new javax.swing.JCheckBox();
        discrimina = new javax.swing.JCheckBox();
        jLabel36 = new javax.swing.JLabel();
        sisub = new javax.swing.JCheckBox();
        nosub = new javax.swing.JCheckBox();
        jLabel37 = new javax.swing.JLabel();
        sitienecat = new javax.swing.JCheckBox();
        notienecat = new javax.swing.JCheckBox();
        jLabel38 = new javax.swing.JLabel();
        directa = new javax.swing.JCheckBox();
        colegio = new javax.swing.JCheckBox();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        cbotipofac = new javax.swing.JComboBox();
        cbotipoiva = new javax.swing.JComboBox();
        jLabel15 = new javax.swing.JLabel();
        cbonbu = new javax.swing.JComboBox();
        progreso = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(650, 400));

        ObraSocial.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        ObraSocial.setPreferredSize(new java.awt.Dimension(580, 350));
        ObraSocial.setVerifyInputWhenFocusTarget(false);

        btnsiguiente.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728938 - flag green.png"))); // NOI18N
        btnsiguiente.setMnemonic('a');
        btnsiguiente.setText("Siguiente");
        btnsiguiente.setPreferredSize(new java.awt.Dimension(108, 23));
        btnsiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsiguienteActionPerformed(evt);
            }
        });

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btncancelar.setMnemonic('c');
        btncancelar.setText("Volver");
        btncancelar.setPreferredSize(new java.awt.Dimension(108, 23));
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Codigo Obra Social:");

        txtcodobrasoc.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcodobrasoc.setForeground(new java.awt.Color(0, 102, 204));
        txtcodobrasoc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcodobrasocKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Razon Social:");

        txtrazonsoc.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtrazonsoc.setForeground(new java.awt.Color(0, 102, 204));
        txtrazonsoc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtrazonsocKeyReleased(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Nombre:");

        txtnombre.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnombreKeyReleased(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Direccion:");

        txtdireccion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtdireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdireccionKeyReleased(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Cuit:");

        txtcuit.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcuit.setForeground(new java.awt.Color(0, 102, 204));
        txtcuit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcuitKeyReleased(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Localidad: ");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Provincia:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("C.P.:");

        txtcp.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcp.setForeground(new java.awt.Color(0, 102, 204));
        txtcp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcpKeyReleased(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Telefono:");

        txttelefono.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txttelefono.setForeground(new java.awt.Color(0, 102, 204));
        txttelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttelefonoKeyReleased(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("Fax:");

        txtfax.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtfax.setForeground(new java.awt.Color(0, 102, 204));
        txtfax.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtfaxKeyReleased(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("Http:");

        txthttp.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txthttp.setForeground(new java.awt.Color(0, 102, 204));
        txthttp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txthttpKeyReleased(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText("Mail1:");

        txtmail1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmail1.setForeground(new java.awt.Color(0, 102, 204));
        txtmail1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmail1KeyReleased(evt);
            }
        });

        txtmail2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmail2.setForeground(new java.awt.Color(0, 102, 204));
        txtmail2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmail2KeyReleased(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setText("Mail2:");

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel27.setText("Mail3:");

        txtmail3.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmail3.setForeground(new java.awt.Color(0, 102, 204));
        txtmail3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmail3KeyReleased(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel28.setText("Fecha de Alta:");

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel29.setText("Fecha de Baja:");

        fechalta.setForeground(new java.awt.Color(0, 102, 204));
        fechalta.setDateFormatString("dd-MM-yyyy");
        fechalta.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        fechabaja.setForeground(new java.awt.Color(0, 102, 204));
        fechabaja.setDateFormatString("dd-MM-yyyy");
        fechabaja.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                    .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fechabaja, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fechalta, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(fechalta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(fechabaja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel28)
                        .addGap(13, 13, 13)
                        .addComponent(jLabel29)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel30.setText("Nombre Refente:");

        txtreferente.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtreferente.setForeground(new java.awt.Color(0, 102, 204));
        txtreferente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtreferenteKeyReleased(evt);
            }
        });

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel31.setText("Celular Referente:");

        txtcelreferente.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcelreferente.setForeground(new java.awt.Color(0, 102, 204));
        txtcelreferente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcelreferenteKeyReleased(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setText("Codigo de Facturacion:");

        txtcodfac.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtcodfac.setForeground(new java.awt.Color(0, 102, 204));

        cboprovincia.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cboprovincia.setForeground(new java.awt.Color(0, 102, 204));
        cboprovincia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboprovinciaActionPerformed(evt);
            }
        });

        cbolocalidad.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbolocalidad.setForeground(new java.awt.Color(0, 102, 204));

        javax.swing.GroupLayout ObraSocialLayout = new javax.swing.GroupLayout(ObraSocial);
        ObraSocial.setLayout(ObraSocialLayout);
        ObraSocialLayout.setHorizontalGroup(
            ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ObraSocialLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ObraSocialLayout.createSequentialGroup()
                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(ObraSocialLayout.createSequentialGroup()
                                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cboprovincia, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18))
                            .addGroup(ObraSocialLayout.createSequentialGroup()
                                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnsiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(ObraSocialLayout.createSequentialGroup()
                                            .addComponent(jLabel9)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(txtnombre, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                                                .addComponent(txttelefono)))
                                        .addGroup(ObraSocialLayout.createSequentialGroup()
                                            .addComponent(jLabel12)
                                            .addGap(32, 32, 32)
                                            .addComponent(txtmail1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(20, 20, 20)))
                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ObraSocialLayout.createSequentialGroup()
                                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ObraSocialLayout.createSequentialGroup()
                                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel6)
                                            .addComponent(jLabel4)
                                            .addComponent(jLabel10))
                                        .addGap(12, 12, 12))
                                    .addGroup(ObraSocialLayout.createSequentialGroup()
                                        .addComponent(jLabel13)
                                        .addGap(37, 37, 37)))
                                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtfax, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtmail2, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbolocalidad, javax.swing.GroupLayout.Alignment.LEADING, 0, 168, Short.MAX_VALUE)
                                    .addComponent(txtdireccion))
                                .addGap(18, 18, 18)
                                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(ObraSocialLayout.createSequentialGroup()
                                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel27)
                                            .addComponent(jLabel11))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txthttp)
                                            .addComponent(txtmail3)))
                                    .addGroup(ObraSocialLayout.createSequentialGroup()
                                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addGroup(ObraSocialLayout.createSequentialGroup()
                                                .addGap(2, 2, 2)
                                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(7, 7, 7)
                                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtcuit)
                                            .addComponent(txtcp)))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ObraSocialLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(ObraSocialLayout.createSequentialGroup()
                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ObraSocialLayout.createSequentialGroup()
                                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, ObraSocialLayout.createSequentialGroup()
                                        .addComponent(jLabel14)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtcodfac))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, ObraSocialLayout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtcodobrasoc, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(43, 43, 43)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtrazonsoc, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(ObraSocialLayout.createSequentialGroup()
                                .addGap(195, 195, 195)
                                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(ObraSocialLayout.createSequentialGroup()
                                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel31))
                                        .addGap(12, 12, 12)
                                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtcelreferente, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtreferente, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                        .addGap(0, 107, Short.MAX_VALUE)))
                .addContainerGap())
        );
        ObraSocialLayout.setVerticalGroup(
            ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ObraSocialLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtcodobrasoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtrazonsoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtcodfac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ObraSocialLayout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addComponent(jLabel8))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ObraSocialLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ObraSocialLayout.createSequentialGroup()
                                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7)
                                    .addComponent(cboprovincia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cbolocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel10)
                                    .addComponent(txtfax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txttelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel11))
                                .addGap(18, 18, 18)
                                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel13)
                                    .addComponent(txtmail2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtmail1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel27)))
                            .addGroup(ObraSocialLayout.createSequentialGroup()
                                .addComponent(txtcuit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtcp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txthttp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtmail3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)))
                .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ObraSocialLayout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel30)
                            .addComponent(txtreferente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel31)
                            .addComponent(txtcelreferente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 101, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ObraSocialLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(ObraSocialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnsiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())))
        );

        jTabbedPane1.addTab("Obra Social", ObraSocial);

        ObraSocial1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        ObraSocial1.setPreferredSize(new java.awt.Dimension(580, 350));
        ObraSocial1.setVerifyInputWhenFocusTarget(false);

        btnagregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        btnagregar.setMnemonic('a');
        btnagregar.setText("Agregar");
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        btncancelar1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btncancelar1.setMnemonic('c');
        btncancelar1.setText("Volver");
        btncancelar1.setPreferredSize(new java.awt.Dimension(108, 23));
        btncancelar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelar1ActionPerformed(evt);
            }
        });

        btnatras.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnatras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728939 - flag yellow.png"))); // NOI18N
        btnatras.setMnemonic('a');
        btnatras.setText("Atras");
        btnatras.setPreferredSize(new java.awt.Dimension(108, 23));
        btnatras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnatrasActionPerformed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setText("Unidad de Importe de Arancel:");

        txtunidadarancel.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtunidadarancel.setForeground(new java.awt.Color(0, 102, 204));
        txtunidadarancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtunidadarancelKeyReleased(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel19.setText("%  a cargo del Afiliado:");

        txtncontrato.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtncontrato.setForeground(new java.awt.Color(0, 102, 204));
        txtncontrato.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtncontratoKeyReleased(evt);
            }
        });

        txtporcacargoafiliado.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtporcacargoafiliado.setForeground(new java.awt.Color(0, 102, 204));
        txtporcacargoafiliado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtporcacargoafiliadoKeyReleased(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel20.setText("N° Res/Contrato");

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setText("% de descuento:");

        txtporcdedesc.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtporcdedesc.setForeground(new java.awt.Color(0, 102, 204));
        txtporcdedesc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtporcdedescKeyReleased(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        sifac.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        sifac.setText("Si");
        sifac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sifacActionPerformed(evt);
            }
        });

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel25.setText("¿ Factura Alta Complejidad Por Separado ?");

        nofac.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nofac.setSelected(true);
        nofac.setText("No");
        nofac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nofacActionPerformed(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel26.setText("¿ Factura no Nomenclados ?");

        sinome.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        sinome.setSelected(true);
        sinome.setText("Si");
        sinome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sinomeActionPerformed(evt);
            }
        });

        nonome.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nonome.setText("No");
        nonome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nonomeActionPerformed(evt);
            }
        });

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel32.setText("¿ Factura por ?");

        cupon.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cupon.setSelected(true);
        cupon.setText("Cupon");
        cupon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cuponActionPerformed(evt);
            }
        });

        ingreso.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ingreso.setText("Paciente Completo");
        ingreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ingresoActionPerformed(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel33.setText("¿ Factura Por Paciente  ?");

        sipaciente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        sipaciente.setSelected(true);
        sipaciente.setText("Si");
        sipaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sipacienteActionPerformed(evt);
            }
        });

        nopaciente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nopaciente.setText("No");
        nopaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nopacienteActionPerformed(evt);
            }
        });

        jLabel34.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel34.setText("¿ Imprime Doble informe ?");

        siempre.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        siempre.setText("Siempre");
        siempre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                siempreActionPerformed(evt);
            }
        });

        altacomp.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        altacomp.setText("Selectivo");
        altacomp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                altacompActionPerformed(evt);
            }
        });

        nunca.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nunca.setSelected(true);
        nunca.setText("Nunca");
        nunca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuncaActionPerformed(evt);
            }
        });

        jLabel35.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel35.setText("¿ d998 ?");

        incluye.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        incluye.setSelected(true);
        incluye.setText("Incluye");
        incluye.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                incluyeActionPerformed(evt);
            }
        });

        noincluye.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        noincluye.setText("No Incluye");
        noincluye.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                noincluyeActionPerformed(evt);
            }
        });

        discrimina.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        discrimina.setText("Discrimina");
        discrimina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                discriminaActionPerformed(evt);
            }
        });

        jLabel36.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel36.setText("¿ Subtotal Por Paciente  ?");

        sisub.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        sisub.setText("Si");
        sisub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sisubActionPerformed(evt);
            }
        });

        nosub.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nosub.setSelected(true);
        nosub.setText("No");
        nosub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nosubActionPerformed(evt);
            }
        });

        jLabel37.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel37.setText("¿ Tiene Categorizacion  ?");

        sitienecat.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        sitienecat.setText("Si");
        sitienecat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sitienecatActionPerformed(evt);
            }
        });

        notienecat.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        notienecat.setSelected(true);
        notienecat.setText("No");
        notienecat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                notienecatActionPerformed(evt);
            }
        });

        jLabel38.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel38.setText("¿ Tipo de Facturacion  ?");

        directa.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        directa.setSelected(true);
        directa.setText("Directa");
        directa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                directaActionPerformed(evt);
            }
        });

        colegio.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        colegio.setText("Colegio");
        colegio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colegioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel32)
                    .addComponent(jLabel34)
                    .addComponent(jLabel35)
                    .addComponent(jLabel25)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel38, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel37, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel36, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel33, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel26, javax.swing.GroupLayout.Alignment.LEADING)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(incluye)
                    .addComponent(sipaciente)
                    .addComponent(cupon)
                    .addComponent(sinome)
                    .addComponent(sifac)
                    .addComponent(sisub)
                    .addComponent(sitienecat)
                    .addComponent(directa)
                    .addComponent(nunca))
                .addGap(24, 24, 24)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(colegio)
                    .addComponent(notienecat)
                    .addComponent(nosub)
                    .addComponent(nofac)
                    .addComponent(nonome)
                    .addComponent(nopaciente)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(noincluye)
                            .addComponent(ingreso)
                            .addComponent(siempre))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(altacomp)
                            .addComponent(discrimina))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(jPanel2Layout.createSequentialGroup()
                                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                .addGroup(jPanel2Layout.createSequentialGroup()
                                                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                .addGroup(jPanel2Layout.createSequentialGroup()
                                                                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                        .addComponent(sifac)
                                                                                        .addComponent(nofac))
                                                                                    .addGap(32, 32, 32))
                                                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                    .addComponent(sinome)
                                                                                    .addComponent(nonome)))
                                                                            .addGap(32, 32, 32))
                                                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                            .addComponent(cupon)
                                                                            .addComponent(ingreso)))
                                                                    .addGap(32, 32, 32))
                                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                    .addComponent(sipaciente)
                                                                    .addComponent(nopaciente)))
                                                            .addGap(32, 32, 32))
                                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                            .addComponent(nunca)
                                                            .addComponent(siempre)
                                                            .addComponent(altacomp)))
                                                    .addGap(32, 32, 32))
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                    .addComponent(incluye)
                                                    .addComponent(noincluye)
                                                    .addComponent(discrimina)))
                                            .addGap(32, 32, 32))
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(sisub)
                                            .addComponent(nosub)))
                                    .addGap(32, 32, 32))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(sitienecat)
                                    .addComponent(notienecat)))
                            .addGap(32, 32, 32))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(directa)
                            .addComponent(colegio)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel25)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel26)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel32)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel33)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel34)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel35)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel36)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel37)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel38)))
                .addGap(0, 8, Short.MAX_VALUE))
        );

        jLabel39.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel39.setText("Tipo de Facturacion:");

        jLabel40.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel40.setText("Tipo IVA:");

        cbotipofac.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbotipofac.setForeground(new java.awt.Color(0, 102, 204));
        cbotipofac.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Importes", "NBU", "Convenio", "CONV+DCTO", "S/Detalle" }));

        cbotipoiva.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbotipoiva.setForeground(new java.awt.Color(0, 102, 204));
        cbotipoiva.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Excento", "10.5 %", "21 %", "Responsable Inscripto" }));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Año de NBU :");

        cbonbu.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cbonbu.setForeground(new java.awt.Color(0, 102, 204));

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setString("");

        javax.swing.GroupLayout ObraSocial1Layout = new javax.swing.GroupLayout(ObraSocial1);
        ObraSocial1.setLayout(ObraSocial1Layout);
        ObraSocial1Layout.setHorizontalGroup(
            ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ObraSocial1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(ObraSocial1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnatras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnagregar)
                        .addGap(18, 18, 18)
                        .addComponent(btncancelar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(ObraSocial1Layout.createSequentialGroup()
                        .addGroup(ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ObraSocial1Layout.createSequentialGroup()
                                .addComponent(jLabel39)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbotipofac, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel40)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbotipoiva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(ObraSocial1Layout.createSequentialGroup()
                                .addGroup(ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(ObraSocial1Layout.createSequentialGroup()
                                        .addComponent(jLabel20)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtncontrato))
                                    .addGroup(ObraSocial1Layout.createSequentialGroup()
                                        .addComponent(jLabel17)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtunidadarancel, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(ObraSocial1Layout.createSequentialGroup()
                                        .addComponent(jLabel19)
                                        .addGap(4, 4, 4)
                                        .addComponent(txtporcacargoafiliado, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(ObraSocial1Layout.createSequentialGroup()
                                        .addComponent(jLabel15)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cbonbu, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGap(18, 18, 18)
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtporcdedesc, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        ObraSocial1Layout.setVerticalGroup(
            ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ObraSocial1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ObraSocial1Layout.createSequentialGroup()
                        .addGroup(ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel21)
                                .addComponent(txtporcdedesc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel17)
                                .addComponent(txtunidadarancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel15)
                                .addComponent(cbonbu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20)
                            .addComponent(txtncontrato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(ObraSocial1Layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addGroup(ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(txtporcacargoafiliado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(jLabel40)
                    .addComponent(cbotipofac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbotipoiva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(progreso, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ObraSocial1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btncancelar1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnatras, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Configuracion", ObraSocial1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 727, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 534, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncancelarActionPerformed

    void cargacombo() {
        try {
            cbonbu.removeAllItems();
            cbonbu.addItem("...");
            ConexionMySQLLocal mysql = new ConexionMySQLLocal();
            Connection cn = mysql.Conectar();

            /////////////////////////////////////////////////////
            String sql = "SELECT id_nbu, añonbu  FROM nbu";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                cbonbu.addItem(rs.getString("añonbu"));
                idnbu[i] = rs.getInt("id_nbu");
                i++;
            }
            ///  cn.close();
        } catch (SQLException ex) {

        }

    }


    private void btnsiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsiguienteActionPerformed
        if (txtcodobrasoc.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Debe ingresar el código de la obra social");
        } else {
            if (txtrazonsoc.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "La obra social debe tener una razón social");
            } else {
                jTabbedPane1.setEnabledAt(1, true);
                jTabbedPane1.setSelectedIndex(1);
            }

        }
    }//GEN-LAST:event_btnsiguienteActionPerformed

    boolean isNumeric(String cadena) {
        try {
            Double.parseDouble(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        iniciarSplash();
        hilo = new HiloOrdenes(progreso);
        hilo.start();
        hilo = null;
    }//GEN-LAST:event_btnagregarActionPerformed

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    private void btnatrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnatrasActionPerformed
        jTabbedPane1.setSelectedIndex(0);
    }//GEN-LAST:event_btnatrasActionPerformed

    private void siempreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_siempreActionPerformed
        if (siempre.isSelected()) {
            altacomp.setSelected(false);
            nunca.setSelected(false);
        } else {
            altacomp.setSelected(true);
            nunca.setSelected(false);
        }

    }//GEN-LAST:event_siempreActionPerformed

    private void nuncaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nuncaActionPerformed

        if (nunca.isSelected()) {
            siempre.setSelected(false);
            altacomp.setSelected(false);
        } else {
            siempre.setSelected(true);
            altacomp.setSelected(false);
        }


    }//GEN-LAST:event_nuncaActionPerformed

    private void incluyeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_incluyeActionPerformed

        if (incluye.isSelected()) {
            noincluye.setSelected(false);
            discrimina.setSelected(false);
        } else {
            noincluye.setSelected(true);
            discrimina.setSelected(false);
        }
    }//GEN-LAST:event_incluyeActionPerformed

    private void discriminaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_discriminaActionPerformed

        if (discrimina.isSelected()) {
            incluye.setSelected(false);
            noincluye.setSelected(false);
        } else {
            incluye.setSelected(true);
            noincluye.setSelected(false);
        }
    }//GEN-LAST:event_discriminaActionPerformed

    private void noincluyeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_noincluyeActionPerformed

        if (noincluye.isSelected()) {
            incluye.setSelected(false);
            discrimina.setSelected(false);
        } else {
            discrimina.setSelected(true);
            incluye.setSelected(false);
        }
    }//GEN-LAST:event_noincluyeActionPerformed

    private void sipacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sipacienteActionPerformed
        if (sipaciente.isSelected()) {
            nopaciente.setSelected(false);
        } else {
            nopaciente.setSelected(true);
        }

    }//GEN-LAST:event_sipacienteActionPerformed

    private void txtrazonsocKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtrazonsocKeyReleased
        String texto = txtrazonsoc.getText().toUpperCase();
        txtrazonsoc.setText(texto);
    }//GEN-LAST:event_txtrazonsocKeyReleased

    private void txtnombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyReleased
        String texto = txtnombre.getText().toUpperCase();
        txtnombre.setText(texto);
    }//GEN-LAST:event_txtnombreKeyReleased

    private void txtdireccionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdireccionKeyReleased
        String texto = txtdireccion.getText().toUpperCase();
        txtdireccion.setText(texto);
    }//GEN-LAST:event_txtdireccionKeyReleased

    private void txthttpKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txthttpKeyReleased
        String texto = txthttp.getText().toUpperCase();
        txthttp.setText(texto);
    }//GEN-LAST:event_txthttpKeyReleased

    private void txtmail1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmail1KeyReleased
        String texto = txtmail1.getText().toUpperCase();
        txtmail1.setText(texto);
    }//GEN-LAST:event_txtmail1KeyReleased

    private void txtmail2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmail2KeyReleased
        String texto = txtmail2.getText().toUpperCase();
        txtmail2.setText(texto);
    }//GEN-LAST:event_txtmail2KeyReleased

    private void txtmail3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmail3KeyReleased
        String texto = txtmail3.getText().toUpperCase();
        txtmail3.setText(texto);
    }//GEN-LAST:event_txtmail3KeyReleased

    private void txtreferenteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtreferenteKeyReleased
        String texto = txtreferente.getText().toUpperCase();
        txtreferente.setText(texto);
    }//GEN-LAST:event_txtreferenteKeyReleased

    private void txtcuitKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcuitKeyReleased
        if (!isNumeric(txtcuit.getText())) {
            txtcuit.setText("");
        }
    }//GEN-LAST:event_txtcuitKeyReleased

    private void txttelefonoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelefonoKeyReleased
        if (!isNumeric(txttelefono.getText())) {
            txttelefono.setText("");
    }//GEN-LAST:event_txttelefonoKeyReleased
    }
    private void txtfaxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtfaxKeyReleased
        if (!isNumeric(txtfax.getText())) {
            txtfax.setText("");
    }//GEN-LAST:event_txtfaxKeyReleased
    }
    private void txtcelreferenteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcelreferenteKeyReleased
        if (!isNumeric(txtcelreferente.getText())) {
            txtcelreferente.setText("");
    }//GEN-LAST:event_txtcelreferenteKeyReleased

    }
    private void txtcodobrasocKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcodobrasocKeyReleased

        txtcodfac.setText(txtcodobrasoc.getText());
    }//GEN-LAST:event_txtcodobrasocKeyReleased

    private void txtcpKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcpKeyReleased
        String texto = txtcp.getText().toUpperCase();
        txtcp.setText(texto);
    }//GEN-LAST:event_txtcpKeyReleased

    private void txtunidadarancelKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtunidadarancelKeyReleased
        if (!isNumeric(txtunidadarancel.getText())) {
            txtunidadarancel.setText("");
        }
    }//GEN-LAST:event_txtunidadarancelKeyReleased

    private void txtncontratoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtncontratoKeyReleased
        if (!isNumeric(txtncontrato.getText())) {
            txtncontrato.setText("");
        }
    }//GEN-LAST:event_txtncontratoKeyReleased

    private void txtporcacargoafiliadoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtporcacargoafiliadoKeyReleased
        if (!isNumeric(txtporcacargoafiliado.getText())) {
            txtporcacargoafiliado.setText("");
        }
    }//GEN-LAST:event_txtporcacargoafiliadoKeyReleased

    private void txtporcdedescKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtporcdedescKeyReleased
        if (!isNumeric(txtporcdedesc.getText())) {
            txtporcdedesc.setText("");
        }
    }//GEN-LAST:event_txtporcdedescKeyReleased

    private void sifacActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sifacActionPerformed
        if (sifac.isSelected()) {
            nofac.setSelected(false);
        } else {
            nofac.setSelected(true);
        }
    }//GEN-LAST:event_sifacActionPerformed

    private void nofacActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nofacActionPerformed
        if (nofac.isSelected()) {
            sifac.setSelected(false);
        } else {
            sifac.setSelected(true);
        }
    }//GEN-LAST:event_nofacActionPerformed

    private void cuponActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cuponActionPerformed
        if (cupon.isSelected()) {
            ingreso.setSelected(false);

        } else {
            ingreso.setSelected(true);

        }
    }//GEN-LAST:event_cuponActionPerformed

    private void ingresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ingresoActionPerformed
        if (ingreso.isSelected()) {
            cupon.setSelected(false);

        } else {
            ingreso.setSelected(false);

        }

    }//GEN-LAST:event_ingresoActionPerformed

    private void nopacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nopacienteActionPerformed
        if (nopaciente.isSelected()) {
            sipaciente.setSelected(false);
        } else {
            sipaciente.setSelected(true);
        }
    }//GEN-LAST:event_nopacienteActionPerformed

    private void altacompActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_altacompActionPerformed

        if (altacomp.isSelected()) {
            siempre.setSelected(false);
            nunca.setSelected(false);
        } else {
            nunca.setSelected(true);
            siempre.setSelected(false);
        }

    }//GEN-LAST:event_altacompActionPerformed

    private void sisubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sisubActionPerformed

        if (sisub.isSelected()) {
            nosub.setSelected(false);
        } else {
            nosub.setSelected(true);
        }
    }//GEN-LAST:event_sisubActionPerformed

    private void nosubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nosubActionPerformed

        if (nosub.isSelected()) {
            sisub.setSelected(false);
        } else {
            sisub.setSelected(true);
        }
    }//GEN-LAST:event_nosubActionPerformed

    private void notienecatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_notienecatActionPerformed
        if (notienecat.isSelected()) {
            sitienecat.setSelected(false);
        } else {
            sitienecat.setSelected(true);
        }
    }//GEN-LAST:event_notienecatActionPerformed

    private void sitienecatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sitienecatActionPerformed

        if (sitienecat.isSelected()) {
            notienecat.setSelected(false);
        } else {
            notienecat.setSelected(true);
        }
    }//GEN-LAST:event_sitienecatActionPerformed

    private void directaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_directaActionPerformed

        if (directa.isSelected()) {
            colegio.setSelected(false);
        } else {
            colegio.setSelected(true);
        }
    }//GEN-LAST:event_directaActionPerformed

    private void colegioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colegioActionPerformed

        if (colegio.isSelected()) {
            directa.setSelected(false);
        } else {
            directa.setSelected(true);
        }


    }//GEN-LAST:event_colegioActionPerformed

    private void sinomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sinomeActionPerformed
        if (sinome.isSelected()) {
            nonome.setSelected(false);
        } else {
            nonome.setSelected(true);
        }
    }//GEN-LAST:event_sinomeActionPerformed

    private void nonomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nonomeActionPerformed
        if (nonome.isSelected()) {
            sinome.setSelected(false);
        } else {
            sinome.setSelected(true);
        }
    }//GEN-LAST:event_nonomeActionPerformed

    private void btncancelar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncancelar1ActionPerformed

    private void cboprovinciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboprovinciaActionPerformed
        int index = 1 + cboprovincia.getSelectedIndex(), i;
        cbolocalidad.removeAllItems();
        for (i = 0; i < MainB.matloc.length; i++) {
            if (Integer.parseInt(MainB.matloc[i][1]) == index) {
                cbolocalidad.addItem(MainB.matloc[i][2]);
            }
        }
        AutoCompleteDecorator.decorate(cbolocalidad);
    }//GEN-LAST:event_cboprovinciaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel ObraSocial;
    private javax.swing.JPanel ObraSocial1;
    private javax.swing.JCheckBox altacomp;
    private javax.swing.JButton btnagregar;
    private javax.swing.JButton btnatras;
    private javax.swing.JButton btncancelar;
    private javax.swing.JButton btncancelar1;
    private javax.swing.JButton btnsiguiente;
    private javax.swing.JComboBox cbolocalidad;
    private javax.swing.JComboBox cbonbu;
    private javax.swing.JComboBox cboprovincia;
    private javax.swing.JComboBox cbotipofac;
    private javax.swing.JComboBox cbotipoiva;
    private javax.swing.JCheckBox colegio;
    private javax.swing.JCheckBox cupon;
    private javax.swing.JCheckBox directa;
    private javax.swing.JCheckBox discrimina;
    private com.toedter.calendar.JDateChooser fechabaja;
    private com.toedter.calendar.JDateChooser fechalta;
    private javax.swing.JCheckBox incluye;
    private javax.swing.JCheckBox ingreso;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JCheckBox nofac;
    private javax.swing.JCheckBox noincluye;
    private javax.swing.JCheckBox nonome;
    private javax.swing.JCheckBox nopaciente;
    private javax.swing.JCheckBox nosub;
    private javax.swing.JCheckBox notienecat;
    private javax.swing.JCheckBox nunca;
    private javax.swing.JProgressBar progreso;
    private javax.swing.JCheckBox siempre;
    private javax.swing.JCheckBox sifac;
    private javax.swing.JCheckBox sinome;
    private javax.swing.JCheckBox sipaciente;
    private javax.swing.JCheckBox sisub;
    private javax.swing.JCheckBox sitienecat;
    private javax.swing.JTextField txtcelreferente;
    private javax.swing.JTextField txtcodfac;
    private javax.swing.JTextField txtcodobrasoc;
    private javax.swing.JTextField txtcp;
    private javax.swing.JTextField txtcuit;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtfax;
    private javax.swing.JTextField txthttp;
    private javax.swing.JTextField txtmail1;
    private javax.swing.JTextField txtmail2;
    private javax.swing.JTextField txtmail3;
    private javax.swing.JTextField txtncontrato;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextField txtporcacargoafiliado;
    private javax.swing.JTextField txtporcdedesc;
    private javax.swing.JTextField txtrazonsoc;
    private javax.swing.JTextField txtreferente;
    private javax.swing.JTextField txttelefono;
    private javax.swing.JTextField txtunidadarancel;
    // End of variables declaration//GEN-END:variables
}
