package Formularios;

import Clases.ConexionMySQL;
import Clases.ConexionMySQLLocal;

import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import org.bouncycastle.asn1.x500.style.RFC4519Style;
import org.bouncycastle.crypto.tls.KeyExchangeAlgorithm;

public class Proceso extends javax.swing.JDialog {

    HiloOrdenes hilo;
    private int id_ddjj;
    public static String periodo, fecha = "";

    public Proceso(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        iniciarSplash();
        hilo = new HiloOrdenes(progreso);
        hilo.start();
        hilo = null;
    }

    void cargarperiodo() {

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);

        fecha = formato.format(currentDate);

    }

    private void progresoStateChanged(javax.swing.event.ChangeEvent evt) {
        if (progreso.getValue() == 100) {
            this.dispose();
        }
    }

    public double Redondearcentavos(double numero) {
        return Math.rint(numero * 100) % 100;
    }

    public double Redondearpesos(double numero) {
        return Math.rint(numero);
    }

    public class HiloOrdenes extends Thread {

        JProgressBar progreso;

        public HiloOrdenes(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;

        }

        public void run() {
            int i = 0, f = 0;
            ConexionMySQL CR = new ConexionMySQL();
            Connection cr = CR.Conectar();
            try {
                String sSQL1 = "SELECT *, round(importeunidaddearancel_obrasocial,2) FROM obrasocial where estado_obrasocial=1\n"
                        + " order by int_codigo_obrasocial";
                /*tring sSQL1 = "SELECT *, round(importeunidaddearancel_obrasocial,2) FROM obrasocial where int_codigo_obrasocial=13800 \n"
                        + " order by int_codigo_obrasocial";*/
                Statement st = cr.createStatement();
                ResultSet rs = st.executeQuery(sSQL1);

                while (rs.next()) {
                    ConexionMySQLLocal CL = new ConexionMySQLLocal();
                    Connection cl = CL.Conectar();
                   System.out.println("Obra Social: "+rs.getString("int_codigo_obrasocial"));
                    try {
                        /////////// VERIFICO SI LA OBRA SOCIAL SE ENCUENTRA EN LA BASE DE DATOS
                        String sSQL6 = "SELECT int_codigo_obrasocial,id_obrasocial,añonbu FROM obrasocial where int_codigo_obrasocial=" + rs.getString("int_codigo_obrasocial");
                        Statement cl6 = cl.createStatement();
                        ResultSet rs6 = cl6.executeQuery(sSQL6);

                        if (rs6.next()) {
                            /////////////////////////// ACTUALIZA OBRASOCIAL/////////////////////////////////////////////
                            /////Actualiza Taba OS
                            String sSQL3 = "update obrasocial set importeunidaddearancel_obrasocial=?, razonsocial_obrasocial=?, estado_obrasocial=?,"
                                    + "tipodefacturaciondirectaocolegio=?, nombre_obrasocial=?,añonbu=? "
                                    + "where Int_codigo_obrasocial=" + rs.getString("int_codigo_obrasocial");
                            PreparedStatement pst = cl.prepareStatement(sSQL3);
                            pst.setString(1, rs.getString("importeunidaddearancel_obrasocial"));
                            pst.setString(2, rs.getString("razonsocial_obrasocial"));
                            pst.setString(3, rs.getString("estado_obrasocial"));
                            pst.setString(4, rs.getString("tipodefacturaciondirectaocolegio"));
                            pst.setString(5, rs.getString("nombre_obrasocial"));
                            pst.setString(6, rs.getString("añonbu"));
                            pst.executeUpdate();

                            /////////////////Anulo todas las practicas - Tablas os_tiene_practicas_NBU///////////////////////////////
                            String sSQL7 = "update obrasocial_tiene_practicas_nbu set estado=? where id_obrasocial=" + rs6.getString("id_obrasocial");
                            PreparedStatement pst3 = cl.prepareStatement(sSQL7);
                            pst3.setString(1, "0");
                            pst3.executeUpdate();

                            if (rs.getInt("estado_obrasocial") == 1) {

                                /////////////////////////Traigo las practicas de la tabla os_tiene_practicas NBU del colegio
                                String sSQL2 = "Select codigo_practica, codigo_fac_practicas_obrasocial, round(unidaddebioquimica_practica,2) as unidaddebioquimica_practica ,round(importeunidaddearancel_obrasocial,2) as importeunidaddearancel_obrasocial,round(preciofijo,2) as preciofijo ,round(preciototal,2) as preciototal\n"
                                        + "from obrasocial_tiene_practicasnbu\n"
                                        + "where id_obrasocial=" + rs.getString("id_obrasocial") + "\n"
                                        + "order by codigo_practica";
                                Statement st2 = cr.createStatement();
                                ResultSet rs2 = st2.executeQuery(sSQL2);

                                while (rs2.next()) {

                                    int p = 0;

                                    ////////////////////// Actualizo las practicas del COBIAR
                                    String sSQL4 = "update obrasocial_tiene_practicas_nbu o1 INNER JOIN vista_obrasocial_practicas v1 on v1.id_practicasnbu=o1.id_practicasnbu\n"
                                            + "set codigo_fac_practicas_obrasocial=?,o1.unidadbioquimica=?, importeunidaddearancel_obrasocial=?,o1.preciofijo=?, o1.preciototal=?,o1.estado=? \n"
                                            + "where o1.id_obrasocial=" + rs6.getString("id_obrasocial") + " and v1.codigo_practica=" + rs2.getString("codigo_practica");

                                    PreparedStatement pst2 = cl.prepareStatement(sSQL4);
                                    pst2.setString(1, rs2.getString("codigo_fac_practicas_obrasocial"));
                                    pst2.setString(2, rs2.getString("unidaddebioquimica_practica"));
                                    pst2.setString(3, rs2.getString("importeunidaddearancel_obrasocial"));
                                    pst2.setString(4, rs2.getString("preciofijo"));
                                    pst2.setString(5, rs2.getString("preciototal"));
                                    pst2.setString(6, "1");
                                    p = pst2.executeUpdate();

                                    System.out.println("Actualiza Practica: " + rs2.getString("codigo_practica") + " OS:" + rs.getString("int_codigo_obrasocial") + "NBU:" + rs6.getString("añonbu"));
                                    ///////////////////////Si p=0 es una practica nueva por esto "insert" la practica
                                    if (p == 0) {
                                        System.out.println("Agrega Practica: " + rs2.getString("codigo_practica") + " OS:" + rs.getString("int_codigo_obrasocial") + "NBU:" + rs6.getString("añonbu"));
                                        ///////////////////////Selecciono los valores a insertar en la tabla desde la vista
                                        String sSQL9 = "Select id_practicasnbu from vista_nomencladores where id_nbu=" + rs6.getString("añonbu") + " and codigo_practica=" + rs2.getString("codigo_practica");
                                        Statement cl9 = cl.createStatement();
                                        ResultSet rs9 = cl9.executeQuery(sSQL9);
                                        if (rs9.next()) {
                                            String sSQL8 = "INSERT INTO obrasocial_tiene_practicas_nbu(id_obrasocial,id_practicasnbu,codigo_fac_practicas_obrasocial\n"
                                                    + ",unidadbioquimica, importeunidaddearancel_obrasocial, preciofijo, preciototal, id_nbu,id_usuarios,estado)\n"
                                                    + "VALUES(?,?,?,?,?,?,?,?,?,?)";

                                            PreparedStatement pst8 = cl.prepareStatement(sSQL8);
                                            pst8.setString(1, rs6.getString("id_obrasocial"));   ///////// id_obrasocial
                                            pst8.setString(2, rs9.getString("id_practicasnbu")); //////// id_practicasnbu
                                            pst8.setString(3, rs2.getString("codigo_fac_practicas_obrasocial"));////codigo_fac_practicas_obrasocial
                                            pst8.setString(4, rs2.getString("unidaddebioquimica_practica"));/// unidadbioquimica
                                            pst8.setString(5, rs2.getString("importeunidaddearancel_obrasocial")); /////
                                            pst8.setString(6, rs2.getString("preciofijo"));
                                            pst8.setString(7, rs2.getString("preciototal"));
                                            pst8.setString(8, rs6.getString("añonbu"));
                                            pst8.setString(9, "1"); //// Usuario 
                                            pst8.setString(10, "1");
                                            pst8.executeUpdate();

                                            System.out.println("Se agrego la Practica correctamente:" + rs2.getString("codigo_practica"));
                                        }

                                    }

                                }

                            }
                            //////////////////////////// FIN DE ACTUALIZAR E INSERTAR PRACTICAS SI EXISTE LA OS
                        } else {
                            System.out.println("No se encuentra la OS:" + rs.getString("int_codigo_obrasocial"));

                            String sSQL9 = "INSERT INTO obrasocial (importeunidaddearancel_obrasocial, razonsocial_obrasocial, estado_obrasocial,"
                                    + "tipodefacturaciondirectaocolegio, nombre_obrasocial,añonbu,Int_codigo_obrasocial,codigofacturacion_obrasocial,codigo_obrasocial)\n"
                                    + "VALUES(?,?,?,?,?,?,?,?,?)";

                            PreparedStatement pst9 = cl.prepareStatement(sSQL9);
                            pst9.setString(1, rs.getString("importeunidaddearancel_obrasocial"));
                            pst9.setString(2, rs.getString("razonsocial_obrasocial"));
                            pst9.setString(3, rs.getString("estado_obrasocial"));
                            pst9.setString(4, rs.getString("tipodefacturaciondirectaocolegio"));
                            pst9.setString(5, rs.getString("nombre_obrasocial"));
                            pst9.setString(6, rs.getString("añonbu"));
                            pst9.setString(7, rs.getString("Int_codigo_obrasocial"));
                            pst9.setString(8, rs.getString("codigofacturacion_obrasocial"));
                            pst9.setString(9, rs.getString("codigo_obrasocial"));
                            pst9.executeUpdate();

                            System.out.println("Se Creo la obra social" + rs.getString("int_codigo_obrasocial"));

                            ///////////////////////////////// Agrego Practicas
                            String sSQL2 = "Select codigo_practica, codigo_fac_practicas_obrasocial, round(unidaddebioquimica_practica,2) as unidaddebioquimica_practica ,round(importeunidaddearancel_obrasocial,2) as importeunidaddearancel_obrasocial,round(preciofijo,2) as preciofijo ,round(preciototal,2) as preciototal\n"
                                    + "from obrasocial_tiene_practicasnbu\n"
                                    + "where id_obrasocial=" + rs.getString("id_obrasocial") + "\n"
                                    + "order by codigo_practica";
                            Statement st2 = cr.createStatement();
                            ResultSet rs2 = st2.executeQuery(sSQL2);

                            while (rs2.next()) {

                                System.out.println("Agrega Practica: " + rs2.getString("codigo_practica") + rs.getString("int_codigo_obrasocial"));

                                ///////////////////////Selecciono los valores a insertar en la tabla desde la vista
                                String sSQL11 = "SELECT int_codigo_obrasocial,id_obrasocial,añonbu FROM obrasocial where int_codigo_obrasocial=" + rs.getString("int_codigo_obrasocial");
                                Statement cl11 = cl.createStatement();
                                ResultSet rs11 = cl11.executeQuery(sSQL11);
                                rs11.next();

                                String sSQL10 = "Select id_practicasnbu from vista_nomencladores where id_nbu=" + rs11.getString("añonbu") + " and codigo_practica=" + rs2.getString("codigo_practica");
                                Statement cl10 = cl.createStatement();
                                ResultSet rs10 = cl10.executeQuery(sSQL10);

                                if (rs10.next()) {

                                    String sSQL8 = "INSERT INTO obrasocial_tiene_practicas_nbu(id_obrasocial,id_practicasnbu,codigo_fac_practicas_obrasocial\n"
                                            + ",unidadbioquimica, importeunidaddearancel_obrasocial, preciofijo, preciototal, id_nbu,id_usuarios,estado)\n"
                                            + "VALUES(?,?,?,?,?,?,?,?,?,?)";

                                    PreparedStatement pst8 = cl.prepareStatement(sSQL8);
                                    pst8.setString(1, rs11.getString("id_obrasocial"));   ///////// id_obrasocial
                                    pst8.setString(2, rs10.getString("id_practicasnbu")); //////// id_practicasnbu
                                    pst8.setString(3, rs2.getString("codigo_fac_practicas_obrasocial"));////codigo_fac_practicas_obrasocial
                                    pst8.setString(4, rs2.getString("unidaddebioquimica_practica"));/// unidadbioquimica
                                    pst8.setString(5, rs2.getString("importeunidaddearancel_obrasocial")); /////
                                    pst8.setString(6, rs2.getString("preciofijo"));
                                    pst8.setString(7, rs2.getString("preciototal"));
                                    pst8.setString(8, rs11.getString("añonbu"));
                                    pst8.setString(9, "1"); //// Usuario 
                                    pst8.setString(10, "1");
                                    pst8.executeUpdate();

                                    System.out.println("Se agrego la Practica correctamente:" + rs2.getString("codigo_practica"));
                                }
                                rs10.close();
                            }
                            rs2.close();

                        }
                        rs6.close();
                        try {

                        } catch (Exception e) {
                          //  JOptionPane.showMessageDialog(null, e);
                        }

                    } catch (Exception e) {
                       // JOptionPane.showMessageDialog(null, e);
                    }
                    progreso.setValue(i++);
                }

                JOptionPane.showMessageDialog(null, "Se realizo la actualización de las Obras Sociales contratadas por el colegio");
            } catch (Exception e) {
               // JOptionPane.showMessageDialog(null, e);
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {

            }
            dispose();

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public double Redondear(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }

    void cargarddjj() {
        String sSQL = "";
        String numero = "";
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();

        sSQL = "SELECT MAX(id_declaracion_jurada) AS id_declaracion_jurada FROM declaracion_jurada";

        //“SELECT MAX(id) AS id FROM tabla”
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.last();
            if (rs.getInt("id_declaracion_jurada") != 0) {
                id_ddjj = rs.getInt("id_declaracion_jurada");
            } else {
                id_ddjj = 1;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    String completarceros(String v, int d) {
        if (v.length() < d) {
            String ceros = "";
            for (int i = v.length(); i < d; i++) {
                ceros += "0";
            }
            v = ceros + v;
        }
        return v;
    }

    public double Redondeardosdigitos(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        progreso = new javax.swing.JProgressBar();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Actualizando bases de datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("En Proceso espere unos minutos...");

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setString("");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Cargar.gif"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JProgressBar progreso;
    // End of variables declaration//GEN-END:variables
}
