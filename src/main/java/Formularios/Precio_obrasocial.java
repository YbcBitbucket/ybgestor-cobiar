package Formularios;

import Clases.ConexionMySQLLocal;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

public class Precio_obrasocial extends javax.swing.JDialog {

    int contadorj = 0, id_obra_social, idobraimprime, id_usuario_validador, periodo_colegiado, id_usuario;
    String ObraSocial, CodObra, año, mes, periodo, colegiado, fecha2, periodo2;
    TextAutoCompleter textAutoAcompleter2;
    public static String[] obrasocial = new String[500];
    public static int[] idobrasocial = new int[500];
    public static String[] nombreobrasocial = new String[500];
    public static Double[] arancel = new Double[500];
    public static int[] idobra = new int[150000];
    public static int contadorobrasocial = 0;
    Hiloobrasocial hilo2;
    Hiloobrasocialtodas hilo3;
    Hilo_verificar_practicas hiloverificar;

    public Precio_obrasocial(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Precio Obra Social");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        
        progreso.setVisible(true);
        textAutoAcompleter2 = new TextAutoCompleter(txtobrasocial);
        traerobrasosial();
        //cargarperiodo();
        cargarobrasocial();
        this.setLocationRelativeTo(null);
    }

    void traerobrasosial() {

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT id_obrasocial,razonsocial_obrasocial,importeunidaddearancel_obrasocial,codigo_obrasocial FROM obrasocial ";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                idobrasocial[contadorobrasocial] = rs.getInt("id_obrasocial");
                nombreobrasocial[contadorobrasocial] = (rs.getString("razonsocial_obrasocial"));
                arancel[contadorobrasocial] = (rs.getDouble("importeunidaddearancel_obrasocial"));
                obrasocial[contadorobrasocial] = (rs.getString("codigo_obrasocial") + " - " + rs.getString("razonsocial_obrasocial"));
                contadorobrasocial++;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarobrasocial() {
        TextAutoCompleter textAutoAcompleter = new TextAutoCompleter(txtobrasocial);

        int i = 0;

        // Recorro y cargo las obras sociales
        while (i < contadorobrasocial) {
            textAutoAcompleter.addItem(obrasocial[i]);

            i++;
        }
        //textAutoAcompleter.setMode(-1); // prefijo, viene por defecto
        textAutoAcompleter.setMode(0); // infijo     

        // textAutoAcompleter.setMode(1); // sufijo
        //textAutoAcompleter.setCaseSensitive(true); // Sensible a mayúsculas
        textAutoAcompleter.setCaseSensitive(false); //No sensible a mayúsculas        

    }

    public class Hiloobrasocial extends Thread {

        JProgressBar progreso;

        public Hiloobrasocial(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        public void run() {
            chkobra.setEnabled(false);
            chkperiodo.setEnabled(false);
            fechainicial.setEnabled(false);
            fechafinal.setEnabled(false);
            txtobrasocial.setEnabled(false);
            btnimprimir.setEnabled(false);
            btnsalir.setEnabled(false);
            btnimprimir1.setEnabled(false);
            ConexionMySQLLocal mysql = new ConexionMySQLLocal();
            Connection cn = mysql.Conectar();
            try {
                ///////////////////////// Selecciono las Practicas Facturadas de una obra social en un periodo ordenadas de menor a mayor
                Statement st4 = cn.createStatement();
                //String sql4 ="SELECT DISTINCT(detalle_ordenes.cod_practica), detalle_ordenes.precio_practica FROM ordenes INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " AND ordenes.id_obrasocial=" + id_obra_social + " and detalle_ordenes.estado=" + 0 + "  order by (detalle_ordenes.cod_practica) ";
                String sql4 = "SELECT DISTINCT(practicas.codigo_practica),practicas.id_practicas, ordenes_tienen_practicas.precio_practica, ordenes_tienen_practicas.cod_practica_fac \n"
                        + "FROM ordenes \n"
                        + "INNER JOIN ordenes_tienen_practicas  using(id_ordenes) \n"
                        + "INNER JOIN obrasocial  using(id_obrasocial) \n"
                        + "INNER JOIN practicas_nbu  using(id_practicas) \n"
                        + "INNER JOIN obrasocial_tiene_practicas_nbu  using(id_practicasnbu)\n"
                        + "INNER JOIN practicas  using(id_practicas)\n"
                        + "WHERE DATE(fecha) BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "'  and ordenes.id_obrasocial=" + id_obra_social + " and ordenes_tienen_practicas.factura=1  order by (practicas_nbu.id_practicas) ";
                //JOptionPane.showMessageDialog(null, id_obra_social);
                // JOptionPane.showMessageDialog(null, invertir(fechainicial.getText()));
                //JOptionPane.showMessageDialog(null, invertir(fechafinal.getText()));
                ResultSet rs4 = st4.executeQuery(sql4);
                while (rs4.next()) {
                    //JOptionPane.showMessageDialog(null, rs4.getString(2));

                    ///////////////////////////busca en obra social tiene practicas//////////////////////////
                    Statement st1 = cn.createStatement();
                    String sql1 = "SELECT  practicas_nbu.id_practicas,obrasocial_tiene_practicas_nbu.codigo_fac_practicas_obrasocial,obrasocial_tiene_practicas_nbu.preciototal\n"
                            + "FROM obrasocial_tiene_practicas_nbu \n"
                            + "INNER JOIN practicas_nbu  using(id_practicasnbu) \n"
                            + "INNER JOIN obrasocial  using(id_obrasocial) \n"
                            + "WHERE obrasocial.id_obrasocial=" + id_obra_social + " and practicas_nbu.id_practicas=" + rs4.getString(2);
                    //String sql1 = "SELECT determinacion,preciototal,codigo_fac_practicas_obrasocial,codigo_practica FROM obrasocial_tiene_practicasnbu WHERE id_obrasocial=" + id_obra_social + " and codigo_practica=" + rs4.getInt("detalle_ordenes.cod_practica");
                    ResultSet rs1 = st1.executeQuery(sql1);
                    rs1.next();
                    ///////////////////////////busca en las ordenes con cada obra social//////////////////////////
                    // String sSQL2 = "UPDATE detalle_ordenes d INNER JOIN ordenes o ON d.id_orden = o.id_orden SET d.precio_practica=" + rs1.getDouble("preciototal") + " , d.cod_practica_fac=" + rs1.getInt("codigo_fac_practicas_obrasocial") + " WHERE o.id_obrasocial=" + id_obra_social + " and (o.periodo=" + periodo2 + " and d.cod_practica=" + rs1.getInt("codigo_practica") + " and detalle_ordenes.estado=0 )";
                    //String sSQL2 = "UPDATE detalle_ordenes d INNER JOIN ordenes o ON d.id_orden = o.id_orden SET d.precio_practica=" + rs1.getDouble("preciototal") + " , d.cod_practica_fac=" + rs1.getInt("codigo_fac_practicas_obrasocial") + " WHERE o.id_obrasocial=" + id_obra_social + " and (o.periodo=" + periodo2 + " and d.cod_practica=" + rs1.getInt("codigo_practica") + " and d.estado=0 )";
                    String sSQL2 = "UPDATE ordenes_tienen_practicas d \n"
                            + "INNER JOIN ordenes o ON d.id_ordenes = o.id_ordenes \n"
                            + "SET d.precio_practica=" + rs1.getDouble("preciototal") + " , d.cod_practica_fac=lpad(" + rs1.getString("codigo_fac_practicas_obrasocial") + ",6,'0') \n"
                            + "WHERE DATE(fecha) BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' and  d.id_practicas=" + rs4.getString(2) + " and o.id_obrasocial=" + id_obra_social + "   and o.estado_orden=0\n"
                            + "and d.factura=1";
                    PreparedStatement pst = cn.prepareStatement(sSQL2);

                    int n = pst.executeUpdate();

                    if (n > 0) {

                    }
                }
                JOptionPane.showMessageDialog(null, "Proceso realizado con exito...");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
            chkobra.setEnabled(true);
            chkperiodo.setEnabled(true);
            fechafinal.setEnabled(true);
            fechainicial.setEnabled(true);
            txtobrasocial.setEnabled(true);
            btnimprimir.setEnabled(true);
            btnimprimir1.setEnabled(true);
            btnsalir.setEnabled(true);
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public class Hiloobrasocialtodas extends Thread {

        JProgressBar progreso;

        public Hiloobrasocialtodas(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

//////// por periodo facturado actualiza
        public void run() {
            {
                chkobra.setEnabled(false);
                chkperiodo.setEnabled(false);
                fechainicial.setEnabled(false);
                fechafinal.setEnabled(false);
                txtobrasocial.setEnabled(false);
                btnimprimir.setEnabled(false);
                btnsalir.setEnabled(false);
                btnimprimir1.setEnabled(false);
                ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                Connection cn = mysql.Conectar();
                try {
                    //////////////////////// Verifico las obras Sociales facturadas en la fecha 
                    Statement st5 = cn.createStatement();
                    String sql5 = "select DISTINCT(id_obrasocial)\n"
                            + "from ordenes\n"
                            + "WHERE DATE(fecha) BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' \n"
                            + " order by ordenes.id_obrasocial";
                    ResultSet rs5 = st5.executeQuery(sql5);

                    while (rs5.next()) {
                        ///////////////////////// Selecciono las Practicas Facturadas de una obra social en un periodo ordenadas de menor a mayor
                        Statement st4 = cn.createStatement();
                        String sql4 = "SELECT DISTINCT(practicas.codigo_practica),practicas.id_practicas, ordenes_tienen_practicas.precio_practica, ordenes_tienen_practicas.cod_practica_fac \n"
                                + "FROM ordenes \n"
                                + "INNER JOIN ordenes_tienen_practicas  using(id_ordenes) \n"
                                + "INNER JOIN obrasocial  using(id_obrasocial) \n"
                                + "INNER JOIN practicas_nbu  using(id_practicas) \n"
                                + "INNER JOIN obrasocial_tiene_practicas_nbu  using(id_practicasnbu)\n"
                                + "INNER JOIN practicas  using(id_practicas)\n"
                                + "WHERE DATE(fecha) BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "'  and ordenes.id_obrasocial=" + rs5.getString(1) + " and ordenes_tienen_practicas.factura=1  order by (practicas_nbu.id_practicas) ";
                        ResultSet rs4 = st4.executeQuery(sql4);

                        while (rs4.next()) {
                            ///////////////////////////busca en obra social tiene practicas//////////////////////////
                            Statement st1 = cn.createStatement();
                            String sql1 = "SELECT  practicas_nbu.id_practicas,obrasocial_tiene_practicas_nbu.codigo_fac_practicas_obrasocial,obrasocial_tiene_practicas_nbu.preciototal\n"
                                    + "FROM obrasocial_tiene_practicas_nbu \n"
                                    + "INNER JOIN practicas_nbu  using(id_practicasnbu) \n"
                                    + "INNER JOIN obrasocial  using(id_obrasocial) \n"
                                    + "WHERE obrasocial.id_obrasocial=" + rs5.getString(1) + " and practicas_nbu.id_practicas=" + rs4.getString(2);
                            ResultSet rs1 = st1.executeQuery(sql1);
                            rs1.next();

                            //JOptionPane.showMessageDialog(null, rs5.getString(1));
                            //JOptionPane.showMessageDialog(null, invertir(fechainicial.getText()));
                            //JOptionPane.showMessageDialog(null, invertir(fechafinal.getText()));

                            ///////////////////////////busca en las ordenes con cada obra social//////////////////////////
                            String sSQL2 = "UPDATE ordenes_tienen_practicas d \n"
                                    + "INNER JOIN ordenes o ON d.id_ordenes = o.id_ordenes \n"
                                    + "SET d.precio_practica=" + rs1.getDouble("preciototal") + " , d.cod_practica_fac=lpad(" + rs1.getString("codigo_fac_practicas_obrasocial") + ",6,'0') \n"
                                    + "WHERE DATE(fecha) BETWEEN '" + invertir(fechainicial.getText()) + "'" + " AND '" + invertir(fechafinal.getText()) + "' and  d.id_practicas=" + rs4.getString(2) + " and o.id_obrasocial=" + rs5.getString(1) + "   and o.estado_orden=0\n"
                                    + "and d.factura=1";
                            PreparedStatement pst = cn.prepareStatement(sSQL2);

                            int n = pst.executeUpdate();

                            if (n > 0) {

                            }
                        }

                    }
                    JOptionPane.showMessageDialog(null, "Proceso realizado con exito...");
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
                chkobra.setEnabled(true);
                chkperiodo.setEnabled(true);
                fechafinal.setEnabled(true);
                fechainicial.setEnabled(true);
                txtobrasocial.setEnabled(true);
                btnimprimir.setEnabled(true);
                btnsalir.setEnabled(true);
                btnimprimir1.setEnabled(true);
            }

        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }

    }

    public class Hilo_verificar_practicas extends Thread {

        JProgressBar progreso;

        public Hilo_verificar_practicas(JProgressBar progreso1) {
            super();
            this.progreso = progreso1;
        }

        //////// por periodo facturado actualiza
        public void run() {
            chkobra.setEnabled(false);
            chkperiodo.setEnabled(false);
            fechainicial.setEnabled(false);
            fechafinal.setEnabled(false);
            txtobrasocial.setEnabled(false);
            btnimprimir.setEnabled(false);
            btnimprimir1.setEnabled(false);
            btnsalir.setEnabled(false);

            ConexionMySQLLocal mysql = new ConexionMySQLLocal();
            Connection cn = mysql.Conectar();

            try {
                Statement st8 = cn.createStatement();
                String sql8 = "SELECT DISTINCT(ordenes.id_obrasocial) FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " order by (obrasocial.Int_codigo_obrasocial)";
                //String sql8 = "SELECT DISTINCT(ordenes.id_obrasocial) FROM ordenes INNER JOIN colegiados ON colegiados.id_colegiados=ordenes.id_colegiados INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and  obrasocial.id_obrasocial="+idobrasocial+"";
                ResultSet rs8 = st8.executeQuery(sql8);

                while (rs8.next()) {
                    // JOptionPane.showMessageDialog(null, rs8.getInt(1));
                    ///////////////////////////////////////////////////////////////////////
                    //if(rs8.getInt(1)>=24 || rs8.getInt(1)>=25 ){
                    if (rs8.getInt(1) == id_obra_social) {
                        // JOptionPane.showMessageDialog(null, rs8.getInt(1));
                        Statement st4 = cn.createStatement();
                        String sql4 = "SELECT DISTINCT(detalle_ordenes.cod_practica), detalle_ordenes.precio_practica FROM ordenes INNER JOIN detalle_ordenes ON detalle_ordenes.id_orden=ordenes.id_orden INNER JOIN obrasocial ON obrasocial.id_obrasocial = ordenes.id_obrasocial WHERE ordenes.periodo=" + periodo2 + " and ordenes.id_obrasocial=" + rs8.getInt(1) + " and detalle_ordenes.estado=" + 0 + " order by (detalle_ordenes.cod_practica) ";
                        ResultSet rs4 = st4.executeQuery(sql4);

                        while (rs4.next()) {
                            ////////////// VERIFICA PRACTICAS
                            Statement st3 = cn.createStatement();
                            String sql3 = "SELECT codigo_practica, preciototal FROM obrasocial_tiene_practicasnbu WHERE id_obrasocial= " + rs8.getInt(1);
                            ResultSet rs3 = st3.executeQuery(sql3);
                            int banderacambiaestado = 0;
                            while (rs3.next()) {
                                if ((rs4.getInt("detalle_ordenes.cod_practica") == rs3.getInt("codigo_practica"))) {
                                    if ((rs4.getDouble("detalle_ordenes.precio_practica") == rs3.getDouble("preciototal"))) {
                                        rs3.last();
                                    } else {
                                        String sSQL5 = "UPDATE detalle_ordenes d INNER JOIN ordenes o ON d.id_orden = o.id_orden SET d.estado=" + 1 + " WHERE o.id_obrasocial=" + rs8.getInt(1) + " and (o.periodo=" + periodo2 + " and d.cod_practica=" + rs4.getInt("detalle_ordenes.cod_practica") + " and (d.precio_practica=" + rs4.getDouble("detalle_ordenes.precio_practica") + "))";
                                        PreparedStatement pst5 = cn.prepareStatement(sSQL5);
                                        int n = pst5.executeUpdate();
                                        if (n > 0) {
                                        }
                                    }
                                    banderacambiaestado = 1;
                                }
                            }
                            if (banderacambiaestado == 0) {
                                String sSQL6 = "UPDATE detalle_ordenes d INNER JOIN ordenes o ON d.id_orden = o.id_orden SET d.estado=" + 1 + " WHERE o.id_obrasocial=" + rs8.getInt(1) + " and (o.periodo=" + periodo2 + " and d.cod_practica=" + rs4.getInt("detalle_ordenes.cod_practica") + ")";
                                PreparedStatement pst6 = cn.prepareStatement(sSQL6);
                                int n = pst6.executeUpdate();
                                if (n > 0) {
                                }
                            }
                        }
                    }
                    ////////////////////////////////////////////////////////////////////////////////////
                }
                JOptionPane.showMessageDialog(null, "Proceso realizado con exito...");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

            chkobra.setEnabled(true);
            chkperiodo.setEnabled(true);
            fechafinal.setEnabled(true);
            fechafinal.setEnabled(true);
            txtobrasocial.setEnabled(true);
            btnimprimir1.setEnabled(true);
            btnimprimir.setEnabled(true);
            btnsalir.setEnabled(true);
        }

        public void pausa(int mlSeg) {
            try {
                // pausa para el splash
                Thread.sleep(mlSeg);
            } catch (Exception e) {
            }

        }
    }

    void cargarperiodo() {
        ////////////////////////////////////////////////////////////////////
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        try {
            String sSQL = "SELECT * FROM periodos ";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.next();
            periodo = rs.getString("periodo");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        //////////////////////////////////

        SimpleDateFormat formato2 = new SimpleDateFormat("d' de 'MMMM' de 'yyyy");

        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);

        fecha2 = formato2.format(currentDate);
        /////////////////////////////////////////////////////////       

        calendar.setTime(currentDate);

    }

    public double Redondearcentavos(double numero) {
        return Math.rint(numero * 100) % 100;
    }

    public double Redondearpesos(double numero) {
        return Math.rint(numero);
    }

    public double Redondeardosdigitos(double numero) {
        return Math.rint(numero * 100) / 100;
    }

    public int completatotal(double numero) {
        //int centavos=Math.rint(numero * 100) % 100;
        int n = (int) numero;
        return n;
    }

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(false);
        this.getjProgressBar1().setForeground(new Color(100, 100, 100, 100));
        //[77,239,38]
        this.getjProgressBar1().setStringPainted(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        fechainicial = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        fechafinal = new javax.swing.JFormattedTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtobrasocial = new javax.swing.JTextField();
        progreso = new javax.swing.JProgressBar();
        btnimprimir = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        chkobra = new javax.swing.JCheckBox();
        chkperiodo = new javax.swing.JCheckBox();
        btnimprimir1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Período a Imprimir", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Desde:");

        fechainicial.setForeground(new java.awt.Color(0, 102, 204));
        try {
            fechainicial.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        fechainicial.setToolTipText("");
        fechainicial.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        fechainicial.setDropMode(javax.swing.DropMode.INSERT);
        fechainicial.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        fechainicial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fechainicialActionPerformed(evt);
            }
        });
        fechainicial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechainicialKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Hasta:");

        fechafinal.setForeground(new java.awt.Color(0, 102, 204));
        try {
            fechafinal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        fechafinal.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        fechafinal.setDropMode(javax.swing.DropMode.INSERT);
        fechafinal.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        fechafinal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fechafinalActionPerformed(evt);
            }
        });
        fechafinal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechafinalKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                fechafinalKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(fechainicial, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(fechafinal, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(fechafinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(fechainicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(3, 3, 3))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Resumen de una Obra Social", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Obra Social:");

        txtobrasocial.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtobrasocial.setForeground(new java.awt.Color(0, 102, 204));
        txtobrasocial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtobrasocialMouseClicked(evt);
            }
        });
        txtobrasocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtobrasocialActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtobrasocial)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtobrasocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        progreso.setFont(new java.awt.Font("Tahoma", 0, 6)); // NOI18N
        progreso.setForeground(new java.awt.Color(100, 100, 100));
        progreso.setString("");

        btnimprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728924 - clock time.png"))); // NOI18N
        btnimprimir.setText("Procesar");
        btnimprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimirActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir.setMnemonic('v');
        btnsalir.setText("Volver");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        chkobra.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chkobra.setText("Por Obra Social");

        chkperiodo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chkperiodo.setText("Por Periodo");
        chkperiodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkperiodoActionPerformed(evt);
            }
        });

        btnimprimir1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728951 - electricity lightning.png"))); // NOI18N
        btnimprimir1.setText("Verificar practicas");
        btnimprimir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimir1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(chkobra)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(chkperiodo))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnimprimir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnimprimir1)
                        .addGap(18, 18, 18)
                        .addComponent(btnsalir))
                    .addComponent(progreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkobra)
                    .addComponent(chkperiodo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progreso, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnimprimir1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtobrasocialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtobrasocialMouseClicked
        String cod = "", nom = "", cadena = txtobrasocial.getText();
        int i = 0, k = 0, j = 1;
        ///cod/// 0000 - aaaaaaaaa
        while (k < cadena.length()) {
            if (String.valueOf(cadena.charAt(k)).equals("-")) {
                j = k + 2;
                k = cadena.length();
            } else {
                cod = cod + cadena.charAt(k);
            }
            k++;
        }
        ///nom/// 0000 - aaaaaaaaa
        while (j < cadena.length()) {
            nom = nom + cadena.charAt(j);
            j++;
        }
        while (i < contadorobrasocial) {
            if (cadena.equals(obrasocial[i])) {
                id_obra_social = idobrasocial[i];
                break;
            }
            i++;
        }
        ObraSocial = nom;
        CodObra = cod;
        idobraimprime = id_obra_social;
        txtobrasocial.transferFocus();
    }//GEN-LAST:event_txtobrasocialMouseClicked

    private void txtobrasocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtobrasocialActionPerformed
        String cod = "", nom = "", cadena = txtobrasocial.getText();
        int i = 0, k = 0, j = 1;
        ///cod/// 0000 - aaaaaaaaa
        while (k < cadena.length()) {
            if (String.valueOf(cadena.charAt(k)).equals("-")) {
                j = k + 2;
                k = cadena.length();
            } else {
                cod = cod + cadena.charAt(k);
            }
            k++;
        }
        ///nom/// 0000 - aaaaaaaaa
        while (j < cadena.length()) {
            nom = nom + cadena.charAt(j);
            j++;
        }
        while (i < contadorobrasocial) {
            if (cadena.equals(obrasocial[i])) {
                id_obra_social = idobrasocial[i];
                break;
            }
            i++;
        }
        ObraSocial = nom;
        CodObra = cod;
        idobraimprime = id_obra_social;
        txtobrasocial.transferFocus();
    }//GEN-LAST:event_txtobrasocialActionPerformed

    private void btnimprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimirActionPerformed

        if (chkobra.isSelected()) {
            iniciarSplash();
            hilo2 = new Hiloobrasocial(progreso);
            hilo2.start();
            hilo2 = null;
        }
        if (chkperiodo.isSelected()) {
            iniciarSplash();
            hilo3 = new Hiloobrasocialtodas(progreso);
            hilo3.start();
            hilo3 = null;
        }

    }//GEN-LAST:event_btnimprimirActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnimprimir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimir1ActionPerformed

        iniciarSplash();
        hiloverificar = new Hilo_verificar_practicas(progreso);
        hiloverificar.start();
        hiloverificar = null;

    }//GEN-LAST:event_btnimprimir1ActionPerformed

    private void chkperiodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkperiodoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkperiodoActionPerformed

    private void fechainicialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fechainicialActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_fechainicialActionPerformed

    private void fechainicialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechainicialKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            fechainicial.transferFocus();
            fechafinal.select(0, 0);
        }
    }//GEN-LAST:event_fechainicialKeyPressed

    String invertir(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }
        String salida = "";
        int i = 0;
        /////Año/////
        for (i = 6; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "/";
        ///Mes///
        for (i = 3; i <= 4; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "/";
        ////Dia////
        for (i = 0; i <= 1; i++) {
            salida = salida + entrada.charAt(i);
        }

        return salida;

    }


    private void fechafinalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fechafinalActionPerformed

    }//GEN-LAST:event_fechafinalActionPerformed

    private void fechafinalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechafinalKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (chkperiodo.isSelected()) {
                txtobrasocial.setEnabled(false);
                txtobrasocial.transferFocus();
            } else {
                fechafinal.transferFocus();
            }

        }
    }//GEN-LAST:event_fechafinalKeyPressed

    private void fechafinalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechafinalKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_fechafinalKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnimprimir;
    private javax.swing.JButton btnimprimir1;
    private javax.swing.JButton btnsalir;
    private javax.swing.JCheckBox chkobra;
    private javax.swing.JCheckBox chkperiodo;
    private javax.swing.JFormattedTextField fechafinal;
    private javax.swing.JFormattedTextField fechainicial;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JProgressBar progreso;
    private javax.swing.JTextField txtobrasocial;
    // End of variables declaration//GEN-END:variables
}
