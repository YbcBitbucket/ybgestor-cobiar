package Formularios;

import Clases.ConexionMySQLLocal;
import Clases.Escape;
import Clases.HistoriaClinica;
import Clases.RowsRenderer;
import Clases.camposinformes;
import Clases.camposinformes2;
import Clases.camposinformes3;
import Clases.camposinformes4;
import Clases.camposinformes5;
import static Formularios.Historias_Clinicas.bandera_informe;
import static Formularios.Informe.id_orden;
import static Formularios.Mensaje_Mail.asunto;
import static Formularios.Mensaje_Mail.mensaje;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;

import javax.swing.table.TableRowSorter;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class Resultados extends javax.swing.JDialog {

    DefaultTableModel model, model1;
    public static String nombre_paciente = "", mail = "", fechaPaciente;
    public static int enviar = 0, idpratica, idpaciente, idanalisis, imprime_analisis = 0, id_historia_clinica = 0;
    public static String Username = "ybcomputacion@gmail.com";
    public static String PassWord = "31bruluma", resultado = "0", unidad, observacion = "", referencia = "";
    String Mensage = "Prueba", observaciones = null, metodo = null;
    String To = "";
    String Subject = "Prueba", fechainforme, horainforme, fechaEntrega;
    String protocolo = "", paciente = "", dni = "";
    Image imagen2;
    DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
    DecimalFormat formato;
    RowsRenderer rr = new RowsRenderer(5);
    int estado_entrega = 0;

    public Resultados(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Resultados");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        this.setLocationRelativeTo(null);
        cargartablaResultados("");
        cargarfecha();
        cargardatoslab();
        tablapracticas.getColumnModel().getColumn(1).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(1).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(1).setPreferredWidth(0);
        //////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(4).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(4).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(4).setPreferredWidth(0);
        /////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(8).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(8).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(8).setPreferredWidth(0);
        //////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(9).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(9).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(9).setPreferredWidth(0);
        ///////////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(10).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(10).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(10).setPreferredWidth(0);
        ///////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(11).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(11).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(11).setPreferredWidth(0);
        ///////////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(12).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(12).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(12).setPreferredWidth(0);
        ///////////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(13).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(13).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(13).setPreferredWidth(0);
        /////////////////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(14).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(14).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(14).setPreferredWidth(0);
        ///////////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(15).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(15).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(15).setPreferredWidth(0);
        ///////////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(16).setMaxWidth(0);
        tablapracticas.getColumnModel().getColumn(16).setMinWidth(0);
        tablapracticas.getColumnModel().getColumn(16).setPreferredWidth(0);
        //////////////////////////////////////////////////////////////////
        tablapracticas.getColumnModel().getColumn(0).setPreferredWidth(1);
        tablapracticas.getColumnModel().getColumn(2).setPreferredWidth(15);
        tablapracticas.getColumnModel().getColumn(3).setPreferredWidth(15);
        tablapracticas.getColumnModel().getColumn(5).setPreferredWidth(150);
        tablapracticas.getColumnModel().getColumn(6).setPreferredWidth(10);
        tablapracticas.getColumnModel().getColumn(7).setPreferredWidth(100);
        tablapracticas.getColumnModel().getColumn(17).setPreferredWidth(15);
        unclickreferencia();
        txtnombre.setText(nombre_paciente);
        if (bandera_informe == 1) {
            btnimprimir1.doClick();
            btncancelar.doClick();
        }
        Escape.funcionescape(this);
        tablapracticas.setRowSelectionInterval(0, 0);
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtreferencia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });

    }

    void unclickreferencia() {
        tablapracticas.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    int i = tablapracticas.getSelectedRow();

                    if (tablapracticas.getValueAt(i, 7) == null) {
                        txtreferencia.setText(" Valores de Referencia: ");
                    } else {
                        txtreferencia.setText(" Valores de Referencia: " + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 7).toString());
                    }
                }
            }
        });
        txtprotocolo.setText(completarceros(String.valueOf(id_orden), 8));
        id_historia_clinica = id_orden;
        txtprotocolo.setEnabled(true);
    }

    String completarceros(String v, int d) {
        if (v.length() < d) {
            String ceros = "";
            for (int i = v.length(); i < d; i++) {
                ceros += "0";
            }
            v = ceros + v;
        }
        return v;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablapracticas = new javax.swing.JTable();
        txtnbu = new javax.swing.JTextField();
        txtnombre = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txtprotocolo = new javax.swing.JTextField();
        btncancelar = new javax.swing.JButton();
        btnimprimir1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtreferencia = new javax.swing.JTextArea();
        btnimprimir2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cargar Resultados", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablapracticas.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablapracticas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablapracticas.setOpaque(false);
        tablapracticas.setSelectionBackground(new java.awt.Color(0, 0, 0));
        tablapracticas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tablapracticas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablapracticasKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablapracticasKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(tablapracticas);

        txtnbu.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtnbu.setForeground(new java.awt.Color(0, 102, 204));
        txtnbu.setNextFocusableComponent(tablapracticas);
        txtnbu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnbuActionPerformed(evt);
            }
        });
        txtnbu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnbuKeyReleased(evt);
            }
        });

        txtnombre.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 102, 204));
        jLabel1.setText("Protocolo:");

        txtprotocolo.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        txtprotocolo.setForeground(new java.awt.Color(0, 102, 204));
        txtprotocolo.setBorder(null);
        txtprotocolo.setOpaque(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(txtnbu, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtprotocolo, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 952, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtnbu, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtnombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtprotocolo, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(11, 11, 11)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
                .addContainerGap())
        );

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728980 - home.png"))); // NOI18N
        btncancelar.setMnemonic('s');
        btncancelar.setText("Volver");
        btncancelar.setToolTipText("[Alt + s]");
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        btnimprimir1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728900 - application browser.png"))); // NOI18N
        btnimprimir1.setMnemonic('i');
        btnimprimir1.setText("Generar Informe");
        btnimprimir1.setToolTipText("[Alt + i]");
        btnimprimir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimir1ActionPerformed(evt);
            }
        });

        txtreferencia.setEditable(false);
        txtreferencia.setColumns(20);
        txtreferencia.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtreferencia.setForeground(new java.awt.Color(51, 51, 51));
        txtreferencia.setRows(2);
        jScrollPane1.setViewportView(txtreferencia);

        btnimprimir2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnimprimir2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728962 - discount pricetag shopping.png"))); // NOI18N
        btnimprimir2.setMnemonic('i');
        btnimprimir2.setText("Enviar Informe");
        btnimprimir2.setToolTipText("[Alt + i]");
        btnimprimir2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnimprimir2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnimprimir1)
                        .addGap(20, 20, 20)
                        .addComponent(btnimprimir2)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 476, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btncancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btncancelar)
                        .addComponent(btnimprimir1)
                        .addComponent(btnimprimir2))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    int EstadoEntrega() {
        int i = 0, n = tablapracticas.getRowCount();
        int contador = 0;
        while (i < n) {
            if (!tablapracticas.getValueAt(i, 5).equals("-")) {
                estado_entrega = 1;
            } else {
                estado_entrega = 2;
                break;
            }
            i++;
        }
        return estado_entrega;
    }

    private void tablapracticasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablapracticasKeyPressed

        DefaultTableModel temp = (DefaultTableModel) tablapracticas.getModel();
        String formula = "";
        int i = 0;
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            if (tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 7) == null) {
                txtreferencia.setText(" Valores de Referencia: ");
            } else {
                txtreferencia.setText(" Valores de Referencia: " + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 7).toString());
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            if (tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 7) == null) {
                txtreferencia.setText(" Valores de Referencia: ");
            } else {
                txtreferencia.setText(" Valores de Referencia: " + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 7).toString());
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            tablapracticas.transferFocus();
            evt.consume();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (tablapracticas.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila...");
            } else {

                if (tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 6) == null) {
                    unidad = "";
                } else {
                    unidad = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 6).toString();
                }
                if (tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 7) == null) {
                    referencia = "";
                } else {
                    referencia = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 7).toString();
                }
                idpratica = Integer.valueOf(tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 9).toString());
                idanalisis = Integer.valueOf(tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 1).toString());
                System.out.println("-----------------------------------------------------------------");
                Resultado.nombre = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 3).toString() + " - " + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 11).toString();
                if (!tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 3).toString().equals("")) {
                    if (!tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 16).toString().equals("")) {
                        int bandera_signos = 0, bandera_operador = 0;
                        String aux = "", operador = "", constante = "0";
                        double operando1 = 0.00, operando2 = 0.00;
                        formula = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 16).toString();
                        while (i < formula.length()) {
                            if (String.valueOf(formula.charAt(i)).equals("(") || String.valueOf(formula.charAt(i)).equals(")") || String.valueOf(formula.charAt(i)).equals("[") || String.valueOf(formula.charAt(i)).equals("]") || String.valueOf(formula.charAt(i)).equals("{") || String.valueOf(formula.charAt(i)).equals("}")) {
                                if (bandera_signos == 1) {
                                    int j = 0, n = tablapracticas.getRowCount();
                                    while (j < n) {
                                        if (tablapracticas.getValueAt(j, 3).toString().equals(aux)) {
                                            operando2 = Double.valueOf(tablapracticas.getValueAt(j, 5).toString());
                                            System.out.println("operando2 " + operando2);
                                            if (operador.equals("/")) {
                                                operando1 = ((operando1 / operando2) * 100.00) / 100.00;
                                            }
                                            if (operador.equals("*")) {
                                                operando1 = ((operando1 * operando2) * 100.00) / 100.00;
                                            }
                                            if (operador.equals("+")) {
                                                operando1 = ((operando1 + operando2) * 100.00) / 100.00;
                                            }
                                            if (operador.equals("-")) {
                                                operando1 = ((operando1 - operando2) * 100.00) / 100.00;
                                            }
                                            System.out.println("operando1 " + operando1);
                                        }
                                        j++;
                                    }
                                    bandera_signos = 0;
                                    aux = "";
                                    operando2 = 0.0;
                                    operador = "";
                                } else {
                                    bandera_signos = 1;
                                }
                            } else {
                                if (bandera_operador == 0) {
                                    if (String.valueOf(formula.charAt(i)).equals("-") || String.valueOf(formula.charAt(i)).equals("+") || String.valueOf(formula.charAt(i)).equals("/") || String.valueOf(formula.charAt(i)).equals("*")) {
                                        operador = operador + formula.charAt(i);
                                        bandera_operador = 1;
                                        int j = 0, n = tablapracticas.getRowCount();
                                        while (j < n) {//(Hematocritos1/Eritrocitos)*10
                                            if (tablapracticas.getValueAt(j, 3).toString().equals(aux)) {
                                                if (operando1 == 0) {
                                                    operando1 = Double.valueOf(tablapracticas.getValueAt(j, 5).toString());
                                                    aux = "";
                                                    bandera_operador = 0;
                                                    j = n;
                                                } else {
                                                    operando2 = Double.valueOf(tablapracticas.getValueAt(j, 5).toString());
                                                    System.out.println("operando2 " + operando2);
                                                    if (String.valueOf(operador.charAt(0)).equals("/")) {
                                                        operando1 = operando1 / operando2;
                                                    }
                                                    if (String.valueOf(operador.charAt(0)).equals("*")) {
                                                        operando1 = operando1 * operando2;
                                                    }
                                                    if (String.valueOf(operador.charAt(0)).equals("+")) {
                                                        operando1 = operando1 + operando2;
                                                    }
                                                    if (String.valueOf(operador.charAt(0)).equals("-")) {
                                                        operando1 = operando1 - operando2;
                                                    }
                                                    aux = "";
                                                    for (int k = 1; k < operador.length(); k++) {///borro el primer operando
                                                        aux = String.valueOf(operador.charAt(k));
                                                    }
                                                    System.out.println("operando1 " + operando1);
                                                    operador = aux;
                                                    aux = "";
                                                    bandera_operador = 0;
                                                    j = n;
                                                }
                                            } else {
                                                if ("".equals(aux)) {
                                                    System.out.println("entra al if");
                                                    for (int k = i + 1; k < formula.length(); k++) {
                                                        ///borro el primer operando
                                                        if (!String.valueOf(formula.charAt(k)).equals("-") && !String.valueOf(formula.charAt(k)).equals("+") && !String.valueOf(formula.charAt(k)).equals("/") && !String.valueOf(formula.charAt(k)).equals("*") && !String.valueOf(formula.charAt(k)).equals("/") && !String.valueOf(formula.charAt(k)).equals("=")) {
                                                            aux = aux + formula.charAt(k);
                                                        } else {
                                                            k = formula.length();
                                                        }
                                                    }
                                                }
                                                System.out.println("aux: " + aux);
                                                System.out.println("sale del if");
                                            }
                                            j++;
                                        }
                                    } else {
                                        aux = aux + formula.charAt(i);
                                    }
                                } else {
                                    if (String.valueOf(formula.charAt(i)).equals("1") || String.valueOf(formula.charAt(i)).equals("2") || String.valueOf(formula.charAt(i)).equals("3") || String.valueOf(formula.charAt(i)).equals("4") || String.valueOf(formula.charAt(i)).equals("5") || String.valueOf(formula.charAt(i)).equals("6") || String.valueOf(formula.charAt(i)).equals("7") || String.valueOf(formula.charAt(i)).equals("8") || String.valueOf(formula.charAt(i)).equals("9") || String.valueOf(formula.charAt(i)).equals("0") || String.valueOf(formula.charAt(i)).equals(".")) {

                                        constante = constante + formula.charAt(i);
                                        System.out.println("constante=" + constante);
                                    } else {
                                        aux = aux + formula.charAt(i);
                                    }
                                }
                            }
                            if (String.valueOf(formula.charAt(i)).equals("=")) {
                                Double numEntero = Double.valueOf(constante);

                                System.out.println("operando1 " + operando1);
                                System.out.println("operador " + operador);
                                System.out.println("operando2 " + operando2);
                                System.out.println("numEntero " + numEntero);
                                if (numEntero != 0 || operando2 != 0) {
                                    if (numEntero != 0) {

                                        if (operador.equals("/")) {
                                            operando1 = operando1 / numEntero;
                                        }
                                        if (operador.equals("*")) {
                                            operando1 = operando1 * numEntero;
                                        }
                                        if (operador.equals("+")) {
                                            operando1 = operando1 + numEntero;
                                        }
                                        if (operador.equals("-")) {
                                            operando1 = operando1 - numEntero;
                                        }
                                        System.out.println("operando1 " + operando1);
                                    }
                                    if (operando2 != 0) {
                                        if (operador.equals("/")) {
                                            operando1 = operando1 / operando2;
                                        }
                                        if (operador.equals("*")) {
                                            operando1 = operando1 * operando2;
                                        }
                                        if (operador.equals("+")) {
                                            operando1 = operando1 + operando2;
                                        }
                                        if (operador.equals("-")) {
                                            operando1 = operando1 - operando2;
                                        }
                                        System.out.println("operando1 " + operando1);
                                    }
                                }
                            }
                            i++;
                            System.out.println("Resultado: " + operando1);
                        }
                        simbolos.setDecimalSeparator('.');
                        formato = new DecimalFormat("####0.00", simbolos);
                        tablapracticas.setValueAt(formato.format(operando1), tablapracticas.getSelectedRow(), 5);
                        ConexionMySQLLocal cc = new ConexionMySQLLocal();
                        Connection cn = cc.Conectar();
                        String id_orden = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 8).toString();
                        String id_analisis = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 1).toString();
                        System.out.println("-----------------------------------------------------------------");
                        try {
                            String sSQL3 = "update resultados set resultado=?, estado_imprime=1 where id_ordenes=" + id_orden + " and id_analisis=" + id_analisis;
                            PreparedStatement pst = cn.prepareStatement(sSQL3);
                            pst.setString(1, formato.format(operando1));
                            int n4 = pst.executeUpdate();
                            if (n4 > 0) {
                                tablapracticas.setValueAt(formato.format(operando1), tablapracticas.getSelectedRow(), 5);
                                if (!tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 5).equals("-")) {
                                    tablapracticas.setValueAt(true, tablapracticas.getSelectedRow(), 17);
                                }
                            }
                            cn.close();
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }
                    } else {
                        resultado = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 5).toString();
                        new Resultado(null, true).setVisible(true);
                        if (!resultado.equals("")) {
                            ConexionMySQLLocal cc = new ConexionMySQLLocal();
                            Connection cn = cc.Conectar();
                            String id_orden = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 8).toString();
                            String id_analisis = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 1).toString();
                            try {
                                String sSQL3 = "update resultados set resultado=?, estado_imprime=1 where id_ordenes=" + id_orden + " and id_analisis=" + id_analisis;
                                PreparedStatement pst = cn.prepareStatement(sSQL3);
                                pst.setString(1, resultado);
                                int n4 = pst.executeUpdate();
                                if (n4 > 0) {
                                    tablapracticas.setValueAt(resultado, tablapracticas.getSelectedRow(), 5);
                                    if (!tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 5).toString().equals("-")) {
                                        tablapracticas.setValueAt(true, tablapracticas.getSelectedRow(), 17);
                                    }
                                }
                                cn.close();
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(null, e);
                            }
                        }
                    }
                } else {
                    ConexionMySQLLocal cc = new ConexionMySQLLocal();
                    Connection cn = cc.Conectar();
                    String id_orden = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 8).toString();
                    String id_analisis = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 1).toString();
                    try {
                        String sSQL3 = "update resultados set resultado=?, estado_imprime=1 where id_ordenes=" + id_orden + " and id_analisis=" + id_analisis;
                        PreparedStatement pst = cn.prepareStatement(sSQL3);
                        pst.setString(1, "");
                        int n4 = pst.executeUpdate();
                        if (n4 > 0) {
                            tablapracticas.setValueAt("", tablapracticas.getSelectedRow(), 5);
                            if (!tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 5).toString().equals("-")) {
                                tablapracticas.setValueAt(true, tablapracticas.getSelectedRow(), 17);
                            }
                        }
                        cn.close();
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                }
            }

        }
        if (evt.getKeyCode() == KeyEvent.VK_ADD) {
            if (tablapracticas.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila...");
            } else {
                /*
                 0 num
                 1 analisis.id_analisis,
                 2 practicas.codigo_practica, 
                 3 analisis.codigo_interno, 
                 4 analisis.nombre,
                 5 resultados.resultado, 
                 6 analisis.unidad, 
                 7 analisis.valoresreferencia, 
                 8 resultados.id_ordenes, 
                 9 resultados.id_practicas, 
                 10 resultados.observacion, 
                 11 titulos.nombre 
                 */

                if (tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 6) == null) {
                    unidad = "";
                } else {
                    unidad = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 6).toString();
                }
                observacion = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 10).toString();
                new Observacion(null, true).setVisible(true);
                ConexionMySQLLocal cc = new ConexionMySQLLocal();
                Connection cn = cc.Conectar();
                String id_orden = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 8).toString();
                String id_practicas = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 9).toString();
                /// resultado = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 5).toString();
                //observacion = tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 10).toString();
                try {
                    String sSQL3 = "update resultados set observacion=? where id_ordenes=" + id_orden + " and id_practicas=" + id_practicas;
                    PreparedStatement pst = cn.prepareStatement(sSQL3);
                    pst.setString(1, observacion);

                    int n4 = pst.executeUpdate();
                    if (n4 > 0) {
                        tablapracticas.setValueAt(observacion, tablapracticas.getSelectedRow(), 10);
                        txtreferencia.setText(observacion);
                    }
                    cn.close();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
            }
        }
        tablapracticas.setDefaultRenderer(Object.class, rr);
    }//GEN-LAST:event_tablapracticasKeyPressed

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncancelarActionPerformed

    public double Redondear(double numero) {
        return (numero * 100) / 100.0;
    }

    private void txtnbuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnbuActionPerformed
        if (tablapracticas.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "No hay resultados que cargar...");
        } else {
            txtnbu.transferFocus();
            tablapracticas.setRowSelectionInterval(0, 0);
        }
    }//GEN-LAST:event_txtnbuActionPerformed

    private void txtnbuKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnbuKeyReleased
        TableRowSorter sorter = new TableRowSorter(model);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtnbu.getText() + ".*"));
        tablapracticas.setRowSorter(sorter);
    }//GEN-LAST:event_txtnbuKeyReleased

    void cargartablaResultados(String valor) {
//                          0           1           2           3                   4           5           6               7           8           9           10             11           12          13      14          15            16        17                     
        String[] Titulo = {"N°", "id analisis", "Codigo", "Cod. Interno", "Determinación", "Resultado", "Unidad", "Referencia", "id_orden", "ID_practicas", "Observación", "Titulo", "Nomb prac", "estado", "metodo", "tipo informe", "formula", "Imprimir"};
        Object[] Registros = new Object[18];
        tablapracticas.setDefaultRenderer(Object.class, rr);
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        int contador = 1;
        //String sql = "SELECT analisis.id_analisis, practicas.codigo_practica, analisis.codigo_interno, analisis.nombre, resultados.resultado, analisis.unidad, analisis.valoresreferencia, resultados.id_ordenes, resultados.id_practicas, resultados.observacion, titulos.nombre, practicas.determinacion_practica, analisis.estado_titulo, practicas.metodo,resultados.id_pacientes, practicas.tipo_informe, analisis.unidad_extra FROM resultados INNER JOIN practicas ON practicas.id_practicas = resultados.id_practicas INNER JOIN analisis ON analisis.id_analisis = resultados.id_analisis LEFT JOIN titulos ON titulos.id_titulo = analisis.id_titulo INNER JOIN pacientes ON pacientes.id_pacientes = resultados.id_pacientes INNER JOIN personas ON personas.dni = pacientes.personas_dni  INNER JOIN historia_clinica ON historia_clinica.id_ordenes = resultados.id_ordenes INNER JOIN secciones ON secciones.id_secciones = practicas.id_seccion WHERE historia_clinica.idhistoria_clinica=" + id_orden + " ORDER BY historia_clinica.idhistoria_clinica, secciones.prioridad, practicas.prioridad, analisis.prioridad, practicas.determinacion_practica, titulos.nombre";
        String sql = " SELECT analisis.id_analisis, "
                + "practicas.codigo_practica, "
                + "analisis.codigo_interno, "
                + "analisis.nombre, "
                + "resultados.resultado, "
                + "analisis.unidad, \n"
                + "analisis.valoresreferencia, "
                + "resultados.id_ordenes, "
                + "resultados.id_practicas, "
                + "resultados.observacion, "
                + "titulos.nombre, \n"
                + "practicas.determinacion_practica, "
                + "analisis.estado_titulo,"
                + "metodos.Nombre as metodo,"
                + "resultados.id_pacientes, "
                + "practicas.tipo_informe, "
                + "analisis.unidad_extra,"
                + "resultados.estado_imprime,"
                + "secciones.nombre \n"
                + "FROM resultados \n"
                + "INNER JOIN practicas ON practicas.id_practicas = resultados.id_practicas \n"
                + "INNER JOIN analisis ON analisis.id_analisis = resultados.id_analisis \n"
                + "LEFT JOIN titulos ON titulos.id_titulo = analisis.id_titulo \n"
                + "INNER JOIN pacientes ON pacientes.id_pacientes = resultados.id_pacientes \n"
                + "INNER JOIN personas ON personas.dni = pacientes.personas_dni  \n"
                + "INNER JOIN historia_clinica ON historia_clinica.id_ordenes = resultados.id_ordenes \n"
                + "LEFT JOIN secciones ON secciones.id_secciones = practicas.id_seccion \n"
                + "LEFT JOIN metodos ON metodos.idmetodos=analisis.idmetodos\n"
                + "WHERE historia_clinica.idhistoria_clinica=" + id_orden + " and analisis.estado=1\n"
                + "ORDER BY historia_clinica.idhistoria_clinica, secciones.prioridad, practicas.prioridad, analisis.prioridad, \n"
                + "practicas.determinacion_practica, titulos.nombre";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            @Override
            public Class<?> getColumnClass(int i) {
                if (i == 17) {
                    return java.lang.Boolean.class;
                }
                return super.getColumnClass(i);
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 17) {
                    return true;
                } else {
                    return false;
                }
            }
        };
        int aux[] = new int[200];
        int i = 0, bandera = 0;
        boolean imprime;
        String titulo = "";
        try {
            Statement St = cn.createStatement();
            ResultSet rs = St.executeQuery(sql);
            while (rs.next()) {
                if (rs.getInt(13) == 1) {
                    titulo = rs.getString(11);
                } else {
                    titulo = "";
                }
                idpaciente = rs.getInt(15);
                if (rs.getInt(18) == 1) {
                    //   System.out.println("imprime=1");
                    imprime = true;
                } else {
                    imprime = false;
                }
//"N°", "id analisis", "Codigo", "Cod. Interno", "Determinación", "Resultado", "Unidad", "Referencia", "id?orden", "ID_practicas", "Observación", "Titulo", "Nomb prac", "estado", "metodo", "tipo informe", "formula", "Imprimir"
                Registros[0] = contador; //
                Registros[1] = rs.getString(1);
                Registros[2] = rs.getString(2);
                Registros[3] = rs.getString(3);
                Registros[4] = rs.getString(4);
                Registros[5] = rs.getString(5);
                Registros[6] = rs.getString(6);
                Registros[7] = rs.getString(7);
                Registros[8] = rs.getString(8);
                Registros[9] = rs.getString(9);
                Registros[10] = rs.getString(10);
                Registros[11] = titulo;
                Registros[12] = rs.getString(12);
                Registros[13] = rs.getString(13);
                Registros[14] = rs.getString(14);
                Registros[15] = rs.getString(16);
                Registros[16] = rs.getString(17);
                Registros[17] = imprime;
                model.addRow(Registros);
                if (rs.getString(3).equals("")) {
                    bandera = 1;
                    aux[i] = contador;
                    i++;
                }
                contador++;
            }
            tablapracticas.setModel(model);
            tablapracticas.setAutoCreateRowSorter(true);
            /////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(1).setMaxWidth(0);
            tablapracticas.getColumnModel().getColumn(1).setMinWidth(0);
            tablapracticas.getColumnModel().getColumn(1).setPreferredWidth(0);
            //////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(4).setMaxWidth(0);
            tablapracticas.getColumnModel().getColumn(4).setMinWidth(0);
            tablapracticas.getColumnModel().getColumn(4).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(8).setMaxWidth(0);
            tablapracticas.getColumnModel().getColumn(8).setMinWidth(0);
            tablapracticas.getColumnModel().getColumn(8).setPreferredWidth(0);
            //////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(9).setMaxWidth(0);
            tablapracticas.getColumnModel().getColumn(9).setMinWidth(0);
            tablapracticas.getColumnModel().getColumn(9).setPreferredWidth(0);
            ///////////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(10).setMaxWidth(0);
            tablapracticas.getColumnModel().getColumn(10).setMinWidth(0);
            tablapracticas.getColumnModel().getColumn(10).setPreferredWidth(0);
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(11).setMaxWidth(0);
            tablapracticas.getColumnModel().getColumn(11).setMinWidth(0);
            tablapracticas.getColumnModel().getColumn(11).setPreferredWidth(0);
            ///////////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(12).setMaxWidth(0);
            tablapracticas.getColumnModel().getColumn(12).setMinWidth(0);
            tablapracticas.getColumnModel().getColumn(12).setPreferredWidth(0);
            ///////////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(13).setMaxWidth(0);
            tablapracticas.getColumnModel().getColumn(13).setMinWidth(0);
            tablapracticas.getColumnModel().getColumn(13).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(14).setMaxWidth(0);
            tablapracticas.getColumnModel().getColumn(14).setMinWidth(0);
            tablapracticas.getColumnModel().getColumn(14).setPreferredWidth(0);
            ///////////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(15).setMaxWidth(0);
            tablapracticas.getColumnModel().getColumn(15).setMinWidth(0);
            tablapracticas.getColumnModel().getColumn(15).setPreferredWidth(0);
            ///////////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(16).setMaxWidth(0);
            tablapracticas.getColumnModel().getColumn(16).setMinWidth(0);
            tablapracticas.getColumnModel().getColumn(16).setPreferredWidth(0);
            //////////////////////////////////////////////////////////////////
            tablapracticas.getColumnModel().getColumn(0).setPreferredWidth(1);
            tablapracticas.getColumnModel().getColumn(2).setPreferredWidth(15);
            tablapracticas.getColumnModel().getColumn(3).setPreferredWidth(15);
            tablapracticas.getColumnModel().getColumn(5).setPreferredWidth(150);
            tablapracticas.getColumnModel().getColumn(6).setPreferredWidth(10);
            tablapracticas.getColumnModel().getColumn(7).setPreferredWidth(100);
            tablapracticas.getColumnModel().getColumn(17).setPreferredWidth(15);
            ///////////////////////////////////////////////////////////////////////////////
            if (bandera == 1) {
                int j = 0;
                while (j < i) {
                    tablapracticas.setRowHeight(aux[j] - 1, 1);
                    j++;
                }
            }
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    void cargardatoslab() {
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        String sql = "SELECT mail_direccion, mail_contraseña FROM laboratorios WHERE id_laboratorios=1";
        try {
            Statement St = cn.createStatement();
            ResultSet rs = St.executeQuery(sql);
            rs.next();
            Username = rs.getString("mail_direccion");
            PassWord = rs.getString("mail_contraseña");
            Mensage = "Informe de Entrega de Laboratorio de Análisis Clínicos";
            Subject = "Informe de Entrega de Laboratorio de Análisis Clínicos";
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);

        }
    }

    void cargarfecha() {

        //SimpleDateFormat formatoTiempo = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat formatoTiempo = new SimpleDateFormat("HH:mm");
        java.util.Date currentDate1 = new java.util.Date();
        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(currentDate1);
        horainforme = formatoTiempo.format(currentDate1);
        /////////////////////////////////////
        SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date currentDate = new java.util.Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fechainforme = formato.format(currentDate);
        fechaEntrega = formato2.format(currentDate);
    }

    void cursor() {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        /* this.btnCursor02.setCursor(new Cursor(Cursor.HAND_CURSOR));
         this.btnCursor03.setCursor(new Cursor(Cursor.MOVE_CURSOR));
         this.btnCursor04.setCursor(new Cursor(Cursor.TEXT_CURSOR));*/

        //this.add(this.btnimprimir1);
        /* this.add(this.btnCursor02);
         this.add(this.btnCursor03);
         this.add(this.btnCursor04);*/
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.pack();
    }

    void cursor2() {
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        /* this.btnCursor02.setCursor(new Cursor(Cursor.HAND_CURSOR));
         this.btnCursor03.setCursor(new Cursor(Cursor.MOVE_CURSOR));
         this.btnCursor04.setCursor(new Cursor(Cursor.TEXT_CURSOR));*/

        //this.add(this.btnimprimir1);
        /* this.add(this.btnCursor02);
         this.add(this.btnCursor03);
         this.add(this.btnCursor04);*/
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.pack();
    }

    void generar_informe() {
        cursor();
        /////////////////////IMPRIMIR O NO //////////////////////
        int n2 = tablapracticas.getRowCount();
        int j1 = 0;
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();

        try {
            String id_orden = tablapracticas.getValueAt(0, 8).toString();
            String sSQL3 = "update resultados set estado_imprime=1 where id_ordenes=" + id_orden;
            PreparedStatement pst = cn.prepareStatement(sSQL3);
            pst.executeUpdate();

        } catch (Exception ex) {
            cursor2();
            JOptionPane.showMessageDialog(null, ex);
        }

        while (n2 > j1) {//recorro para guardar estado de impresion
            String id_orden = tablapracticas.getValueAt(j1, 8).toString();
            String id_analisis = tablapracticas.getValueAt(j1, 1).toString();

            try {
                if (tablapracticas.getValueAt(j1, 17).toString().equals("false")) {
                    String sSQL3 = "update resultados set estado_imprime=0 where id_ordenes=" + id_orden + " and id_analisis=" + id_analisis;
                    PreparedStatement pst = cn.prepareStatement(sSQL3);
                    pst.executeUpdate();
                }
//                else {
//                    String sSQL3 = "update resultados set estado_imprime=1 where id_ordenes=" + id_orden + " and id_analisis=" + id_analisis;
//                    PreparedStatement pst = cn.prepareStatement(sSQL3);
//                    pst.executeUpdate();
//                }
            } catch (Exception ex) {
                cursor2();
                JOptionPane.showMessageDialog(null, ex);
            }
            j1++;
        }

        String observacion_practica[] = new String[100];
        /////////////////////////////////////////////////// Variables /////////////////////////////////

        String titulos = "", analisis = "", resultados = "", unidades = "", referencias = "", resultadoProteina = "", valorrefproteina = "";

        camposinformes tipo1;
        //////////////////////////////////////// Armado de resultados//////////////////////////////////////////////
        int n = tablapracticas.getRowCount(), tipo_informe = 0;
        if (n != 0) {
            int i = 0;
            while (i < n) {
                /*                    
        0        "N°",
        1        "id analisis",
        2        "Codigo",
        3        "Cod. Interno",
        4        "Determinación",
        5        "Resultado",
        6        "Unidad",
        7        "Referencia",
        8        "id_orden",
        9        "ID_practicas",
        10        "Observación",
        11        "Titulo",
        12        "Nomb prac",
        13        "estado",
        14        "metodo",
        15        "tipo informe",
        16        "formula",
        17        "Imprimir"
                 */
                tipo_informe = Integer.valueOf(tablapracticas.getValueAt(i, 15).toString());

                i++;
                observaciones = null;
                metodo = null;

            }

            //////////////////////////////////////////////Reportes segun el envio ////////////////////////
            try {
                //////////////////////////////////////////////////////////////////
                String sql7 = "Select formato, logo, nombre, direccion, telefono, mail, observacion, logo_horizontal1,observacion2,hora_vertical3 as margen from reportes";
                Statement St7 = cn.createStatement();
                ResultSet rs7 = St7.executeQuery(sql7);

                rs7.next();
                int margen = rs7.getInt("margen");
                ///////////////////////////////////////////////////////////////////
                if (rs7.getString(1).equals("A4")) {
                    System.out.println("a4");
                    String observacion2 = rs7.getString("observacion2");
                    ////////////////////////////////////////////////////////////
                    String medico = "", especialidad = "", motivo = "", lugar = "", matricula = "", telefono = "";
                    int caratula;

                    String sql2 = "SELECT distinct(historia_clinica.idhistoria_clinica),personas.apellido, personas.nombre, ordenes.tipo_orden, medicos.apellido, medicos.nombre, especialidades.nombre_esp,nombre_recien_nacido,if((historia_clinica.descripcion=\"VIH.\" or  historia_clinica.descripcion=\"S/D.\"),\"-\",personas.dni) as dni FROM ordenes INNER JOIN pacientes ON pacientes.id_Pacientes = ordenes.id_Pacientes INNER JOIN personas ON personas.dni = pacientes.personas_dni INNER JOIN medicos ON medicos.id_medicos = ordenes.id_medicos INNER JOIN especialidades ON especialidades.id_especialidades = ordenes.id_especialidades INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes  WHERE ordenes.estado_orden=0 and historia_clinica.idhistoria_clinica=" + id_orden;
                    Statement St = cn.createStatement();
                    ResultSet rs = St.executeQuery(sql2);
                    rs.next();
                    protocolo = String.valueOf(id_orden);
                    if (rs.getString("nombre_recien_nacido").equals("")) {
                        paciente = rs.getString(2) + " " + rs.getString(3);
                    } else {
                        paciente = rs.getString("nombre_recien_nacido");
                    }
                    medico = rs.getString(5) + " " + rs.getString(6);
                    especialidad = rs.getString(7);
                    motivo = rs.getString(4);
                    caratula = rs7.getInt(8);
                    telefono = rs7.getString("telefono");
                    lugar = tablapracticas.getValueAt(0, 8).toString();
                    dni = rs.getString("dni");
                    ////////////////////////////////////////////
                    String sql3 = "SELECT lugar_lab, nombre_lab FROM licencia";
                    Statement St2 = cn.createStatement();
                    ResultSet rs2 = St2.executeQuery(sql3);
                    rs2.next();
                    lugar = rs2.getString(1);
                    matricula = rs2.getString(2);
                    //////////////////////////////////
                    byte[] img = rs7.getBytes(2);
                    if (img != null) {
                        try {
                            imagen2 = ConvertirImagen(img);
                        } catch (IOException ex) {
                            JOptionPane.showMessageDialog(null, ex);
                        }
                        Icon icon = new ImageIcon(imagen2);
                    } else {
                        imagen2 = null;
                    }
                    JasperPrint jPrintCaratula = null;
                    //////////////////////////////////////////
                    System.out.println("defino Parametros");
                    Map parametros2 = new HashMap();
                    parametros2.put("protocolo", txtprotocolo.getText());
                    parametros2.put("logo", imagen2);
                    parametros2.put("laboratorio", rs7.getString(3));
                    parametros2.put("paciente", paciente);
                    parametros2.put("medico", medico);
                    parametros2.put("especialidad", especialidad);
                    parametros2.put("motivo", motivo);
                    parametros2.put("lugar", rs7.getString(4) + " " + lugar);
                    parametros2.put("telefono", telefono);
                    //fechaPaciente = revertirfecha(fechaPaciente);
                    parametros2.put("fecha", fechaPaciente);
                    parametros2.put("hora", horainforme);
                    parametros2.put("dni", dni);
                    parametros2.put("observacion", rs7.getString(7));
                    parametros2.put("observacion2", observacion2);
                    //System.out.println("id_historia_clinica " + id_historia_clinica);
                    parametros2.put("id_historia_clinica", id_historia_clinica);
                    System.out.println("Parametros definidos");
                    if (caratula == 1) {
                        JasperReport CARATULA = null;
                        System.out.println("arma caratula");
                        if (matricula.equals("80")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1GER.jasper"));
                        }
                        if (matricula.equals("1165")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                        }
                        if (matricula.equals("424")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                        }
                        if (matricula.equals("1349")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                        }
                        if (matricula.equals("1290")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1DANT.jasper"));
                        }
                        if (matricula.equals("1340")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                        }
                        if (matricula.equals("1376")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1PATA.jasper"));
                        }
                        if (matricula.equals("127")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                        }
                        if (matricula.equals("20009")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1YANE.jasper"));
                        }

                        if (matricula.equals("1066") || matricula.equals("312")) {
                            if (enviar == 3) {
                                System.out.println("caratula 1");
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1_A4_ABDL.jasper"));
                            }
                            if (enviar == 1) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1_A4_ABDL_mail.jasper"));
                                System.out.println("caratula 2");
                            }
                        }
                        if (matricula.equals("1202")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                        }
                        if (matricula.equals("1114")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1GER.jasper"));
                        }
                        if (matricula.equals("99999")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                        }
                        if (matricula.equals("1088")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                        }
                        if (matricula.equals("1278")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                        }
                        if (matricula.equals("482")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1YANE.jasper"));
                        }
                        if (matricula.equals("1073")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                        }
                        if (matricula.equals("20220")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                        }
                        if (matricula.equals("1515")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1_METTOLA.jasper"));
                        }
                        if (matricula.equals("1366")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                        }
                        if (matricula.equals("283")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1SAND.jasper"));
                        }
                        if (matricula.equals("941")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1SAND.jasper"));
                        }
                        if (matricula.equals("986")) {
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1BOHOR.jasper"));
                        }

                        jPrintCaratula = JasperFillManager.fillReport(CARATULA, parametros2, cn);
                    }
                    System.out.println("Arma A4");
                    JasperReport A4 = null;
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Reporte", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
                    viewer.setSize(1024, 768);
                    viewer.setLocationRelativeTo(null);

                    System.out.println("a4");

                    if (enviar == 3) {
                        System.out.println("enviar 3");
                        A4 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/PaginaA4.jasper"));
                    }
                    if (enviar == 1) {
                        A4 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/PaginaA4_digital.jasper"));
                        System.out.println("enviar 1");
                    }

                    JasperPrint jPrintProtocolos = JasperFillManager.fillReport(A4, parametros2, cn);
                    ///JasperViewer.viewReport(jPrintFilas, true);
                    JasperViewer jv = null;
                    /* viewer.getContentPane().add(jv.getContentPane());
                    viewer.setVisible(true);*/

                    JasperPrint analisis_completo = null;

                    int b_analisis = 0, b_caratula = 0, b_filas = 0;
                    List pages = null;
                    ///////////////caratula no null

                    if (jPrintCaratula != null) {
                        analisis_completo = jPrintCaratula;
                        b_analisis = 1;
                        b_caratula = 1;
                        System.out.println("agrega caratula");
                    }

                    if (jPrintProtocolos != null && b_analisis == 0) {
                        analisis_completo = jPrintProtocolos;
                        b_analisis = 1;
                        b_filas = 1;
                    }

                    if (jPrintCaratula != null && b_caratula == 0) {
                        pages = jPrintCaratula.getPages();
                        for (int indice = 0; indice < pages.size(); indice++) {
                            JRPrintPage object = (JRPrintPage) pages.get(indice);
                            analisis_completo.addPage(object);
                        }
                    }

                    if (jPrintProtocolos != null && b_filas == 0) {
                        pages = jPrintProtocolos.getPages();
                        for (int indice = 0; indice < pages.size(); indice++) {
                            JRPrintPage object = (JRPrintPage) pages.get(indice);
                            analisis_completo.addPage(object);
                        }
                        
                    }

                    if (enviar == 3) {
                        JasperExportManager.exportReportToPdfFile(analisis_completo, "C:\\Informes_Pacientes\\Impresos\\" + txtprotocolo.getText() + ".-" + paciente + ".pdf");
                        jv = new JasperViewer(analisis_completo);
                        viewer.getContentPane().add(jv.getContentPane());
                        viewer.setVisible(true);
                        HistoriaClinica nuevo = new HistoriaClinica();
                        nuevo.ActualizaEstadoHistoriaClinica(id_historia_clinica, "Impreso", estado_entrega, fechaEntrega);
                    }
                    if (enviar == 1) {

                        JasperExportManager.exportReportToPdfFile(analisis_completo, "C:\\Informes_Pacientes\\Enviados\\" + txtprotocolo.getText() + "-" + paciente + ".pdf");
                        jv = new JasperViewer(analisis_completo);
                        viewer.getContentPane().add(jv.getContentPane());
                        viewer.setVisible(true);
                        //////////////enviar - Mail////////////////////////////////////

                        new Aviso(null, true).setVisible(true);
                        if (Aviso.opcion == 1) {
                            new Mensaje_Mail(null, true).setVisible(true);
                            Properties props = new Properties();
                            props.put("mail.debug", "true");
                            props.put("mail.smtp.auth", "true");
                            props.put("mail.smtp.starttls.enable", "true");
                            System.out.println("pasa props");

                            String mail2 = Username.substring(Username.indexOf("@"), Username.indexOf(".com"));
                            System.out.println("mail2----------- " + mail2);
                            if (mail2.equals("@hotmail")) {
                                props.put("mail.smtp.host", "smtp.live.com");
                                props.put("mail.smtp.port", "587");
                                System.out.println("smtp 587");
                            }
                            if (mail2.equals("@gmail")) {
//                                props.put("mail.smtp.host", "mail.gmail.com");
//                                props.put("mail.smtp.port", "587");
//                                props.put("mail.smtp.starttls.enable", "true");
//                                props.put("mail.debug", "true");

                                props.put("mail.smtp.auth", "true");
                                props.put("mail.smtp.starttls.enable", "true");
                                props.put("mail.smtp.host", "smtp.gmail.com");
                                props.put("mail.smtp.port", "587");

                                System.out.println("smtp 587");
                            }
                            if (mail2.equals("@yahoo")) {
                                props.put("mail.smtp.host", "smtp.mail.yahoo.com");
                                props.put("mail.smtp.port", "587");
                                System.out.println("smtp 587");
                            }

                            if (mail2.equals("@laboratorionorte")) {
                                System.out.println("ingresa a  laboratorionorte - smtp 587");
                                props.put("mail.smtp.host", "mail.laboratorionorte.com.ar");
                                props.put("mail.smtp.port", "587");
                                props.put("mail.smtp.starttls.enable", "true");
                                props.put("mail.debug", "true");
                                System.out.println("smtp 587");
                            }

                            /* props.put("mail.smtp.host", "mail.cobituc.org.ar");
                        props.put("mail.smtp.port", "587");*/
                            To = mail;
                            System.out.println("Username " + Username);
                            System.out.println("PassWord " + PassWord);
                            System.out.println("To " + To);
                            // Hotmail o Gmail o yahoo 
                            ///props.put("mail.smtp.port", "587");
                            Session session = Session.getInstance(props,
                                    //                            Session session = Session.getDefaultInstance(props,
                                    new javax.mail.Authenticator() {
                                protected PasswordAuthentication getPasswordAuthentication() {
                                    return new PasswordAuthentication(Username, PassWord);
                                }
                            });

                            try {
                                BodyPart texto = new MimeBodyPart();
                                texto.setText(mensaje);
                                // Se compone el adjunto con la imagen.,
                                MimeMultipart multiParte = new MimeMultipart();
                                multiParte.addBodyPart(texto);

                                BodyPart adjunto = new MimeBodyPart();
                                adjunto.setDataHandler(
                                        new DataHandler(new FileDataSource("C:\\Informes_Pacientes\\Enviados\\" + txtprotocolo.getText() + "-" + paciente + ".pdf")));
                                adjunto.setFileName(protocolo + "- " + paciente + ".pdf");
                                multiParte.addBodyPart(adjunto);

                                Message message = new MimeMessage(session);
                                message.setFrom(new InternetAddress(Username));
                                message.setRecipients(Message.RecipientType.TO,
                                        InternetAddress.parse(To));
                                message.setSubject(asunto);
                                message.setText(Mensage);
                                message.setContent(multiParte);
                                // Se envia el correo.
                                Transport.send(message);
                                JOptionPane.showMessageDialog(this, "Su mensaje ha sido enviado");
                                //////////////////////////////////////////////////////////////////
                                HistoriaClinica nuevo = new HistoriaClinica();
                                nuevo.ActualizaEstadoHistoriaClinica(id_historia_clinica, "Enviado", estado_entrega, fechaEntrega);
                            } catch (MessagingException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }

                }
                if (rs7.getString(1).equals("A5")) {
                    String observacion2 = rs7.getString("observacion2");
                    System.out.println("A5");
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    String medico = "", especialidad = "", motivo = "", lugar = "", matricula = "", hora = "";
                    int caratula;
                    String sql2 = "SELECT distinct(historia_clinica.idhistoria_clinica),personas.apellido, personas.nombre, ordenes.tipo_orden, medicos.apellido, medicos.nombre, especialidades.nombre_esp,if((historia_clinica.descripcion=\"VIH.\" or  historia_clinica.descripcion=\"S/D.\"),\"-\",personas.dni) as dni,nombre_recien_nacido FROM ordenes INNER JOIN pacientes ON pacientes.id_Pacientes = ordenes.id_Pacientes INNER JOIN personas ON personas.dni = pacientes.personas_dni INNER JOIN medicos ON medicos.id_medicos = ordenes.id_medicos INNER JOIN especialidades ON especialidades.id_especialidades = ordenes.id_especialidades INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes  WHERE ordenes.estado_orden=0 and historia_clinica.idhistoria_clinica=" + id_orden;
                    Statement St = cn.createStatement();
                    ResultSet rs = St.executeQuery(sql2);
                    rs.next();
                    protocolo = String.valueOf(id_orden);
                    if (rs.getString("nombre_recien_nacido").equals("")) {
                        paciente = rs.getString(2) + " " + rs.getString(3);
                    } else {
                        paciente = rs.getString("nombre_recien_nacido");
                    }
                    medico = rs.getString(5) + " " + rs.getString(6);
                    especialidad = rs.getString(7);
                    motivo = rs.getString(4);
                    lugar = tablapracticas.getValueAt(0, 8).toString();
                    caratula = rs7.getInt(8);
                    dni = rs.getString("dni");
                    ////////////////////////////////////////////
                    String sql3 = "SELECT lugar_lab, nombre_lab FROM licencia";
                    Statement St2 = cn.createStatement();
                    ResultSet rs2 = St2.executeQuery(sql3);
                    rs2.next();
                    lugar = rs2.getString(1);
                    matricula = rs2.getString(2);
                    //////////////////////////////////
                    byte[] img = rs7.getBytes(2);
                    if (img != null) {
                        try {
                            imagen2 = ConvertirImagen(img);
                        } catch (IOException ex) {
                            JOptionPane.showMessageDialog(null, ex);
                        }
                        Icon icon = new ImageIcon(imagen2);
                    } else {
                        imagen2 = null;
                    }
                    /////////////////////// Defino la previsualizacion ///////////////////
                    JDialog viewer = new JDialog(new javax.swing.JFrame(), "Reporte", true);
                    viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
                    viewer.setSize(800, 600);
                    viewer.setLocationRelativeTo(null);
                    System.out.println("previsualizacion definida");
                    /////////////////// Definimos las variables
                    JasperPrint jPrintCaratula = null;
                    JasperPrint jPrintFilas1 = null;
                    JasperPrint jPrintFilas2 = null;
                    JasperPrint jPrintOrina = null;
                    JasperPrint jPrintColumnas = null;
                    JasperPrint jPrintProteinograma = null;
                    JasperPrint jPrintBacteriologico = null;
                    JasperPrint jPrintBacteriologico2 = null;
                    JasperPrint analisis_completo = null;
                    JasperViewer jv = null;
                    JasperViewer jv_1 = null;
                    System.out.println("defino Parametros");
                    /////////////////////////Defino los Parametros///////////////////////////////////
                    Map parametros = new HashMap();
                    parametros.put("protocolo", txtprotocolo.getText());
                    parametros.put("logo", imagen2);
                    parametros.put("laboratorio", rs7.getString(3));
                    parametros.put("paciente", paciente);
                    parametros.put("medico", medico);
                    parametros.put("especialidad", especialidad);
                    parametros.put("motivo", motivo);
                    parametros.put("lugar", lugar);
                    parametros.put("dni", dni);
                    //fechaPaciente = fechaPaciente);
                    //System.out.println("fechaPaciente:" + fechaPaciente);
                    parametros.put("fecha", fechaPaciente);
                    parametros.put("fecha_impresion", fechainforme);
                    parametros.put("hora", "");
                    parametros.put("direccion", rs7.getString(4));
                    parametros.put("telefono", rs7.getString(5));
                    parametros.put("mail", rs7.getString(6));
                    parametros.put("observacion", rs7.getString(7));
                    parametros.put("observacion2", observacion2);
                    parametros.put("id_historia_clinica", id_historia_clinica);

                    if (margen == 1) {
                        parametros.put("margen", "1");
                    }
//                    else {
//                    parametros.put("margen"," ");
//                    }

                    System.out.println("parametros definidos");
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ////////////////////////CARATULA//////////////////////////////////////////////////////////////////////////////  
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if (caratula == 1) {
                        JasperReport CARATULA = null;
                        System.out.println("arma caratula");
                        ////////////////////////////////////////////////////////////////
                        if (enviar == 1) {//mail
                            System.out.println("caratula mail A4");
                            CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1_A4_ABDL_mail.jasper"));
                        } else {
                            if (matricula.equals("80")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1GER.jasper"));
                            }
                            if (matricula.equals("1165")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("908")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("424")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("1349")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("1290")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1DANT.jasper"));
                            }
                            if (matricula.equals("1340")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("1376")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1PATA.jasper"));
                            }
                            if (matricula.equals("127")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("20009")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1YANE.jasper"));
                            }
                            if (matricula.equals("1066")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("1202")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("1114")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1GER.jasper"));
                            }
                            if (matricula.equals("99999")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("1088")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("1278")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("482")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1YANE.jasper"));
                            }
                            if (matricula.equals("1073")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("20220")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("1515")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1_METTOLA.jasper"));
                            }
                            if (matricula.equals("1366")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                            }
                            if (matricula.equals("283")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1SAND.jasper"));
                            }
                            if (matricula.equals("941")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1SAND.jasper"));
                            }
                            if (matricula.equals("986")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1BOHOR.jasper"));
                            }
                            if (matricula.equals("1578")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1Boero.jasper"));
                            }
                            if (matricula.equals("312")) {
                                CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1_312.jasper"));
                            }
                        }
                        jPrintCaratula = JasperFillManager.fillReport(CARATULA, parametros, new JREmptyDataSource());

                    }
                    System.out.println("Arma A5");
                    JasperReport A5 = null;

                    if (enviar == 3) {
                        System.out.println("enviar 3");
                        A5 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/PaginaA5.jasper"));
                    }
                    if (enviar == 1) {
                        A5 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/PaginaA4_digital.jasper"));
                        System.out.println("enviar 1");
                    }

                    JasperPrint jPrintProtocolos = JasperFillManager.fillReport(A5, parametros, cn);
                    ///JasperViewer.viewReport(jPrintFilas, true);
                    jv = null;
                    /* viewer.getContentPane().add(jv.getContentPane());
                    viewer.setVisible(true);*/

                    analisis_completo = null;

                    int b_analisis = 0, b_caratula = 0, b_filas = 0;
                    List pages = null;
                    ///////////////caratula no null

                    if (jPrintCaratula != null) {
                        analisis_completo = jPrintCaratula;
                        b_analisis = 1;
                        b_caratula = 1;
                        System.out.println("agrega caratula");
                    }

                    if (jPrintProtocolos != null && b_analisis == 0) {
                        analisis_completo = jPrintProtocolos;
                        b_analisis = 1;
                        b_filas = 1;
                        System.out.println("agrega Protocolos");
                    }

                    if (jPrintCaratula != null && b_caratula == 0) {
                        pages = jPrintCaratula.getPages();
                        for (int indice = 0; indice < pages.size(); indice++) {
                            JRPrintPage object = (JRPrintPage) pages.get(indice);
                            analisis_completo.addPage(object);
                        }
                        System.out.println("agrega caratula");
                    }

                    if (jPrintProtocolos != null && b_filas == 0) {
                        pages = jPrintProtocolos.getPages();
                        for (int indice = 0; indice < pages.size(); indice++) {
                            JRPrintPage object = (JRPrintPage) pages.get(indice);
                            analisis_completo.addPage(object);                            
                        }
                        System.out.println("agrega caratula + protocolos");
                    }

                    if (enviar == 3) {
                        JasperExportManager.exportReportToPdfFile(analisis_completo, "C:\\Informes_Pacientes\\Impresos\\" + txtprotocolo.getText() + ".-" + paciente + ".pdf");
                        jv = new JasperViewer(analisis_completo);
                        viewer.getContentPane().add(jv.getContentPane());
                        viewer.setVisible(true);
                        HistoriaClinica nuevo = new HistoriaClinica();
                        nuevo.ActualizaEstadoHistoriaClinica(id_historia_clinica, "Impreso", estado_entrega, fechaEntrega);
                    }
                    if (enviar == 1) {
                        new Mensaje_Mail(null, true).setVisible(true);
//                        JasperReport A4 = null;
//                        A4 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/PaginaA4_digital.jasper"));
//
//                        analisis_completo = JasperFillManager.fillReport(A4, parametros, cn);

                        jv = new JasperViewer(analisis_completo);
                        viewer.getContentPane().add(jv.getContentPane());
                        viewer.setVisible(true);
                        JasperExportManager.exportReportToPdfFile(analisis_completo, "C:\\Informes_Pacientes\\Enviados\\" + txtprotocolo.getText() + "-" + paciente + ".pdf");
                        new Aviso(null, true).setVisible(true);
                        if (Aviso.opcion == 1) {

                            //////////////enviar - Mail////////////////////////////////////
                            Properties props = new Properties();
                            props.put("mail.debug", "true");
                            props.put("mail.smtp.auth", "true");
                            props.put("mail.smtp.starttls.enable", "true");

                            String mail2 = Username.substring(Username.indexOf("@"), Username.indexOf(".com"));

                            if (mail2.equals("@hotmail")) {
                                props.put("mail.smtp.host", "smtp.live.com");
                                props.put("mail.smtp.port", "587");
                            }

                            if (mail2.equals("@gmail")) {
                                System.out.println("smtp 465--------");
                                // props.put("mail.smtp.host", "mail.gmail.com");
//                                props.put("mail.smtp.host", "mail.gmail.com");
//                                props.put("mail.smtp.port", "587");
//                                props.put("mail.smtp.starttls.enable", "true");
//                                props.put("mail.debug", "true");
                                props.put("mail.smtp.host", "smtp.gmail.com");
                                props.put("mail.smtp.socketFactory.port", "465");
                                props.put("mail.smtp.socketFactory.class",
                                        "javax.net.ssl.SSLSocketFactory");
                                props.put("mail.smtp.auth", "true");
                                props.put("mail.smtp.port", "465");
                                System.out.println("smtp 465--------");

                            }

//                            if (mail2.equals("@gmail")) {
//                                props.put("mail.smtp.host", "smtp.gmail.com");
//                                props.put("mail.smtp.port", "587");
//                            }
                            if (mail2.equals("@yahoo")) {
                                props.put("mail.smtp.host", "smtp.mail.yahoo.com");
                                props.put("mail.smtp.port", "587");
                            }
                            ///////LABORATORIO NORTE
                            if (mail2.equals("@laboratorionorte")) {
                                System.out.println("ingresa a  laboratorionorte - smtp 587");
                                props.put("mail.smtp.host", "mail.laboratorionorte.com.ar");
                                props.put("mail.smtp.port", "587");
                                props.put("mail.smtp.starttls.enable", "true");
                                props.put("mail.debug", "true");
                                System.out.println("smtp 587");
                            }
                            To = mail;
                            // Hotmail o Gmail o yahoo 
                            ///props.put("mail.smtp.port", "587");
                            Session session = Session.getInstance(props,
                                    new javax.mail.Authenticator() {
                                protected PasswordAuthentication getPasswordAuthentication() {
                                    return new PasswordAuthentication(Username, PassWord);
                                }
                            });
                            try {
                                MimeBodyPart texto = new MimeBodyPart();
                                texto.setText(mensaje);
                                // Se compone el adjunto con la imagen.,
                                MimeMultipart multiParte = new MimeMultipart();
                                multiParte.addBodyPart(texto);

                                MimeBodyPart adjunto = new MimeBodyPart();
                                adjunto.setDataHandler(
                                        new DataHandler(new FileDataSource("C:\\Informes_Pacientes\\Enviados\\" + txtprotocolo.getText() + "-" + paciente + ".pdf")));
                                adjunto.setFileName(protocolo + "- " + paciente + ".pdf");
                                multiParte.addBodyPart(adjunto);

                                Message message = new MimeMessage(session);
                                message.setFrom(new InternetAddress(Username));
                                message.setRecipients(Message.RecipientType.TO,
                                        InternetAddress.parse(To));
                                message.setSubject(asunto);
                                message.setText(Mensage);
                                message.setContent(multiParte);
                                // Se envia el correo.
                                Transport.send(message);
                                JOptionPane.showMessageDialog(this, "Su mensaje ha sido enviado");
                                HistoriaClinica nuevo = new HistoriaClinica();
                                nuevo.ActualizaEstadoHistoriaClinica(id_historia_clinica, "Enviado", estado_entrega, fechaEntrega);
                            } catch (MessagingException e) {
                                throw new RuntimeException(e);
                            }
                        }

                    }

                }
                this.dispose();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
                JOptionPane.showMessageDialog(null, "No se cargo ningun resultado...");
                this.dispose();
            }

            try {
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(Resultados.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void btnimprimir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimir1ActionPerformed
        if (bandera_informe == 0) {
            /// new Envio_mail(null, true).setVisible(true);
            Resultados.enviar = 3;
        }
        EstadoEntrega();
        generar_informe();
        // }JOptionPane.showMessageDialog(null, "Los analisis no tienen resultado ...")
        ////////////////////////////////////////

    }//GEN-LAST:event_btnimprimir1ActionPerformed

    String completarpuntosizquierda(String v, int d
    ) {
        if (v.length() < d) {
            String ceros = "";
            for (int i = v.length(); i < d; i++) {
                ceros += ".";
            }
            v = ceros + v;
        }
        return v;
    }

    String completarpuntosderecha(String v, int d
    ) {
        if (v.length() < d) {
            String ceros = "";
            for (int i = v.length(); i < d; i++) {
                ceros += ".";
            }
            v = v + ceros;
        }
        return v;
    }

    String saltodelinea(String v
    ) {
        String valor = "";

        for (int j = 0; j < v.length(); j++) {
            if (!String.valueOf(v.charAt(j)).equals("$")) {
                valor = valor + String.valueOf(v.charAt(j));
            } else {
                valor = valor + "\r\n";
            }
        }
        return valor;
    }

    private Image ConvertirImagen(byte[] bytes) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Iterator readers = ImageIO.getImageReadersByFormatName("jpeg");
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis; // File or InputStream
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
        return reader.read(0, param);
    }

    public String revertirfecha(String entrada) {
        if ((null == entrada) || (entrada.length() <= 1)) {
            return entrada;
        }

        String salida = "";
        System.out.println("entrada fecha " + entrada);
        //salida = entrada.substring(0, 2) + "/" + entrada.substring(3, 5) + "/" + entrada.substring(6,10);
        salida = entrada.substring(8, 10) + "/" + entrada.substring(5, 7) + "/" + entrada.substring(0, 4);
        System.out.println("salida fecha " + salida);
        //int i = 0;
        ////Dia////
        /*for (i = 8; i <= 9; i++) {
            salida = salida + entrada.charAt(i);
        }
        salida = salida + "/";
        ///Mes///
        for (i = 5; i <= 6; i++) {
            salida = salida + entrada.charAt(i);
        }
        /////Año/////
        salida = salida + "/";
        for (i = 0; i <= 3; i++) {
            salida = salida + entrada.charAt(i);
        }*/

        return salida;

    }
    private void tablapracticasKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablapracticasKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            if (tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 7) == null) {
                txtreferencia.setText(" Valores de Referencia: ");
            } else {
                txtreferencia.setText(" Valores de Referencia: " + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 7).toString());
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            if (tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 7) == null) {
                txtreferencia.setText(" Valores de Referencia: ");
            } else {
                txtreferencia.setText(" Valores de Referencia: " + tablapracticas.getValueAt(tablapracticas.getSelectedRow(), 7).toString());
            }
        }        // TODO add your handling code here:
    }//GEN-LAST:event_tablapracticasKeyReleased

    private void btnimprimir2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnimprimir2ActionPerformed
        if (bandera_informe == 0) {
            Resultados.enviar = 1;
        }
        EstadoEntrega();
        generar_informe();
    }//GEN-LAST:event_btnimprimir2ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btncancelar;
    private javax.swing.JButton btnimprimir1;
    private javax.swing.JButton btnimprimir2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    public static javax.swing.JTable tablapracticas;
    private javax.swing.JTextField txtnbu;
    private javax.swing.JLabel txtnombre;
    private javax.swing.JTextField txtprotocolo;
    private javax.swing.JTextArea txtreferencia;
    // End of variables declaration//GEN-END:variables
}
