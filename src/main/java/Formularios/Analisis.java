package Formularios;

import Clases.ClaseAnalisis;
import Clases.ConexionMySQLLocal;
import Clases.Escape;
import Clases.Metodo;
import Clases.TreeState;
import Clases.camposinformes;
import Clases.camposinformes2;
import Clases.camposinformes3;
import Clases.camposinformes4;
import Clases.camposinformes5;
import static Formularios.Formulas.formula;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import net.sf.jasperreports.view.JasperViewer;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JDialog;
import java.io.IOException;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

public class Analisis extends javax.swing.JDialog {

    DefaultMutableTreeNode lab = new DefaultMutableTreeNode("Práctica");
    DefaultMutableTreeNode practicas;
    DefaultMutableTreeNode analisis;
    DefaultMutableTreeNode IdAnalisis;
    TextAutoCompleter textAutoAcompleterPractica;
    TextAutoCompleter textAutoAcompleterMetodo;
    int contadorj = 0;
    public static String nbreAnalisis;
    public int idunidad[] = new int[200];
    public int id_practicas[] = new int[2000], idsecciones, idresultados[] = new int[3000];
    public String nombre_practicas[] = new String[2000];
    DefaultTableModel model;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    String codigointerno, secciones, nompractica;
    public int idderivacion[] = new int[500], idseccion[] = new int[500];
    int nivel, nivel0, nivel1, nivel2;
    String nombre_material, nombre_analisis;
    int codigo_material;
    public static int idtitulo = 1;
    public static int idpracticas, sig = 0, id_analisis = 0;
    public static String seccion = "";
    DefaultMutableTreeNode labPrioridad = new DefaultMutableTreeNode("Secciones");
    DefaultMutableTreeNode practicasPrioridad;
    DefaultMutableTreeNode analisisPrioridad;
    DefaultMutableTreeNode seccionPrioridad;
    DefaultTableModel modelPrioridad;
    String padre, izq;
    Image imagen2;
    ArrayList<Metodo> ListaMetodos = new ArrayList<Metodo>();
    ArrayList<ClaseAnalisis> ListaAnalisis = new ArrayList<ClaseAnalisis>();
    static int cantidad;
    String nombre_analisisPrioridad, padrePrioridad, izqPrioridad;
    public static String nombre_titulo = "";
    int banderaBaja = 0, idMetodo;
    String[] tooltips = new String[100];

    class MyComboBoxRenderer extends BasicComboBoxRenderer {

        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
                if (-1 < index) {
                    list.setToolTipText(tooltips[index]);
                }
            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            setFont(list.getFont());
            setText((value == null) ? "" : value.toString());
            return this;
        }
    }

    public Analisis(java.awt.Frame parent, boolean modal) {

        super(parent, modal);
        initComponents();
        setTitle("Alta análisis");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(true);
        Escape.funcionescape(this);
        cargarmateriales();
        tablamaterial.setVisible(false);
        radiono.setSelected(true);
        grupoderiva.add(radiosi);
        grupoderiva.add(radiono);
        radionum.setSelected(true);
        grupoboton.add(radionum);
        grupoboton.add(radioposneg);
        grupoboton.add(radioposnegnum);
        tpPracticas.setEnabledAt(0, true);
        tpPracticas.setEnabledAt(1, false);
        tpPracticas.setEnabledAt(2, false);
        btnDerivaciones.setEnabled(false);
        textAutoAcompleterPractica = new TextAutoCompleter(txtnombre);
        textAutoAcompleterMetodo = new TextAutoCompleter(txtMetodo);
        cargarpracticaconnbu();
        cargarcombounidad();
        cargarcomboderivaciones();
        eventojtreeAnalisis();
        cargarcombo();
        dobleclick();
        dobleclicktablamateriales();
        cargarMetodo();
        dobleclicTitulo();
        btnModificarAnalisis.setEnabled(false);
        SiguienteGeneral.setEnabled(false);

        for (Component component : panelderiva.getComponents()) {
            component.setEnabled(false);
        }
        txtnombre.requestFocus();

    }

    void dobleclicktablamateriales() {

        tablamaterial.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int n = tablautilizado.getRowCount();
                    int i = 0, ban = 0;
                    ////////////////////////////////////
                    int fila = 0;
                    codigo_material = Integer.valueOf(tablamaterial.getValueAt(tablamaterial.getSelectedRow(), 0).toString());
                    nombre_material = tablamaterial.getValueAt(tablamaterial.getSelectedRow(), 1).toString();
                    new cantidad(null, true).setVisible(true);
                    if (cantidad != 0) {
                        DefaultTableModel model2 = (DefaultTableModel) tablautilizado.getModel();
                        if (tablautilizado.getRowCount() == 0) {
                            Object nuevo[] = {
                                "1", "", ""};
                            model2.addRow(nuevo);
                            tablautilizado.setValueAt(codigo_material, fila, 0);
                            tablautilizado.setValueAt(nombre_material, fila, 1);
                            tablautilizado.setValueAt(cantidad, fila, 2);
                        } else {
                            i = 0;
                            while (i < n) {
                                if (codigo_material != Integer.valueOf(tablautilizado.getValueAt(i, 0).toString())) {
                                    ban = 0;
                                } else {
                                    ban = 1;
                                    break;
                                }
                                i++;
                            }
                            if (ban == 0) {
                                fila = n;
                                Object nuevo[] = {
                                    fila + 1, "", ""};
                                model2.addRow(nuevo);
                                tablautilizado.setValueAt(codigo_material, fila, 0);
                                tablautilizado.setValueAt(nombre_material, fila, 1);
                                tablautilizado.setValueAt(cantidad, fila, 2);
                            } else {
                                int cant = Integer.valueOf(tablautilizado.getValueAt(i, 2).toString());
                                tablautilizado.setValueAt(cant + cantidad, i, 2);
                            }
                            i++;
                            //////////////////////////////////////////////
                        }
                        tablautilizado.setAutoCreateRowSorter(true);
                        alinear();
                        tablautilizado.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                        tablautilizado.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                        tablautilizado.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                        cantidad = 0;
                    }

                }
            }
        });

    }

    void cargarMetodo() {

        String sql = "SELECT idMetodos, Nombre FROM metodos";
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        textAutoAcompleterMetodo.removeAllItems();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                ListaMetodos.add(new Metodo(rs.getInt("idMetodos"), rs.getString("Nombre")));
                textAutoAcompleterMetodo.addItem(rs.getString("Nombre"));
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        } finally {
            try {
                if (cn != null) {
                    cn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void cargarcombo() {
        int i = 0;
        String sql = "SELECT id_secciones, nombre FROM secciones WHERE estado = 1";
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        cboseccion.removeAllItems();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                idseccion[i] = (rs.getInt(1));
                cboseccion.addItem(rs.getString(2));
                i++;
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        } finally {

            try {
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    void cargarvaloresderef(String valor) {

        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {

            String sql = "SELECT valoresreferencia FROM analisis WHERE CONCAT(id_analisis, '-', nombre)='" + valor + "' AND id_practicas=" + idpracticas;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                txavaloresreferencia.setText(rs.getString(1));
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, ex);
        } finally {
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    void dobleclick() {
        jTAnalisis.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    eventojtreeAnalisis();
                }
            }
        });
    }

    void dobleclicTitulo() {
        tablaanalisis.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {

                    /* ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                    Connection cn = mysql.Conectar();
                    if (tablaanalisis.getSelectedRow() != -1) {
                        System.out.println("!= -1");
                        new Titulo(null, true).setVisible(true);
                        tablaanalisis.setValueAt(nombre_titulo, tablaanalisis.getSelectedRow(), 2);
                        tablaanalisis.setValueAt(idtitulo, tablaanalisis.getSelectedRow(), 1);
                        if (!nombre_titulo.equals("")) {
                            try {
                                int id_analisis = Integer.valueOf(tablaanalisis.getValueAt(tablaanalisis.getSelectedRow(), 0).toString());
                                String actualizar = "UPDATE analisis SET id_titulo=?, estado_titulo=? WHERE id_analisis= " + id_analisis + " AND estado = 1";
                                PreparedStatement pst = cn.prepareStatement(actualizar);
                                pst.setInt(1, idtitulo);
                                pst.setInt(2, 1);
                                pst.executeUpdate();
                                ///cn.close();
                            } catch (SQLException ex) {
                                JOptionPane.showMessageDialog(null, "Error en la base de datos");
                                JOptionPane.showMessageDialog(null, ex);
                            }
                        } else {
                            try {
                                int id_analisis = Integer.valueOf(tablaanalisis.getValueAt(0, 0).toString());
                                String actualizar = "UPDATE analisis SET id_titulo=?, estado_titulo=? WHERE id_analisis= " + id_analisis + " AND estado = 1";
                                PreparedStatement pst = cn.prepareStatement(actualizar);
                                pst.setInt(1, 1);
                                pst.setInt(2, 0);
                                pst.executeUpdate();
                                ////cn.close();
                            } catch (SQLException ex) {
                                JOptionPane.showMessageDialog(null, "Error en la base de datos");
                                JOptionPane.showMessageDialog(null, ex);
                            }
                        }
                    } else {
                        System.out.println("== -1");
                        new Titulo(null, true).setVisible(true);
                        Object[] Registros = new Object[4];
                        Object[] Titulo = {"idanalisis", "idtitulo", "Título", "Análisis"};
                        model = new DefaultTableModel(null, Titulo) {
                            ////Celdas no editables////////
                            public boolean isCellEditable(int row, int column) {
                                return false;
                            }
                        };

                        Registros[0] = "";
                        Registros[1] = idtitulo;
                        Registros[2] = nombre_titulo;
                        Registros[3] = txtFormatoDeInforme.getText();
                        model.addRow(Registros);
                        tablaanalisis.setModel(model);
                        tablaanalisis.setAutoCreateRowSorter(true);
                        tablaanalisis.getColumnModel().getColumn(0).setMaxWidth(0);
                        tablaanalisis.getColumnModel().getColumn(0).setMinWidth(0);
                        tablaanalisis.getColumnModel().getColumn(0).setPreferredWidth(0);
                        tablaanalisis.getColumnModel().getColumn(1).setMaxWidth(0);
                        tablaanalisis.getColumnModel().getColumn(1).setMinWidth(0);
                        tablaanalisis.getColumnModel().getColumn(1).setPreferredWidth(0);
                        alinear();
                        tablaanalisis.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                        tablaanalisis.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                    }*/
                    btnagregartitulo.doClick();
                }
            }
        });
    }

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    void dobleclicktablaanalisis() {
        tablaanalisis.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    //////////////////////////////
                    if (tablaanalisis.getSelectedRow() != -1) {
                        new Titulo(null, true).setVisible(true);

                        tablaanalisis.setValueAt(nombre_titulo, tablaanalisis.getSelectedRow(), 2);
                        tablaanalisis.setValueAt(idtitulo, tablaanalisis.getSelectedRow(), 1);
                        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                        Connection cn = mysql.Conectar();
                        try {
                            if (!nombre_titulo.equals("")) {

                                int id_analisis = Integer.valueOf(tablaanalisis.getValueAt(tablaanalisis.getSelectedRow(), 0).toString());
                                String actualizar = "UPDATE analisis SET id_titulo=?, estado_titulo=? WHERE id_analisis= " + id_analisis + " AND estado = 1";
                                PreparedStatement pst = cn.prepareStatement(actualizar);
                                pst.setInt(1, idtitulo);
                                pst.setInt(2, 1);
                                pst.executeUpdate();
                                cn.close();

                            } else {

                                int id_analisis = Integer.valueOf(tablaanalisis.getValueAt(tablaanalisis.getSelectedRow(), 0).toString());

                                String actualizar = "UPDATE analisis SET id_titulo=?, estado_titulo=? WHERE id_analisis= " + id_analisis + " AND estado = 1";
                                PreparedStatement pst = cn.prepareStatement(actualizar);
                                pst.setInt(1, 1);
                                pst.setInt(2, 0);
                                pst.executeUpdate();
                                cn.close();

                            }
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, "Error en la base de datos");
                            JOptionPane.showMessageDialog(null, ex);
                        } finally {
                            if (cn != null) {
                                try {
                                    cn.close();
                                } catch (SQLException ex) {
                                    Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila");
                    }
                }
            }

        });

    }

    void cargarmateriales() {
        Object[] Titulo = {"N°", "Nombre", "Precio", "Stock"};
        Object[] Registros = new Object[4];
        String sql = "SELECT id_materiales, nombre_mat, precio, stock FROM materiales";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString("id_materiales");
                Registros[1] = rs.getString("nombre_mat");
                Registros[2] = rs.getString("precio");
                Registros[3] = rs.getDouble("stock");
                model.addRow(Registros);
            }
            tablamaterial.setModel(model);
            tablamaterial.setAutoCreateRowSorter(true);
            /////////////////////////////////////////////////////////////
            tablamaterial.getColumnModel().getColumn(0).setMaxWidth(0);
            tablamaterial.getColumnModel().getColumn(0).setMinWidth(0);
            tablamaterial.getColumnModel().getColumn(0).setPreferredWidth(0);
            alinear();
            tablamaterial.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
            tablamaterial.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
            tablamaterial.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
            tablamaterial.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        } finally {
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    void cargarpracticaconnbu() {
        contadorj = 0;
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        textAutoAcompleterPractica.removeAllItems();
        try {
            Statement St = cn.createStatement();
            ResultSet Rs = St.executeQuery("SELECT practicas.id_practicas, practicas.codigo_practica, practicas.determinacion_practica FROM practicas");
            while (Rs.next()) {
                id_practicas[contadorj] = (Rs.getInt(1));
                nombre_practicas[contadorj] = (Rs.getInt(2) + " - " + Rs.getString(3));
                textAutoAcompleterPractica.addItem(nombre_practicas[contadorj]);
                contadorj++;
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        textAutoAcompleterPractica.setMode(0); // infijo
        textAutoAcompleterPractica.setCaseSensitive(false);
    }

    void cargarinstruccion() {
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {

            String sql = "SELECT instrucciones, tiempo_procesamiento, tipo_informe, secciones.nombre FROM practicas inner join secciones on practicas.id_seccion = secciones.id_secciones WHERE id_practicas=" + idpracticas;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                txainst.setText(rs.getString(1));
                cbotiempo.setSelectedItem(rs.getString(2));
                cbotipo.setSelectedIndex(rs.getInt(3) - 1);
                if (rs.getString(4) != null) {
                    cboseccion.setSelectedItem(rs.getString(4));
                }
            }
        } catch (SQLException ex) {

        } finally {
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    void cargarpreciosyderivaciones() {

        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            String sql = "SELECT estado_deriva, id_derivaciones FROM practicas WHERE id_practicas=" + idpracticas;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                if (rs.getInt(1) != 0) {
                    radiosi.setSelected(true);
                    cboderivaciones.setEnabled(true);
                    cboderivaciones.setSelectedIndex(rs.getInt(2));
                } else {
                    radiono.setSelected(true);
                    cboderivaciones.setEnabled(false);
                }
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    void cargartablautilizado() {
        String[] Titulo = {"Codigo", "Nombre", "Cantidad"};
        String[] Registros = new String[3];
        int box = 0;
        String sql = "SELECT id_materiales, nombre_mat, cantidad FROM practicas INNER JOIN practicas_tienen_materiales USING (id_practicas) INNER JOIN materiales USING (id_materiales) WHERE id_practicas ='" + idpracticas + "'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        if (idpracticas != 0) {
            try {
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    Registros[0] = rs.getString("id_materiales");
                    Registros[1] = rs.getString("nombre_mat");
                    Registros[2] = rs.getString("cantidad");
                    model.addRow(Registros);

                }

                tablautilizado.setModel(model);
                alinear();
                tablautilizado.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                tablautilizado.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                tablautilizado.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "Error en la Base de Datos");
            } finally {

                try {
                    cn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        } else {
            tablautilizado.setModel(model);
            alinear();
            tablautilizado.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
            tablautilizado.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
            tablautilizado.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);

        }
    }

    void cargartablaanalisis() {
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        Object[] Registros = new Object[10];
        Object[] Titulo = {"idanalisis", "idtitulo", "Título", "Análisis", "Unidad", "VR", "Metodo", "Practica", "cod Int", "Tipo"};
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String sSQL = "SELECT id_practicas,determinacion_practica, tipo_informe FROM practicas WHERE id_practicas = " + idpracticas;
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {

                nompractica = rs.getString(2);

                String sSQL2 = "SELECT id_analisis,analisis.nombre, estado_titulo, id_titulo,unidad,valoresreferencia, metodos.Nombre,codigo_interno \n"
                        + "FROM analisis \n"
                        + "left join metodos on analisis.idMetodos=metodos.idMetodos \n"
                        + "where id_practicas=" + rs.getInt(1) + " AND estado = 1";
                Statement st2 = cn.createStatement();
                ResultSet rs2 = st2.executeQuery(sSQL2);
                while (rs2.next()) {
                    if (rs2.getInt(3) == 1) {
                        String sSQL3 = "SELECT id_titulo, nombre FROM titulos WHERE id_titulo=" + rs2.getString(4);
                        Statement st3 = cn.createStatement();
                        ResultSet rs3 = st3.executeQuery(sSQL3);
                        rs3.next();

                        Registros[0] = rs2.getString(1);
                        Registros[1] = rs3.getString(1);
                        Registros[2] = rs3.getString(2);
                        Registros[3] = rs2.getString(2);
                        Registros[4] = rs2.getString(5);
                        Registros[5] = rs2.getString(6);
                        Registros[6] = rs2.getString(7);
                        Registros[7] = rs.getString(2);
                        Registros[8] = rs2.getString(8);
                        Registros[9] = rs.getString(3);
                    } else {
                        Registros[0] = rs2.getString(1);
                        Registros[1] = "";
                        Registros[2] = "";
                        Registros[3] = rs2.getString(2);
                        Registros[4] = rs2.getString(5);
                        Registros[5] = rs2.getString(6);
                        Registros[6] = rs2.getString(7);
                        Registros[7] = rs.getString(2);
                        Registros[8] = rs2.getString(8);
                        Registros[9] = rs.getString(3);
                    }
                    model.addRow(Registros);
                }
                tablaanalisis.setModel(model);
                tablaanalisis.setAutoCreateRowSorter(true);
                tablaanalisis.getColumnModel().getColumn(0).setMaxWidth(0);
                tablaanalisis.getColumnModel().getColumn(0).setMinWidth(0);
                tablaanalisis.getColumnModel().getColumn(0).setPreferredWidth(0);
                tablaanalisis.getColumnModel().getColumn(1).setMaxWidth(0);
                tablaanalisis.getColumnModel().getColumn(1).setMinWidth(0);
                tablaanalisis.getColumnModel().getColumn(1).setPreferredWidth(0);
                tablaanalisis.getColumnModel().getColumn(4).setMaxWidth(0);
                tablaanalisis.getColumnModel().getColumn(4).setMinWidth(0);
                tablaanalisis.getColumnModel().getColumn(4).setPreferredWidth(0);
                tablaanalisis.getColumnModel().getColumn(5).setMaxWidth(0);
                tablaanalisis.getColumnModel().getColumn(5).setMinWidth(0);
                tablaanalisis.getColumnModel().getColumn(5).setPreferredWidth(0);
                tablaanalisis.getColumnModel().getColumn(6).setMaxWidth(0);
                tablaanalisis.getColumnModel().getColumn(6).setMinWidth(0);
                tablaanalisis.getColumnModel().getColumn(6).setPreferredWidth(0);
                tablaanalisis.getColumnModel().getColumn(7).setMaxWidth(0);
                tablaanalisis.getColumnModel().getColumn(7).setMinWidth(0);
                tablaanalisis.getColumnModel().getColumn(7).setPreferredWidth(0);
                tablaanalisis.getColumnModel().getColumn(8).setMaxWidth(0);
                tablaanalisis.getColumnModel().getColumn(8).setMinWidth(0);
                tablaanalisis.getColumnModel().getColumn(8).setPreferredWidth(0);
                tablaanalisis.getColumnModel().getColumn(9).setMaxWidth(0);
                tablaanalisis.getColumnModel().getColumn(9).setMinWidth(0);
                tablaanalisis.getColumnModel().getColumn(9).setPreferredWidth(0);
                alinear();
                tablaanalisis.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                tablaanalisis.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
                tablaanalisis.getColumnModel().getColumn(2).setPreferredWidth(100);
                tablaanalisis.getColumnModel().getColumn(3).setPreferredWidth(100);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {

            try {
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    void cargarcomboderivaciones() {
        int i = 0;
        String sql = "SELECT id_derivaciones, nombre FROM derivaciones";
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        cboderivaciones.removeAllItems();
        cboderivaciones.addItem("...");
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                idderivacion[i] = (rs.getInt(1));
                cboderivaciones.addItem(rs.getString(2));
                i++;
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        } finally {

            try {
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    

    void crearArboles() {
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        String sSQL = "SELECT id_practicas,determinacion_practica FROM practicas WHERE id_practicas = " + idpracticas;
        lab.removeAllChildren();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            int i = 0;
            if (rs.next()) {
                practicas = new DefaultMutableTreeNode(rs.getString(2));
                nompractica = rs.getString(2);
                lab.add(practicas);
                String sSQL2 = "SELECT id_analisis, nombre FROM analisis where id_practicas=" + rs.getString(1) + " AND estado = 1";
                Statement st2 = cn.createStatement();
                ResultSet rs2 = st2.executeQuery(sSQL2);
                while (rs2.next()) {
                    ListaAnalisis.add(new ClaseAnalisis(rs2.getInt("id_analisis"), rs2.getString("nombre")));
                    analisis = new DefaultMutableTreeNode(rs2.getInt("id_analisis") + "-" + rs2.getString("nombre"));
                    i++;
                    practicas.add(analisis);
                }
            }
            ///cn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        DefaultTreeModel modelo = new DefaultTreeModel(lab);
        jTAnalisis.setModel(modelo);
        TreeState.setTreeState(jTAnalisis, true);
    }

    void cargarcomboresutados() {
        int i = 0, j = 0;
        String sql = "SELECT id, nombre FROM tipo_resultados where id_practicas =" + idpracticas + " and id_analisis=" + id_analisis;
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        cboTexto.removeAllItems();
        cboTexto.addItem("...");
        tooltips[j] = ("...");
        j++;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                idresultados[i] = (rs.getInt(1));
                cboTexto.addItem(rs.getString(2));
                tooltips[j] = rs.getString(2);
                i++;
                j++;

            }
            cboTexto.setRenderer(new MyComboBoxRenderer());

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        } finally {

            try {
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    void cargarnivel(int valor) {
        if (valor == 0) {
            ConexionMySQLLocal cc = new ConexionMySQLLocal();
            Connection cn = cc.Conectar();
            try {

                String[] Titulo = {"Nombre", "Prioridad", "id"};
                String[] Registros = new String[3];
                modelPrioridad = new DefaultTableModel(null, Titulo) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return true;
                    }
                };

                String sql = "SELECT nombre, prioridad, id_secciones FROM secciones where estado!=0 order by secciones.prioridad";
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    Registros[0] = rs.getString(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    modelPrioridad.addRow(Registros);
                }
                tablaprioridad.setModel(modelPrioridad);
                ////////////////////////////////////////////////////////////////////
                tablaprioridad.getColumnModel().getColumn(2).setMaxWidth(0);
                tablaprioridad.getColumnModel().getColumn(2).setMinWidth(0);
                tablaprioridad.getColumnModel().getColumn(2).setPreferredWidth(0);
                ////////////////////////////////////////////////////////////////////
                // 
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, ex);
            } finally {

                try {
                    cn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        if (valor == 1) {

            ConexionMySQLLocal cc = new ConexionMySQLLocal();
            Connection cn = cc.Conectar();

            try {

                String[] Titulo = {"Nombre", "Prioridad", "id"};
                String[] Registros = new String[3];
                model = new DefaultTableModel(null, Titulo) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return true;
                    }
                };
                int fin;
                int inicio = nombre_analisisPrioridad.indexOf("-") + 1;
                fin = nombre_analisisPrioridad.length();

                String sSQL = "SELECT determinacion_practica, practicas.prioridad, id_practicas FROM practicas INNER JOIN secciones ON practicas.id_seccion=secciones.id_secciones  where secciones.nombre='" + nombre_analisisPrioridad.substring(inicio, fin) + "' order by practicas.prioridad";
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sSQL);
                while (rs.next()) {
                    Registros[0] = rs.getString(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    model.addRow(Registros);
                }

                tablaprioridad.setModel(model);
                ////////////////////////////////////////////////////////////////////
                tablaprioridad.getColumnModel().getColumn(2).setMaxWidth(0);
                tablaprioridad.getColumnModel().getColumn(2).setMinWidth(0);
                tablaprioridad.getColumnModel().getColumn(2).setPreferredWidth(0);
                ////////////////////////////////////////////////////////////////////ç
                //
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, ex);
            } finally {

                try {
                    cn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        if (valor == 2) {

            ConexionMySQLLocal cc = new ConexionMySQLLocal();
            Connection cn = cc.Conectar();

            try {

                String[] Titulo = {"Nombre", "Prioridad", "id"};
                String[] Registros = new String[3];
                model = new DefaultTableModel(null, Titulo) {
                    ////Celdas no editables////////
                    public boolean isCellEditable(int row, int column) {
                        return true;
                    }
                };

                String sql = "SELECT nombre, analisis.prioridad,id_analisis,codigo_interno FROM analisis INNER JOIN practicas ON practicas.id_practicas=analisis.id_practicas where practicas.determinacion_practica='" + nombre_analisisPrioridad + "' AND analisis.estado = 1 order by analisis.prioridad";
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    //Registros[0] = rs.getString(1);
                    Registros[0] = rs.getString(4) + "-" + rs.getString(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    model.addRow(Registros);
                }
                tablaprioridad.setModel(model);
                ////////////////////////////////////////////////////////////////////
                tablaprioridad.getColumnModel().getColumn(2).setMaxWidth(0);
                tablaprioridad.getColumnModel().getColumn(2).setMinWidth(0);
                tablaprioridad.getColumnModel().getColumn(2).setPreferredWidth(0);
                ////////////////////////////////////////////////////////////////////
                //
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, ex);
            } finally {

                try {
                    cn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }

    void eventojtreePrioridad() {

        jTree4.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {

            public void valueChanged(TreeSelectionEvent e) {
                // Se obtiene el Path seleccionado
                TreePath path = e.getPath();
                Object[] nodos = path.getPath();
                for (Object nodo : nodos) {
                    nodo.toString();
                }
                // Se obtiene el Nodo seleccionado
                DefaultMutableTreeNode NodoSeleccionado = (DefaultMutableTreeNode) nodos[nodos.length - 1];
                nombre_analisisPrioridad = NodoSeleccionado.getUserObject().toString();
                if (NodoSeleccionado.getPreviousNode() != null) {
                    nivel = NodoSeleccionado.getLevel();
                    if (!nombre_analisisPrioridad.equals("Secciones")) {
                        cargarnivel(nivel);
                    }
                } else {
                    cargarnivel(0);
                }
            }
        });

    }

    void cargardatos(String valor) {

        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();

        try {

            String sql = "SELECT analisis.codigo_interno, analisis.nombre, analisis.valoresreferencia FROM analisis WHERE CONCAT (id_analisis, '-', analisis.nombre)='" + valor + "' AND analisis.id_practicas=" + idpracticas + " AND analisis.estado = 1";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                //cboseccion.setSelectedItem(rs.getString(1));
                //lblSeccion.setText(rs.getString(1));
                //lblPrioridad.setText(rs.getString(3));
                txtFormatoDeInforme.setText(rs.getString(2));
                txtFormatoDeMesada.setText(rs.getString(1));
                txavaloresreferencia.setText(rs.getString(3));

            }

            //// cn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    void cargarSeccion() {

        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();

        try {
            String sql = "SELECT secciones.nombre, tiempo_procesamiento, tipo_informe, practicas.prioridad,practicas.id_seccion FROM practicas inner join secciones on secciones.id_secciones = practicas.id_seccion where id_practicas =" + idpracticas;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                cboseccion.setSelectedItem(rs.getString(1));
                System.out.println("carga cboseccion " + rs.getString(1));
                cbotiempo.setSelectedItem(rs.getInt(2));
                cbotipo.setSelectedIndex(rs.getInt(3));
                lblPrioridadInforme.setText(rs.getString(4));
                System.out.println("prioridad: " + (rs.getString(4)));
                //idseccion=rs.getInt(5);
            }
            //// cn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void TraeSeccion() {

        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();

        try {
            String sql = "SELECT id_secciones FROM secciones where nombre ='" + seccion + "'";
            System.out.println("TraeSeccion()" + seccion);
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                idsecciones = rs.getInt(1);
                System.out.println("Traeidseccion" + idsecciones);
            }
            //// cn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void eventojtreeAnalisis() {
        jTAnalisis.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {

            public void valueChanged(TreeSelectionEvent e) {
                // Se obtiene el Path seleccionado
                TreePath path = e.getPath();
                Object[] nodos = path.getPath();
                for (Object nodo : nodos) {
                    nodo.toString();
                }
                ConexionMySQLLocal cc = new ConexionMySQLLocal();
                Connection cn = cc.Conectar();
                Connection cn2 = cc.Conectar();
                DefaultMutableTreeNode NodoSeleccionado = (DefaultMutableTreeNode) nodos[nodos.length - 1];
                if (!NodoSeleccionado.getUserObject().toString().equals("Práctica") && !NodoSeleccionado.getUserObject().toString().equals(nompractica)) {
                    btnGuardarAnalisis.setEnabled(false);
                    btnModificarAnalisis.setEnabled(true);
                    cargardatos(NodoSeleccionado.getUserObject().toString());
                    String nodo = "";
                    if (!jTAnalisis.isSelectionEmpty()) {
                        nodo = jTAnalisis.getSelectionPath().getLastPathComponent().toString();
                    }
                    try {
                        String select = "SELECT id_analisis FROM analisis WHERE CONCAT (id_analisis, '-', nombre )= '" + nodo + "' AND id_practicas=" + idpracticas + " AND estado = 1";
                        Statement stt = cn.createStatement();
                        ResultSet rss = stt.executeQuery(select);
                        id_analisis = 0;
                        while (rss.next()) {
                            id_analisis = rss.getInt(1);
                        }
                    } catch (SQLException ec) {
                        JOptionPane.showMessageDialog(null, ec);
                    } finally {
                        try {
                            cn.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    String sSQL2 = "SELECT * FROM vista_analisis where id_analisis=" + id_analisis;

                    try {
                        Statement st2 = cn2.createStatement();
                        ResultSet rs2 = st2.executeQuery(sSQL2);
                        if (rs2.next()) {
                            if (rs2.getString(2).equals("int")) {
                                radionum.setSelected(true);
                                cboPosneg.setEnabled(false);
                                cboTexto.setEnabled(true);
                                cbounidad.setSelectedItem(rs2.getString(1));
                                txtformula.setText("");
                                /// txttexto.setSelectedIndex(0);
                                /// txtposneg.setSelectedIndex(0);
                                cbounidad.setEnabled(true);
                            }
                            if (rs2.getString(2).equals("booleano")) {
                                radioposneg.setSelected(true);
                                cboPosneg.setEnabled(true);
                                cboTexto.setEnabled(true);
                                /// txtposneg.setSelectedItem(rs2.getString(1));
                                txtformula.setText("");
                                cboTexto.setSelectedIndex(0);
                                cbounidad.setSelectedIndex(0);
                                cbounidad.setEnabled(false);
                            }

                            if (rs2.getString(2).equals("booleano_y_numerico")) {
                                radioposnegnum.setSelected(true);
                                cboPosneg.setEnabled(false);
                                cboTexto.setEnabled(true);
                                txtformula.setText("");
                                /// txtposneg.setSelectedIndex(0);
                                cboTexto.setSelectedIndex(0);
                                cbounidad.setSelectedIndex(0);
                                cbounidad.setEnabled(false);
                                cboUnidadPositivoNegativo.setEnabled(true);
                                txtPositivoNegativoNumerico.setEnabled(true);
                                cboUnidadPositivoNegativo.setSelectedItem(rs2.getString(1));
                                txtPositivoNegativoNumerico.setText(rs2.getString(3));
                            }
                            if (rs2.getString(2).equals("")) {
                                radionum.setSelected(true);
                                cboPosneg.setEnabled(false);
                                /// txtposneg.setSelectedIndex(0);
                                cboTexto.setEnabled(true);
                                ///txttexto.setSelectedIndex(0);
                                txtformula.setText("");
                                cbounidad.setSelectedIndex(0);
                                cbounidad.setEnabled(true);
                            }
                            if (rs2.getObject(3) != null) {
                                if (!rs2.getString(3).equals("")) {
                                    txtformula.setText(rs2.getString(3));
                                }
                            }
                            txtMetodo.setText(rs2.getString(4));
                            cargarcomboresutados();
                        }
                        ////cn.close();
                    } catch (SQLException ec) {
                        JOptionPane.showMessageDialog(null, ec);
                    } finally {

                        try {
                            cn2.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                    btnGuardarAnalisis.setEnabled(false);
                    btnModificarAnalisis.setEnabled(true);
                    banderaBaja = 1;
                } else {
                    txtFormatoDeInforme.setText("");
                    txtFormatoDeMesada.setText("");
                    txtMetodo.setText("");
                    radionum.setSelected(true);
                    txtFormatoDeMesada.requestFocus();
                    cbounidad.setSelectedIndex(0);
                    btnGuardarAnalisis.setEnabled(true);
                    btnModificarAnalisis.setEnabled(false);
                    //TraerDatosAnalisis();
                    banderaBaja = 0;
                }
                System.out.println("eventojtreeAnalisis bandera:" + banderaBaja);
            }
        });
    }

    void TraerDatosAnalisis() {

        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {

            String sql = "SELECT  WHERE id_practicas=" + idpracticas;
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
            }
        } catch (SQLException ex) {
        } finally {
            try {
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void cargarcombounidad() {
        cbounidad.removeAllItems();
        cboUnidadPositivoNegativo.removeAllItems();
        cboUnidadPositivoNegativo.addItem("...");
        int i = 0;
        String sql = "SELECT id_unidades, nombre_uni FROM unidades";
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                cbounidad.addItem(rs.getString("nombre_uni"));
                cboUnidadPositivoNegativo.addItem(rs.getString("nombre_uni"));
                idunidad[i] = rs.getInt("id_unidades");
                i++;
            }
            ////cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos");
        } finally {
            try {
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void crearArbolesPrioridad() {

        //////////////////////////////////////////////////////////////
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        String sSQLsecc = "SELECT nombre, prioridad, id_secciones FROM secciones where estado!=0 order by secciones.prioridad";

        try {
            Statement st2 = cn.createStatement();
            ResultSet rs2 = st2.executeQuery(sSQLsecc);
            labPrioridad.removeAllChildren();
            while (rs2.next()) {
                seccionPrioridad = new DefaultMutableTreeNode(String.format("%03d", rs2.getInt("prioridad")) + "-" + rs2.getString(1));
                labPrioridad.add(seccionPrioridad);
                String sSQL = "SELECT id_practicas,determinacion_practica FROM practicas INNER JOIN secciones ON practicas.id_seccion=secciones.id_secciones  where id_secciones=" + rs2.getString(3) + " order by practicas.prioridad";
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sSQL);
                rs.beforeFirst();
                while (rs.next()) {
                    practicasPrioridad = new DefaultMutableTreeNode(rs.getString(2));
                    seccionPrioridad.add(practicasPrioridad);
                    String sSQL3 = "SELECT id_analisis, nombre FROM analisis where id_practicas=" + rs.getString(1) + " AND estado = 1 order by analisis.prioridad";
                    Statement st3 = cn.createStatement();
                    ResultSet rs3 = st3.executeQuery(sSQL3);
                    rs3.beforeFirst();
                    while (rs3.next()) {
                        analisisPrioridad = new DefaultMutableTreeNode(rs3.getString(2));
                        practicasPrioridad.add(analisisPrioridad);
                    }
                }

                ///////////////////////////////////////////////////////////////
            }
            DefaultTreeModel modelo = new DefaultTreeModel(labPrioridad);
            this.jTree4.setModel(modelo);
            TreeState.setTreeState(jTree4, false);
            //
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {

            try {
                cn.close();
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoboton = new javax.swing.ButtonGroup();
        grupoderiva = new javax.swing.ButtonGroup();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        Baja = new javax.swing.JMenuItem();
        tpPracticas = new javax.swing.JTabbedPane();
        jgeneral = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txainst = new javax.swing.JTextArea();
        panelmaterial = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablamaterial = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablautilizado = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        cboseccion = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        cbotipo = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        cbotiempo = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        btnNuevaPractica = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        SiguienteGeneral = new javax.swing.JButton();
        panelderiva = new javax.swing.JPanel();
        cboderivaciones = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        radiosi = new javax.swing.JRadioButton();
        radiono = new javax.swing.JRadioButton();
        btnDerivaciones = new javax.swing.JButton();
        janalisis = new javax.swing.JPanel();
        lblpractica = new javax.swing.JLabel();
        btnatras = new javax.swing.JButton();
        btnprincipal = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txavaloresreferencia = new javax.swing.JTextArea();
        jtiporesultado = new javax.swing.JPanel();
        radionum = new javax.swing.JRadioButton();
        radioposneg = new javax.swing.JRadioButton();
        cbounidad = new javax.swing.JComboBox();
        txtformula = new javax.swing.JLabel();
        radioposnegnum = new javax.swing.JRadioButton();
        cboUnidadPositivoNegativo = new javax.swing.JComboBox();
        txtPositivoNegativoNumerico = new javax.swing.JTextField();
        btnCargarFormula = new javax.swing.JButton();
        cboTexto = new javax.swing.JComboBox();
        cboPosneg = new javax.swing.JComboBox();
        btnAgregarUnidad = new javax.swing.JButton();
        chkFormula = new javax.swing.JCheckBox();
        jButton3 = new javax.swing.JButton();
        btnGuardarAnalisis = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        SiguienteAnalisis = new javax.swing.JButton();
        btnBaja = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        txtMetodo = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        txtFormatoDeMesada = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        txtFormatoDeInforme = new javax.swing.JTextField();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane11 = new javax.swing.JScrollPane();
        jTAnalisis = new javax.swing.JTree();
        btnModificarAnalisis = new javax.swing.JButton();
        chkMetodo = new javax.swing.JCheckBox();
        btnAgregarMetodo = new javax.swing.JButton();
        jInforme = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        tablaanalisis = new javax.swing.JTable();
        btnagregartitulo = new javax.swing.JButton();
        btnatras1 = new javax.swing.JButton();
        btnprincipal1 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tablaprioridad = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTree4 = new javax.swing.JTree();
        jButton2 = new javax.swing.JButton();
        lblSeccionInforme = new javax.swing.JLabel();
        lblPrioridadInforme = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        btnModificarPrioridad = new javax.swing.JButton();
        lblpracticaInforme = new javax.swing.JLabel();
        btninforme = new javax.swing.JButton();

        Baja.setText("Baja");
        Baja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BajaActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Baja);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        tpPracticas.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        tpPracticas.setMinimumSize(new java.awt.Dimension(880, 732));

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Práctica:");

        txtnombre.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Instrucciones:");

        txainst.setColumns(20);
        txainst.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txainst.setForeground(new java.awt.Color(0, 102, 204));
        txainst.setRows(5);
        jScrollPane4.setViewportView(txainst);

        panelmaterial.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Material a utilizar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 10))); // NOI18N
        panelmaterial.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        tablamaterial.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablamaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablamaterial.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tablamaterial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablamaterialKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablamaterial);

        tablautilizado.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablautilizado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tablautilizado);

        javax.swing.GroupLayout panelmaterialLayout = new javax.swing.GroupLayout(panelmaterial);
        panelmaterial.setLayout(panelmaterialLayout);
        panelmaterialLayout.setHorizontalGroup(
            panelmaterialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelmaterialLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 385, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 496, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panelmaterialLayout.setVerticalGroup(
            panelmaterialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelmaterialLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelmaterialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder()));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Sección:");

        cboseccion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cboseccion.setForeground(new java.awt.Color(0, 102, 204));
        cboseccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboseccionActionPerformed(evt);
            }
        });
        cboseccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cboseccionKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Tipo:");

        cbotipo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cbotipo.setForeground(new java.awt.Color(0, 102, 204));
        cbotipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tipo 0 Fila 1", "Tipo 1 Fila numerica", "Tipo 2 Columnas", "Tipo 3 Fila 2", "Tipo 4 Fila  cultivo", "Tipo 5 Proteinograma", "Tipo 6 Fila cultivo 2", "Tipo 7 Fila 2", "Tipo 8 Fila 3 por titulo", "Tipo 9 Fila 4 (Hormonas)" }));
        cbotipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbotipoActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Tiempo de Procesamiento:");

        cbotiempo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cbotiempo.setForeground(new java.awt.Color(0, 102, 204));
        cbotiempo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7" }));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 51, 51));
        jLabel9.setText("Dias");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(cboseccion, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(cbotipo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(cbotiempo, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cboseccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cbotipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbotiempo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9))
                    .addComponent(jLabel7))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnNuevaPractica.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnNuevaPractica.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        btnNuevaPractica.setMnemonic('s');
        btnNuevaPractica.setText("Nueva práctica");
        btnNuevaPractica.setMaximumSize(new java.awt.Dimension(113, 43));
        btnNuevaPractica.setMinimumSize(new java.awt.Dimension(113, 43));
        btnNuevaPractica.setPreferredSize(new java.awt.Dimension(121, 43));
        btnNuevaPractica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevaPracticaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelmaterial, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtnombre))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnNuevaPractica, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNuevaPractica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane4))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(panelmaterial, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728980 - home.png"))); // NOI18N
        btnsalir.setMnemonic('p');
        btnsalir.setText("Principal");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 43));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 43));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 43));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        SiguienteGeneral.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        SiguienteGeneral.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728938 - flag green.png"))); // NOI18N
        SiguienteGeneral.setMnemonic('s');
        SiguienteGeneral.setText("Siguiente");
        SiguienteGeneral.setMaximumSize(new java.awt.Dimension(113, 43));
        SiguienteGeneral.setMinimumSize(new java.awt.Dimension(113, 43));
        SiguienteGeneral.setPreferredSize(new java.awt.Dimension(121, 43));
        SiguienteGeneral.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SiguienteGeneralActionPerformed(evt);
            }
        });

        panelderiva.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Derivación", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 10))); // NOI18N

        cboderivaciones.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        cboderivaciones.setForeground(new java.awt.Color(0, 102, 204));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Se deriva?");

        grupoderiva.add(radiosi);
        radiosi.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        radiosi.setText("Si");
        radiosi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radiosiActionPerformed(evt);
            }
        });

        grupoderiva.add(radiono);
        radiono.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        radiono.setText("No");
        radiono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radionoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelderivaLayout = new javax.swing.GroupLayout(panelderiva);
        panelderiva.setLayout(panelderivaLayout);
        panelderivaLayout.setHorizontalGroup(
            panelderivaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelderivaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelderivaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboderivaciones, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelderivaLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(30, 30, 30)
                        .addComponent(radiono)
                        .addGap(18, 18, 18)
                        .addComponent(radiosi)
                        .addGap(0, 86, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelderivaLayout.setVerticalGroup(
            panelderivaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelderivaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelderivaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(radiosi)
                    .addComponent(radiono))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                .addComponent(cboderivaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnDerivaciones.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnDerivaciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728919 - car taxi transfer transport travel.png"))); // NOI18N
        btnDerivaciones.setText("Derivaciones");
        btnDerivaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDerivacionesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jgeneralLayout = new javax.swing.GroupLayout(jgeneral);
        jgeneral.setLayout(jgeneralLayout);
        jgeneralLayout.setHorizontalGroup(
            jgeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jgeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jgeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jgeneralLayout.createSequentialGroup()
                        .addComponent(panelderiva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnDerivaciones)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(SiguienteGeneral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jgeneralLayout.setVerticalGroup(
            jgeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jgeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jgeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jgeneralLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jgeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(SiguienteGeneral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnDerivaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jgeneralLayout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(panelderiva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        tpPracticas.addTab("General", jgeneral);

        lblpractica.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblpractica.setForeground(new java.awt.Color(0, 102, 204));

        btnatras.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnatras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728939 - flag yellow.png"))); // NOI18N
        btnatras.setMnemonic('a');
        btnatras.setText("Atrás");
        btnatras.setMaximumSize(new java.awt.Dimension(95, 43));
        btnatras.setMinimumSize(new java.awt.Dimension(95, 43));
        btnatras.setPreferredSize(new java.awt.Dimension(115, 43));
        btnatras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnatrasActionPerformed(evt);
            }
        });

        btnprincipal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnprincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728980 - home.png"))); // NOI18N
        btnprincipal.setMnemonic('p');
        btnprincipal.setText("Principal");
        btnprincipal.setMaximumSize(new java.awt.Dimension(110, 43));
        btnprincipal.setMinimumSize(new java.awt.Dimension(110, 43));
        btnprincipal.setPreferredSize(new java.awt.Dimension(115, 43));
        btnprincipal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnprincipalActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Valores de Referencia", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 10))); // NOI18N

        txavaloresreferencia.setColumns(20);
        txavaloresreferencia.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        txavaloresreferencia.setForeground(new java.awt.Color(0, 102, 204));
        txavaloresreferencia.setRows(5);
        jScrollPane6.setViewportView(txavaloresreferencia);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6)
                .addContainerGap())
        );

        jtiporesultado.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Tipo de Resultado", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 10))); // NOI18N

        grupoboton.add(radionum);
        radionum.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        radionum.setText("Numérico");
        radionum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radionumActionPerformed(evt);
            }
        });

        grupoboton.add(radioposneg);
        radioposneg.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        radioposneg.setText("Positivo/Negativo");
        radioposneg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioposnegActionPerformed(evt);
            }
        });

        cbounidad.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cbounidad.setForeground(new java.awt.Color(0, 102, 204));

        txtformula.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtformula.setForeground(new java.awt.Color(0, 0, 153));

        grupoboton.add(radioposnegnum);
        radioposnegnum.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        radioposnegnum.setText("+/- y Numérico");
        radioposnegnum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioposnegnumActionPerformed(evt);
            }
        });

        cboUnidadPositivoNegativo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cboUnidadPositivoNegativo.setForeground(new java.awt.Color(0, 102, 204));

        txtPositivoNegativoNumerico.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtPositivoNegativoNumerico.setForeground(new java.awt.Color(0, 102, 204));

        btnCargarFormula.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnCargarFormula.setText("Cargar Formula");
        btnCargarFormula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarFormulaActionPerformed(evt);
            }
        });

        cboTexto.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cboTexto.setForeground(new java.awt.Color(0, 102, 204));
        cboTexto.setMaximumSize(new java.awt.Dimension(28, 20));
        cboTexto.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                cboTextoMouseMoved(evt);
            }
        });
        cboTexto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTextoActionPerformed(evt);
            }
        });

        cboPosneg.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cboPosneg.setForeground(new java.awt.Color(0, 102, 204));
        cboPosneg.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Positivo", "Negativo", "No Contiene" }));

        btnAgregarUnidad.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAgregarUnidad.setMnemonic('u');
        btnAgregarUnidad.setText("Agregar Unidad");
        btnAgregarUnidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarUnidadActionPerformed(evt);
            }
        });

        chkFormula.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chkFormula.setText("Fórmula");
        chkFormula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkFormulaActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton3.setText("Tipo de Resultado");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jtiporesultadoLayout = new javax.swing.GroupLayout(jtiporesultado);
        jtiporesultado.setLayout(jtiporesultadoLayout);
        jtiporesultadoLayout.setHorizontalGroup(
            jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jtiporesultadoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtformula, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jtiporesultadoLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jtiporesultadoLayout.createSequentialGroup()
                                    .addComponent(radionum)
                                    .addGap(40, 40, 40))
                                .addComponent(radioposneg, javax.swing.GroupLayout.Alignment.TRAILING))
                            .addComponent(chkFormula)
                            .addGroup(jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jButton3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(radioposnegnum, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jtiporesultadoLayout.createSequentialGroup()
                                .addComponent(cboUnidadPositivoNegativo, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPositivoNegativoNumerico))
                            .addGroup(jtiporesultadoLayout.createSequentialGroup()
                                .addGroup(jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jtiporesultadoLayout.createSequentialGroup()
                                        .addComponent(cbounidad, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnAgregarUnidad))
                                    .addComponent(btnCargarFormula)
                                    .addComponent(cboPosneg, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cboTexto, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        jtiporesultadoLayout.setVerticalGroup(
            jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jtiporesultadoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbounidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radionum)
                    .addComponent(btnAgregarUnidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioposneg)
                    .addComponent(cboPosneg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboTexto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioposnegnum)
                    .addComponent(cboUnidadPositivoNegativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPositivoNegativoNumerico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jtiporesultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCargarFormula)
                    .addComponent(chkFormula, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtformula, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        btnGuardarAnalisis.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnGuardarAnalisis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728991 - diskette save.png"))); // NOI18N
        btnGuardarAnalisis.setMnemonic('g');
        btnGuardarAnalisis.setText("Guardar");
        btnGuardarAnalisis.setPreferredSize(new java.awt.Dimension(115, 43));
        btnGuardarAnalisis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarAnalisisActionPerformed(evt);
            }
        });

        SiguienteAnalisis.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        SiguienteAnalisis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728938 - flag green.png"))); // NOI18N
        SiguienteAnalisis.setMnemonic('s');
        SiguienteAnalisis.setText("Siguiente");
        SiguienteAnalisis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SiguienteAnalisisActionPerformed(evt);
            }
        });

        btnBaja.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnBaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        btnBaja.setText("Dar de baja");
        btnBaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBajaActionPerformed(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Método", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 10))); // NOI18N

        txtMetodo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtMetodo.setForeground(new java.awt.Color(0, 102, 204));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtMetodo)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtMetodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Nombre en formato de mesada", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 10))); // NOI18N

        txtFormatoDeMesada.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtFormatoDeMesada.setForeground(new java.awt.Color(0, 102, 204));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtFormatoDeMesada)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtFormatoDeMesada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Nombre en formato de informe", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 10))); // NOI18N

        txtFormatoDeInforme.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtFormatoDeInforme.setForeground(new java.awt.Color(0, 102, 204));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtFormatoDeInforme)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtFormatoDeInforme, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Análisis", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 10))); // NOI18N
        jPanel10.setPreferredSize(new java.awt.Dimension(455, 265));

        jTAnalisis.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jTAnalisis.setToolTipText("Hacer Clic sobre la Practica y luego sobre el analisis para modificar");
        jScrollPane11.setViewportView(jTAnalisis);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnModificarAnalisis.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnModificarAnalisis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728914 - brush paint.png"))); // NOI18N
        btnModificarAnalisis.setText("Modificar");
        btnModificarAnalisis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarAnalisisActionPerformed(evt);
            }
        });

        chkMetodo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chkMetodo.setText("Un sólo método para la práctica");

        btnAgregarMetodo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAgregarMetodo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        btnAgregarMetodo.setText("Agregar método");
        btnAgregarMetodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarMetodoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout janalisisLayout = new javax.swing.GroupLayout(janalisis);
        janalisis.setLayout(janalisisLayout);
        janalisisLayout.setHorizontalGroup(
            janalisisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(janalisisLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(janalisisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(janalisisLayout.createSequentialGroup()
                        .addGroup(janalisisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jtiporesultado, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(janalisisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, janalisisLayout.createSequentialGroup()
                                .addGap(0, 2, Short.MAX_VALUE)
                                .addGroup(janalisisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, janalisisLayout.createSequentialGroup()
                                        .addComponent(btnModificarAnalisis)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnGuardarAnalisis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(SiguienteAnalisis))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, janalisisLayout.createSequentialGroup()
                                        .addComponent(chkMetodo)
                                        .addGap(97, 97, 97)
                                        .addComponent(btnAgregarMetodo))))
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(janalisisLayout.createSequentialGroup()
                        .addGroup(janalisisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(janalisisLayout.createSequentialGroup()
                                .addComponent(btnprincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnatras, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBaja))
                            .addComponent(lblpractica, javax.swing.GroupLayout.PREFERRED_SIZE, 940, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        janalisisLayout.setVerticalGroup(
            janalisisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(janalisisLayout.createSequentialGroup()
                .addComponent(lblpractica, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(janalisisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(janalisisLayout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(janalisisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(chkMetodo)
                            .addComponent(btnAgregarMetodo)))
                    .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(janalisisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(janalisisLayout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(janalisisLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(janalisisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jtiporesultado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(18, 18, 18)
                .addGroup(janalisisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SiguienteAnalisis)
                    .addComponent(btnGuardarAnalisis, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnModificarAnalisis)
                    .addComponent(btnprincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnatras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBaja, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        tpPracticas.addTab("Analisis", janalisis);

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Titulos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 10))); // NOI18N

        tablaanalisis.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaanalisis.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaanalisis.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tablaanalisis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaanalisisKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaanalisisKeyReleased(evt);
            }
        });
        jScrollPane10.setViewportView(tablaanalisis);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane10)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnagregartitulo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregartitulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728900 - application browser.png"))); // NOI18N
        btnagregartitulo.setMnemonic('t');
        btnagregartitulo.setText("Agregar título");
        btnagregartitulo.setMaximumSize(new java.awt.Dimension(135, 43));
        btnagregartitulo.setMinimumSize(new java.awt.Dimension(135, 43));
        btnagregartitulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregartituloActionPerformed(evt);
            }
        });

        btnatras1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnatras1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728939 - flag yellow.png"))); // NOI18N
        btnatras1.setMnemonic('a');
        btnatras1.setText("Atrás");
        btnatras1.setMaximumSize(new java.awt.Dimension(95, 43));
        btnatras1.setMinimumSize(new java.awt.Dimension(95, 43));
        btnatras1.setPreferredSize(new java.awt.Dimension(115, 43));
        btnatras1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnatras1ActionPerformed(evt);
            }
        });

        btnprincipal1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnprincipal1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728980 - home.png"))); // NOI18N
        btnprincipal1.setMnemonic('p');
        btnprincipal1.setText("Principal");
        btnprincipal1.setMaximumSize(new java.awt.Dimension(110, 43));
        btnprincipal1.setMinimumSize(new java.awt.Dimension(110, 43));
        btnprincipal1.setPreferredSize(new java.awt.Dimension(115, 43));
        btnprincipal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnprincipal1ActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Prioridades", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 10))); // NOI18N

        tablaprioridad.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablaprioridad.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre", "Prioridad"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaprioridad.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane7.setViewportView(tablaprioridad);

        jTree4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jTree4.setComponentPopupMenu(jPopupMenu1);
        jScrollPane5.setViewportView(jTree4);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 398, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728937 - blue flag.png"))); // NOI18N
        jButton2.setText("Finalizar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        lblSeccionInforme.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblSeccionInforme.setForeground(new java.awt.Color(184, 0, 0));
        lblSeccionInforme.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lblPrioridadInforme.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblPrioridadInforme.setForeground(new java.awt.Color(184, 0, 0));
        lblPrioridadInforme.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("Sección:");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel16.setText("Prioridad:");

        btnModificarPrioridad.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnModificarPrioridad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728933 - document edit.png"))); // NOI18N
        btnModificarPrioridad.setText("Modificar prioridad");
        btnModificarPrioridad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarPrioridadActionPerformed(evt);
            }
        });

        lblpracticaInforme.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblpracticaInforme.setForeground(new java.awt.Color(0, 102, 204));

        btninforme.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btninforme.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728928 - document paper.png"))); // NOI18N
        btninforme.setMnemonic('a');
        btninforme.setText("Previsualizar Informe");
        btninforme.setMaximumSize(new java.awt.Dimension(95, 43));
        btninforme.setMinimumSize(new java.awt.Dimension(95, 43));
        btninforme.setPreferredSize(new java.awt.Dimension(115, 43));
        btninforme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btninformeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jInformeLayout = new javax.swing.GroupLayout(jInforme);
        jInforme.setLayout(jInformeLayout);
        jInformeLayout.setHorizontalGroup(
            jInformeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInformeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jInformeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jInformeLayout.createSequentialGroup()
                        .addComponent(btnprincipal1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnatras1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(102, 102, 102)
                        .addComponent(btninforme, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnModificarPrioridad)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2))
                    .addGroup(jInformeLayout.createSequentialGroup()
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jInformeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jInformeLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 324, Short.MAX_VALUE)
                                .addComponent(btnagregartitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jInformeLayout.createSequentialGroup()
                                .addGroup(jInformeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jInformeLayout.createSequentialGroup()
                                        .addGap(14, 14, 14)
                                        .addComponent(jLabel16))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jInformeLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel10)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jInformeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblSeccionInforme, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblPrioridadInforme, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                    .addGroup(jInformeLayout.createSequentialGroup()
                        .addComponent(lblpracticaInforme, javax.swing.GroupLayout.PREFERRED_SIZE, 943, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jInformeLayout.setVerticalGroup(
            jInformeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jInformeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblpracticaInforme, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(jInformeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jInformeLayout.createSequentialGroup()
                        .addGroup(jInformeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblSeccionInforme, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(27, 27, 27)
                        .addGroup(jInformeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblPrioridadInforme, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(btnagregartitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 21, Short.MAX_VALUE)
                        .addGroup(jInformeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton2)
                            .addComponent(btnModificarPrioridad)
                            .addComponent(btnprincipal1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnatras1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btninforme, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jInformeLayout.createSequentialGroup()
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        tpPracticas.addTab("Informe", jInforme);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tpPracticas, javax.swing.GroupLayout.PREFERRED_SIZE, 980, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tpPracticas, javax.swing.GroupLayout.PREFERRED_SIZE, 684, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SiguienteGeneralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SiguienteGeneralActionPerformed
        if (!txtnombre.getText().equals("")) {
            tpPracticas.setEnabledAt(1, true);
            ConexionMySQLLocal mysql = new ConexionMySQLLocal();
            Connection cn = mysql.Conectar();
            int deriv;
//            try {
//                int tamañoutilizado = tablautilizado.getRowCount();
//                int i = 0;
//                String borrar = "DELETE FROM practicas_tienen_materiales WHERE id_practicas=" + idpracticas;
//                PreparedStatement pstborrar = cn.prepareStatement(borrar);
//                int borra = pstborrar.executeUpdate();
//                while (i < tamañoutilizado) {
//                    String insertmateriales = "INSERT INTO practicas_tienen_materiales (id_practicas, id_materiales, cantidad ) VALUES (?,?,?)";
//                    PreparedStatement pst1 = cn.prepareStatement(insertmateriales);
//                    pst1.setInt(1, idpracticas);
//                    pst1.setInt(2, Integer.valueOf(tablautilizado.getValueAt(i, 0).toString()));
//                    pst1.setInt(3, Integer.valueOf(tablautilizado.getValueAt(i, 2).toString()));
//                    pst1.executeUpdate();
//                    i++;
//                }
//                cargartablautilizado();
//
//            } catch (SQLException ex) {
//                JOptionPane.showMessageDialog(null, ex);
//            }

            if (radiosi.isSelected()) {

                if (!cboderivaciones.getSelectedItem().equals("...")) {
                    deriv = idderivacion[cboderivaciones.getSelectedIndex() - 1];
                    try {
                        TraeSeccion();
                        String actualizar = "UPDATE practicas SET instrucciones=?, estado_deriva=?, id_derivaciones=?, tipo_informe=?, id_seccion=?, tiempo_procesamiento=? WHERE id_practicas=" + idpracticas;
                        PreparedStatement pst2 = cn.prepareStatement(actualizar);
                        pst2.setString(1, txainst.getText());
                        pst2.setInt(2, 1);
                        pst2.setInt(3, deriv);
                        pst2.setInt(4, cbotipo.getSelectedIndex());
                        pst2.setInt(5, idsecciones);
                        pst2.setInt(6, Integer.valueOf(cbotiempo.getSelectedItem().toString()));
                        pst2.executeUpdate();
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, ex);
                    } finally {
                        if (cn != null) {
                            try {
                                cn.close();
                            } catch (SQLException ex) {
                                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                    sig = 1;
                } else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un laboratorio para derivar la páctica");
                }
            } else {
                try {
                    TraeSeccion();
                    String actualizar = "UPDATE practicas SET instrucciones=?, estado_deriva=?, id_derivaciones=?, tipo_informe=?, id_seccion=?, tiempo_procesamiento=? WHERE id_practicas=" + idpracticas;
                    PreparedStatement pst3 = cn.prepareStatement(actualizar);
                    pst3.setString(1, txainst.getText());
                    pst3.setInt(2, 0);
                    pst3.setObject(3, null);
                    pst3.setInt(4, cbotipo.getSelectedIndex());
                    pst3.setInt(5, idsecciones);
                    pst3.setInt(6, Integer.valueOf(cbotiempo.getSelectedItem().toString()));
                    pst3.executeUpdate();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                } finally {
                    if (cn != null) {
                        try {
                            cn.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                }
                sig = 1;
            }

            crearArboles();
            tpPracticas.setSelectedIndex(1);
           // cargarpreciosobrasocial();
            
            crearArbolesPrioridad();
            eventojtreePrioridad();
            
            
             tpPracticas.setEnabledAt(1, true);
        tpPracticas.setSelectedIndex(1);
        radionum.setSelected(true);
        cboPosneg.setEnabled(false);
        cboTexto.setEnabled(true);
        cboUnidadPositivoNegativo.setEnabled(false);
        txtPositivoNegativoNumerico.setEnabled(false);
        btnCargarFormula.setEnabled(false);
            
        } else {
            JOptionPane.showMessageDialog(null, "No hay ninguna práctica seleccionada");
        }
    }//GEN-LAST:event_SiguienteGeneralActionPerformed

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed

        String cod = "", nom = "", cadena = txtnombre.getText();
        int i = 0, j = 1;

        while (i < cadena.length()) {
            if (String.valueOf(cadena.charAt(i)).equals("-")) {
                j = i + 2;
                i = cadena.length();
            } else {
                cod = cod + cadena.charAt(i);
            }
            i++;
        }

        while (j < cadena.length()) {
            nom = nom + cadena.charAt(j);
            j++;
        }

        int band = 0;
        if (!cadena.equals("")) {
            i = 0;
            while (i < contadorj) {
                if (cadena.equals(nombre_practicas[i])) {
                    idpracticas = id_practicas[i];
                    band = 1;
                    break;
                }
                i++;
                SiguienteGeneral.setEnabled(true);

            }
        }
        if (band == 0) {
            JOptionPane.showMessageDialog(null, "La practica no es aceptada por la Obra Social...");
            for (Component component : panelderiva.getComponents()) {
                component.setEnabled(false);
            }
            cboderivaciones.setEnabled(false);
            tablamaterial.setVisible(false);
            tablautilizado.setVisible(false);
            txainst.setText("");
            txtnombre.requestFocus();
            txtnombre.selectAll();

        } else {
            for (Component component : panelderiva.getComponents()) {
                component.setEnabled(true);
            }

            btnDerivaciones.setEnabled(true);
            lblpractica.setText(txtnombre.getText());
            cargarinstruccion();
            cargarpreciosyderivaciones();
            cargartablautilizado();
            cargartablaanalisis();
            cargarSeccion();
            sig = 1;
            txainst.requestFocus();
            tablamaterial.setVisible(true);
            tablautilizado.setVisible(true);
        }


    }//GEN-LAST:event_txtnombreActionPerformed

    private void radiosiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radiosiActionPerformed

        if (radiosi.isSelected()) {
            cboderivaciones.setEnabled(true);
        }

    }//GEN-LAST:event_radiosiActionPerformed

    private void radionoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radionoActionPerformed

        if (radiono.isSelected()) {
            cboderivaciones.setEnabled(false);
        }

    }//GEN-LAST:event_radionoActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed

        this.dispose();

    }//GEN-LAST:event_btnsalirActionPerformed

    private void radionumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radionumActionPerformed
        cbounidad.setEnabled(true);
        btnAgregarUnidad.setEnabled(true);
        cboPosneg.setEnabled(false);
        cboTexto.setEnabled(true);
        txtPositivoNegativoNumerico.setEnabled(false);
        cboUnidadPositivoNegativo.setEnabled(false);
        txtPositivoNegativoNumerico.setText("");
    }//GEN-LAST:event_radionumActionPerformed

    private void radioposnegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioposnegActionPerformed

        cbounidad.setEnabled(false);
        btnAgregarUnidad.setEnabled(false);
        cboPosneg.setEnabled(true);
        cboTexto.setEnabled(true);
        txtPositivoNegativoNumerico.setEnabled(false);
        cboUnidadPositivoNegativo.setEnabled(false);
        txtformula.setText("");
        txtPositivoNegativoNumerico.setText("");
        btnCargarFormula.setEnabled(false);

    }//GEN-LAST:event_radioposnegActionPerformed

    private void radioposnegnumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioposnegnumActionPerformed

        cbounidad.setEnabled(false);
        cboPosneg.setEnabled(false);
        cboTexto.setEnabled(true);
        txtPositivoNegativoNumerico.setEnabled(true);
        cboUnidadPositivoNegativo.setEnabled(true);
        cboUnidadPositivoNegativo.setSelectedItem("...");
        txtPositivoNegativoNumerico.setText("");
        btnCargarFormula.setEnabled(false);

    }//GEN-LAST:event_radioposnegnumActionPerformed

    private void SiguienteAnalisisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SiguienteAnalisisActionPerformed

        tpPracticas.setSelectedIndex(2);
        tpPracticas.setEnabledAt(2, true);
        lblSeccionInforme.setText(cboseccion.getSelectedItem().toString());
        lblpracticaInforme.setText(lblpractica.getText());


    }//GEN-LAST:event_SiguienteAnalisisActionPerformed

    private void btnatras1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnatras1ActionPerformed

        tpPracticas.setSelectedIndex(2);

    }//GEN-LAST:event_btnatras1ActionPerformed

    private void btnprincipal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnprincipal1ActionPerformed

        this.dispose();

    }//GEN-LAST:event_btnprincipal1ActionPerformed

    private void btnprincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnprincipalActionPerformed

        this.dispose();

    }//GEN-LAST:event_btnprincipalActionPerformed

    private void btnatrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnatrasActionPerformed

        tpPracticas.setSelectedIndex(0);

    }//GEN-LAST:event_btnatrasActionPerformed

    private void btnDerivacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDerivacionesActionPerformed

        new Derivaciones(null, true).setVisible(true);
        cargarcomboderivaciones();

    }//GEN-LAST:event_btnDerivacionesActionPerformed

    private void btnagregartituloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregartituloActionPerformed

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        if (tablaanalisis.getSelectedRow() != -1) {
            new Titulo(null, true).setVisible(true);
            tablaanalisis.setValueAt(nombre_titulo, tablaanalisis.getSelectedRow(), 2);
            tablaanalisis.setValueAt(idtitulo, tablaanalisis.getSelectedRow(), 1);
            if (!nombre_titulo.equals("")) {
                try {
                    int id_analisis = Integer.valueOf(tablaanalisis.getValueAt(tablaanalisis.getSelectedRow(), 0).toString());
                    String actualizar = "UPDATE analisis SET id_titulo=?, estado_titulo=? WHERE id_analisis= " + id_analisis + " AND estado = 1";
                    PreparedStatement pst = cn.prepareStatement(actualizar);
                    pst.setInt(1, idtitulo);
                    pst.setInt(2, 1);
                    pst.executeUpdate();
                    ///cn.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos");
                    JOptionPane.showMessageDialog(null, ex);
                }
            } else {
                try {
                    int id_analisis = Integer.valueOf(tablaanalisis.getValueAt(0, 0).toString());
                    String actualizar = "UPDATE analisis SET id_titulo=?, estado_titulo=? WHERE id_analisis= " + id_analisis + " AND estado = 1";
                    PreparedStatement pst = cn.prepareStatement(actualizar);
                    pst.setInt(1, 1);
                    pst.setInt(2, 0);
                    pst.executeUpdate();
                    ////cn.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos");
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione una fila");
        }

    }//GEN-LAST:event_btnagregartituloActionPerformed

    private void btnGuardarAnalisisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarAnalisisActionPerformed

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        for (int i = 0; i < ListaMetodos.size(); i++) {
            if (ListaMetodos.get(i).getNombre().equals(txtMetodo.getText())) {
                idMetodo = ListaMetodos.get(i).getIdMetodo();
                break;
            }
        }
        try {
            String insert = "INSERT INTO analisis (nombre, valoresreferencia, unidad, id_practicas, codigo_interno, tipo_resultado, unidad_extra, idMetodos) VALUES (?,?,?,?,?,?,?,?) ";
            PreparedStatement pst = cn.prepareStatement(insert);
            pst.setString(1, txtFormatoDeInforme.getText());
            pst.setString(2, txavaloresreferencia.getText());
            if (radionum.isSelected()) {
                pst.setString(3, cbounidad.getSelectedItem().toString());
            } else {
                pst.setString(3, null);
            }
            pst.setInt(4, idpracticas);
            pst.setString(5, txtFormatoDeMesada.getText());

            if (radionum.isSelected() || radioposnegnum.isSelected()) {
                pst.setString(6, "int");
            } else {
                pst.setString(6, "varchar");
            }

            if (chkFormula.isSelected()) {
                pst.setString(7, txtformula.getText());
            } else {
                pst.setString(7, "");
            }
            pst.setInt(8, idMetodo);

            int nsert = pst.executeUpdate();
            if (nsert > 0) {
                crearArboles();
                txtFormatoDeInforme.setText("");
                txtFormatoDeMesada.setText("");
                txtMetodo.setText("");
                jTAnalisis.setSelectionRow(1);
                cargartablaanalisis();
                banderaBaja = 0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_btnGuardarAnalisisActionPerformed

    private void btnAgregarUnidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarUnidadActionPerformed
        new Alta_Unidad(null, true).setVisible(true);
        cargarcombounidad();
    }//GEN-LAST:event_btnAgregarUnidadActionPerformed

    private void btnCargarFormulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarFormulaActionPerformed

        new Formulas(null, true).setVisible(true);
        txtformula.setText(formula);

    }//GEN-LAST:event_btnCargarFormulaActionPerformed

    private void btnAgregarMetodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarMetodoActionPerformed

        new Metodos(null, true).setVisible(true);
        cargarMetodo();

    }//GEN-LAST:event_btnAgregarMetodoActionPerformed

    private void cboseccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboseccionActionPerformed

        if (cboseccion.getItemCount() > 0) {
            /// idsecciones = idseccion[cboseccion.getSelectedIndex()];
            seccion = cboseccion.getSelectedItem().toString();
            System.out.println("seleccion cboseccion " + seccion);
            txtFormatoDeMesada.setEnabled(true);
            txtFormatoDeInforme.setEnabled(true);
            cbotipo.setEnabled(true);
            cbotiempo.setEnabled(true);
            txtMetodo.setEnabled(true);
            btnAgregarMetodo.setEnabled(true);
            chkMetodo.setEnabled(true);
            btnGuardarAnalisis.setEnabled(true);
            SiguienteAnalisis.setEnabled(true);
            radiono.setEnabled(true);
            radionum.setEnabled(true);
            radioposneg.setEnabled(true);
            radioposnegnum.setEnabled(true);
            radiosi.setEnabled(true);
            cbounidad.setEnabled(true);
            btnAgregarUnidad.setEnabled(true);
        }

    }//GEN-LAST:event_cboseccionActionPerformed

    private void cboseccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cboseccionKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtFormatoDeMesada.requestFocus();
        }

    }//GEN-LAST:event_cboseccionKeyPressed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        tpPracticas.setSelectedIndex(0);
        txtnombre.setText("");
        txainst.setText("");
        idpracticas = 0;
        tablautilizado.setVisible(false);
        tablamaterial.setVisible(false);
        cargarcomboderivaciones();
        cargarpreciosyderivaciones();
        radiono.setSelected(true);
        grupoderiva.add(radiosi);
        grupoderiva.add(radiono);
        crearArbolesPrioridad();

/////////////////////////////////////////////
        cargarcombounidad();
        cargarcomboderivaciones();
        crearArbolesPrioridad();
        eventojtreePrioridad();
        eventojtreeAnalisis();
        cargarcombo();
        dobleclick();
        dobleclicktablamateriales();
        cargarMetodo();
        //eventojtree3();
        //eventojtree1();
        // dobleclicTitulo();
        cargarpracticaconnbu();

    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnBajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBajaActionPerformed

        if (banderaBaja == 1) {
            ConexionMySQLLocal mysql = new ConexionMySQLLocal();
            Connection cn = mysql.Conectar();
            for (int i = 0; i < ListaAnalisis.size(); i++) {

                if (ListaAnalisis.get(i).getNombre().equals(nbreAnalisis)) {
                    id_analisis = ListaAnalisis.get(i).getIdAnalisis();
                    break;
                }
            }
            try {
                String bajaAnalisis = "UPDATE analisis set estado = 0 WHERE id_analisis = " + id_analisis;
                PreparedStatement pst = cn.prepareStatement(bajaAnalisis);
                int ps2 = pst.executeUpdate();
                if (ps2 > 0) {
                    JOptionPane.showMessageDialog(null, "Exito al dar de baja el análisis");
                    crearArboles();
                    crearArbolesPrioridad();
                    cargartablaanalisis();
                    banderaBaja = 0;
                }
            } catch (SQLException ex) {
                Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No seleccionó ningún análisis");
        }


    }//GEN-LAST:event_btnBajaActionPerformed

    private void btnModificarPrioridadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarPrioridadActionPerformed

        if (tablaprioridad.getSelectedRow() >= 0) {
            int niveles = 0;
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTree4.getSelectionPath().getLastPathComponent();
            niveles = node.getLevel();

            //JOptionPane.showMessageDialog(null, "Nivel: " +  node.toString());
            System.out.println("NODO:" + node.getLevel());
            //JOptionPane.showMessageDialog(null, "Nivel: " + node.getLevel());

            if (niveles == 0) {
                try {
                    // JOptionPane.showMessageDialog(null, "Entra nivel 0");
                    int n2 = 0, j = 0;
                    ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                    Connection cn = mysql.Conectar();
                    n2 = tablaprioridad.getRowCount();
                    //JOptionPane.showMessageDialog(null, "Entra nivel: " + n2);
                    while (n2 > j) {
                        String prioridad = "0";
                        if (tablaprioridad.getValueAt(j, 1) != null) {
                            prioridad = tablaprioridad.getValueAt(j, 1).toString().replace(" ", "");
                        }
                        int id_secciones = Integer.valueOf(tablaprioridad.getValueAt(j, 2).toString());
                        String actualizar = "UPDATE secciones SET prioridad=? WHERE id_secciones= " + id_secciones;
                        PreparedStatement pst = cn.prepareStatement(actualizar);
                        pst.setString(1, prioridad);
                        int p = pst.executeUpdate();
                        j++;
                    }

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos");
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
            if (niveles == 1) {
                try {
                    //  JOptionPane.showMessageDialog(null, "Entra nivel 1");
                    int n2 = 0, j = 0;
                    ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                    Connection cn = mysql.Conectar();
                    n2 = tablaprioridad.getRowCount();
                    //  JOptionPane.showMessageDialog(null, "cantidad: " + n2);
                    while (n2 > j) {
                        String prioridad = "0";
                        if (tablaprioridad.getValueAt(j, 1) != null) {
                            prioridad = tablaprioridad.getValueAt(j, 1).toString().replace(" ", "");
                        }

                        //System.out.println("prioridad:"+prioridad);
                        int id_practica = Integer.valueOf(tablaprioridad.getValueAt(j, 2).toString());
                        System.out.println("id_practica:" + Integer.valueOf(tablaprioridad.getValueAt(j, 2).toString()));
                        //JOptionPane.showMessageDialog(null, "cantidad: " + id_practica);
                        String actualizar = "UPDATE practicas SET prioridad=? WHERE id_practicas= " + id_practica;
                        PreparedStatement pst = cn.prepareStatement(actualizar);
                        pst.setString(1, prioridad);
                        int p = pst.executeUpdate();
                        j++;
                    }

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos");
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
            if (niveles == 2) {
                try {
                    //JOptionPane.showMessageDialog(null, "Entra nivel 2");
                    int n2 = 0, j = 0;
                    ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                    Connection cn = mysql.Conectar();
                    n2 = tablaprioridad.getRowCount();

                    while (n2 > j) {
                        String prioridad = "0";
                        if (tablaprioridad.getValueAt(j, 1) != null) {
                            prioridad = tablaprioridad.getValueAt(j, 1).toString().replace(" ", "");
                        }
                        int id_analisis = Integer.valueOf(tablaprioridad.getValueAt(j, 2).toString());
                        String actualizar = "UPDATE analisis SET prioridad=? WHERE id_analisis= " + id_analisis + " AND estado = 1";
                        PreparedStatement pst = cn.prepareStatement(actualizar);
                        pst.setString(1, prioridad);
                        int p = pst.executeUpdate();
                        j++;
                    }

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos");
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
            cargarnivel(niveles);
        } else {
            JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila");
        }
    }//GEN-LAST:event_btnModificarPrioridadActionPerformed

    private void BajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BajaActionPerformed
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();     // TODO add your handling code here:
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTree4.getSelectionPath().getLastPathComponent();

        String nombre_nodo = "";
        nombre_nodo = node.toString();
        int p;
        p = nombre_nodo.length();
        nombre_nodo = nombre_nodo.substring(4, p);
        try {
            JOptionPane.showMessageDialog(null, "Selecciono : " + nombre_nodo);
            String sql = "Select  secciones.*, practicas.codigo_practica\n"
                    + "from secciones inner JOIN practicas on secciones.id_secciones= practicas.id_seccion\n"
                    + "where nombre='" + nombre_nodo + "'";

            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                JOptionPane.showMessageDialog(null, "La seccion possee Practicas, por ello no puede ser dada de baja");
            } else {
                String actualizar = "UPDATE secciones SET estado=? WHERE nombre='" + nombre_nodo + "' ";
                PreparedStatement pst = cn.prepareStatement(actualizar);
                pst.setInt(1, 0);
                int s = pst.executeUpdate();
                if (s > 0) {
                    JOptionPane.showMessageDialog(null, "La seccion fue dada de baja");

                    crearArbolesPrioridad();
                }
            }
        } catch (SQLException ex) {
        }


    }//GEN-LAST:event_BajaActionPerformed

    private void tablamaterialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablamaterialKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            evt.consume();
            int n = tablautilizado.getRowCount();
            int i = 0, ban = 0;
            ////////////////////////////////////
            int fila = 0;
            codigo_material = Integer.valueOf(tablamaterial.getValueAt(tablamaterial.getSelectedRow(), 0).toString());
            nombre_material = tablamaterial.getValueAt(tablamaterial.getSelectedRow(), 1).toString();
            new cantidad(null, true).setVisible(true);
            if (cantidad != 0) {
                DefaultTableModel model2 = (DefaultTableModel) tablautilizado.getModel();
                if (tablautilizado.getRowCount() == 0) {
                    Object nuevo[] = {
                        "1", "", ""};
                    model2.addRow(nuevo);
                    tablautilizado.setValueAt(codigo_material, fila, 0);
                    tablautilizado.setValueAt(nombre_material, fila, 1);
                    tablautilizado.setValueAt(cantidad, fila, 2);
                } else {
                    i = 0;
                    while (i < n) {
                        if (codigo_material != Integer.valueOf(tablautilizado.getValueAt(i, 0).toString())) {
                            ban = 0;
                        } else {
                            ban = 1;
                            break;
                        }
                        i++;
                    }
                    if (ban == 0) {
                        fila = n;
                        Object nuevo[] = {
                            fila + 1, "", ""};
                        model2.addRow(nuevo);
                        tablautilizado.setValueAt(codigo_material, fila, 0);
                        tablautilizado.setValueAt(nombre_material, fila, 1);
                        tablautilizado.setValueAt(cantidad, fila, 2);
                    } else {
                        int cant = Integer.valueOf(tablautilizado.getValueAt(i, 2).toString());
                        tablautilizado.setValueAt(cant + cantidad, i, 2);
                    }
                    i++;
                    //////////////////////////////////////////////
                }
                tablautilizado.setAutoCreateRowSorter(true);
                alinear();
                tablautilizado.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
                tablautilizado.getColumnModel().getColumn(1).setCellRenderer(alinearCentro);
                tablautilizado.getColumnModel().getColumn(2).setCellRenderer(alinearCentro);
                cantidad = 0;
            }

            ////////////////////////////////////////////////////////////////
        }
    }//GEN-LAST:event_tablamaterialKeyPressed

    private void btnModificarAnalisisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarAnalisisActionPerformed

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        String actualizar = "UPDATE analisis SET nombre=?, "
                + "valoresreferencia=?, "
                + "unidad=?, "
                + "id_practicas=?, "
                + "codigo_interno=?, "
                + "tipo_resultado=?, "
                + "unidad_extra=?, "
                + "idMetodos=? WHERE CONCAT ( id_analisis, '-', nombre) = '" + jTAnalisis.getSelectionPath().getLastPathComponent().toString() + "' AND id_practicas=" + idpracticas + " AND estado = 1";

        if (!txtMetodo.getText().equals("")) {
            for (int i = 0; i < ListaMetodos.size(); i++) {
                if (ListaMetodos.get(i).getNombre().equals(txtMetodo.getText())) {
                    idMetodo = ListaMetodos.get(i).getIdMetodo();
                    System.out.println("idmetodos: " + idMetodo);
                    break;
                }
            }

        } else {
            idMetodo = 999999;

        }
        System.out.println("idmetodos1: " + idMetodo);
        try {
            PreparedStatement pst = cn.prepareStatement(actualizar);
            pst.setString(1, txtFormatoDeInforme.getText());
            pst.setString(2, txavaloresreferencia.getText());
            if (radionum.isSelected()) {
                pst.setString(3, cbounidad.getSelectedItem().toString());
            } else {
                pst.setString(3, null);
            }
            pst.setInt(4, idpracticas);
            pst.setString(5, txtFormatoDeMesada.getText());
            if (radionum.isSelected() || radioposnegnum.isSelected()) {
                pst.setString(6, "int");
            } else if (radioposneg.isSelected()) {
                pst.setString(6, "varchar");
            } else {
                pst.setString(6, "");
            }
            if (chkFormula.isSelected()) {
                pst.setString(7, txtformula.getText());
            } else {
                pst.setString(7, "");
            }
            if (idMetodo != 999999) {
                pst.setInt(8, idMetodo);
            } else {
                pst.setString(8, null);
            }

            int actual = pst.executeUpdate();
            if (actual > 0) {
                crearArboles();
                txtFormatoDeInforme.setText("");
                txtFormatoDeMesada.setText("");
                txtMetodo.setText("");
                txavaloresreferencia.setText("");
                jTAnalisis.setSelectionRow(1);
                cargartablaanalisis();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Analisis.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_btnModificarAnalisisActionPerformed

    private void chkFormulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkFormulaActionPerformed

        txtformula.setText("");
        btnCargarFormula.setEnabled(true);


    }//GEN-LAST:event_chkFormulaActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        String nodo = "";
        if (!jTAnalisis.isSelectionEmpty()) {
            nodo = jTAnalisis.getSelectionPath().getLastPathComponent().toString();
        }
        ///////////////////////////////////////////////////////////////////////////////////
        cbounidad.setEnabled(false);
        cboPosneg.setEnabled(false);
        cboTexto.setEnabled(true);
        txtPositivoNegativoNumerico.setEnabled(false);
        cboUnidadPositivoNegativo.setEnabled(false);
        ///////////////////////////////
        txtformula.setText("");
        txtPositivoNegativoNumerico.setText("");
        btnCargarFormula.setEnabled(false);
        new Alta_Tipo_Resultado(null, true).setVisible(true);
        cargarcomboresutados();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void cboTextoMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cboTextoMouseMoved

        /*if(cboTexto.getItemCount()>0){
        cboTexto.setToolTipText(cboTexto.getSelectedItem().toString());
    }*/

    }//GEN-LAST:event_cboTextoMouseMoved

    private void cboTextoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTextoActionPerformed
        if (cboTexto.getItemCount() > 0) {
            cboTexto.setToolTipText(cboTexto.getSelectedItem().toString());
        }
    }//GEN-LAST:event_cboTextoActionPerformed

    private void cbotipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbotipoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbotipoActionPerformed

    private void btninformeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btninformeActionPerformed
        /////////////////////IMPRIMIR O NO //////////////////////
        String matriz[][] = null;
        String matriz2[][] = new String[6][9];
        String matriz3[][] = new String[1][16];
        String matriz4[][] = new String[1][11];
        String observacion_practica[] = new String[100];
        int tamaño_tabla = 0;
        int j = 0, k = 0, m = 0, w = 0, h = 0, t = 0, caso_base = 0, caso_base2 = 0, caso_base3 = 0;
        String titulos = "", analisis = "", resultados = "", unidades = "", referencias = "", resultadoProteina = "", valorrefproteina = "", metodo = "", observaciones = "";
        String practicaP = "", analisisP = "", referenciaP = "";
        String analisisA = "";
        String analisisB = "";
        /////////////////////////////////////////////////// Variables /////////////////////////////////
        int i = 0, es_nulo = 0;
        camposinformes tipo1;
        //////////////////////////////////////// Armado de resultados//////////////////////////////////////////////
        System.out.println("3");
        /////imprimir
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();

        int contadorfilas = 0;

        tamaño_tabla = tablaanalisis.getRowCount();

        matriz = new String[tamaño_tabla][11];
        for (int x = 0; x < tamaño_tabla; x++) {
            for (int y = 0; y < 11; y++) {
                matriz[x][y] = "";
            }
        }
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 9; y++) {
                matriz2[x][y] = "";
            }
        }
        for (int x = 0; x < 1; x++) {
            for (int y = 0; y < 16; y++) {
                matriz3[x][y] = "";
            }
        }
        for (int x = 0; x < 1; x++) {
            for (int y = 0; y < 11; y++) {
                matriz4[x][y] = "";
            }
        }
        ////////////////////////////////////

        ///  while (rs.next()) {
        /* if (tablaanalisis.getValueAt(fila, 17).toString().equals("true")) {
                
        //////////////////////formato proteinograma//////////////////////////////////////////////////////////////////////////////////////                    
        /*  if (rs.getInt(16) == 2) {
                    //   JOptionPane.showMessageDialog(null, "si formato");
                    if (caso_base == 0) {
                        practicaP = rs.getString(11);
                        analisisP = rs.getString(5);//////caso base del proteinograma
                        referenciaP = rs.getString(7);
                        caso_base = 1;
                        j--;
                    } else {
                        /// JOptionPane.showMessageDialog(null, j);
                        if (j < 6) {
                            matriz2[j][0] = practicaP;
                            matriz2[j][1] = analisisP;
                            matriz2[j][2] = referenciaP;////////////cargo los datos en la matriz de la primera hasta sexta comlumna
                            matriz2[j][3] = rs.getString(4);
                            matriz2[j][4] = rs.getString(5);
                            matriz2[j][5] = rs.getString(7);

                            ///System.out.print(matriz2[j][0] + " " + matriz2[j][1] + " " + matriz2[j][2] + " " + matriz2[j][3] + " " + matriz2[j][4] + " " + matriz2[j][5] + " " + matriz2[j][6] + " " + matriz2[j][7] + " " + matriz2[j][8] +"\n");
                        } else {
                            if (k < 6) {
                                matriz2[k][6] = rs.getString(5);
                                matriz2[k][7] = rs.getString(7);////////////cargo los datos en la matriz de la septima hasta la octava comlumna
                            }
///                                System.out.print(matriz2[k][0] + " " + matriz2[k][1] + " " + matriz2[k][2] + " " + matriz2[k][3] + " " + matriz2[k][4] + " " + matriz2[k][5] + " " + matriz2[k][6] + " " + matriz2[k][7] + " " + matriz2[k][8]+ "\n");
                            k++;
                        }
                    }

                    if (j == 12) {
                        matriz2[0][8] = rs.getString(5);
                        matriz2[1][8] = rs.getString(5);
                        matriz2[2][8] = rs.getString(5);
                        matriz2[3][8] = rs.getString(5);////////////cargo los datos en la matriz de la ultima comlumna
                        matriz2[4][8] = rs.getString(5);
                        matriz2[5][8] = rs.getString(5);
///                            System.out.print(matriz2[k][0] + " " + matriz2[k][1] + " " + matriz2[k][2] + " " + matriz2[k][3] + " " + matriz2[k][4] + " " + matriz2[k][5] + " " + matriz2[k][6] + " " + matriz2[k][7] + " " + matriz2[k][8]);
                    }
                    j++;
                }*/
        ////////////////////////////ANTIBIOGRAMA////////////////////////////////////////////////////
        /* if (rs.getInt(16) == 3) {
                    if (caso_base2 == 0) {
                        matriz3[0][0] = rs.getString(12);
                        analisisA = rs.getString(5);
                        caso_base2 = 1;
                        m++;
                    } else {
                        if (m == 1) {
                            matriz3[w][m] = analisisA;
                            matriz3[w][2] = rs.getString(5);
                            m++;
                        } else {
                            if (m == 2) {
                                matriz3[w][3] = fechaPaciente;
                                matriz3[w][4] = rs.getString(5);
                                m = 5;
                            } else {
                                if (m < 8) {
                                    matriz3[w][m] = rs.getString(5);
                                    m++;
                                }
                            }
                        }
                    }
                }*/
        ////////////////////////////CULTIVO SIN ANTIBIOGRAMA////////////////////////////////////////////////////
        /*if (rs.getInt(16) == 4) {
                    if (caso_base3 == 0) {
                        matriz4[0][0] = rs.getString(12);
                        matriz4[0][6] = rs.getString(4);
                        analisisB = rs.getString(5) + " " + rs.getString(6) + " " + rs.getString(7);
                        caso_base3 = 1;
                        t++;
                    } else {
                        if (t == 1) {
                            matriz4[h][t] = analisisB;
                            matriz4[h][2] = rs.getString(5) + " " + rs.getString(6) + " " + rs.getString(7);
                            matriz4[0][7] = rs.getString(4);
                            t++;
                        } else {
                            if (t == 2) {
                                matriz4[h][3] = fechaPaciente;
                                matriz4[h][4] = rs.getString(5) + " " + rs.getString(6) + " " + rs.getString(7);
                                matriz4[0][8] = rs.getString(4);
                                t = 5;
                            } else {
                                if (t < 6) {
                                    matriz4[h][t] = rs.getString(5) + " " + rs.getString(6) + " " + rs.getString(7);
                                    matriz4[0][9] = rs.getString(4);
                                    t++;
                                } else {
                                    if (t == 6) {
                                        if (rs.getString(5) != null) {
                                            matriz4[0][10] = "Observación: " + rs.getString(5);
                                        } else {
                                            matriz4[0][10] = "Observación: ";
                                        }

                                    }
                                }
                            }
                        }
                    }
                }*/
        // }
        //  fila++;
        //}
        /////////////////////////////imprime reporte////////////////////////////////        
        LinkedList<camposinformes> Resultados = new LinkedList<camposinformes>();
        Resultados.clear();
        LinkedList<camposinformes2> Resultados2 = new LinkedList<camposinformes2>();
        Resultados2.clear();
        LinkedList<camposinformes3> Resultados3 = new LinkedList<camposinformes3>();
        Resultados3.clear();
        LinkedList<camposinformes4> Resultados4 = new LinkedList<camposinformes4>();
        Resultados4.clear();
        LinkedList<camposinformes5> Resultados5 = new LinkedList<camposinformes5>();
        Resultados5.clear();
        LinkedList<camposinformes> Resultados6 = new LinkedList<camposinformes>();
        Resultados6.clear();

        int n = tablaanalisis.getRowCount(), Bandera_Tipo = 0, bandera_tipo = 0, bandera_tipo2 = 0, bandera_tipo3 = 0, bandera_tipo4 = 0, Bandera_Tipo_2 = 0, contador_f = 0;
        System.out.println("Tipo --->" + tablaanalisis.getValueAt(0, 9).toString());
        if (tablaanalisis.getValueAt(0, 9).toString().equals("0")) {
            i = 0;
            System.out.println("--->0");
            while (i < n) {

                if (tablaanalisis.getValueAt(i, 2) == null) {
                    titulos = "";
                } else {
                    titulos = tablaanalisis.getValueAt(i, 2).toString();
                }
                analisis = tablaanalisis.getValueAt(i, 3).toString();
                resultados = "-";
                /*if (analisis.equals("Proteinas Totales")) {
                            resultadoProteina = tablaanalisis.getValueAt(i, 5).toString();
                            valorrefproteina = tablaanalisis.getValueAt(i, 7).toString();
                        }*/
                if (tablaanalisis.getValueAt(i, 4) == null) {
                    unidades = "";
                } else {
                    unidades = tablaanalisis.getValueAt(i, 4).toString();
                }
                if (tablaanalisis.getValueAt(i, 5) == null) {
                    referencias = "";
                } else {
                    referencias = tablaanalisis.getValueAt(i, 5).toString();
                }
                if (tablaanalisis.getValueAt(i, 6) == null) {
                    metodo = "Método: ";
                    observaciones = "Observaciones: ";
                } else {
                    metodo = "Método: " + tablaanalisis.getValueAt(i, 6).toString();
                    observaciones = "Observaciones: ";
                }
                //if (tablaanalisis.getValueAt(i, 6).toString().equals("0")) {
                camposinformes tipo;
                tipo = new camposinformes(titulos, analisis, resultados, unidades, referencias, observaciones, metodo);
                Resultados.add(tipo);
                Bandera_Tipo = 1;
                //}
                /* /////////////////////////////Fila 2 /////////////////////////////
                    if (tablaanalisis.getValueAt(i, 15).toString().equals("5")) {
                        if (!tablaanalisis.getValueAt(i, 13).toString().equals("0")) {
                            //  JOptionPane.showMessageDialog(null, tablaanalisis.getValueAt(i, 13).toString());
                            tipo1 = new camposinformes(titulos, analisis, resultados, unidades, referencias, observaciones, metodo);
                        } else {
                            tipo1 = new camposinformes(tablaanalisis.getValueAt(i, 12).toString(), analisis, resultados, unidades, referencias, observaciones, metodo);
                        }
                        Resultados6.add(tipo1);

                        Bandera_Tipo_2 = 5;
                        contador_f++;
                    }

                    ///////////////////////////////FILA 3 - HEMOGRAMA FIJO-////////////////////////////////////////
                    if (tablaanalisis.getValueAt(i, 15).toString().equals("6")) {
                        if (!tablaanalisis.getValueAt(i, 13).toString().equals("0")) {
                            //  JOptionPane.showMessageDialog(null, tablaanalisis.getValueAt(i, 13).toString());
                            tipo1 = new camposinformes(titulos, analisis, resultados, unidades, referencias, observaciones, metodo);
                        } else {
                            tipo1 = new camposinformes(tablaanalisis.getValueAt(i, 12).toString(), analisis, resultados, unidades, referencias, observaciones, metodo);
                        }
                        Resultados6.add(tipo1);

                        Bandera_Tipo_2 = 6;
                        contador_f++;
                    }

                    //////////////////////////////////////////////////////////////////////////////// 
                    /////////////////////////////////////////////////////////////////
                    if (tablaanalisis.getValueAt(i, 15).toString().equals("1")) {
                        bandera_tipo = 1;////columnas
                    }
                    if (tablaanalisis.getValueAt(i, 15).toString().equals("2")) {
                        ///   JOptionPane.showMessageDialog(null, "si 2");
                        bandera_tipo2 = 1;///PROTEINOGRAMA
                    }
                    if (tablaanalisis.getValueAt(i, 15).toString().equals("3")) {
                        ///   JOptionPane.showMessageDialog(null, "si 2");
                        bandera_tipo3 = 1;///BACTERIOLOGICOS
                    }
                    if (tablaanalisis.getValueAt(i, 15).toString().equals("4")) {
                        ///   JOptionPane.showMessageDialog(null, "si 2");
                        bandera_tipo4 = 1;///BACTERIOLOGICOS sin anti
                    }
                    i++;
                    observaciones = null;
                    metodo = null;*/
                i++;
            }
            System.out.println("--->1");
            /* if (bandera_tipo == 1) {
                    String aux = "";
                    int practicas = 0;
                    for (int x = 0; x < contadorfilas; x++) {
                        if (aux.equals("")) {
                            aux = matriz[x][0];
                        }
                        camposinformes2 tipo2;
                        if (matriz[x][0].equals("")) {
                            tipo2 = new camposinformes2("", "", "", "", "", "", "", "", "", "", "", "");
                        } else {
                            if (x == 0) {
                                System.out.println("--->" + 1);
                                tipo2 = new camposinformes2(matriz[x][0], matriz[x][1], matriz[x][2], matriz[x][3], matriz[x][4], matriz[x][5], matriz[x][6], matriz[x][7], matriz[x][8], matriz[x][9], matriz[x][10], observacion_practica[practicas]);
                                System.out.println(observacion_practica[practicas]);
                            } else {
                                if (matriz[x][0].equals(aux)) {
                                    System.out.println("--->" + 1);
                                    tipo2 = new camposinformes2(matriz[x][0], matriz[x][1], matriz[x][2], matriz[x][3], matriz[x][4], matriz[x][5], matriz[x][6], matriz[x][7], matriz[x][8], matriz[x][9], matriz[x][10], observacion_practica[practicas]);
                                    System.out.println(observacion_practica[practicas]);

                                } else {
                                    if (null != (observacion_practica[practicas + 1])) {
                                        practicas++;
                                    }

                                    System.out.println("--->" + 2);
                                    tipo2 = new camposinformes2(matriz[x][0], matriz[x][1], matriz[x][2], matriz[x][3], matriz[x][4], matriz[x][5], matriz[x][6], matriz[x][7], matriz[x][8], matriz[x][9], matriz[x][10], observacion_practica[practicas]);
                                    System.out.println(observacion_practica[practicas]);

                                }
                            }
                        }
                        Resultados2.add(tipo2);
                    }
                }*/
 /* if (bandera_tipo2 == 1) {
                    for (int x = 0; x < 6; x++) {
                        camposinformes3 tipo3;
                        System.out.println("matriz2 x8=" + matriz2[x][8]);
                        tipo3 = new camposinformes3(matriz2[x][0], matriz2[x][1], matriz2[x][2], matriz2[x][3], matriz2[x][4], matriz2[x][5], matriz2[x][6], matriz2[x][7], String.valueOf(Math.round(Double.valueOf(matriz2[x][8]) * 100) / 100.0));
                        Resultados3.add(tipo3);
                    }
                }*/
            ///////////////////////////////////////////////////
            /* if (bandera_tipo3 == 1) {
                    for (int x = 0; x < 1; x++) {
                        camposinformes4 tipo4;
                        tipo4 = new camposinformes4(matriz3[x][0], matriz3[x][1], matriz3[x][2], matriz3[x][4], matriz3[x][5], matriz3[x][7], matriz3[x][3], matriz3[x][6]);
                        Resultados4.add(tipo4);
                    }
                }*/
            //////////////////////////
            /*if (bandera_tipo4 == 1) {
                    for (int x = 0; x < 1; x++) {
                        camposinformes5 tipo5;
                        tipo5 = new camposinformes5(matriz4[x][0], matriz4[x][1], matriz4[x][2], matriz4[x][4], matriz4[x][5], matriz4[x][3], matriz4[x][6], matriz4[x][7], matriz4[x][8], matriz4[x][9], matriz4[x][10]);
                        Resultados5.add(tipo5);
                    }
                }*/

        }
        System.out.println("----------------------------------------------------------------------------");
        String practica_aux = "";
        int columna = 0, contador_aux1 = 0, contador_aux2 = 0, J = 0;
        if (tablaanalisis.getValueAt(0, 9).toString().equals("1")) {
            i = 0;
            while (J < n) {
                System.out.println("fila " + i);
                if (practica_aux.equals("")) {///caso base de primera practica                    
                    practica_aux = tablaanalisis.getValueAt(J, 7).toString();
                    System.out.println("practica_aux " + practica_aux);
                    System.out.println("titulos " + titulos);
                    if (titulos.equals("")) {
                        titulos = tablaanalisis.getValueAt(J, 2).toString();///titulo                 
                        System.out.println("titulos " + titulos);
                        matriz[i][0] = practica_aux;
                        matriz[i][1] = titulos;
                        contadorfilas++;
                        if (!tablaanalisis.getValueAt(J, 8).toString().equals(" ")) {//verifico codigo interno si cambia
                            matriz[i][2] = tablaanalisis.getValueAt(J, 3).toString();//analisis
                            matriz[i][5] = "-";//resultado
                        } else {
                            matriz[i][2] = "";//analisis
                            matriz[i][5] = "-";//resultado
                        }
                        System.out.println("analisis " + tablaanalisis.getValueAt(J, 3).toString());
                        if (tablaanalisis.getValueAt(J, 5).toString() != null) {
                            matriz[i][7] = tablaanalisis.getValueAt(J, 5).toString();// Valor de refernecia
                        } else {
                            matriz[i][7] = "";// Valor de refernecia
                        }
                        if (tablaanalisis.getValueAt(J, 4).toString() != null) {// Unidad
                            matriz[i][9] = tablaanalisis.getValueAt(J, 4).toString();// Unidad
                        } else {
                            matriz[i][9] = "";// Unidad
                        }

                    } else {
                        System.out.println("columna " + columna);
                        if (columna == 0) {
                            System.out.println("titulos " + titulos);
                            if (titulos.equals(tablaanalisis.getValueAt(J, 2).toString())) {
                                columna = 0;
                                System.out.println("titulos " + titulos);
                                matriz[i][0] = tablaanalisis.getValueAt(J, 7).toString();//practica
                                matriz[i][1] = titulos;
                                contadorfilas++;
                                if (!tablaanalisis.getValueAt(J, 8).toString().equals(" ")) {//codigo interno
                                    matriz[i][2] = tablaanalisis.getValueAt(J, 3).toString();//analisis
                                    matriz[i][5] = "-";//resultado
                                } else {
                                    matriz[i][2] = "";//analisis
                                    matriz[i][5] = "-";//resultado
                                }
                                System.out.println("analisis " + tablaanalisis.getValueAt(J, 3).toString());
                                if (tablaanalisis.getValueAt(J, 5).toString() != null) {// Valor de refernecia
                                    matriz[i][7] = tablaanalisis.getValueAt(J, 5).toString();// Valor de refernecia
                                } else {
                                    matriz[i][7] = "";// Valor de refernecia
                                }
                                if (tablaanalisis.getValueAt(J, 4).toString() != null) {// Unidad
                                    matriz[i][9] = tablaanalisis.getValueAt(J, 4).toString();// Unidad
                                } else {
                                    matriz[i][9] = "";// Unidad
                                }
                            } else {
                                contador_aux1 = i;
                                columna = 1;
                                i = contador_aux2;
                                titulos = tablaanalisis.getValueAt(J, 2).toString();///titulo                 
                                matriz[i][3] = titulos;
                                System.out.println("titulos " + titulos);
                                if (!tablaanalisis.getValueAt(J, 8).toString().equals(" ")) {///codigo interno
                                    matriz[i][4] = tablaanalisis.getValueAt(J, 3).toString();//analisis
                                    matriz[i][6] = "-";
                                } else {
                                    matriz[i][4] = "";
                                    matriz[i][6] = "";
                                }
                                System.out.println("analisis " + tablaanalisis.getValueAt(J, 3).toString());
                                if (tablaanalisis.getValueAt(J, 5).toString() != null) {// Valor de refernecia
                                    matriz[i][8] = tablaanalisis.getValueAt(J, 5).toString();// Valor de refernecia
                                } else {
                                    matriz[i][8] = "";
                                }
                                if (tablaanalisis.getValueAt(J, 4).toString() != null) {//unidad
                                    matriz[i][10] = tablaanalisis.getValueAt(J, 4).toString();// Unidad
                                } else {
                                    matriz[i][10] = "";
                                }
                            }
                        } else {
                            System.out.println("titulos " + titulos);
                            if (titulos.equals(tablaanalisis.getValueAt(J, 2).toString())) {//titulo
                                columna = 1;
                                matriz[i][3] = titulos;
                                System.out.println("titulos " + titulos);
                                if (!tablaanalisis.getValueAt(J, 8).toString().equals(" ")) {//codigo interno
                                    matriz[i][4] = tablaanalisis.getValueAt(J, 3).toString();//analisis
                                    matriz[i][6] = "-";
                                } else {
                                    matriz[i][4] = "";
                                    matriz[i][6] = "";
                                }
                                System.out.println("analisis " + tablaanalisis.getValueAt(J, 3).toString());
                                if (tablaanalisis.getValueAt(J, 5).toString() != null) {// Valor de refernecia
                                    matriz[i][8] = tablaanalisis.getValueAt(J, 5).toString();// Valor de refernecia
                                } else {
                                    matriz[i][8] = "";
                                }
                                if (tablaanalisis.getValueAt(J, 4).toString() != null) {//unidad
                                    matriz[i][10] = tablaanalisis.getValueAt(J, 4).toString();// Unidad
                                } else {
                                    matriz[i][10] = "";
                                }
                            } else {

                                contador_aux2 = i;
                                columna = 0;
                                i = contador_aux1;
                                titulos = tablaanalisis.getValueAt(J, 2).toString();///titulo 
                                System.out.println("titulos " + titulos);
                                matriz[i][0] = tablaanalisis.getValueAt(J, 7).toString();//practica
                                matriz[i][1] = titulos;
                                contadorfilas++;
                                if (!tablaanalisis.getValueAt(J, 8).toString().equals(" ")) {//codigo interno
                                    matriz[i][2] = tablaanalisis.getValueAt(J, 3).toString();//analisis
                                    matriz[i][5] = "-";
                                } else {
                                    matriz[i][2] = "";
                                    matriz[i][5] = "";
                                }
                                System.out.println("analisis " + tablaanalisis.getValueAt(J, 3).toString());
                                if (tablaanalisis.getValueAt(J, 5).toString() != null) {// Valor de refernecia
                                    matriz[i][7] = tablaanalisis.getValueAt(J, 5).toString();// Valor de refernecia
                                } else {
                                    matriz[i][7] = "";
                                }
                                if (tablaanalisis.getValueAt(J, 4).toString() != null) {//unidad
                                    matriz[i][9] = tablaanalisis.getValueAt(J, 4).toString();// Unidad
                                } else {
                                    matriz[i][9] = "";
                                }
                            }
                        }
                    }
                } else {
                    System.out.println("practica_aux " + practica_aux);
                    if (practica_aux.equals(tablaanalisis.getValueAt(J, 7).toString())) {//practica
                        System.out.println("practica_aux " + practica_aux);
                        if (titulos.equals("")) {
                            titulos = tablaanalisis.getValueAt(J, 2).toString();///titulo 
                            System.out.println("titulos " + titulos);
                            matriz[i][0] = tablaanalisis.getValueAt(J, 7).toString();//practica
                            matriz[i][1] = titulos;
                            contadorfilas++;
                            if (!tablaanalisis.getValueAt(J, 8).toString().equals(" ")) {//codigo interno
                                matriz[i][2] = tablaanalisis.getValueAt(J, 3).toString();//analisis
                                matriz[i][5] = "-";
                            } else {
                                matriz[i][2] = "";
                                matriz[i][5] = "";
                            }
                            System.out.println("analisis " + tablaanalisis.getValueAt(J, 3).toString());
                            if (tablaanalisis.getValueAt(J, 5).toString() != null) {// Valor de refernecia
                                matriz[i][7] = tablaanalisis.getValueAt(J, 5).toString();// Valor de refernecia
                            } else {
                                matriz[i][7] = "";
                            }
                            if (tablaanalisis.getValueAt(J, 4).toString() != null) {//unidad
                                matriz[i][9] = tablaanalisis.getValueAt(J, 4).toString();// Unidad
                            } else {
                                matriz[i][9] = "";
                            }
                        } else {
                            System.out.println("columna " + columna);
                            if (columna == 0) {
                                System.out.println("titulos 1 " + titulos);
                                System.out.println("titulos 2 " + tablaanalisis.getValueAt(J, 2).toString());
                                if (titulos.equals(tablaanalisis.getValueAt(J, 2).toString())) {//titulo
                                    System.out.println("titulos " + titulos);
                                    columna = 0;
                                    matriz[i][0] = practica_aux;
                                    matriz[i][1] = titulos;
                                    contadorfilas++;
                                    if (!tablaanalisis.getValueAt(J, 8).toString().equals(" ")) {//codigo interno
                                        matriz[i][2] = tablaanalisis.getValueAt(J, 3).toString();//analisis
                                        matriz[i][5] = "-";
                                    } else {
                                        matriz[i][2] = "";
                                        matriz[i][5] = "";
                                    }
                                    System.out.println("analisis " + tablaanalisis.getValueAt(J, 3).toString());
                                    if (tablaanalisis.getValueAt(J, 5).toString() != null) {// Valor de refernecia
                                        matriz[i][7] = tablaanalisis.getValueAt(J, 5).toString();// Valor de refernecia
                                    } else {
                                        matriz[i][7] = "";
                                    }
                                    if (tablaanalisis.getValueAt(J, 4).toString() != null) {//unidad
                                        matriz[i][9] = tablaanalisis.getValueAt(J, 4).toString();// Unidad
                                    } else {
                                        matriz[i][9] = "";
                                    }
                                } else {
                                    contador_aux1 = i;
                                    columna = 1;
                                    i = contador_aux2;
                                    titulos = tablaanalisis.getValueAt(J, 2).toString();///titulo 
                                    System.out.println("titulos 3" + titulos);
                                    matriz[i][3] = titulos;
                                    if (!tablaanalisis.getValueAt(J, 8).toString().equals(" ")) {//codigo interno
                                        matriz[i][4] = tablaanalisis.getValueAt(J, 3).toString();//analisis
                                        matriz[i][6] = "-";
                                    } else {
                                        matriz[i][4] = "";
                                        matriz[i][6] = "";
                                    }
                                    System.out.println("analisis " + tablaanalisis.getValueAt(J, 3).toString());
                                    if (tablaanalisis.getValueAt(J, 5).toString() != null) {// Valor de refernecia
                                        matriz[i][8] = tablaanalisis.getValueAt(J, 5).toString();// Valor de refernecia
                                    } else {
                                        matriz[i][8] = "";
                                    }
                                    if (tablaanalisis.getValueAt(J, 4).toString() != null) {//unidad
                                        matriz[i][10] = tablaanalisis.getValueAt(J, 4).toString();// Unidad
                                    } else {
                                        matriz[i][10] = "";
                                    }
                                }
                            } else {
                                if (titulos.equals(tablaanalisis.getValueAt(J, 2).toString())) {//titulo
                                    System.out.println("columna " + columna);

                                    columna = 1;
                                    matriz[i][3] = titulos;
                                    System.out.println("titulos " + titulos);
                                    if (!tablaanalisis.getValueAt(J, 8).toString().equals(" ")) {//codigo interno
                                        matriz[i][4] = tablaanalisis.getValueAt(J, 3).toString();//analisis
                                        matriz[i][6] = "-";
                                    } else {
                                        matriz[i][4] = "";
                                        matriz[i][6] = "";
                                    }
                                    System.out.println("analisis " + tablaanalisis.getValueAt(J, 3).toString());
                                    if (tablaanalisis.getValueAt(J, 5).toString() != null) {// Valor de refernecia
                                        matriz[i][8] = tablaanalisis.getValueAt(J, 5).toString();// Valor de refernecia
                                    } else {
                                        matriz[i][8] = "";
                                    }
                                    if (tablaanalisis.getValueAt(J, 4).toString() != null) {
                                        matriz[i][10] = tablaanalisis.getValueAt(J, 4).toString();// Unidad
                                    } else {
                                        matriz[i][10] = "";
                                    }
                                } else {
                                    contador_aux2 = i;
                                    columna = 0;
                                    System.out.println("columna " + columna);
                                    i = contador_aux1;
                                    titulos = tablaanalisis.getValueAt(J, 2).toString();///titulo 
                                    System.out.println("titulos " + titulos);
                                    matriz[i][0] = tablaanalisis.getValueAt(J, 7).toString();//practica
                                    matriz[i][1] = titulos;
                                    contadorfilas++;
                                    if (!tablaanalisis.getValueAt(J, 8).toString().equals(" ")) {//codigo interno
                                        matriz[i][2] = tablaanalisis.getValueAt(J, 3).toString();//analisis
                                        matriz[i][5] = "-";
                                    } else {
                                        matriz[i][2] = "";
                                        matriz[i][5] = "";
                                    }
                                    System.out.println("analisis " + tablaanalisis.getValueAt(J, 3).toString());
                                    if (tablaanalisis.getValueAt(J, 5).toString() != null) {// Valor de refernecia
                                        matriz[i][7] = tablaanalisis.getValueAt(J, 5).toString();// Valor de refernecia
                                    } else {
                                        matriz[i][7] = "";
                                    }
                                    if (tablaanalisis.getValueAt(J, 4).toString() != null) {//unidad
                                        matriz[i][9] = tablaanalisis.getValueAt(J, 4).toString();// Unidad
                                    } else {
                                        matriz[i][9] = "";
                                    }
                                }
                            }
                        }
                    } else {
                        practica_aux = tablaanalisis.getValueAt(J, 7).toString();//practica
                        System.out.println("practica_aux " + practica_aux);
                        if (i > contador_aux1 && i > contador_aux2) {
                            contador_aux2 = i;
                            contador_aux1 = i;
                            columna = 0;
                        }
                        if (contador_aux1 > contador_aux2 && contador_aux1 > i) {
                            contador_aux2 = contador_aux1;
                            i = contador_aux1;
                            columna = 0;
                        }
                        if (contador_aux2 > contador_aux1 && contador_aux2 > i) {

                            contador_aux1 = contador_aux2;
                            i = contador_aux2;
                            columna = 0;
                        }
                        titulos = tablaanalisis.getValueAt(J, 2).toString();///titulo 
                        System.out.println("titulos " + titulos);
                        matriz[i][0] = tablaanalisis.getValueAt(J, 7).toString();//practica
                        matriz[i][1] = titulos;
                        contadorfilas++;
                        if (!tablaanalisis.getValueAt(J, 8).toString().equals(" ")) {//codigo interno
                            matriz[i][2] = tablaanalisis.getValueAt(J, 3).toString();//analisis
                            matriz[i][5] = "-";
                        } else {
                            matriz[i][2] = "";
                            matriz[i][5] = "";
                        }
                        System.out.println("analisis " + tablaanalisis.getValueAt(J, 3).toString());
                        if (tablaanalisis.getValueAt(J, 5).toString() != null) {// Valor de refernecia
                            matriz[i][7] = tablaanalisis.getValueAt(J, 5).toString();// Valor de refernecia
                        } else {
                            matriz[i][7] = "";
                        }
                        if (tablaanalisis.getValueAt(J, 4).toString() != null) {// Unidad
                            matriz[i][9] = tablaanalisis.getValueAt(J, 4).toString();// Unidad
                        } else {
                            matriz[i][9] = "";
                        }
                        ///////////////////////////////////////////

                    }
                }
                J++;
                i++;
            }
            String aux = "";
            int practicas = 0;
            for (int x = 0; x < contadorfilas; x++) {
                if (aux.equals("")) {
                    aux = matriz[x][0];
                }
                camposinformes2 tipo2;
                if (matriz[x][0].equals("")) {
                    tipo2 = new camposinformes2("", "", "", "", "", "", "", "", "", "", "", "");
                } else {
                    if (x == 0) {
                        System.out.println("--->" + 1);
                        tipo2 = new camposinformes2(matriz[x][0], matriz[x][1], matriz[x][2], matriz[x][3], matriz[x][4], matriz[x][5], matriz[x][6], matriz[x][7], matriz[x][8], matriz[x][9], matriz[x][10], "");
                    } else {
                        if (matriz[x][0].equals(aux)) {
                            System.out.println("--->" + 2);
                            tipo2 = new camposinformes2(matriz[x][0], matriz[x][1], matriz[x][2], matriz[x][3], matriz[x][4], matriz[x][5], matriz[x][6], matriz[x][7], matriz[x][8], matriz[x][9], matriz[x][10], "");
                        } else {
                            if (null != (observacion_practica[practicas + 1])) {
                                practicas++;
                            }
                            System.out.println("--->" + 3);
                            tipo2 = new camposinformes2(matriz[x][0], matriz[x][1], matriz[x][2], matriz[x][3], matriz[x][4], matriz[x][5], matriz[x][6], matriz[x][7], matriz[x][8], matriz[x][9], matriz[x][10], "");
                        }
                    }
                }
                Resultados2.add(tipo2);
            }
            if (tablaanalisis.getValueAt(0, 9).toString().equals("1")) {
                bandera_tipo = 1;////columnas
            }
        }
        ////////////////////////////PROTEINOGRAMA
        if (tablaanalisis.getValueAt(0, 9).toString().equals("2")) {
            System.out.println("--->  Entra");
            j = 0;
            k = 0;
            i = 0;
            bandera_tipo2 = 1;
            while (i < n) {
                if (caso_base == 0) {
                    practicaP = tablaanalisis.getValueAt(i, 7).toString();//practica
                    analisisP = "-";//////caso base del proteinograma
                    referenciaP = tablaanalisis.getValueAt(i, 5).toString();
                    caso_base = 1;
                    j--;
                } else {
                    /// JOptionPane.showMessageDialog(null, j);
                    if (j < 6) {
                        matriz2[j][0] = practicaP;
                        matriz2[j][1] = analisisP;
                        matriz2[j][2] = referenciaP;////////////cargo los datos en la matriz de la primera hasta sexta comlumna
                        matriz2[j][3] = tablaanalisis.getValueAt(i, 3).toString();
                        matriz2[j][4] = "-";
                        matriz2[j][5] = tablaanalisis.getValueAt(i, 5).toString();

                        ///System.out.print(matriz2[j][0] + " " + matriz2[j][1] + " " + matriz2[j][2] + " " + matriz2[j][3] + " " + matriz2[j][4] + " " + matriz2[j][5] + " " + matriz2[j][6] + " " + matriz2[j][7] + " " + matriz2[j][8] +"\n");
                    } else {
                        if (k < 6) {
                            matriz2[k][6] = "-";
                            matriz2[k][7] = tablaanalisis.getValueAt(i, 5).toString();////////////cargo los datos en la matriz de la septima hasta la octava comlumna
                        }
///                                System.out.print(matriz2[k][0] + " " + matriz2[k][1] + " " + matriz2[k][2] + " " + matriz2[k][3] + " " + matriz2[k][4] + " " + matriz2[k][5] + " " + matriz2[k][6] + " " + matriz2[k][7] + " " + matriz2[k][8]+ "\n");
                        k++;
                    }
                }
                if (j == 12) {
                    matriz2[0][8] = "-";
                    matriz2[1][8] = "-";
                    matriz2[2][8] = "-";
                    matriz2[3][8] = "-";////////////cargo los datos en la matriz de la ultima comlumna
                    matriz2[4][8] = "-";
                    matriz2[5][8] = "-";
///                            System.out.print(matriz2[k][0] + " " + matriz2[k][1] + " " + matriz2[k][2] + " " + matriz2[k][3] + " " + matriz2[k][4] + " " + matriz2[k][5] + " " + matriz2[k][6] + " " + matriz2[k][7] + " " + matriz2[k][8]);
                }
                j++;
                i++;
            }
        }
        if (bandera_tipo2 == 1) {
            for (int x = 0; x < 6; x++) {
                camposinformes3 tipo3;
                System.out.println("matriz2 x8=" + matriz2[x][8]);
                tipo3 = new camposinformes3(matriz2[x][0], matriz2[x][1], matriz2[x][2], matriz2[x][3], matriz2[x][4], matriz2[x][5], matriz2[x][6], matriz2[x][7], "-");
                Resultados3.add(tipo3);
            }
        }

        /////////////////////////BACTERIOLOGICOS///ANTIBIOGRAMA////////////////////////////////////////////////////
        if (tablaanalisis.getValueAt(0, 9).toString().equals("3")) {
            i = 0;
            m = 0;
            while (i < n) {
                if (caso_base2 == 0) {
                    matriz3[0][0] = tablaanalisis.getValueAt(i, 7).toString();
                    analisisA = "-";
                    caso_base2 = 1;
                    m++;
                } else {
                    if (m == 1) {
                        matriz3[0][m] = analisisA;
                        matriz3[0][2] = "-";
                        m++;
                    } else {
                        if (m == 2) {
                            matriz3[0][3] = "01-01-2019";
                            matriz3[0][4] = "-";
                            m = 5;
                        } else {
                            if (m < 8) {
                                matriz3[0][m] = "-";
                                m++;
                            } else {
                                if (m >= 8) {
                                    if (m == 12) {
                                        matriz3[0][m] = "ANTIBIOGRAMA";
                                    } else {
                                        matriz3[0][m] = tablaanalisis.getValueAt(i, 3).toString();
                                    }
                                    m++;
                                }
                            }
                        }
                    }
                }
                i++;
            }
            for (int x = 0; x < 1; x++) {
                camposinformes4 tipo4;
                tipo4 = new camposinformes4(matriz3[x][0], matriz3[x][1], matriz3[x][2], matriz3[x][4], matriz3[x][5], matriz3[x][7], matriz3[x][3], matriz3[x][6]);
                Resultados4.add(tipo4);
            }
            bandera_tipo3 = 1;///BACTERIOLOGICOS
        }

        if (tablaanalisis.getValueAt(0, 9).toString().equals("4")) {
            i = 0;
            while (i < n) {
                if (caso_base3 == 0) {
                    matriz4[0][0] = tablaanalisis.getValueAt(i, 7).toString();
                    matriz4[0][6] = tablaanalisis.getValueAt(i, 3).toString();
                    analisisB = "-" + " " + tablaanalisis.getValueAt(i, 4).toString() + " " + tablaanalisis.getValueAt(i, 5).toString();
                    caso_base3 = 1;
                    t++;
                } else {
                    if (t == 1) {
                        matriz4[h][t] = analisisB;
                        matriz4[h][2] = "-" + " " + tablaanalisis.getValueAt(i, 4).toString() + " " + tablaanalisis.getValueAt(i, 5).toString();
                        matriz4[0][7] = tablaanalisis.getValueAt(i, 3).toString();
                        t++;
                    } else {
                        if (t == 2) {
                            matriz4[h][3] = "01/01/2019";//fechaPaciente;
                            matriz4[h][4] = "-" + " " + tablaanalisis.getValueAt(i, 4).toString() + " " + tablaanalisis.getValueAt(i, 5).toString();
                            matriz4[0][8] = tablaanalisis.getValueAt(i, 3).toString();
                            t = 5;
                        } else {
                            if (t < 6) {
                                matriz4[h][t] = "-" + " " + tablaanalisis.getValueAt(i, 4).toString() + " " + tablaanalisis.getValueAt(i, 5).toString();
                                matriz4[0][9] = tablaanalisis.getValueAt(i, 3).toString();
                                t++;
                            } else {
                                if (t == 6) {
                                    matriz4[0][10] = "Observación: ";
                                }
                            }
                        }
                    }
                }
            }
        }
        //////////////////////////////////////////////Reportes segun el envio ////////////////////////
        //if (enviar == 3) {
        //////////// Vista Previa
        //if (es_nulo== 0) {
        try {
            //////////////////////////////////////////////////////////////////
            String sql7 = "Select formato, logo, nombre, direccion, telefono, mail, observacion, logo_horizontal1,observacion2 from reportes";
            Statement St7 = cn.createStatement();
            ResultSet rs7 = St7.executeQuery(sql7);

            rs7.next();
            ///////////////////////////////////////////////////////////////////               
            if (rs7.getString(1).equals("A5")) {
                System.out.println("--->2");
                String observacion2 = rs7.getString("observacion2");
                System.out.println("A5");
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                String medico = "", especialidad = "", motivo = "", lugar = "", matricula = "", hora = "";
                int caratula;

                caratula = rs7.getInt(8);
                ////////////////////////////////////////////
                String sql3 = "SELECT lugar_lab, nombre_lab FROM licencia";
                Statement St2 = cn.createStatement();
                ResultSet rs2 = St2.executeQuery(sql3);
                rs2.next();
                lugar = rs2.getString(1);
                matricula = rs2.getString(2);
                //////////////////////////////////
                byte[] img = rs7.getBytes(2);
                if (img != null) {
                    try {
                        imagen2 = ConvertirImagen(img);
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(null, ex);
                    }
                    Icon icon = new ImageIcon(imagen2);
                } else {
                    imagen2 = null;
                }
                /////////////////////// Defino la previsualizacion ///////////////////
                JDialog viewer = new JDialog(new javax.swing.JFrame(), "Reporte", true);
                viewer.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
                viewer.setSize(800, 600);
                viewer.setLocationRelativeTo(null);
                System.out.println("previsualizacion definida");
                /////////////////// Definimos las variables
                JasperPrint jPrintCaratula = null;
                JasperPrint jPrintFilas1 = null;
                JasperPrint jPrintFilas2 = null;
                JasperPrint jPrintFilas = null;
                JasperPrint jPrintColumnas = null;
                JasperPrint jPrintProteinograma = null;
                JasperPrint jPrintBacteriologico = null;
                JasperPrint jPrintBacteriologico2 = null;
                JasperPrint analisis_completo = null;
                JasperViewer jv = null;
                System.out.println("vareables definidas");
                /////////////////////////Defino los Parametros///////////////////////////////////
                Map parametros = new HashMap();
                parametros.put("protocolo", "00000001");
                parametros.put("logo", imagen2);
                parametros.put("laboratorio", rs7.getString(3));
                parametros.put("paciente", "Nombre Paciente");
                parametros.put("medico", "Nombre Medico");
                parametros.put("especialidad", "");
                parametros.put("motivo", "Motivo");
                parametros.put("lugar", "");
                parametros.put("dni", "Nº de Documento");
                parametros.put("fecha", "01/01/2019");
                parametros.put("fecha_impresion", "01/01/2019");
                parametros.put("hora", "");
                parametros.put("direccion", "");
                parametros.put("telefono", "");
                parametros.put("mail", "");
                parametros.put("observacion", "");
                parametros.put("observacion2", observacion2);
                System.out.println("parametros definidos");

                /////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////CARATULA//////////////////////////////////////////////////////////////////////////////  
                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (caratula == 1) {
                    JasperReport CARATULA = null;
                    System.out.println("arma caratula");
                    if (matricula.equals("80")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1GER.jasper"));
                    }
                    if (matricula.equals("1165")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("424")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("1349")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("1290")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1DANT.jasper"));
                    }
                    if (matricula.equals("1340")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("1376")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1PATA.jasper"));
                    }
                    if (matricula.equals("127")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("20009")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1YANE.jasper"));
                    }
                    if (matricula.equals("1066")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("1202")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("1114")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1GER.jasper"));
                    }
                    if (matricula.equals("99999")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("1088")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("1278")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("482")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1YANE.jasper"));
                    }
                    if (matricula.equals("1073")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("20220")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("1515")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1_METTOLA.jasper"));
                    }
                    if (matricula.equals("1366")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1.jasper"));
                    }
                    if (matricula.equals("283")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1SAND.jasper"));
                    }
                    if (matricula.equals("941")) {
                        CARATULA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_1SAND.jasper"));
                    }

                    jPrintCaratula = JasperFillManager.fillReport(CARATULA, parametros, new JREmptyDataSource());
                }
                /////////////////////////////////////////////////////////////////////////////////////////////
                ///////////////////////FILAS///////////////////////////////////////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                System.out.println("Bandera_Tipo " + Bandera_Tipo);
                if (Bandera_Tipo == 1) {
                    System.out.println("--->3");
                    System.out.println("arma filas");
                    JasperReport FILAS = null;
                    //1
                    if (matricula.equals("80")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("1165")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("424")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("1349")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("1290")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("1340")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("1376")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1PATA.jasper"));
                    }

                    if (matricula.equals("127")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("20009")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1_YANE.jasper"));
                    }

                    if (matricula.equals("1066")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("1202")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("1114")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("99999")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1.jasper"));
                    }

                    if (matricula.equals("1088")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1.jasper"));
                    }

                    if (matricula.equals("1278")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("482")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("447")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1BEN.jasper"));
                    }

                    if (matricula.equals("1073")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("20812")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1MAY.jasper"));
                    }

                    if (matricula.equals("1343")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }

                    if (matricula.equals("1515")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1MET.jasper"));
                    }
                    if (matricula.equals("1366")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1GER.jasper"));
                    }
                    if (matricula.equals("283")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1_1578.jasper"));
                    }

                    if (matricula.equals("1578")) {
                        //FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1_1578.jasper"));
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1_1578.jasper"));
                        // JOptionPane.showMessageDialog(null,dni);
                    }
                    if (matricula.equals("941")) {
                        FILAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1MAY.jasper"));
                    }
                    //////////////////////////////////////////////////////////////////////////////////
                    jPrintFilas = JasperFillManager.fillReport(FILAS, parametros, new JRBeanCollectionDataSource(Resultados));

                }

                /////////////////////COLUMNAS//////////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////
                System.out.println("--->bandera_tipo " + bandera_tipo);
                if (bandera_tipo == 1) {
                    System.out.println("arma columnas");
                    JasperReport COLUMNAS = null;
                    //   JOptionPane.showMessageDialog(null, "entro en columna");

                    if (matricula.equals("80")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("1165")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("424")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("1349")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("1290")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("1340")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("1376")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("127")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("20009")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2YANE.jasper"));
                    }

                    if (matricula.equals("1066")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("1202")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("1114")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("99999")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("1088")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("1278")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("482")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("447")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("1073")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("20812")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2MAY.jasper"));
                    }

                    if (matricula.equals("1343")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }

                    if (matricula.equals("1515")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2MET.jasper"));
                    }
                    if (matricula.equals("1366")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2GER.jasper"));
                    }
                    if (matricula.equals("283")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2_1578.jasper"));
                    }

                    if (matricula.equals("1578")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2_1578.jasper"));
                    }
                    if (matricula.equals("941")) {
                        COLUMNAS = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_2MAY.jasper"));
                    }
                    System.out.println("jPrintColumnas");
                    ////////////////////////////////////////////////////////////////////////////////////
                    jPrintColumnas = JasperFillManager.fillReport(COLUMNAS, parametros, new JRBeanCollectionDataSource(Resultados2));
                }
                System.out.println("--->4");
                /* ///////////////////////////////////////Fila2 - cultivo/////////////////////////
                    if (Bandera_Tipo_2 == 5) {
                        System.out.println("arma fila 2 cultivo");
                        JasperReport FILAS1 = null;

                        if (matricula.equals("20009")) {
                            FILAS1 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1_1_1.jasper"));
                        } else {
                            FILAS1 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1_1578_1.jasper"));
                        }
                        //////////////////////////////////////////////////////////////////////////////////
                        jPrintFilas1 = JasperFillManager.fillReport(FILAS1, parametros, new JRBeanCollectionDataSource(Resultados6));
                    }

                    ///////////////////////////////////////Fila2 - HEMOGRAMA FIJO/////////////////////////
                    if (Bandera_Tipo_2 == 6) {

                        parametros.put("eri", Resultados6.get(0).getResultados());
                        parametros.put("hema", Resultados6.get(1).getResultados());
                        parametros.put("hemo", Resultados6.get(2).getResultados());
                        parametros.put("vcm", Resultados6.get(3).getResultados());
                        parametros.put("chcm", Resultados6.get(4).getResultados());
                        parametros.put("hcm", Resultados6.get(5).getResultados());
                        parametros.put("leuco", Double.valueOf(Resultados6.get(6).getResultados()));
                        parametros.put("cay", Double.valueOf(Resultados6.get(7).getResultados()));
                        parametros.put("neutro", Double.valueOf(Resultados6.get(8).getResultados()));
                        parametros.put("baso", Double.valueOf(Resultados6.get(9).getResultados()));
                        parametros.put("eosi", Double.valueOf(Resultados6.get(10).getResultados()));
                        parametros.put("linfo", Double.valueOf(Resultados6.get(11).getResultados()));
                        parametros.put("mono", Double.valueOf(Resultados6.get(12).getResultados()));
                        //JOptionPane.showMessageDialog(null, Resultados6.get(12).getResultados());
                        JasperReport FILAS2 = null;
                        if (matricula.equals("1578")) {
                            FILAS2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_1_1578_1.jasper"));
                        }

                        if (matricula.equals("20009")) {
                            FILAS2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_YANE.jasper"));
                            //JOptionPane.showMessageDialog(null, Resultados6.get(12).getResultados());
                        }
                        //////////////////////////////////////////////////////////////////////////////////
                        //jPrintFilas1 = JasperFillManager.fillReport(FILAS2, parametros, new JRBeanCollectionDataSource(Resultados6));
                        jPrintFilas2 = JasperFillManager.fillReport(FILAS2, parametros, new JREmptyDataSource());
                        //JasperViewer.viewReport(jPrintFilas2, true);
                    }

                 */
                //////////////////////////////////////////////////////////////////////////////////////////////
                ///////////////////////////PROTEINOGRAMA/////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (bandera_tipo2 == 1) {///3
                    System.out.println("arma proteinograma");
                    JasperReport PROTEINOGRAMA = null;

                    if (matricula.equals("80")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }

                    if (matricula.equals("1165")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }

                    if (matricula.equals("424")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }

                    if (matricula.equals("1349")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }

                    if (matricula.equals("1290")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("1340")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("1376")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("127")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("20009")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3YANE.jasper"));
                    }
                    if (matricula.equals("1066")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("1202")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("1114")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("99999")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("1088")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("1278")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("482")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("447")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("1073")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("20812")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3MAY.jasper"));
                    }
                    if (matricula.equals("1343")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("1515")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3MET.jasper"));
                    }
                    if (matricula.equals("1366")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3GER.jasper"));
                    }
                    if (matricula.equals("283")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_4_1578.jasper"));
                    }
                    if (matricula.equals("1578")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_4_1578.jasper"));
                    }
                    if (matricula.equals("941")) {
                        PROTEINOGRAMA = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_3MAY.jasper"));
                    }
                    //////////////////////////////////////////
                    jPrintProteinograma = JasperFillManager.fillReport(PROTEINOGRAMA, parametros, new JRBeanCollectionDataSource(Resultados3));
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////
                ///////////////////////BACTERIOLOGICOS/////////////////////////////////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////////////////////////////
                if (bandera_tipo3 == 1) {///4
                    System.out.println("arma bacteriologicos");
                    JasperReport BACTERIOLOGICO = null;

                    if (matricula.equals("80")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("1165")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("424")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("1349")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("1290")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("1340")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("1376")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("127")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("20009")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5YANE.jasper"));
                    }
                    if (matricula.equals("1066")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("1202")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("1114")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("99999")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("1088")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("1278")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("482")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("447")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("1073")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("20812")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5MAY.jasper"));
                    }
                    if (matricula.equals("1343")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("1515")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("1366")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_GER.jasper"));
                    }
                    if (matricula.equals("283")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_1578.jasper"));
                    }
                    if (matricula.equals("1578")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5_1578.jasper"));
                    }
                    if (matricula.equals("941")) {
                        BACTERIOLOGICO = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_5MAY.jasper"));
                    }

                    //////////////////////////////////////////
                    jPrintBacteriologico = JasperFillManager.fillReport(BACTERIOLOGICO, parametros, new JRBeanCollectionDataSource(Resultados4));
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////
                ///////////////////////BACTERIOLOGICOS SIN ANTIBIOGRAMA/////////////////////////////////////////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////
                /*    if (bandera_tipo4 == 1) {///4
                        System.out.println("arma sin atibiograma");
                        JasperReport BACTERIOLOGICO2 = null;

                        if (matricula.equals("80")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }
                        if (matricula.equals("1165")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }
                        if (matricula.equals("424")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("1349")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("1290")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("1340")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("1376")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("127")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("20009")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6YANE.jasper"));
                        }

                        if (matricula.equals("1066")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("1202")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("1114")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("99999")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("1088")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("1278")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("482")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("447")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("1073")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("20812")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6MAY.jasper"));
                        }

                        if (matricula.equals("1343")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }

                        if (matricula.equals("1515")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }
                        if (matricula.equals("1366")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_GER.jasper"));
                        }
                        if (matricula.equals("283")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_1578.jasper"));
                        }

                        if (matricula.equals("1578")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6_1578.jasper"));
                        }
                        if (matricula.equals("941")) {
                            BACTERIOLOGICO2 = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/pagina_2_A5_6MAY.jasper"));
                        }
                        jPrintBacteriologico2 = JasperFillManager.fillReport(BACTERIOLOGICO2, parametros, new JRBeanCollectionDataSource(Resultados5));
                    }*/
                ////////////////////////// Agrupo los Reportes
                int b_analisis = 0, b_caratula = 0, b_columnas = 0, b_filas = 0, b_filas1 = 0, b_filas2 = 0,
                        b_proteinograma = 0, b_bacteriologico = 0, b_bacteriologico1 = 0;
                List pages = null;
                ///////////////caratula no null
                System.out.println("---->a1");
                if (jPrintCaratula != null) {
                    analisis_completo = jPrintCaratula;
                    b_analisis = 1;
                    b_caratula = 1;
                }
                if (jPrintColumnas != null && b_analisis == 0) {
                    analisis_completo = jPrintColumnas;
                    System.out.println("---->a2");
                    b_analisis = 1;
                    b_columnas = 1;
                }
                if (jPrintFilas != null && b_analisis == 0) {
                    System.out.println("---->a3");
                    analisis_completo = jPrintFilas;
                    b_analisis = 1;
                    b_filas = 1;
                }
                if (jPrintFilas1 != null && b_analisis == 0) {
                    System.out.println("---->a4");
                    analisis_completo = jPrintFilas1;
                    b_analisis = 1;
                    b_filas1 = 1;
                }
                if (jPrintFilas2 != null && b_analisis == 0) {
                    System.out.println("---->a5");
                    analisis_completo = jPrintFilas1;
                    b_analisis = 1;
                    b_filas2 = 1;
                }

                if (jPrintProteinograma != null && b_analisis == 0) {
                    System.out.println("---->a6");
                    analisis_completo = jPrintProteinograma;
                    b_analisis = 1;
                    b_proteinograma = 1;
                }
                if (jPrintBacteriologico != null && b_analisis == 0) {
                    System.out.println("---->a7");
                    analisis_completo = jPrintBacteriologico;
                    b_analisis = 1;
                    b_bacteriologico = 1;
                }
                if (jPrintBacteriologico2 != null && b_analisis == 0) {
                    System.out.println("---->a8");
                    analisis_completo = jPrintBacteriologico2;
                    b_bacteriologico1 = 1;
                }

                if (jPrintCaratula != null && b_caratula == 0) {
                    System.out.println("---->a9");
                    pages = jPrintCaratula.getPages();
                    for (int indice = 0; indice < pages.size(); indice++) {
                        JRPrintPage object = (JRPrintPage) pages.get(indice);
                        analisis_completo.addPage(object);
                    }
                }
                if (jPrintColumnas != null && b_columnas == 0) {
                    System.out.println("---->a10");
                    pages = jPrintColumnas.getPages();
                    for (int indice = 0; indice < pages.size(); indice++) {
                        JRPrintPage object = (JRPrintPage) pages.get(indice);
                        analisis_completo.addPage(object);
                    }
                }
                if (jPrintFilas != null && b_filas == 0) {
                    System.out.println("---->a11");
                    pages = jPrintFilas.getPages();
                    for (int indice = 0; indice < pages.size(); indice++) {
                        JRPrintPage object = (JRPrintPage) pages.get(indice);
                        analisis_completo.addPage(object);
                    }
                }
                if (jPrintFilas1 != null && b_filas1 == 0) {
                    System.out.println("---->a12");
                    pages = jPrintFilas1.getPages();
                    for (int indice = 0; indice < pages.size(); indice++) {
                        JRPrintPage object = (JRPrintPage) pages.get(indice);
                        analisis_completo.addPage(object);
                    }
                }

                if (jPrintFilas2 != null && b_filas2 == 0) {
                    System.out.println("---->a13");
                    pages = jPrintFilas2.getPages();
                    for (int indice = 0; indice < pages.size(); indice++) {
                        JRPrintPage object = (JRPrintPage) pages.get(indice);
                        analisis_completo.addPage(object);
                    }
                }
                if (jPrintProteinograma != null && b_proteinograma == 0) {
                    System.out.println("---->a14");
                    pages = jPrintProteinograma.getPages();
                    for (int indice = 0; indice < pages.size(); indice++) {
                        JRPrintPage object = (JRPrintPage) pages.get(indice);
                        analisis_completo.addPage(object);
                    }
                }
                if (jPrintBacteriologico != null && b_bacteriologico == 0) {
                    System.out.println("---->a15");
                    pages = jPrintBacteriologico.getPages();
                    for (int indice = 0; indice < pages.size(); indice++) {
                        JRPrintPage object = (JRPrintPage) pages.get(indice);
                        analisis_completo.addPage(object);
                    }
                }
                if (jPrintBacteriologico2 != null && b_bacteriologico1 == 0) {
                    System.out.println("---->a16");
                    pages = jPrintBacteriologico2.getPages();
                    for (int indice = 0; indice < pages.size(); indice++) {
                        JRPrintPage object = (JRPrintPage) pages.get(indice);
                        analisis_completo.addPage(object);
                    }
                }
                System.out.println("--->8");
                JasperExportManager.exportReportToPdfFile(analisis_completo, "C:\\Informes_Pacientes\\00000001.-PacientePrueba.pdf");
                System.out.println("--->8.5");
                jv = new JasperViewer(analisis_completo);
                System.out.println("--->9");
                viewer.getContentPane().add(jv.getContentPane());
                System.out.println("--->10");
                viewer.setVisible(true);
                System.out.println("--->11");
            }
            this.dispose();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "No se cargo ningun resultado...");
            this.dispose();
        }

    }//GEN-LAST:event_btninformeActionPerformed

    private void tablaanalisisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaanalisisKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tablaanalisisKeyPressed

    private void tablaanalisisKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaanalisisKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tablaanalisisKeyReleased

    private void btnNuevaPracticaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevaPracticaActionPerformed

        new Agregar_Practica(null, true).setVisible(true);

    }//GEN-LAST:event_btnNuevaPracticaActionPerformed

    private Image ConvertirImagen(byte[] bytes) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Iterator readers = ImageIO.getImageReadersByFormatName("jpeg");
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis; // File or InputStream
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
        return reader.read(0, param);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Baja;
    private javax.swing.JButton SiguienteAnalisis;
    private javax.swing.JButton SiguienteGeneral;
    private javax.swing.JButton btnAgregarMetodo;
    private javax.swing.JButton btnAgregarUnidad;
    private javax.swing.JButton btnBaja;
    private javax.swing.JButton btnCargarFormula;
    private javax.swing.JButton btnDerivaciones;
    private javax.swing.JButton btnGuardarAnalisis;
    private javax.swing.JButton btnModificarAnalisis;
    private javax.swing.JButton btnModificarPrioridad;
    private javax.swing.JButton btnNuevaPractica;
    private javax.swing.JButton btnagregartitulo;
    private javax.swing.JButton btnatras;
    private javax.swing.JButton btnatras1;
    private javax.swing.JButton btninforme;
    private javax.swing.JButton btnprincipal;
    private javax.swing.JButton btnprincipal1;
    private javax.swing.JButton btnsalir;
    private javax.swing.JComboBox cboPosneg;
    private javax.swing.JComboBox cboTexto;
    private javax.swing.JComboBox cboUnidadPositivoNegativo;
    private javax.swing.JComboBox cboderivaciones;
    private javax.swing.JComboBox cboseccion;
    private javax.swing.JComboBox cbotiempo;
    private javax.swing.JComboBox cbotipo;
    private javax.swing.JComboBox cbounidad;
    private javax.swing.JCheckBox chkFormula;
    private javax.swing.JCheckBox chkMetodo;
    private javax.swing.ButtonGroup grupoboton;
    private javax.swing.ButtonGroup grupoderiva;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JPanel jInforme;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTree jTAnalisis;
    private javax.swing.JTree jTree4;
    private javax.swing.JPanel janalisis;
    private javax.swing.JPanel jgeneral;
    private javax.swing.JPanel jtiporesultado;
    private javax.swing.JLabel lblPrioridadInforme;
    private javax.swing.JLabel lblSeccionInforme;
    private javax.swing.JLabel lblpractica;
    private javax.swing.JLabel lblpracticaInforme;
    private javax.swing.JPanel panelderiva;
    private javax.swing.JPanel panelmaterial;
    private javax.swing.JRadioButton radiono;
    private javax.swing.JRadioButton radionum;
    private javax.swing.JRadioButton radioposneg;
    private javax.swing.JRadioButton radioposnegnum;
    private javax.swing.JRadioButton radiosi;
    private javax.swing.JTable tablaanalisis;
    private javax.swing.JTable tablamaterial;
    private javax.swing.JTable tablaprioridad;
    private javax.swing.JTable tablautilizado;
    private javax.swing.JTabbedPane tpPracticas;
    private javax.swing.JTextArea txainst;
    private javax.swing.JTextArea txavaloresreferencia;
    private javax.swing.JTextField txtFormatoDeInforme;
    private javax.swing.JTextField txtFormatoDeMesada;
    private javax.swing.JTextField txtMetodo;
    private javax.swing.JTextField txtPositivoNegativoNumerico;
    private javax.swing.JLabel txtformula;
    private javax.swing.JTextField txtnombre;
    // End of variables declaration//GEN-END:variables
}
