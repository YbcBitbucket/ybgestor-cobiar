package Formularios;

import Clases.ConexionMySQLLocal;
import Clases.Escape;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Alta_Medico extends javax.swing.JDialog {

    DefaultTableModel model;
    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;
    public int idespecialidad[] = new int[100];
    public int bmodificar = 0;

    public Alta_Medico(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Alta médico");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        cargartabla("");
        eventotabla();
        cargarcombo();
        dobleclick();
        Escape.funcionescape(this);
        this.setLocationRelativeTo(null);
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txaobservaciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toLowerCase(c));
                }
            }
        });
        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
    }

    void cargarcombo() {

        cboespecialidad.removeAllItems();
        cboespecialidad.addItem("...");
        int i = 0;
        String sql = "SELECT id_especialidades, nombre_esp FROM especialidades order by nombre_esp ";
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                cboespecialidad.addItem(rs.getString("nombre_esp"));
                idespecialidad[i] = rs.getInt("id_especialidades");
                i++;
            }
            ///cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos");
        }

    }

    void dobleclick() {
        tablamedico.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    txtnombre.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 2).toString());
                    txtapellido.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 1).toString());
                    txtmatricula.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 3).toString());
                    cboespecialidad.setSelectedItem(tablamedico.getValueAt(tablamedico.getSelectedRow(), 4).toString());
                    if (tablamedico.getValueAt(tablamedico.getSelectedRow(), 5) != null) {
                        txaobservaciones.setText(tablamedico.getValueAt(tablamedico.getSelectedRow(), 5).toString());
                    }
                    if (tablamedico.getValueAt(tablamedico.getSelectedRow(), 6).equals("OK")) {
                        bmodificar = 1;
                    } else {
                        JOptionPane.showMessageDialog(null, "El Medico se encuentra dado de Baja");
                        bmodificar = 0;
                    }

                }
            }
        });
    }

    void cargartabla(String valor) {
        String[] Titulo = {"id_medicos", "Apellido", "Nombre", "Matricula", "Especialidad", "Obs", "Estado"};
        String[] Registros = new String[7];
        String sql = "SELECT id_medicos, nombre, apellido, medicos.matricula, medicos.observaciones, nombre_esp, medicos.estado FROM medicos INNER JOIN medicos_tienen_especialidades USING (id_medicos) INNER JOIN especialidades USING (id_especialidades) WHERE concat(apellido,'', nombre,'',medicos.matricula) LIKE '%" + valor + "%'";

        int i = 0;
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString("id_medicos");
                Registros[1] = rs.getString("apellido");
                Registros[2] = rs.getString("nombre");
                Registros[3] = rs.getString("medicos.matricula");
                Registros[4] = rs.getString("nombre_esp");
                if (rs.getString("medicos.observaciones") != null) {
                    Registros[5] = rs.getString("medicos.observaciones");
                }
                if (rs.getString("medicos.estado").equals("1")) {
                    Registros[6] = "OK";
                } else {
                    Registros[6] = "BAJA";
                }

                model.addRow(Registros);
            }
            tablamedico.setModel(model);
            tablamedico.setAutoCreateRowSorter(true);
            /////////////////////////////////////////////////////////////
            tablamedico.getColumnModel().getColumn(0).setMaxWidth(0);
            tablamedico.getColumnModel().getColumn(0).setMinWidth(0);
            tablamedico.getColumnModel().getColumn(0).setPreferredWidth(0);
            /*    tablamedico.getColumnModel().getColumn(5).setMaxWidth(0);
             tablamedico.getColumnModel().getColumn(5).setMinWidth(0);
             tablamedico.getColumnModel().getColumn(5).setPreferredWidth(0);*/

            /////////////////////////////////////////////////////////////
            alinear();
            tablamedico.getColumnModel().getColumn(0).setCellRenderer(alinearCentro);
            tablamedico.getColumnModel().getColumn(1).setCellRenderer(alinearIzquierda);
            tablamedico.getColumnModel().getColumn(2).setCellRenderer(alinearIzquierda);
            tablamedico.getColumnModel().getColumn(3).setCellRenderer(alinearCentro);
            tablamedico.getColumnModel().getColumn(4).setCellRenderer(alinearCentro);
            tablamedico.getColumnModel().getColumn(5).setCellRenderer(alinearCentro);
            tablamedico.getColumnModel().getColumn(6).setCellRenderer(alinearCentro);

            //////////////////////////
            ///cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos.");
        }
    }

    void alinear() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
    }

    void eventotabla() {
        tablamedico.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        ListSelectionModel rowSM = tablamedico.getSelectionModel();

        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                //Ignore extra messages.
                if (e.getValueIsAdjusting()) {
                    return;
                }

                ListSelectionModel lsm = (ListSelectionModel) e.getSource();

                if (lsm.isSelectionEmpty()) {
                } else {

                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtapellido = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cboespecialidad = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txaobservaciones = new javax.swing.JTextArea();
        txtmatricula = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtmail = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablamedico = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        btnaceptar = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        btnsalir1 = new javax.swing.JButton();
        btnborrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Alta Médicos"));

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Nombre:");

        txtnombre.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtnombre.setForeground(new java.awt.Color(0, 102, 204));
        txtnombre.setNextFocusableComponent(txtapellido);
        txtnombre.setPreferredSize(new java.awt.Dimension(200, 21));
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Apellido:");

        txtapellido.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtapellido.setForeground(new java.awt.Color(0, 102, 204));
        txtapellido.setMinimumSize(new java.awt.Dimension(200, 21));
        txtapellido.setNextFocusableComponent(txtmail);
        txtapellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtapellidoKeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Especialidad:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Matrícula:");

        cboespecialidad.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        cboespecialidad.setForeground(new java.awt.Color(0, 102, 204));
        cboespecialidad.setNextFocusableComponent(txaobservaciones);
        cboespecialidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cboespecialidadKeyPressed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Observaciones"));

        txaobservaciones.setColumns(20);
        txaobservaciones.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txaobservaciones.setForeground(new java.awt.Color(0, 102, 204));
        txaobservaciones.setRows(5);
        txaobservaciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txaobservacionesKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(txaobservaciones);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );

        txtmatricula.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmatricula.setForeground(new java.awt.Color(0, 102, 204));
        txtmatricula.setNextFocusableComponent(cboespecialidad);
        txtmatricula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmatriculaKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Mail:");

        txtmail.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtmail.setForeground(new java.awt.Color(0, 102, 204));
        txtmail.setMinimumSize(new java.awt.Dimension(200, 21));
        txtmail.setNextFocusableComponent(txtmatricula);
        txtmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmailKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtmatricula, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboespecialidad, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtnombre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtapellido, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtmail, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtapellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtmatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cboespecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tablamedico.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablamedico.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablamedico);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Buscar");

        txtBuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtBuscar.setForeground(new java.awt.Color(0, 102, 204));
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnaceptar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728934 - enter login right.png"))); // NOI18N
        btnaceptar.setText("Aceptar");
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir.setText("Volver");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        btnsalir1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728898 - add plus.png"))); // NOI18N
        btnsalir1.setMnemonic('e');
        btnsalir1.setText("Agregar Especialidad");
        btnsalir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir1ActionPerformed(evt);
            }
        });

        btnborrar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnborrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728918 - cancel error exit fault.png"))); // NOI18N
        btnborrar.setMnemonic('b');
        btnborrar.setText("Borrar");
        btnborrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnborrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir1))
                .addGap(83, 83, 83)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(0, 152, Short.MAX_VALUE)
                        .addComponent(btnaceptar)
                        .addGap(18, 18, 18)
                        .addComponent(btnborrar)
                        .addGap(18, 18, 18)
                        .addComponent(btnsalir))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnborrar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        String nombre, apellido, matricula, id_especialidad, observaciones, mail;
        String sSQL = "";
        nombre = txtnombre.getText();
        apellido = txtapellido.getText();
        matricula = txtmatricula.getText();
        mail = txtmail.getText();
        observaciones = txaobservaciones.getText();

        int control = 0;

        if (!"".equals(nombre) && !"".equals(apellido) && !"".equals(matricula) && !"...".equals(cboespecialidad.getSelectedItem().toString())) {

            if (bmodificar == 0) {

                ///////////////////agregar datos//////////////////////////////
                try {
                    id_especialidad = String.valueOf(idespecialidad[cboespecialidad.getSelectedIndex() - 1]);
                    String sql = "SELECT matricula FROM medicos where estado=1";
                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sql);
                    int contador = 0; //inicio
                    if (rs.next()) {
                        contador++;
                    }
                    rs.beforeFirst();// fin
                    if (contador != 0) {
                        while (rs.next()) {

                            if (matricula.equals(rs.getString("matricula"))) {

                                JOptionPane.showMessageDialog(null, "La matricula ya existe");
                                txtmatricula.requestFocus();
                                txtmatricula.selectAll();
                                cboespecialidad.setSelectedIndex(0);
                                control = 1;
                                break;

                            }
                        }
                    }
                    rs.beforeFirst();
                    if (control == 0) {
                        sSQL = "INSERT INTO medicos(nombre, apellido, matricula, observaciones, mail) VALUES (?,?,?,?,?)";
                        PreparedStatement pst = cn.prepareStatement(sSQL);
                        pst.setString(1, nombre);
                        pst.setString(2, apellido);
                        pst.setString(3, matricula);
                        pst.setString(4, observaciones);
                        pst.setString(5, mail);
                        int n = pst.executeUpdate();
                        if (n > 0) {
                            cargartabla("");
                            sSQL = "SELECT id_medicos FROM medicos WHERE matricula=" + matricula;
                            Statement stt = cn.createStatement();
                            ResultSet rss = stt.executeQuery(sSQL);
                            int id_medico = 0;
                            while (rss.next()) {
                                id_medico = rss.getInt("id_medicos");
                            }
                            sSQL = "INSERT INTO medicos_tienen_especialidades  (id_medicos, id_especialidades) VALUES (?,?)";
                            PreparedStatement pst1 = cn.prepareStatement(sSQL);
                            pst1.setInt(1, id_medico);
                            pst1.setString(2, id_especialidad);
                            int m = pst1.executeUpdate();
                            if (m > 0) {
                                txtnombre.setText("");
                                txtapellido.setText("");
                                txtmatricula.setText("");
                                txaobservaciones.setText("");
                                txtmail.setText("");
                                cboespecialidad.setSelectedIndex(0);
                                txtnombre.requestFocus();
                                cargartabla("");
                            }
                        }
                    }
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                }
            } else {
                /////////////////////////////////////////////
                try {

                    id_especialidad = String.valueOf(idespecialidad[cboespecialidad.getSelectedIndex() - 1]);

                    String sSQL3 = "UPDATE medicos SET nombre=?, apellido=?, matricula=?, mail=?, observaciones=? WHERE id_medicos=" + tablamedico.getValueAt(tablamedico.getSelectedRow(), 0).toString();
                    PreparedStatement pst = cn.prepareStatement(sSQL3);
                    pst.setString(1, nombre);
                    pst.setString(2, apellido);
                    pst.setString(3, matricula);
                    pst.setString(4, mail);
                    pst.setString(5, observaciones);
                    int n = pst.executeUpdate();
                    if (n > 0) {
                        String sSQL4 = "UPDATE medicos_tienen_especialidades SET id_especialidades=? WHERE id_medicos=" + tablamedico.getValueAt(tablamedico.getSelectedRow(), 0).toString();
                        PreparedStatement pst2 = cn.prepareStatement(sSQL4);
                        pst2.setString(1, id_especialidad);
                        int n2 = pst2.executeUpdate();
                        if (n2 > 0) {
                            txtnombre.setText("");
                            txtapellido.setText("");
                            txtmatricula.setText("");
                            txaobservaciones.setText("");
                            cboespecialidad.setSelectedIndex(0);
                            txtnombre.requestFocus();
                            txtmatricula.setText("");
                            txtmail.setText("");
                            cargartabla("");
                            txtBuscar.setText("");
                        }
                    }

                    cn.close();
                } catch (SQLException ex) {

                    JOptionPane.showMessageDialog(null, ex);
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");

                }
                /////////////////////////////////////////////
            }
            ////////////////////////modifica datos/////////////////////////////////
        } else {
            JOptionPane.showMessageDialog(null, "Debe completar los campos obligatorios");
        }
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void txaobservacionesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txaobservacionesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            evt.consume();
            btnaceptar.requestFocus();

        }
    }//GEN-LAST:event_txaobservacionesKeyPressed

    private void txtnombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtnombre.transferFocus();
        }
    }//GEN-LAST:event_txtnombreKeyPressed

    private void txtapellidoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtapellidoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtapellido.transferFocus();
        }
    }//GEN-LAST:event_txtapellidoKeyPressed

    private void txtmailKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmailKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtmail.transferFocus();
        }
    }//GEN-LAST:event_txtmailKeyPressed

    private void txtmatriculaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmatriculaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtmatricula.transferFocus();
        }
    }//GEN-LAST:event_txtmatriculaKeyPressed

    private void cboespecialidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cboespecialidadKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cboespecialidad.transferFocus();
        }
    }//GEN-LAST:event_cboespecialidadKeyPressed

    private void btnsalir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir1ActionPerformed
        new Alta_Especialidad(null, true).setVisible(true);
        cargarcombo();
    }//GEN-LAST:event_btnsalir1ActionPerformed

    private void btnborrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnborrarActionPerformed
        if (tablamedico.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila...");
        } else {
            if (!tablamedico.getValueAt(tablamedico.getSelectedRow(), 6).equals("OK")) {
                JOptionPane.showMessageDialog(null, "El Medico ya se encuentra dado de Baja");
            } else {

                int opt = JOptionPane.showConfirmDialog(this, "Desea borrar al Medico?", "Confirmación", JOptionPane.YES_NO_OPTION);
                if (opt == 0) {
                    try {
                        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
                        Connection cn = mysql.Conectar();
                        int id_medicos = Integer.valueOf(tablamedico.getValueAt(tablamedico.getSelectedRow(), 0).toString());
                        String actualizar = "UPDATE medicos SET estado=? WHERE id_medicos= " + id_medicos;
                        PreparedStatement pst = cn.prepareStatement(actualizar);
                        pst.setInt(1, 0);
                        int p = pst.executeUpdate();
                        ///  cn.close();
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Error en la base de datos");
                        JOptionPane.showMessageDialog(null, ex);
                    }
                    cargartabla("");
                    txtBuscar.setText("");
                }
            }
        }
    }//GEN-LAST:event_btnborrarActionPerformed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        cargartabla(txtBuscar.getText());
    }//GEN-LAST:event_txtBuscarKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btnborrar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JButton btnsalir1;
    private javax.swing.JComboBox cboespecialidad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tablamedico;
    private javax.swing.JTextArea txaobservaciones;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JTextField txtapellido;
    private javax.swing.JTextField txtmail;
    private javax.swing.JTextField txtmatricula;
    private javax.swing.JTextField txtnombre;
    // End of variables declaration//GEN-END:variables
}
