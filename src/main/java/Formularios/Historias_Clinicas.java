package Formularios;

import Clases.ConexionMySQLLocal;
import Clases.Escape;
import Clases.RowsRenderer_Historia_clinica;
import static Formularios.Resultados.fechaPaciente;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import static Formularios.Resultados.nombre_paciente;
import static Formularios.Resultados.mail;
import java.awt.Rectangle;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;

public class Historias_Clinicas extends javax.swing.JDialog {

    DefaultTableModel model;
    public String[] descripcion = new String[100000];
    public static int bandera_informe = 0;
    RowsRenderer_Historia_clinica rr = new RowsRenderer_Historia_clinica(13);

    public Historias_Clinicas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Historias clínicas");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());

        this.setLocationRelativeTo(null);
        cargartablapacientes();
        dobleclick();
        Escape.funcionescape(this);
        txtdescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtpacientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
    }

    void dobleclick() {
        tablapacientes.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int totalRow = tablapacientes.getRowCount();
                    if (totalRow != -1) {
                        Informe.id_orden = Integer.valueOf(tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString());
                        nombre_paciente = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 1).toString();
                        mail = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 12).toString();
                        fechaPaciente = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 5).toString();
                        System.out.println("id_orden " + Informe.id_orden + " fechaPaciente " + fechaPaciente + " nombre_paciente " + nombre_paciente + " mail " + mail);
                        new Resultados(null, true).setVisible(true);
                        cargartablapacientes();
                    }
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        Resultados = new javax.swing.JMenuItem();
        jPanel15 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tablapacientes = new javax.swing.JTable();
        txtpacientes = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtdescripcion = new javax.swing.JTextArea();
        btnsalir7 = new javax.swing.JButton();

        Resultados.setText("Cargar Resultados");
        Resultados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ResultadosActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Resultados);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar Pacientes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablapacientes.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tablapacientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablapacientes.setComponentPopupMenu(jPopupMenu1);
        tablapacientes.setOpaque(false);
        tablapacientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablapacientesKeyPressed(evt);
            }
        });
        jScrollPane8.setViewportView(tablapacientes);

        txtpacientes.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtpacientes.setForeground(new java.awt.Color(0, 102, 204));
        txtpacientes.setToolTipText("Buscar por dni paciente o num paciente o nom paciente o num orden");
        txtpacientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpacientesActionPerformed(evt);
            }
        });
        txtpacientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtpacientesKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpacientesKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(txtpacientes, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 981, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtpacientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                .addContainerGap())
        );

        txtdescripcion.setEditable(false);
        txtdescripcion.setColumns(20);
        txtdescripcion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtdescripcion.setForeground(new java.awt.Color(0, 102, 204));
        txtdescripcion.setRows(2);
        txtdescripcion.setBorder(null);
        txtdescripcion.setOpaque(false);
        jScrollPane1.setViewportView(txtdescripcion);

        btnsalir7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728935 - exit left logout.png"))); // NOI18N
        btnsalir7.setMnemonic('s');
        btnsalir7.setText("Volver");
        btnsalir7.setToolTipText("[Alt + s]");
        btnsalir7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalir7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addGap(73, 73, 73)
                        .addComponent(btnsalir7)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir7, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablapacientesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablapacientesKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tablapacientesKeyPressed

    private void txtpacientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpacientesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpacientesActionPerformed

    private void txtpacientesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpacientesKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpacientesKeyPressed

    void cargartablapacientes() {
//                          0       1           2           3       4           5           6      7        8         9        10     11     12       13  
        String[] Titulo = {"N°", "Paciente", "Fecha Nac.", "Tel", "Cel", "Fecha Orden", "Diag", "Tipo", "Servicio", "DNI", "id_OS", "hora", "Mail","Estado"};
        String[] Registros = new String[14];
        double total = 0;
        int i = 0;
        //String sql = "SELECT distinct(historia_clinica.idhistoria_clinica), personas.apellido, personas.nombre, pacientes.fecha_nacimiento, pacientes.telefono, pacientes.celular, ordenes.fecha, ordenes.observaciones, ordenes.tipo_orden, ordenes.servicio, historia_clinica.descripcion, pacientes.personas_dni, ordenes.id_obrasocial,hora, pacientes.mail FROM ordenes INNER JOIN pacientes ON pacientes.id_Pacientes = ordenes.id_Pacientes INNER JOIN personas ON personas.dni = pacientes.personas_dni INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes WHERE ordenes.estado_orden=0 ORDER BY ordenes.id_ordenes";
        String sql = "SELECT distinct(historia_clinica.idhistoria_clinica), personas.apellido, personas.nombre, DATE_FORMAT(pacientes.fecha_nacimiento, '%d-%m-%Y') AS fecha_nacimiento, pacientes.telefono, pacientes.celular,   DATE_FORMAT(ordenes.fecha, '%d-%m-%Y') AS fecha, ordenes.observaciones, ordenes.tipo_orden, ordenes.servicio, historia_clinica.descripcion, \n"
                + "if((historia_clinica.descripcion=\"VIH.\" or  historia_clinica.descripcion=\"S/D.\"),\"-\",pacientes.personas_dni), ordenes.id_obrasocial,hora, pacientes.mail,historia_clinica.estado\n"
                + "FROM ordenes INNER JOIN pacientes ON pacientes.id_Pacientes = ordenes.id_Pacientes\n"
                + "INNER JOIN personas ON personas.dni = pacientes.personas_dni \n"
                + "INNER JOIN historia_clinica ON historia_clinica.id_ordenes = ordenes.id_ordenes \n"
                + "WHERE ordenes.estado_orden=0 and pacientes.estado=1\n"
                + "GROUP BY historia_clinica.idhistoria_clinica ORDER BY historia_clinica.idhistoria_clinica";
//        String sql = "Select * from vista_historia_clinicas ";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQLLocal cc = new ConexionMySQLLocal();
        Connection cn = cc.Conectar();
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                total = 0;
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2) + " " + rs.getString(3);
                Registros[2] = rs.getString(4);
                Registros[3] = rs.getString(5);
                Registros[4] = rs.getString(6);
                Registros[5] = rs.getString(7);
                Registros[6] = rs.getString(8);
                Registros[7] = rs.getString(9);
                Registros[8] = rs.getString(10);
                descripcion[i] = rs.getString(11);
                Registros[9] = rs.getString(12);
                Registros[10] = rs.getString(13);
                Registros[11] = rs.getString(14);
                Registros[12] = rs.getString(15);
                Registros[13] = rs.getString(16);
                model.addRow(Registros);       
                tablapacientes.setDefaultRenderer(Object.class, rr);
                i++;
            }
            tablapacientes.setModel(model);
            tablapacientes.setAutoCreateRowSorter(true);
            /////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(6).setMaxWidth(0);
            tablapacientes.getColumnModel().getColumn(6).setMinWidth(0);
            tablapacientes.getColumnModel().getColumn(6).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(10).setMaxWidth(0);
            tablapacientes.getColumnModel().getColumn(10).setMinWidth(0);
            tablapacientes.getColumnModel().getColumn(10).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(13).setMaxWidth(0);
            tablapacientes.getColumnModel().getColumn(13).setMinWidth(0);
            tablapacientes.getColumnModel().getColumn(13).setPreferredWidth(0);
            /////////////////////////////////////////////////////////////////////
            tablapacientes.getColumnModel().getColumn(11).setMaxWidth(0);
            tablapacientes.getColumnModel().getColumn(11).setMinWidth(0);
            tablapacientes.getColumnModel().getColumn(11).setPreferredWidth(0);
            tablapacientes.getColumnModel().getColumn(1).setPreferredWidth(150);
            Rectangle r = tablapacientes.getCellRect(tablapacientes.getRowCount() - 1, 0, true);
            tablapacientes.scrollRectToVisible(r);
            tablapacientes.getSelectionModel().setSelectionInterval(tablapacientes.getRowCount() - 1, tablapacientes.getRowCount() - 1);
            cn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");

        }

    }

    private void txtpacientesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpacientesKeyReleased
///        filtrarpacientes3(txtpacientes.getText());
        TableRowSorter sorter = new TableRowSorter(model);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtpacientes.getText() + ".*"));
        tablapacientes.setRowSorter(sorter);
    }//GEN-LAST:event_txtpacientesKeyReleased

    private void btnsalir7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalir7ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalir7ActionPerformed

    private void ResultadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResultadosActionPerformed
        int totalRow = tablapacientes.getRowCount();
        if (totalRow != -1) {
            if (totalRow != -1) {
                Informe.id_orden = Integer.valueOf(tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 0).toString());
                nombre_paciente = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 1).toString();
                mail = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 12).toString();
                fechaPaciente = tablapacientes.getValueAt(tablapacientes.getSelectedRow(), 5).toString();
                new Resultados(null, true).setVisible(true);
                cargartablapacientes();
            }
        }
    }//GEN-LAST:event_ResultadosActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Resultados;
    private javax.swing.JButton btnsalir7;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTable tablapacientes;
    private javax.swing.JTextArea txtdescripcion;
    private javax.swing.JTextField txtpacientes;
    // End of variables declaration//GEN-END:variables
}
