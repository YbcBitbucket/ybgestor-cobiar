package Formularios;

import javax.swing.ImageIcon;

public class Envio_mail extends javax.swing.JDialog {

    

    public Envio_mail(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setTitle("Enviar");
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/logo.png")).getImage());
        this.setLocationRelativeTo(null);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                Resultados.enviar = 2;
            }
        });
    }
  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnnomenclador1 = new javax.swing.JButton();
        btnnomenclador2 = new javax.swing.JButton();
        btnnomenclador3 = new javax.swing.JButton();
        btnnomenclador4 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnnomenclador1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728978 - jewel.png"))); // NOI18N
        btnnomenclador1.setMnemonic('n');
        btnnomenclador1.setText("Enviar a Paciente");
        btnnomenclador1.setToolTipText("[Alt + n]");
        btnnomenclador1.setPreferredSize(new java.awt.Dimension(165, 41));
        btnnomenclador1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador1ActionPerformed(evt);
            }
        });
        btnnomenclador1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador1KeyPressed(evt);
            }
        });

        btnnomenclador2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728928 - document paper.png"))); // NOI18N
        btnnomenclador2.setMnemonic('n');
        btnnomenclador2.setText("Vista Previa");
        btnnomenclador2.setToolTipText("[Alt + n]");
        btnnomenclador2.setPreferredSize(new java.awt.Dimension(80, 23));
        btnnomenclador2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador2ActionPerformed(evt);
            }
        });
        btnnomenclador2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador2KeyPressed(evt);
            }
        });

        btnnomenclador3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728913 - book bookmark reading.png"))); // NOI18N
        btnnomenclador3.setMnemonic('n');
        btnnomenclador3.setText("Enviar a Medico");
        btnnomenclador3.setToolTipText("[Alt + n]");
        btnnomenclador3.setPreferredSize(new java.awt.Dimension(80, 23));
        btnnomenclador3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador3ActionPerformed(evt);
            }
        });
        btnnomenclador3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador3KeyPressed(evt);
            }
        });

        btnnomenclador4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnnomenclador4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/32/728905 - bar barchart chart graph stats.png"))); // NOI18N
        btnnomenclador4.setMnemonic('n');
        btnnomenclador4.setText("Imprimir Informe");
        btnnomenclador4.setToolTipText("[Alt + n]");
        btnnomenclador4.setPreferredSize(new java.awt.Dimension(167, 41));
        btnnomenclador4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnomenclador4ActionPerformed(evt);
            }
        });
        btnnomenclador4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnnomenclador4KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnnomenclador2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnnomenclador4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnnomenclador1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnnomenclador3, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnnomenclador1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnnomenclador2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnnomenclador3, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnnomenclador4, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnnomenclador1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador1ActionPerformed
        Resultados.enviar=1;
        this.dispose();
    }//GEN-LAST:event_btnnomenclador1ActionPerformed

    private void btnnomenclador1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador1KeyPressed

    private void btnnomenclador2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador2ActionPerformed
        Resultados.enviar=3;
        this.dispose();
    }//GEN-LAST:event_btnnomenclador2ActionPerformed

    private void btnnomenclador2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador2KeyPressed

    private void btnnomenclador3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador3ActionPerformed
        Resultados.enviar=2;
        this.dispose();
    }//GEN-LAST:event_btnnomenclador3ActionPerformed

    private void btnnomenclador3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador3KeyPressed

    private void btnnomenclador4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnomenclador4ActionPerformed
        Resultados.enviar=0;
        this.dispose();
    }//GEN-LAST:event_btnnomenclador4ActionPerformed

    private void btnnomenclador4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnnomenclador4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnnomenclador4KeyPressed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnnomenclador1;
    private javax.swing.JButton btnnomenclador2;
    private javax.swing.JButton btnnomenclador3;
    private javax.swing.JButton btnnomenclador4;
    // End of variables declaration//GEN-END:variables
}
