package Clases;

import static Clases.Configuracion.tomaDatos;
import Formularios.Login;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

public class HiloInicio extends Thread {

    SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
    JProgressBar progreso;
    public static int bandera_licencia = 0, matricula = 0;
    String fecha = "";
    public static String[] obrasocial = new String[1000];
    public static int[] idobrasocial = new int[1000];
    public static int[] matriculamedico = new int[50000], idmedico = new int[50000];
    public static int[] añonbu = new int[500];
    public static String[] nombreobrasocial = new String[50000], nombremedico = new String[50000];
    public static String[] numafiliado = new String[500000];
    public static Double[] arancel = new Double[1000];
    public static String[] practicaconobra = new String[150000];
    public static String[] analisis = new String[50000];
    public static int[] idlocalidad = new int[2400];

    public static String[] nombrelocalidad = new String[2400];
    public static String novedad = "";
    public static String errores = "";
    public static int[] idobra = new int[150000];
    public static String[] precio_practica = new String[150000];
    public static int contadorpractica = 0, contadorpersona = 0;
    public static int contadoranalisis = 0, contadorlocalidad = 0;
    public static int contadorobrasocial = 0;
    public static int contadorafiliado = 0, contadormedico = 0;
    Logger logger = Logger.getLogger("MyLog");
    FileHandler fh;

    public HiloInicio(JProgressBar progreso1) {
        super();
        this.progreso = progreso1;
    }

    void verificaLicencia() {
        
        // Selecciono SQL la tabla empleados y todos sus atributos
        ConexionMySQL mysql = new ConexionMySQL();
//        Connection cn = mysql.Conectar();
        int i = 0;
        String sSQL = "SELECT cantidad, fecha_fin,estado FROM licencia where matricula_colegiado =" + matricula;
        try(Connection cn = mysql.Conectar()) {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            rs.last();
            ////JOptionPane.showMessageDialog(null, matricula+"   "+rs.getInt(1) +" "+rs.getInt(3) +" "+ rs.getString(2) );

            if (0 != (rs.getInt(3))) {
                if (0 == (rs.getInt(1))) {
                    JOptionPane.showMessageDialog(null, "La licencia ha expirado, debe contactar al servicio tecnico...");
                    bandera_licencia = 0;
                    String SqL = "UPDATE licencia SET estado=0 where matricula_colegiado="+matricula;
                    PreparedStatement st2 = cn.prepareStatement(SqL);
                    st2.executeUpdate();
                } else {
                    if ((rs.getString(2)).equals(fecha)) {
                        JOptionPane.showMessageDialog(null, "La licencia ha expirado, debe contactar al servicio tecnico...");
                        bandera_licencia = 0;
                        String SqL = "UPDATE licencia SET estado=0 where matricula_colegiado="+matricula;
                        PreparedStatement st2 = cn.prepareStatement(SqL);
                        st2.executeUpdate();
                    } else {
                        bandera_licencia = 1;
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "La licencia ha expirado, debe contactar al servicio tecnico...");
                bandera_licencia = 0;
            }
            //cn.close();
        } catch (SQLException e) {
           
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        }

    }

    public void run() {
        int i = 0;
       
        tomaDatos();
        while (i < 30) {
            progreso.setValue(i);
            i++;
            pausa(60);
        }
        cargarlocalidad();
        /// JOptionPane.showMessageDialog(null, "1");
        while (i < 50) {
            progreso.setValue(i);
            i++;
            pausa(60);
        }
        cargarmatricula();
        ///JOptionPane.showMessageDialog(null, "2");
        cargarmatriculabioquimico();
        while (i <= 70) {
            progreso.setValue(i);
            i++;
            pausa(60);
        }
        cargarfecha();
        ///JOptionPane.showMessageDialog(null, "3");
        ///verificaLicencia();
        while (i <= 70) {
            progreso.setValue(i);
            i++;
            pausa(60);
        }
        cargarobrasosial();
        ////JOptionPane.showMessageDialog(null, "4");
        while (i <= 100) {
            progreso.setValue(i);
            i++;
            pausa(60);
        }
        new Login().setVisible(true);
    }

    void cargarmatricula() {

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT medicos.id_medicos,medicos.nombre, medicos.apellido, medicos_tienen_especialidades.matricula FROM medicos INNER JOIN medicos_tienen_especialidades ON medicos_tienen_especialidades.id_medicos = medicos.id_medicos  where estado=1";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                idmedico[contadormedico] = rs.getInt(1);
                matriculamedico[contadormedico] = (rs.getInt(4));
                nombremedico[contadormedico] = (rs.getString(3) + " " + rs.getString(2));
                contadormedico++;
            }
            cn.close();
        } catch (Exception e) {
             Error.escribirLog("C:\\Cobiar\\log.txt", e.getMessage());
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarfecha() {
        String hora = "";
        SimpleDateFormat formatoTiempo = new SimpleDateFormat("HH:mm:ss");
        java.util.Date currentDate1 = new java.util.Date();
        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(currentDate1);
        hora = formatoTiempo.format(currentDate1);
        ///txthora.setText(hora);
        /////////////////////////////////////
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date currentDate = new java.util.Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        fecha = formato.format(currentDate);
        Error.escribirLog("C:\\Cobiar\\log.txt", "Cargarfecha");
    }

    public static Date aDate(String strFecha) {
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
        Date fecha = null;
        try {
            fecha = formatoDelTexto.parse(strFecha);
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        return fecha;
    }

    void cargarnovedad() {
        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();
        String sSQL = "SELECT id_novedad,novedad FROM novedades ";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                novedad = (rs.getString("novedad"));
            }
            cn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarobrasosial() {

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT id_obrasocial,razonsocial_obrasocial,importeunidaddearancel_obrasocial,codigo_obrasocial,añonbu FROM obrasocial where estado_obrasocial=1";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                idobrasocial[contadorobrasocial] = rs.getInt("id_obrasocial");
                nombreobrasocial[contadorobrasocial] = (rs.getString("razonsocial_obrasocial"));
                arancel[contadorobrasocial] = (rs.getDouble("importeunidaddearancel_obrasocial"));
                añonbu[contadorobrasocial] = (rs.getInt("añonbu"));
                obrasocial[contadorobrasocial] = (rs.getString("codigo_obrasocial") + " - " + rs.getString("razonsocial_obrasocial"));
                contadorobrasocial++;
            }
            cn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarmatriculabioquimico() {

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT matricula FROM laboratorios where id_laboratorios = 1";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                matricula = rs.getInt(1);
            }
            cn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    void cargarlocalidad() {

        ConexionMySQLLocal mysql = new ConexionMySQLLocal();
        Connection cn = mysql.Conectar();

        String sSQL = "SELECT id_localidad,nombre_localidad FROM localidad ";
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            // Recorro y cargo las obras sociales
            while (rs.next()) {
                idlocalidad[contadorlocalidad] = rs.getInt(1);
                nombrelocalidad[contadorlocalidad] = (rs.getString(2));
                contadorlocalidad++;
            }
            cn.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public static int fechasDiferenciaEnDias(Date fechaInicial, Date fechaFinal) {
        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String fechaInicioString = df.format(fechaInicial);
        try {
            fechaInicial = df.parse(fechaInicioString);
        } catch (ParseException ex) {
        }

        String fechaFinalString = df.format(fechaFinal);
        try {
            fechaFinal = df.parse(fechaFinalString);
        } catch (ParseException ex) {
        }
        long fechaInicialMs = fechaInicial.getTime();
        long fechaFinalMs = fechaFinal.getTime();
        long diferencia = fechaFinalMs - fechaInicialMs;
        double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
        return ((int) dias);
    }

    public void pausa(int mlSeg) {
        try {
            // pausa para el splash
            Thread.sleep(mlSeg);
        } catch (Exception e) {
        }

    }
}
